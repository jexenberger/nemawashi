/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

import io.nemawashi.api.Type
import nemawashi.nw

nw.task {

    name "sample"
    description "A Sample description"
    returnType Type.string
    returnDescription "A whimsical string"

    parameters {
        simple "A dirt simple parameter which is a string type and is required"
        name {
            description "this is a test"
            type Type.int

        }
        anotherSample {
            description "A sample with regex"
            regex "^\\w+@[a-zA-Z_]+?\\.[a-zA-Z]{2,3}\$"
            defaultValue "test@test.com"
        }
        full {
            description "full property"
            type Type.stringArray
            required false
            alias "qwerty"
            allowedValues([1,2,3,4])
        }
    }

    script { taskId, ctx ->

        println "hello world $name"
        println "hello world $full"
        println "hello world $simple"
        println "hello world $anotherSample"

        def failed = true


        $nw.foreach(items: [1, 2, 3]) {
            $nw.echo(value: __var)
        }

        var page = [1, 2, 3, 4, 5, 6, 7, 8]

        def futures = []
        $nw.paginate(items: page, pageSize: 3) {
            println __page
            $nw.foreach(items: __page, varName: 'qwerty') {
                futures += nw.async {
                    println qwerty

                    def script = "1 + qwerty"
                    return $nw.script(script: script)
                }
                failed = false
            }
        }
        println("pageSize -> " + pageSize)
        def result = futures.collect {
            it.get()
        }
        println "Result = $result"

        if (failed) {
            nw.fail(taskId, "Foreach didn't execute")
        }


        def run = $nw.script(script: "1 + 1")
        //.runAt("http://localhost:8080")

        $nw.jar(path: new File("src/test/resources/hello.jar"))

        def newTest = $nw.sys.new_instance(type: 'hello.Test')

        $nw.sys.echo(value: newTest.helloWorld())

        return run == 2 ? "hello world" : "borked"

    }
}