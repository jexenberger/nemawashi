/*
 *  Copyright 2018 Julian Exenberger
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import io.nemawashi.api.Directive
import io.nemawashi.api.Type
import nemawashi.nw

nw.task{

    description "sample group task"
    parameters {
        start {
            description "start"
            type Type.int
        }
        end {
            description "end"
            type Type.int
        }
    }


    before { id, ctx ->
        def counter = nw["counter"]
        if (counter == null) {
            counter = start
            nw["counter"] = counter
            return Directive.continueExecution
        } else {
            nw["counter"] = counter++

        }
        println "BEFORE -> $counter"
        return counter < end ? Directive.continueExecution : Directive.done
    }

    after {
        println "After started at $start -> ${nw.counter}"
        return Directive.again
    }

    onError {

    }

    onFinally {

    }
}