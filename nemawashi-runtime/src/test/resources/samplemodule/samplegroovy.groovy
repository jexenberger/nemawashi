/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */
import nemawashi.nw

nw.task {
    name "samplegroovy"

    description "A Sample groovy class"

    parameters {
        simple "A dirt simple parameter which is a string type and is required"
    }

    script { taskId, ctx ->
        def test = new hello.Test()
        println(test)
        println "hello world $ctx.simple"
        return "hello world"
    }
}