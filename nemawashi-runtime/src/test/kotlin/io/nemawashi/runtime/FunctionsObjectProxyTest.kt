/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime

import io.nemawashi.SampleFunctionalObject
import io.nemawashi.api.ModuleId
import io.nemawashi.api.NemawashiSettings
import io.nemawashi.api.TaskRequest
import io.nemawashi.api.defaultVersion
import org.junit.jupiter.api.Test
import java.io.File
import kotlin.test.assertNotNull
import kotlin.test.assertSame

class FunctionsObjectProxyTest {

    private val nemawashiSettings = NemawashiSettings(resourceRepositoryPath = File("target/resources.yaml"), secretStorePath = File("target/secretstore.p12"))
    private val nemawashiRuntime = NemawashiRuntime(nemawashiSettings)

    @Test
    internal fun testGet() {
        val taskRequest = TaskRequest(
                module = ModuleId("sys", defaultVersion),
                task = "sleep",
                parameters = mapOf("duration" to "2000")
        )
        val createContext = nemawashiRuntime.createContext(taskRequest)
        ModuleRegistry += TestModule()
        val functionsObjectProxy = FunctionsObjectProxy(createContext)
        val sampleFunctionalObject = functionsObjectProxy["test"] as SampleFunctionalObject
        assertNotNull(sampleFunctionalObject)
        val take2 = functionsObjectProxy["test"] as SampleFunctionalObject
        //cache works
        assertSame(sampleFunctionalObject, take2)
        println(take2.hello())
    }
}