/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime

import io.nemawashi.api.*
import io.nemawashi.api.annotations.Parameter
import io.nemawashi.api.metamodel.ParameterDef
import io.nemawashi.api.metamodel.TaskDef
import io.nemawashi.di.api.DependencyTarget
import io.nemawashi.di.api.TypeId
import io.nemawashi.di.api.addInstance
import io.nemawashi.runtime.container.ParameterDependency
import io.nemawashi.runtime.services.NemawashiScriptingService
import io.nemawashi.util.script.ScriptService
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.fail
import kotlin.reflect.full.findAnnotation
import kotlin.reflect.full.primaryConstructor
import kotlin.test.assertNotNull

class ParameterDependencyTest {


    val aModule: KotlinModule = KotlinModule("test", "test")
    val parameter = SampleSimpleTask::class.primaryConstructor!!.parameters.map { it.name to it }.toMap()
    val annotation = parameter["value"]?.findAnnotation<Parameter>() ?: fail("looks like SampleSimpleTaskChanged....")
    val ctx = TaskContextImpl(TaskRequest("test::test"))
    val taskId = TaskId(TaskType(aModule.id, "test"))

    @Test
    internal fun testResolve() {

        addInstance(NemawashiScriptingService()) withId TypeId(ScriptService::class) into ctx

        ctx.bindings["hello"] = "world"
        ctx.bindings["anArray"] = arrayOf("1", "2")
        ctx.bindings["intArray"] = arrayOf(1, 2)
        ctx.bindings["aMap"] = mapOf("one" to "two", "two" to "three")
        ctx.bindings["collection"] = listOf("1", "2")

        val taskDef = TaskDef(taskId,
                mapOf(
                        "value" to ParameterDef("name", "\${hello}"),
                        "simple" to ParameterDef("name", "simple"),
                        "arrayString" to ParameterDef("name", "\${anArray}"),
                        "arrayInt" to ParameterDef("name", "\${intArray}"),
                        "map" to ParameterDef("name", "\${aMap}")
                ),
                { true },
                LocalTarget
        )
        val taskDefNoScript = TaskDef(taskId, mapOf("value" to ParameterDef("name", "hello")), { true }, LocalTarget)

        val idOne = TaskId(TaskType(aModule.id, "test"))
        addInstance(taskDef) withId TaskDefId(idOne) into ctx
        val idTwo = TaskId(TaskType(aModule.id, "testTwo"))
        addInstance(taskDefNoScript) withId TaskDefId(idTwo) into ctx

        parameter.forEach { (_, v) ->
            val result = ParameterDependency().resolve<Any>(annotation, DependencyTarget(idOne, SimpleTask::class, v), ctx)
            assertNotNull(result)
        }
    }
}