/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime.approvals.yaml

import io.nemawashi.api.NemawashiProcess
import io.nemawashi.api.NemawashiSettings
import io.nemawashi.api.TaskType
import io.nemawashi.api.approvals.ApprovalRequest
import io.nemawashi.api.approvals.ApprovalRequestState
import io.nemawashi.api.resources.CategorisedType
import io.nemawashi.runtime.NemawashiRuntime
import io.nemawashi.runtime.resources.yaml.YamlResourceRepository
import io.nemawashi.util.system
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.io.File
import java.time.LocalDateTime
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertTrue

class YamlApprovalRequestsTest {


    var dbPath: File

    private val repo: YamlResourceRepository

    private val categorisedType: CategorisedType = CategorisedType("category", "name")

    private val runtime = NemawashiRuntime(NemawashiSettings())

    init {
        dbPath = File("${system.homeDir}/.cstest")
        if (dbPath.exists()) {
            dbPath.deleteRecursively()
        }
        repo = YamlResourceRepository("test", dbPath)
    }

    @BeforeEach
    internal fun setUp() {
        dbPath.mkdirs()
    }

    @AfterEach
    internal fun tearDown() {
        dbPath.deleteRecursively()
        dbPath.delete()
    }


    private val approvalRequest: ApprovalRequest = ApprovalRequest(
            process = NemawashiProcess(),
            headline = "The headline",
            description = "The description of the headling",
            assignedTo = arrayOf("yuri", "jim"),
            approveTask = TaskType.fromString("sys::script"),
            approveTaskParameters = mapOf("script" to "1 + 1"),
            rejectTask = TaskType.fromString("sys::shell-script"),
            rejectTaskParameters = mapOf("script" to listOf("echo 'no so well'", "echo 'done'")),
            expiresOn = LocalDateTime.now().plusDays(24)
    )

    @Test
    internal fun testRequest() {

        val requests = YamlApprovalRequests(repo, runtime)
        val request = approvalRequest
        requests.request(request)
        val resource = repo.get(YamlApprovalRequests.resourceType, request.id)
        assertNotNull(resource)
        val otherRequest = requests.fromResource(resource)
        assertEquals(request, otherRequest)
    }


    @Test
    internal fun testApprove() {
        val requests = YamlApprovalRequests(repo, runtime)
        requests.request(approvalRequest)
        val approval = requests.approve(approvalRequest.id)
        assertNotNull(approval)
        assertEquals(ApprovalRequestState.approved, approval.state)
    }

    @Test
    internal fun testReject() {
        val rejectionReason = "because I can"
        val requests = YamlApprovalRequests(repo, runtime)
        requests.request(approvalRequest)
        val approval = requests.reject(approvalRequest.id, rejectionReason)
        assertNotNull(approval)
        assertEquals(ApprovalRequestState.rejected, approval.state)
        assertEquals(rejectionReason, approval.rejectionReason)

    }

    @Test
    internal fun testFindByUser() {
        val requests = YamlApprovalRequests(repo, runtime)
        requests.request(approvalRequest)
        val byUser = requests.findByUser(approvalRequest.assignedTo[0])
        assertTrue { byUser.isNotEmpty() }
    }

    @Test
    internal fun testFindPendingByUser() {
        val requests = YamlApprovalRequests(repo, runtime)
        requests.request(approvalRequest)
        val byUser = requests.findPendingByUser(approvalRequest.assignedTo[0])
        assertTrue { byUser.isNotEmpty() }
        requests.reject(byUser.first().id, "qwerty")
        val newList = requests.findPendingByUser(approvalRequest.assignedTo[0])
        assertTrue { newList.isEmpty() }
    }
}