/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime.resources.yaml

import io.nemawashi.api.resources.Resource
import io.nemawashi.api.resources.CategorisedType
import io.nemawashi.util.system
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.io.File
import kotlin.test.assertEquals
import kotlin.test.assertNull
import kotlin.test.assertTrue

class YamlResourceRepositoryTest {


    var dbPath: File

    private val repo: YamlResourceRepository

    private val categorisedType: CategorisedType = CategorisedType("category", "name")

    init {
        dbPath = File("${system.homeDir}/.cstest")
        if (dbPath.exists()) {
            dbPath.deleteRecursively()
        }
        repo = YamlResourceRepository("test", dbPath)
    }

    @BeforeEach
    internal fun setUp() {
        dbPath.mkdirs()
    }

    @AfterEach
    internal fun tearDown() {
        dbPath.deleteRecursively()
        dbPath.delete()
    }

    @Test
    internal fun testSave() {

        val resource = Resource(categorisedType, "test123", mapOf(
                "a" to "hello world",
                "b" to arrayOf(1, 2, 3),
                "c" to mapOf<String, Any>("hello" to "world"),
                "d" to setOf(true, false, true))
        )
        repo.save(resource)
        val loadedResource = repo.get(categorisedType, "test123")!!
        assertEquals(resource, loadedResource)
        println(loadedResource["d"]!!::class.qualifiedName)
    }


    @Test
    internal fun testFind() {
        for (i in 0..9) {
            val resource = Resource(categorisedType, "test$i", mapOf(
                    "a" to "hello world",
                    "b" to arrayOf(1, 2, 3),
                    "c" to mapOf<String, Any>("hello" to "world"))
            )
            repo.save(resource)
        }
        val result = repo.find(categorisedType)
        val file = File(dbPath, "test/category/name")
        assertEquals(file.list().size, result.size)
    }

    @Test
    internal fun testFindWithAttributes() {
        for (i in 0..9) {
            val aVal = if (i % 2 == 0) "even" else "odd"
            val resource = Resource(categorisedType, "test$i", mapOf(
                    "a" to aVal,
                    "b" to arrayOf(1, 2, 3),
                    "c" to mapOf<String, Any>("hello" to "world"))
            )
            repo.save(resource)
        }
        val result = repo.find(categorisedType, mapOf("a" to "odd"))
        assertEquals(5, result.size)
        result.forEachIndexed { _, it ->
            assertEquals("odd", it["a"])
        }
    }

    @Test
    internal fun testDelete() {
        val resource = Resource(categorisedType, "test123", mapOf(
                "a" to "hello world",
                "b" to arrayOf(1, 2, 3),
                "c" to mapOf<String, Any>("hello" to "world"))
        )
        repo.save(resource)
        val loadedResource = repo.get(categorisedType, "test123")!!
        assertTrue { repo.delete(loadedResource.type, loadedResource.id) }
        assertNull(repo.get(categorisedType, "test123"))
    }
}