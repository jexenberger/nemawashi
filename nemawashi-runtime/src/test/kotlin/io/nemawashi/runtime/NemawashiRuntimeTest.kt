/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime

import io.nemawashi.api.*
import io.nemawashi.api.annotations.Task
import io.nemawashi.runtime.modules.system.Echo
import org.junit.jupiter.api.Test
import java.io.File
import kotlin.reflect.full.findAnnotation
import kotlin.test.assertEquals
import kotlin.test.assertTrue
import kotlin.test.fail

class NemawashiRuntimeTest {


    private val nemawashiSettings = NemawashiSettings(resourceRepositoryPath = File("target/resources.yaml"), secretStorePath = File("target/secretstore.p12"))

    private val nemawashiRuntime = NemawashiRuntime(nemawashiSettings)

    @Test
    internal fun testRunTask() {

        val taskRequest = TaskRequest(
                module = ModuleId("sys", defaultVersion),
                task = "echo",
                parameters = mapOf("value" to "hello world")
        )

        val execution = nemawashiRuntime.runTask(taskRequest, DefaultParameterCallback(), LocalTarget)
        assertTrue { execution.getResult() is Unit }
    }

    @Test
    internal fun testRunningTasks() {

        assertTrue { nemawashiRuntime.runningTasks.isEmpty() }
        val taskRequest = TaskRequest(
                module = ModuleId("sys", defaultVersion),
                task = "sleep",
                parameters = mapOf("duration" to "2000")
        )

        val execution = nemawashiRuntime.runTask(taskRequest, DefaultParameterCallback(), LocalTarget)
        assertTrue { nemawashiRuntime.runningTasks.isNotEmpty() }
        execution.getResult()
        assertTrue { nemawashiRuntime.runningTasks.isEmpty() }
    }

    @Test
    internal fun testRunTaskWithResult() {
        val taskRequest = TaskRequest(
                module = ModuleId("sys", defaultVersion),
                task = "script",
                parameters = mapOf("script" to "1+1")
        )
        val execution = nemawashiRuntime.runTask(taskRequest, DefaultParameterCallback(), LocalTarget)
        val result = execution.getResult()
        assertTrue { result is Integer }
        assertEquals(2, result)
    }

    @Test
    internal fun testShutdown() {
        val runtime = nemawashiRuntime
        val taskRequest = TaskRequest(
                module = ModuleId("sys", defaultVersion),
                task = "echo",
                parameters = mapOf("value" to "hello world")
        )
        runtime.runTask(taskRequest, DefaultParameterCallback(), LocalTarget)
        //runtime.shutdown()
    }

    @Test
    internal fun testRunGroupTask() {
        val taskRequest = TaskRequest(
                module = ModuleId("sys", defaultVersion),
                task = "group",
                parameters = mapOf("value" to "hello world")
        )
        nemawashiRuntime.runTask(taskRequest, DefaultParameterCallback(), LocalTarget)
    }

    @Test
    internal fun testRunGroupTaskWithCallback() {
        val taskRequest = TaskRequest(
                module = ModuleId("sys", defaultVersion),
                task = "echo",
                parameters = emptyMap()
        )
        nemawashiRuntime.runTask(taskRequest, DefaultParameterCallback("red"), LocalTarget)
    }

    @Test
    internal fun testSupportsDynamicType() {
        val runtime = nemawashiRuntime
        assertTrue { runtime.supportsDynamicType(File("test.groovy")) }
        assertTrue { runtime.supportsDynamicType(File("test.yaml")) }
        assertTrue { runtime.supportsDynamicType(File("test.yml")) }
    }

    @Test
    internal fun testModulesByName() {
        val modulesByName = nemawashiRuntime.modulesByName("sys")
        assertTrue { modulesByName.isNotEmpty() }
    }

    @Test
    internal fun testTaskDoc() {
        val taskDoc = nemawashiRuntime.taskDoc(TaskType.fromString("sys::echo"))
        taskDoc
                ?: fail("should have resolved documentation for sys::echo")
        val annot = Echo::class.findAnnotation<Task>()!!
        assertEquals(annot.name, taskDoc.name)
        assertEquals(annot.description, taskDoc.description)
        assertEquals(2, taskDoc.parameters.size)

    }

    /*
     need a better test
    @Test
    internal fun testRunGroupTaskWithCallbackEmpty() {
        try {
            NemawashiRuntime(NemawashiSettings()).runTask(ModuleId("sys", defaultVersion), "echo", emptyMap(), DefaultParameterCallback())
            fail("should have failed")
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }*/


}