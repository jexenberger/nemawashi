package io.nemawashi.runtime.files

import io.nemawashi.api.FileResource
import io.nemawashi.api.resources.CategorisedType
import io.nemawashi.util.fail
import io.nemawashi.util.stringOf
import io.nemawashi.util.system
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.io.ByteArrayInputStream
import java.io.File
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertTrue

class FileSystemFileServiceTest {


    var dbPath: File

    private val repo: FileSystemFileService

    private val categorisedType: CategorisedType = CategorisedType("category", "name")

    init {
        dbPath = File("${system.homeDir}/.cstest")
        if (dbPath.exists()) {
            dbPath.deleteRecursively()
        }
        repo = FileSystemFileService("test", dbPath)
    }

    @BeforeEach
    internal fun setUp() {
        dbPath.mkdirs()
    }

    @AfterEach
    internal fun tearDown() {
        dbPath.deleteRecursively()
        dbPath.delete()
    }

    @Test
    internal fun testSave() {
        val fileResource = FileResource(CategorisedType.fromString("test::test"), "test.txt", "hello world".byteInputStream())
        repo.save(fileResource)
        val file = File(dbPath, "test/test/test/test.txt")
        assertTrue { file.isFile }
        assertEquals("hello world", file.readText())
    }

    @Test
    internal fun testGet() {
        val content = stringOf(1000000, 'x')
        val type = CategorisedType.fromString("test::test")
        val fileResource = FileResource(type, "test.txt", ByteArrayInputStream(content.toByteArray()))
        repo.save(fileResource)
        val actualFile = File(dbPath, "test/test/test/test.txt")
        val file = repo.get(type, "test.txt")
        assertNotNull(file)
        assertEquals(actualFile.readText(), file.text)

    }

    @Test
    internal fun testList() {
        val content = stringOf(100, 'x')
        val type = CategorisedType.fromString("test::test")
        repo.save(FileResource(type, "a.txt", ByteArrayInputStream(content.toByteArray())))
        repo.save(FileResource(type, "b.txt", ByteArrayInputStream(content.toByteArray())))
        val files = repo.list(type)
        assertEquals(2, files.size)
        files.forEach {
            if (!it.id.equals("a.txt") && !it.id.equals("b.txt")) {
                kotlin.test.fail("unexpected file ${it.id}")
            }
        }

    }

    @Test
    internal fun testListDoesntExist() {
        val type = CategorisedType.fromString("x::x")
        val files = repo.list(type)
        assertTrue { files.isEmpty() }
    }
}