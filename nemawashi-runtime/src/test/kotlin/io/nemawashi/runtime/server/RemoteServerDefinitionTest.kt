/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime.server

import io.nemawashi.api.resources.Resource
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertNull
import kotlin.test.assertTrue

class RemoteServerDefinitionTest {


    @Test
    internal fun testFromUrl() {
        val (name, url, user, password, validateSSL) = RemoteServerDefinition.fromUrl("http://localhost:8000")
        assertEquals("localhost", name)
        assertEquals("http://localhost:8000", url)
        assertNull(user)
        assertNull(password)
        assertTrue { validateSSL }
    }

    @Test
    internal fun testFromUrlWithUser() {
        val (name, url, user, password, validateSSL) = RemoteServerDefinition.fromUrl("http://hello:world@localhost:8000")
        assertEquals("localhost", name)
        assertEquals("http://localhost:8000", url)
        assertEquals("hello", user)
        assertEquals("world", password)
        assertTrue { validateSSL }
    }

    @Test
    internal fun testFromUrlNoPort() {
        val (name, url, user, password, validateSSL) = RemoteServerDefinition.fromUrl("https://localhost")
        assertEquals("localhost", name)
        assertEquals("https://localhost", url)
        assertTrue { validateSSL }
    }

    @Test
    internal fun testFromUrlWithUserAndValidateSSL() {
        val (name, url, user, password, validateSSL) = RemoteServerDefinition.fromUrl("http://hello:world@localhost:8000?validateSSL=false")
        assertEquals("localhost", name)
        assertEquals("http://localhost:8000", url)
        assertEquals("hello", user)
        assertEquals("world", password)
        assertFalse { validateSSL }
    }

    @Test
    internal fun testToResource() {
        val fromUrl = RemoteServerDefinition.fromUrl("http://hello:world@localhost:8000/qwerty?validateSSL=false")
        val resource = fromUrl.toResource()
        assertEquals("localhost", resource.id)
        assertEquals("localhost", resource["name"])
        assertEquals("http://localhost:8000/qwerty", resource["url"])
        assertEquals("hello", resource["user"])
        assertEquals("world", resource["password"])
        assertEquals(false, resource["validateSSL"])

    }

    @Test
    internal fun testFromResource() {
        val resource = Resource(RemoteServerDefinition.resourceType, "test", mapOf(
                "name" to "localhost",
                "url" to "http://localhost:8000",
                "user" to "hello",
                "password" to "world",
                "validateSSL" to false

        ))
        val defn = RemoteServerDefinition.fromResource(resource)
        assertEquals(resource["name"], defn.name)
        assertEquals(resource["url"], defn.url)
        assertEquals(resource["user"], defn.user)
        assertEquals(resource["password"], defn.password)
        assertEquals(resource["validateSSL"], defn.validateSSL)

    }
}