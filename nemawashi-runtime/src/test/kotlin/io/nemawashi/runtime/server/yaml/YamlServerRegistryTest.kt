/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime.server.yaml

import io.nemawashi.api.resources.CategorisedType
import io.nemawashi.runtime.resources.yaml.YamlResourceRepository
import io.nemawashi.runtime.server.RemoteServerDefinition
import io.nemawashi.util.system
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.io.File
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

class YamlServerRegistryTest {

    var dbPath: File

    private val repo: YamlResourceRepository

    private val categorisedType: CategorisedType = CategorisedType("category", "name")

    init {
        dbPath = File("${system.homeDir}/.cstest")
        if (dbPath.exists()) {
            dbPath.deleteRecursively()
        }
        repo = YamlResourceRepository("test", dbPath)
    }

    @BeforeEach
    internal fun setUp() {
        dbPath.mkdirs()
    }

    @AfterEach
    internal fun tearDown() {
        dbPath.deleteRecursively()
        dbPath.delete()
    }

    @Test
    internal fun testGet() {
        val yamlServerRegistry = YamlServerRegistry(repo)
        yamlServerRegistry.save(RemoteServerDefinition.fromUrl("http://test:pwd@localhost:8000"))

        val resource = yamlServerRegistry.get("localhost")
        assertNotNull(resource)
        resource?.let {
            assertEquals("localhost", it.name)
            assertEquals("test", it.user)
            assertEquals("pwd", it.password!!)
            assertEquals("http://localhost:8000", it.url)
        }
    }

    @Test
    internal fun testSave() {
        val yamlServerRegistry = YamlServerRegistry(repo)
        yamlServerRegistry.save(RemoteServerDefinition.fromUrl("http://test:test@localhost:8000"))

        val resource = yamlServerRegistry.get("localhost")
        assertNotNull(resource)
    }
}