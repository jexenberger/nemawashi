/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime.dynamic

import io.nemawashi.api.ModuleId
import io.nemawashi.api.TaskId
import io.nemawashi.api.TaskRequest
import io.nemawashi.api.TaskType
import io.nemawashi.api.metamodel.ParameterDef
import io.nemawashi.api.metamodel.TaskDef
import io.nemawashi.di.api.addInstance
import io.nemawashi.runtime.*
import io.nemawashi.runtime.task.TaskDefContext
import io.nemawashi.runtime.yaml.YamlTaskProvider
import io.nemawashi.util.log.NOOPLogger
import io.nemawashi.util.string.FileDataSource
import org.junit.jupiter.api.Test
import java.io.File
import kotlin.test.assertEquals

class CompositeTaskTest {

    private val file = File("src/test/resources/samplemodule")
    val module = DefinedDynamicModule(file)
    init {
        ModuleRegistry += TestModule()
        ModuleRegistry += module
    }

    @Test
    internal fun testCompositeTask() {
        val taskFile = File(file, "sample.yaml")
        val type = TaskType(ModuleId.fromString("samplemodule::1.2.3"), "sample")
        val context = TaskContextImpl(taskRequest = TaskRequest("test::test"))
        val taskId = TaskId(type)
        val task = YamlTaskProvider(FileDataSource(taskFile), module).get(taskId, context, NOOPLogger()) as CompositeTask
        val def = TaskDef(taskId, mapOf(
                "value1" to ParameterDef("value1", 1),
                "value3" to ParameterDef("value3", "one, two, three"),
                "value4" to ParameterDef("value4", listOf("one", "two")),
                "value5" to ParameterDef("value5", mapOf("1" to "hello"))
        ), target = LocalTarget)
        addInstance(def) withId TaskDefId(def.id) into context
        context.bindings["value1"] = 1
        context.bindings["value2"] = "Hello"
        context.bindings["value4"] = "Hello"
        task.evaluate(taskId, context.bindings)
    }

    @Test
    internal fun testFunctionalCompositeTask() {
        val taskFile = File(file, "samplefunctional.yaml")
        val type = TaskType(ModuleId.fromString("samplemodule::1.2.3"), "samplefunctional")
        val context = TaskContextImpl(taskRequest = TaskRequest("test::test"))
        val taskId = TaskId(type)
        val task = YamlTaskProvider(FileDataSource(taskFile), module).get(taskId, context, NOOPLogger()) as CompositeTask
        val def = TaskDef(taskId, mapOf(
                "value" to ParameterDef("value", "hello")
        ), target = LocalTarget)
        TaskDefContext.defn = def
        addInstance(def) withId TaskDefId(def.id) into context
        context.bindings["value"] = "hello"
        val result = task.evaluate(taskId, context.bindings)
        assertEquals("HELLO", result)
    }
}