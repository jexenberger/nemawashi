/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime.dynamic

import org.junit.jupiter.api.Test
import java.io.File
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

class DefinedDynamicModuleTest {

    @Test
    internal fun testCreate() {
        val dynamicModule = DefinedDynamicModule(File("src/test/resources/samplemodule"))
        val (name, description, version, taskDir, properties) = dynamicModule.descriptor
        println(name)
        println(description)
        println(version)
        println(taskDir)
        println(properties)
    }

    @Test
    internal fun testTasks() {
        val tasks = DefinedDynamicModule(File("src/test/resources/samplemodule")).tasks
        assertEquals(6, tasks.size)
    }

    @Test
    internal fun testGetDescriptor() {
        val module = DefinedDynamicModule(File("src/test/resources/samplemodule"))
        val tasks = module.tasks
        val descriptor = module[module.tasks.iterator().next()]
        assertNotNull(descriptor)
    }
}