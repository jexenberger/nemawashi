/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime.groovy

import groovy.lang.Closure
import io.nemawashi.api.TaskRequest
import io.nemawashi.runtime.LocalTarget
import io.nemawashi.runtime.TaskContextImpl
import io.nemawashi.runtime.modules.system.SystemModule
import io.nemawashi.util.script.Eval
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class TaskExecutionHandlerTest {


    @Test
    internal fun testRunTask() {
        //val closure: Closure<Any> = Eval.compile(" { return 'hello'}").eval() as Closure<Any>
        val handler = TaskExecutionHandler(null, SystemModule(), TaskContextImpl(taskRequest = TaskRequest("test::test")), runTarget = LocalTarget)
        val result = handler.runTask("script", mapOf("script" to "1 + 1"), null)
        assertEquals(2, result)
    }

    @Test
    internal fun testRunTaskWithoutResult() {
        //val closure: Closure<Any> = Eval.compile(" { return 'hello'}").eval() as Closure<Any>
        val handler = TaskExecutionHandler(null, SystemModule(), TaskContextImpl(taskRequest = TaskRequest("test::test")), runTarget = LocalTarget)
        val result = handler.runTask("echo", mapOf("value" to "hello"), null)
        assertEquals(Unit, result)
    }

    @Test
    internal fun testRunGroupTask() {
        val closure: Closure<Any?> = Eval.compile("return  { println( 'hello') }").eval() as Closure<Any?>
        val handler = TaskExecutionHandler(null, SystemModule(), TaskContextImpl(taskRequest = TaskRequest("test::test")), runTarget = LocalTarget)
        val result = handler.runTask("foreach", mapOf("items" to listOf("hello","world")), closure)
        assertEquals(Unit, result)
    }
}