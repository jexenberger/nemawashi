/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime.groovy

import io.nemawashi.api.ComponentDefinitionException
import io.nemawashi.api.TaskRequest
import io.nemawashi.di.api.StringId
import io.nemawashi.runtime.TaskContextImpl
import io.nemawashi.runtime.dynamic.SingleFileModule
import io.nemawashi.util.log.NOOPLogger
import io.nemawashi.util.string.FileDataSource
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.io.File
import kotlin.test.assertNotNull
import kotlin.test.assertTrue

class GroovyTaskProviderTest {


    @Test
    internal fun testGet() {
        val file = File("src/test/resources/groovy/sampletask.groovy")
        val provider = GroovyTaskProvider(FileDataSource(file), SingleFileModule(file))
        val ctx = TaskContextImpl(taskRequest = TaskRequest("test::test"), ctx = mutableMapOf(), values = mutableMapOf())
        val task = provider.get(StringId("test"), ctx, NOOPLogger())
        assertNotNull(task)
        assertTrue { task is GroovyTask }
    }

    @Test
    internal fun testGetGroup() {
        val file = File("src/test/resources/groovy/samplegrouptask.groovy")
        val provider = GroovyTaskProvider(FileDataSource(file), SingleFileModule(file))
        val ctx = TaskContextImpl(taskRequest = TaskRequest("test::test"), ctx = mutableMapOf(), values = mutableMapOf())
        val task = provider.get(StringId("test"), ctx, NOOPLogger()) as GroovyGroupTask
        assertNotNull(task.after)
        assertNotNull(task.onError)
        assertNotNull(task.onFinally)
    }

    @Test
    internal fun testGetGroupBroken() {
        val file = File("src/test/resources/groovy/samplebrokengrouptask.groovy")
        val provider = GroovyTaskProvider(FileDataSource(file), SingleFileModule(file))
        val ctx = TaskContextImpl(taskRequest = TaskRequest("test::test"), ctx = mutableMapOf(), values = mutableMapOf())
        assertThrows<ComponentDefinitionException> {
            provider.get(StringId("test"), ctx, NOOPLogger())
        }
    }
}