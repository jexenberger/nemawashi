/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime.groovy

import io.nemawashi.api.Type
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertNotNull

class BaseTaskDescriptorDelegateTest {


    @Test
    internal fun testCreateBaseTaskDescriptor() {
        val taskDescriptorDelegate = GroovyTaskScriptManager("src/test/resources/groovy").loadTask("sampletask.groovy")
        assertEquals("sampletask", taskDescriptorDelegate.name)
        val actual = taskDescriptorDelegate.parameters!!.parameters()
        assertEquals(4, actual.size)

        val name = actual["name"]!!
        assertEquals("this is a test", name.description)
        assertEquals(Type.int, name.type)

        val simple = actual["simple"]!!
        assertEquals("A dirt simple parameter which is a string type and is required", simple.description)
        assertEquals(Type.string, simple.type)

        val anotherSample = actual["anotherSample"]!!
        assertEquals("A sample with regex", anotherSample.description)
        assertEquals(Type.string, anotherSample.type)
        assertEquals("^\\w+@[a-zA-Z_]+?\\.[a-zA-Z]{2,3}\$", anotherSample.regex)

        val full = actual["full"]!!
        assertEquals("full property", full.description)
        assertEquals(Type.stringArray, full.type)
        assertFalse { full.required }
        assertEquals(arrayOf("1", "2", "3", "4").toList(), full.allowedValues.toList())
        assertEquals("qwerty", full.alias)

        assertNotNull(taskDescriptorDelegate.script)
        assertEquals(Type.string, taskDescriptorDelegate.returnType)
        assertEquals("A whimsical string", taskDescriptorDelegate.returnDescription)
    }
}