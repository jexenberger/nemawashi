/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime.groovy

import io.nemawashi.api.*
import io.nemawashi.api.metamodel.ParameterDef
import io.nemawashi.api.metamodel.TaskDef
import io.nemawashi.di.api.StringId
import io.nemawashi.di.api.TypeId
import io.nemawashi.di.api.addInstance
import io.nemawashi.runtime.LocalTarget
import io.nemawashi.runtime.TaskContextImpl
import io.nemawashi.runtime.TaskDefId
import io.nemawashi.runtime.dynamic.DefinedDynamicModule
import io.nemawashi.util.Timer
import io.nemawashi.util.log.NOOPLogger
import io.nemawashi.util.string.FileDataSource
import org.junit.jupiter.api.Test
import java.io.File
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

class GroovyGroupTaskTest {


    @Test
    internal fun testBefore() {
        val (time, result) = Timer.run {
            val file = "src/test/resources/samplemodule/samplegrouptask.groovy"
            val module = DefinedDynamicModule(File("src/test/resources/samplemodule"))
            val factory = GroovyTaskProvider(FileDataSource(File(file)), module)
            val streamContext = TaskContextImpl(taskRequest = TaskRequest("test::test"))
            val task = factory.get(StringId("test"), streamContext, NOOPLogger()) as GroovyGroupTask
            assertNotNull(task)
            val taskId = TaskId(TaskType.fromString("samplemodule::sampletask"))
            val def = TaskDef(taskId, mapOf(
                    "start" to ParameterDef("start", 10),
                    "end" to ParameterDef("simple", 12)
            ), target = LocalTarget)
            addInstance(def) withId TaskDefId(def.id) into streamContext
            addInstance(NemawashiSettings()) withId TypeId(NemawashiSettings::class) into streamContext
            assertNotNull(task)

            assertEquals(Directive.continueExecution, task.before(taskId, streamContext.bindings))
            println(streamContext.bindings["currentKey"])
            assertEquals(Directive.continueExecution, task.before(taskId, streamContext.bindings))
            assertEquals(Directive.done, task.before(taskId, streamContext.bindings))
        }
        println("Took -> $time")
    }


}