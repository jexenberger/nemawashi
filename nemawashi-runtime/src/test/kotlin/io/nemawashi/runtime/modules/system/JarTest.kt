/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime.modules.system

import io.mockk.every
import io.mockk.mockk
import io.nemawashi.api.RunContext
import io.nemawashi.api.TaskId
import io.nemawashi.api.TaskRequest
import io.nemawashi.api.TaskType
import io.nemawashi.runtime.TaskContextImpl
import org.junit.jupiter.api.Test
import java.io.File
import kotlin.test.assertNotNull

class JarTest {

    @Test
    internal fun testRun() {
        val jar = Jar(File("src/test/resources/hello.jar"))
        val ctx = mockk<RunContext>(relaxed = true)
        val taskContext = TaskContextImpl(taskRequest = TaskRequest("test::test"))
        every { ctx["_ctx"] } returns taskContext
        jar.run(TaskId(TaskType.fromString("test::test"), "1"), ctx)
        val clazz = Class.forName("hello.Test", true, taskContext.classLoader)
        val newInstance = clazz.getConstructor().newInstance()
        assertNotNull(newInstance)
    }
}