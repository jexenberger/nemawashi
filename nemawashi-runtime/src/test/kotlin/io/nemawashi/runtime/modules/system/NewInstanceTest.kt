/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime.modules.system

import io.mockk.every
import io.mockk.mockk
import io.nemawashi.api.RunContext
import io.nemawashi.api.TaskId
import io.nemawashi.api.TaskType
import io.nemawashi.runtime.TaskContextImpl
import org.junit.jupiter.api.Test
import kotlin.test.assertTrue

class NewInstanceTest {

    @Test
    internal fun testEvaluate() {
        val task = NewInstance("java.lang.String")
        val ctx = mockk<RunContext>(relaxed = true)
        val taskContext = mockk<TaskContextImpl>(relaxed = true)
        every { taskContext.classLoader } returns Thread.currentThread().contextClassLoader
        every { ctx["_ctx"] } returns taskContext
        val result = task.evaluate(TaskId(TaskType.fromString("test::test"), "1"), ctx)
        assertTrue { result?.toString()?.isEmpty() ?: false }
    }

    @Test
    internal fun testEvaluateWithArgs() {
        val task = NewInstance("java.lang.String", arrayOf("hello world") )
        val ctx = mockk<RunContext>(relaxed = true)
        val taskContext = mockk<TaskContextImpl>(relaxed = true)
        every { taskContext.classLoader } returns Thread.currentThread().contextClassLoader
        every { ctx["_ctx"] } returns taskContext
        val result = task.evaluate(TaskId(TaskType.fromString("test::test"), "1"), ctx)
        assertTrue { result?.toString()?.equals("hello world")  ?: false}
    }
}