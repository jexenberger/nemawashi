/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime.modules.system

import io.nemawashi.api.MapRunContext
import io.nemawashi.api.TaskId
import io.nemawashi.api.TaskType
import io.nemawashi.util.system
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class ShellScriptTest {

    @Test
    internal fun testEvaluate() {

        val commands = if (system.unixVariant) arrayOf("ls", "uname") else arrayOf("dir", "echo")

        val shellScript = ShellScript(commands)
        val result = shellScript.evaluate(TaskId(TaskType.fromString("test::test"), "1"), MapRunContext())
        assertTrue { result!!["success"] as Boolean }
        assertEquals(2, (result!!["processResults"] as IntArray).size)
    }


    @Test
    internal fun testEvaluateWithFailure() {
        if (!system.unixVariant) {
            return
        }
        val shellScript = ShellScript(arrayOf("uname -xxx", "uname"))
        val result = shellScript.evaluate(TaskId(TaskType.fromString("test::test"), "1"), MapRunContext())
        assertFalse { result!!["success"] as Boolean }
        assertEquals(1, (result!!["processResults"] as IntArray).size)
    }

    @Test
    internal fun testEvaluateWithFailureAndResume() {
        if (!system.unixVariant) {
            return
        }
        val shellScript = ShellScript(arrayOf("uname -xxx", "uname"), true)
        val result = shellScript.evaluate(TaskId(TaskType.fromString("test::test"), "1"), MapRunContext())
        assertFalse { result!!["success"] as Boolean }
        assertEquals(2, (result!!["processResults"] as IntArray).size)
    }
}