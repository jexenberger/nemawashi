/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime.modules.csv

import io.nemawashi.api.Directive
import io.nemawashi.api.MapRunContext
import io.nemawashi.api.TaskId
import org.junit.jupiter.api.Test
import java.io.File
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

class ReadCSVFileTest {


    @Test
    internal fun testBeforeUsingFirstLineAsHeader() {
        val context = ReadCSVFileContext()
        val csvFile = ReadCSVFile(
                file = File("src/test/resources/sample_csv.csv"),
                useFirstLineAsHeader = true,
                context = context

        )
        val ctx = MapRunContext()
        val id = TaskId.fromString("test::test")
        val directive = csvFile.before(id, ctx)
        assertEquals(Directive.continueExecution, directive)
        val record = ctx[csvFile.outputVariable] as Map<String, String>
        assertNotNull(record)
        assertEquals("John", record["Name"])
        assertEquals("Smith", record["Surname"])
        assertEquals("1970-01-01", record["DOB"])
        assertEquals("1 Oak Drive", record["Address Line 1"])
        assertEquals("Hilly Heights", record["Address Line 2"])
        assertEquals("City Town", record["City"])
        assertEquals("11111", record["Postal Code"])

        assertEquals(Directive.continueExecution, csvFile.before(id, ctx))
        assertEquals(Directive.continueExecution, csvFile.before(id, ctx))
        assertEquals(Directive.done, csvFile.before(id, ctx))
    }

    @Test
    internal fun testBeforeWithHeader() {
        val context = ReadCSVFileContext()
        val csvFile = ReadCSVFile(
                file = File("src/test/resources/sample_csv.csv"),
                headers = arrayOf("n","s","d","a1","a2","c","p"),
                context = context

        )
        val ctx = MapRunContext()
        val id = TaskId.fromString("test::test")
        val directive = csvFile.before(id, ctx)
        assertEquals(Directive.continueExecution, directive)
        assertEquals(Directive.continueExecution, csvFile.before(id, ctx))
        val record = ctx[csvFile.outputVariable] as Map<String, String>
        assertNotNull(record)
        assertEquals("John", record["n"])
        assertEquals("Smith", record["s"])
        assertEquals("1970-01-01", record["d"])
        assertEquals("1 Oak Drive", record["a1"])
        assertEquals("Hilly Heights", record["a2"])
        assertEquals("City Town", record["c"])
        assertEquals("11111", record["p"])

        assertEquals(Directive.continueExecution, csvFile.before(id, ctx))
        assertEquals(Directive.continueExecution, csvFile.before(id, ctx))
        assertEquals(Directive.done, csvFile.before(id, ctx))
    }

    @Test
    internal fun testBeforeWithNoHeader() {
        val context = ReadCSVFileContext()
        val csvFile = ReadCSVFile(
                file = File("src/test/resources/sample_csv.csv"),
                context = context

        )
        val ctx = MapRunContext()
        val id = TaskId.fromString("test::test")
        val directive = csvFile.before(id, ctx)
        assertEquals(Directive.continueExecution, directive)
        assertEquals(Directive.continueExecution, csvFile.before(id, ctx))
        val record = ctx[csvFile.outputVariable] as Map<String, String>
        assertNotNull(record)
        assertEquals("John", record["0"])
        assertEquals("Smith", record["1"])
        assertEquals("1970-01-01", record["2"])
        assertEquals("1 Oak Drive", record["3"])
        assertEquals("Hilly Heights", record["4"])
        assertEquals("City Town", record["5"])
        assertEquals("11111", record["6"])

        assertEquals(Directive.continueExecution, csvFile.before(id, ctx))
        assertEquals(Directive.continueExecution, csvFile.before(id, ctx))
        assertEquals(Directive.done, csvFile.before(id, ctx))
    }
}