/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime.modules.system

import io.nemawashi.api.Directive
import io.nemawashi.api.TaskId
import io.nemawashi.api.TaskRequest
import io.nemawashi.api.TaskType
import io.nemawashi.runtime.TaskContextImpl
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class ForEachTest {

    @Test
    internal fun testBefore() {
        val task = ForEach(arrayOf(1, 2, 3).iterator())
        val bindings = TaskContextImpl(taskRequest = TaskRequest("test::test"), ctx = mutableMapOf(), values = mutableMapOf()).bindings
        val id = TaskId(TaskType.fromString("test::test"))
        var next = task.before(id, bindings)
        assertTrue(bindings.contains("__var"))
        assertEquals(1, bindings["__var"])
        assertEquals(Directive.continueExecution, next)
        next = task.before(id, bindings)
        assertEquals(2, bindings["__var"])
        assertEquals(Directive.continueExecution, next)
        next = task.before(id, bindings)
        assertEquals(3, bindings["__var"])
        assertEquals(Directive.continueExecution, next)
        next = task.before(id, bindings)
        assertEquals(Directive.done, next)
    }
}