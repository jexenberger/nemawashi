/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime.modules.json

import io.nemawashi.api.TaskId
import io.nemawashi.api.TaskRequest
import io.nemawashi.api.TaskType
import io.nemawashi.runtime.TaskContextImpl
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

@Suppress("UNCHECKED_CAST")
class ParseTest {

    @Test
    internal fun testRun() {
        val bindings = TaskContextImpl(taskRequest = TaskRequest("test::test"), ctx = mutableMapOf(), values = mutableMapOf()).bindings
        val parse = Parse("{ \"hello\": true }")
        val result = parse.evaluate(TaskId(TaskType.fromString("test::test")), bindings)!! as Map<String, Any?>
        assertEquals(true, result["hello"]!!)
    }


    @Test
    internal fun testRunArray() {
        val bindings = TaskContextImpl(taskRequest = TaskRequest("test::test"), ctx = mutableMapOf(), values = mutableMapOf()).bindings
        val parse = Parse("[{ \"hello\": true }]")
        val result = parse.evaluate(TaskId(TaskType.fromString("test::test")), bindings)!! as List<Map<String, Any?>>
        assertEquals(true, result[0]["hello"]!!)
    }


}