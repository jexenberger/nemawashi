/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime.modules.doc

import io.nemawashi.api.TaskId
import io.nemawashi.api.TaskRequest
import io.nemawashi.api.TaskType
import io.nemawashi.runtime.TaskContextImpl
import io.nemawashi.util.system
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import java.io.File
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class GenerateMarkdownTest {

    private val testPath: File = File("${system.tempDir}/nw_docs")

    @AfterEach
    internal fun tearDown() {
        testPath.deleteRecursively()
    }

    @Test
    internal fun testRunMatchSingle() {
        val modulesToGenerateFor = arrayOf("sys*", "doc*", "http*", "json*", "resources*", "service*", "ssh*", "template*")
        val templatingService = io.nemawashi.runtime.services.MvelTemplateService()
        val ctx = TaskContextImpl(taskRequest = TaskRequest("test::test"))
        GenerateMarkdown(templatingService, testPath, modulesToGenerateFor).run(TaskId(TaskType.fromString("test::test")), ctx.bindings)
        assertTrue { testPath.list().isNotEmpty() }
        assertEquals(modulesToGenerateFor.size, testPath.list().size )
    }
}