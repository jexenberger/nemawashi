/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime.modules.system

import io.nemawashi.api.MapRunContext
import io.nemawashi.api.RunContext
import io.nemawashi.api.TaskId
import io.nemawashi.api.TaskType
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class SetManyTest {


    @Test
    internal fun testRun() {
        val setMany = SetMany(mapOf("one" to 1, "two" to 2.0))
        val ctx = MapRunContext()
        setMany.run(TaskId(TaskType.fromString("test::test"), "1"), ctx)
        assertEquals(1, ctx["one"])
        assertEquals(2.0, ctx["two"])
    }

    @Test
    internal fun testRunParent() {
        val setMany = SetMany(mapOf("one" to 1, "two" to 2.0), true)
        val ctx = MapRunContext()
        ctx["_parent"] = MapRunContext()
        setMany.run(TaskId(TaskType.fromString("test::test"), "1"), ctx)
        val parent = ctx["_parent"] as RunContext
        assertEquals(1, parent["one"])
        assertEquals(2.0, parent["two"])
    }
}