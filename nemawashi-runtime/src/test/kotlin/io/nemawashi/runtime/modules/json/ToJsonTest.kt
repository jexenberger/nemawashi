/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime.modules.json

import io.nemawashi.api.TaskId
import io.nemawashi.api.TaskRequest
import io.nemawashi.api.TaskType
import io.nemawashi.runtime.TaskContextImpl
import org.junit.jupiter.api.Test
import kotlin.test.assertNotNull

class ToJsonTest {

    @Test
    internal fun testRun() {
        val bindings = TaskContextImpl(TaskRequest("test::test")).bindings
        val input = mapOf<String, Any?>("hello" to "world")
        val toJson = ToJson(input)
        val result = toJson.evaluate(TaskId(TaskType.fromString("test::test")), bindings)
        assertNotNull(result)
        println(result)
    }
}