/*
 *  Copyright 2018 Julian Exenberger
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package io.nemawashi.runtime

import io.nemawashi.api.*
import io.nemawashi.api.annotations.Parameter
import io.nemawashi.api.annotations.Task
import io.nemawashi.api.annotations.TaskContext

@Task("group", "A group task which does stuff", returnType = Type.unit)
class SimpleGroupTask(
        @Parameter(description = "description", default = "test") val value: String,
        @TaskContext val context: SimpleGroupTaskContext
) : GroupTask {

    override fun before(id: TaskId, ctx: RunContext): Directive {
        context.before = true
        return Directive.continueExecution
    }

    override fun after(id: TaskId, ctx: RunContext): Directive {
        context.after = true
        return Directive.done
    }

    override fun onError(id: TaskId, error: TaskError, ctx: RunContext) {
        context.error = true
    }

    override fun onFinally(id: TaskId, ctx: RunContext) {
        context.onFinally = true
    }


}