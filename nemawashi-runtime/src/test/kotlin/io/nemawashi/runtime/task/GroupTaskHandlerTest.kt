/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime.task

import io.nemawashi.api.TaskId
import io.nemawashi.api.TaskRequest
import io.nemawashi.api.TaskType
import io.nemawashi.api.metamodel.GroupTaskDef
import io.nemawashi.api.metamodel.ParameterDef
import io.nemawashi.runtime.*
import io.nemawashi.runtime.container.TaskScopeId
import org.junit.jupiter.api.Test
import kotlin.test.assertNull
import kotlin.test.assertTrue

class GroupTaskHandlerTest {

    private val taskId: TaskId

    private val defaultTaskDef: GroupTaskDef

    private val module = TestModule()

    init {
        ModuleRegistry += module
        taskId = TaskId(TaskType(module.id, "group"))
        defaultTaskDef = GroupTaskDef(
                taskId,
                mapOf("value" to ParameterDef("value", "test")),
                false,
                runTarget = LocalTarget
        )
    }


    @Test
    internal fun testPreTraversal() {
        val ctx = TaskContextImpl(taskRequest = TaskRequest("test::test"))
        ctx.registerTask(defaultTaskDef)
        val handler = io.nemawashi.runtime.task.GroupTaskHandler(taskId, false, defaultTaskDef)
        handler.enterBranch(ctx)
        handler.preTraversal(ctx)
        val taskContext = ctx.get<SimpleGroupTaskContext>(TaskScopeId(ctx, taskId))!!
        assertTrue { taskContext.before }
    }

    @Test
    internal fun testRunChildren() {
        val ctx = TaskContextImpl(taskRequest = TaskRequest("test::test"))
        ctx.registerTask(defaultTaskDef)
        val handler = io.nemawashi.runtime.task.GroupTaskHandler(taskId, false, defaultTaskDef)
        handler.execute(ctx)
    }

    @Test
    internal fun testPreTraversalConditional() {
        val id = TaskId(TaskType(module.id, "group"))
        val taskDef = GroupTaskDef(
                id,
                mapOf("value" to ParameterDef("value", "test")),
                false,
                { false },
                runTarget = LocalTarget
        )
        val ctx = TaskContextImpl(taskRequest = TaskRequest("test::test"))
        ctx.registerTask(taskDef)
        val handler = io.nemawashi.runtime.task.GroupTaskHandler(id, false, taskDef)
        handler.enterBranch(ctx)
        handler.preTraversal(ctx)
        val taskContext = ctx.get<SimpleGroupTaskContext>(TaskScopeId(ctx, id))
        assertNull(taskContext)
    }

    @Test
    internal fun testPostTraversal() {
        val ctx = TaskContextImpl(taskRequest = TaskRequest("test::test"))
        ctx.registerTask(defaultTaskDef)
        val handler = io.nemawashi.runtime.task.GroupTaskHandler(taskId, false, defaultTaskDef)
        handler.enterBranch(ctx)
        handler.postTraversal(ctx)
        val taskContext = ctx.get<SimpleGroupTaskContext>(TaskScopeId(ctx, taskId))!!
        assertTrue { taskContext.after }
    }

    @Test
    internal fun testOnError() {
        val ctx = TaskContextImpl(TaskRequest("test::test"))
        ctx.registerTask(defaultTaskDef)
        val handler = io.nemawashi.runtime.task.GroupTaskHandler(taskId, false, defaultTaskDef)
        handler.enterBranch(ctx)
        handler.onError(RuntimeException("test"), ctx)
        val taskContext = ctx.get<SimpleGroupTaskContext>(TaskScopeId(ctx, taskId))!!
        assertTrue { taskContext.error }
    }
}