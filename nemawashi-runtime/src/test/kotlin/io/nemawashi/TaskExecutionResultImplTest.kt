/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi

import io.nemawashi.api.ModuleId
import io.nemawashi.api.NemawashiSettings
import io.nemawashi.api.TaskRequest
import io.nemawashi.api.defaultVersion
import io.nemawashi.runtime.DefaultParameterCallback
import io.nemawashi.runtime.LocalTarget
import io.nemawashi.runtime.NemawashiRuntime
import org.junit.jupiter.api.Test
import java.io.File
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class TaskExecutionResultImplTest {

    private val nemawashiSettings = NemawashiSettings(resourceRepositoryPath = File("target/resources.yaml"), secretStorePath = File("target/secretstore.p12"))

    private val nemawashiRuntime = NemawashiRuntime(nemawashiSettings)


    @Test
    internal fun testIsRunning() {
        val taskRequest = TaskRequest(
                module = ModuleId("sys", defaultVersion),
                task = "sleep",
                parameters = mapOf("duration" to "2000")
        )
        val taskExecutionImpl = nemawashiRuntime.createTaskExecution(taskRequest, DefaultParameterCallback(), LocalTarget)
        val taskExecution = taskExecutionImpl.run()
        val result = taskExecution
        assertTrue { result.running }
        Thread.sleep(3000)
        assertFalse { result.running }
    }
}