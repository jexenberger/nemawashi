package io.nemawashi.runtime.services;

import io.nemawashi.api.NemawashiModule;

import java.util.LinkedHashSet;
import java.util.ServiceLoader;
import java.util.Set;

public class ServiceModuleLoader {

    public static Set<NemawashiModule> loadModules() {
        ServiceLoader<NemawashiModule> nemawashiModules = ServiceLoader.load(NemawashiModule.class);
        Set<NemawashiModule> modules = new LinkedHashSet<>();
        for (NemawashiModule nemawashiModule : nemawashiModules) {
            modules.add(nemawashiModule);
        }
        return modules;
    }

}
