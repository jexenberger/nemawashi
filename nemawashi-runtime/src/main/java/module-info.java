/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

import io.nemawashi.api.NemawashiServiceLoaderFactory;
import io.nemawashi.runtime.NemawashiRuntimeServiceLoaderFactory;

open module io.nemawashi.runtime {

    //until ServiceLoader resolution works better
    exports io.nemawashi.runtime;
    exports io.nemawashi.runtime.server;

    requires io.nemawashi.di;
    requires io.nemawashi.api;

    requires transitive io.nemawashi.util;

    requires kotlin.stdlib;
    requires kotlin.reflect;

    requires java.scripting;

    requires semantic.version;

    requires snakeyaml;

    requires mvel2;

    requires org.codehaus.groovy;

    requires com.fasterxml.jackson.core;
    requires com.fasterxml.jackson.databind;
    requires com.fasterxml.jackson.module.kotlin;
    requires jcl.core;
    requires mail;
    requires java.activation;

    uses io.nemawashi.api.NemawashiModule;
    provides NemawashiServiceLoaderFactory with NemawashiRuntimeServiceLoaderFactory;




}