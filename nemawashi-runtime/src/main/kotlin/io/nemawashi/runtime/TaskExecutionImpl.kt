/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime

import io.nemawashi.api.*
import io.nemawashi.api.descriptor.TaskDescriptor
import io.nemawashi.api.metamodel.FunctionalTaskDef
import io.nemawashi.api.metamodel.GroupTaskDef
import io.nemawashi.api.metamodel.ParameterDef
import io.nemawashi.api.metamodel.TaskDef
import io.nemawashi.runtime.task.GroupTaskHandler
import io.nemawashi.runtime.task.NonGroupTaskHandler
import io.nemawashi.runtime.tree.Node
import io.nemawashi.util.ThreadLocalHandler
import java.util.concurrent.Callable
import java.util.concurrent.ExecutorService
import java.util.concurrent.Future
import java.util.concurrent.TimeUnit

class TaskExecutionImpl(
        val runningTaskRegistry: MutableMap<String, TaskExecution>,
        val request: TaskRequest,
        override val taskId: TaskId,
        val taskParams: Map<String, ParameterDef>,
        val taskDescriptor: TaskDescriptor,
        val streamContext: TaskContextImpl,
        val processLog: ProcessLog,
        val executor: ExecutorService,
        val runTarget: RunTarget) : TaskExecution {


    val future: Future<Pair<Any, Map<String, Any?>>> by lazy {
        executor.submit(Callable<Pair<Any, Map<String, Any?>>> { runTask() })
    }

    protected fun runTask(): Pair<Any, Map<String, Any?>> {
        val node: Node<TaskContextImpl> = if (taskDescriptor.groupTask) {
            val defn = GroupTaskDef(taskId, taskParams, false, runTarget = runTarget)
            streamContext.registerTask(defn)
            GroupTaskHandler(taskId, false, defn)
        } else {
            val defn = taskDescriptor.returnDescriptor?.let {
                FunctionalTaskDef(taskId, taskParams, { true }, "__result__", target = runTarget)
            } ?: TaskDef(taskId, taskParams, target = runTarget)
            streamContext.registerTask(defn)
            NonGroupTaskHandler(taskId, defn)
        }
        try {
            processLog.logTaskStart(taskRequest = request)
            ThreadLocalHandler.doWithValue(taskId to streamContext) { ctx ->
                node.execute(streamContext)
            }
            val returnVal = streamContext.bindings["__result__"] ?: Unit
            processLog.logTaskComplete(taskRequest = request)
            return returnVal to streamContext.bindings
        } catch (e: Exception) {
            processLog.logTaskError(request, e)
            throw e
        } finally {
            runningTaskRegistry.remove(this.executionId)
            TaskContextImpl.release(streamContext.id)
        }
    }

    internal fun run(): TaskExecution {
        this.future
        return this
    }

    override val executionId: String
        get() = streamContext.id
    override val running: Boolean
        get() = !future.isDone

    override val ctx: Map<String, Any?>
        get() = future.get().second


    override fun getResult(timeout: Long): Any? {
        val (result, _) = if (timeout > 0) future.get(timeout, TimeUnit.MILLISECONDS) else future.get()
        return result
    }


}