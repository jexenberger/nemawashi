/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime

import io.nemawashi.api.*
import io.nemawashi.api.metamodel.TaskDef

object LocalTarget : RunTarget {

    override fun run(taskId: TaskId, taskDef: TaskDef, ctx: TaskContext): Any? {
        val task = try {
            ctx.get<Task>(taskId) ?: throw IllegalStateException("$taskId not resolved??!!")
        } catch (e: IllegalArgumentException) {
            throw TaskFailedException(taskId, "Unable to create instance -> '${e}'")
        }
        when (task) {
            //current we cannot support group tasks in the target model, must run in container
            is GroupTask -> {
                throw TaskFailedException(taskId, "Group tasks are not supported, must be in container")
            }
            is FunctionalTask<*> -> {
                return task.evaluate(taskId, ctx.bindings)
            }
            else -> {
                (task as SimpleTask).run(taskId, ctx.bindings)
                return Unit
            }
        }
    }

    override val name: String = "local"
}