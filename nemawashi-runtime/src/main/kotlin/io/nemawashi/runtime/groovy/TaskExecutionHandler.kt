/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime.groovy

import groovy.lang.Closure
import io.nemawashi.api.*
import io.nemawashi.api.metamodel.FunctionalTaskDef
import io.nemawashi.api.metamodel.GroupTaskDef
import io.nemawashi.api.metamodel.ParameterDef
import io.nemawashi.runtime.LocalTarget
import io.nemawashi.runtime.TaskContextImpl
import io.nemawashi.runtime.task.GroupTaskHandler
import io.nemawashi.runtime.task.NonGroupTaskHandler
import io.nemawashi.runtime.tree.LambdaLeaf
import io.nemawashi.util.Ids
import io.nemawashi.util.RuntimeUtil
import io.nemawashi.util.ThreadLocalHandler
import io.nemawashi.util.system
import java.util.*

@Suppress("UNCHECKED_CAST")
open class TaskExecutionHandler(
        val callingModule: NemawashiModule?,
        val module: NemawashiModule,
        val ctx: TaskContextImpl,
        val runTarget: RunTarget) : BaseGroovyInterceptable() {

    fun handleMethodMissing(name:String, arguments: List<Any?>): Any? {
        var params = emptyMap<String, Any?>()
        var closure: Closure<Any?>? = null
        for (argument in arguments) {
            when (argument) {
                is Array<*> -> params = argument[0] as Map<String, Any?>
                is Map<*, *> -> params = argument as Map<String, Any?>
                is Closure<*> -> closure = argument as Closure<Any?>
                else -> {
                    val taskType = TaskType(module.id, name)
                    throw TaskFailedException(createTaskId(name, taskType), "Parameter must either be a keyValue or a code block for a group task")
                }
            }
        }
        return runTask(name, params, closure)
    }


    fun runTask(name: String, parameters: Map<String, Any?>, groupHandler: Closure<Any?>?): Any? {
        val attemptOne = module.getByName(name)
        val (taskDescriptor, nameToUse) = if (attemptOne == null) {
            val candidateName = name.replace('_', '-')
            val descriptor = module.getByName(candidateName) ?: throw TaskDoesNotExistException(TaskType(module.id, name))
            descriptor to candidateName
        } else {
            attemptOne to name
        }
        val taskType = TaskType(module.id, nameToUse)
        val id = createTaskId(name, taskType)
        val taskParameters = parameters
                .filterKeys { !it.equals("parallel") }
                .mapValues { (k, v) -> ParameterDef(k, v) }
        val parallel = parameters["parallel"]?.let { it as Boolean } ?: false
        if (taskDescriptor.groupTask) {
            val defn = GroupTaskDef(id, taskParameters, false, runTarget = LocalTarget)
            val handler = GroupTaskHandler(id, parallel, defn)
            if (groupHandler != null) {
                handler += LambdaLeaf(UUID.randomUUID().toString()) { ctx ->
                    ThreadLocalHandler.doWithValue(id to ctx) {
                        GroovyClosureHandler(groupHandler).handleClosureCall(id, ctx)
                    }
                }
            }
            ctx.registerTask(defn,callingModule )
            handler.execute(ctx)
            return Unit
        } else {
            val result = "__result__"
            val defn = FunctionalTaskDef(id, taskParameters, assign = result, target = this.runTarget)
            ctx.registerTask(defn,callingModule )
            val handler = NonGroupTaskHandler(id, defn)
            handler.execute(ctx)
            return ctx.bindings[result] ?: Unit
        }
    }

    private fun createTaskId(nameToUse: String, taskType: TaskType): TaskId {
        val executingLine = RuntimeUtil.findByFilter { it.fileName.endsWith(".groovy") }
        val lineFormatId = LineFormatId(this.module.id, "$nameToUse.groovy", executingLine?.lineNumber
                ?: 1, Ids.next().toString())
        val id = TaskId(taskType, lineFormatId)
        return id
    }

    override fun invokeMethod(name: String?, parameters: Any?): Any {
        val parms = if (parameters is Array<*>) parameters.toList() else listOf(parameters)
        return handleMethodMissing(name!!, parms)!!
    }

    override fun setProperty(name: String?, args: Any?) {
    }

    override fun getProperty(name: String?): Any {
        return Unit
    }

}