/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime.groovy

import groovy.lang.Closure
import io.nemawashi.api.NemawashiSettings
import io.nemawashi.api.TaskError
import io.nemawashi.api.TaskId
import io.nemawashi.api.descriptor.TaskDescriptor
import io.nemawashi.runtime.TaskContextImpl
import io.nemawashi.runtime.dynamic.DynamicFunctionalTask
import io.nemawashi.util.ThreadLocalHandler

class GroovyTask(val script: Closure<Any?>, override val descriptor: TaskDescriptor, override val nestedContext: TaskContextImpl) : DynamicFunctionalTask {

    override fun runTask(id: TaskId, ctx: TaskContextImpl) = GroovyClosureHandler(script).handleClosureCall(id, ctx)
}