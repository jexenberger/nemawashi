/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime.groovy

import groovy.lang.Closure
import io.nemawashi.api.*
import io.nemawashi.runtime.TaskContextImpl
import io.nemawashi.util.ThreadLocalHandler
import java.util.*

class GroovyClosureHandler(
        val script: Closure<Any?>
) {


    fun handleClosureCall(id: TaskId, ctx: TaskContextImpl): Any? {
        val taskModule = ctx.bindings["__taskModule"] as NemawashiModule?
        return ThreadLocalHandler.doWithValue(id to ctx) { theContext ->
            return@doWithValue run(script, id, ctx, taskModule)
        }
    }

    internal fun run(script: Closure<Any?>, id: TaskId, ctx: TaskContextImpl, taskModule: NemawashiModule?): Any? {
        val bindings = ctx.bindings
        val parentTaskModule = bindings["\$nw"]?.let { (it as ModuleExecutionHandler).owningModule }
                ?: taskModule
        bindings["\$nw"] = ModuleExecutionHandler(parentTaskModule, ctx)
        val closure = script.rehydrate(bindings, bindings, bindings)
        closure.resolveStrategy = 1

        val noParameters = closure.maximumNumberOfParameters
        val result = when (noParameters) {
            0 -> closure.call()
            1 -> closure.call(id)
            2 -> closure.call(id, bindings)
            else -> {
                throw TaskFailedException(id, "Script  can support between 0 and 2 parameters")
            }
        }
        return result
    }
}