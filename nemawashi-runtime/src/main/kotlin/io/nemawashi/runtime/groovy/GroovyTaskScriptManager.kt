/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime.groovy


import groovy.lang.Binding
import groovy.util.GroovyScriptEngine
import io.nemawashi.api.ComponentDefinitionException
import io.nemawashi.util.Timer
import io.nemawashi.util.log.Log
import org.xeustechnologies.jcl.JarClassLoader
import java.io.File
import java.net.URL

class GroovyTaskScriptManager(val baseUrl: String) {


    private val manager: GroovyScriptEngine by lazy {
        val libFolder = File(baseUrl, "lib")
        val loader = if (libFolder.exists() && libFolder.isDirectory) {
            val libLoader = JarClassLoader(GroovyTaskScriptManager::class.java.module.classLoader)
            libLoader.add("$libFolder/")
            libLoader
        } else {
            GroovyTaskScriptManager::class.java.module.classLoader
        }
        GroovyScriptEngine(loadClasses(), loader)
    }

    fun loadClasses(buffer: MutableList<URL> = mutableListOf()) : Array<URL> {
        val moduleBase = File(baseUrl)
        buffer += moduleBase.toURI().toURL()
        val classesDir = File(moduleBase, "classes")
        if (classesDir.isDirectory) {
            findDirectories(classesDir.absolutePath, buffer)
        }
        return buffer.toTypedArray()
    }


    fun findDirectories(baseUrl: String, buffer: MutableList<URL> = mutableListOf()) {
        val file = File(baseUrl)
        if (file.isDirectory) {
            buffer += file.toURI().toURL()
            file.listFiles()
                    .filter { it.isDirectory }
                    .forEach { findDirectories(it.absolutePath, buffer) }
        }
    }

    fun loadTask(componentId: String): BaseTaskDescriptorDelegate {
        val (time, delegate) = Timer.run {
            val run = manager.run(File(componentId).name, Binding())
            val baseTaskDescriptorDelegate = ((run
                    ?: throw ComponentDefinitionException(componentId, "This script needs to contain a task definition")) as? BaseTaskDescriptorDelegate
                    ?: throw ComponentDefinitionException(componentId, "This script needs to contain a task definition"))
            baseTaskDescriptorDelegate.name = File(componentId).nameWithoutExtension
            baseTaskDescriptorDelegate
        }
        Log.debug("Task $componentId to ${time}ms")
        return delegate
    }
}