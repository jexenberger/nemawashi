/*
 *  Copyright 2018 Julian Exenberger
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package io.nemawashi.runtime.groovy

import groovy.lang.Closure
import io.nemawashi.api.descriptor.ParameterDescriptor
import java.util.*

open class BaseParametersDelegate : GroovyMapDelegate() {

    override fun invokeMethod(method: String?, parameters: Any?): Any {
        val argsList = Arrays.asList(parameters as Array<Any>)
        val arg = (argsList[0] as Array<Any>)[0]
        val result = when (arg) {
            is Closure<*> -> {
                arg.resolveStrategy = Closure.DELEGATE_FIRST
                arg.delegate = BaseParameterDelegate()
                arg.call()
                arg.delegate
            }
            else -> {
                BaseParameterDelegate(arg.toString())
            }
        }
        map[method!!] = result
        return result
    }

    fun parameters(): Map<String, ParameterDescriptor> {
        return map.mapValues { (k, v) ->
            (v as BaseParameterDelegate).parameterDescriptor(k)
        }
    }

}