/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime.groovy

import io.nemawashi.api.ComponentDefinitionException
import io.nemawashi.api.ModuleException
import io.nemawashi.api.Task
import io.nemawashi.api.descriptor.TaskDescriptor
import io.nemawashi.di.api.ComponentId
import io.nemawashi.di.api.Context
import io.nemawashi.runtime.TaskContextImpl
import io.nemawashi.runtime.dynamic.BaseDynamicModule
import io.nemawashi.runtime.dynamic.DynamicTaskProvider
import io.nemawashi.util.log.LogProvider
import javax.activation.DataSource
import javax.script.ScriptException

class GroovyTaskProvider(source: DataSource, module: BaseDynamicModule) : DynamicTaskProvider(source, module) {

    private val manager: GroovyTaskScriptManager = GroovyTaskScriptManager(module.modulePath)
    private val delegate: BaseTaskDescriptorDelegate by lazy {
        manager.loadTask(source.name)
    }

    override fun get(id: ComponentId, ctx: Context, log: LogProvider): Task {
        return try {

            val streamContext = ctx as TaskContextImpl
            val taskDescriptor = delegate.taskDescriptor(module, this)
            val taskContext = createDynamicTaskContext(streamContext)
            if (delegate.isFunctionalTask) {
                GroovyTask(
                        delegate.script!!,
                        taskDescriptor,
                        taskContext
                )
            } else {
                val beforeClosure = delegate.before
                        ?: throw ComponentDefinitionException(id.stringId, "This task has neither a script{} for a normal task or a before{} for a group task")
                GroovyGroupTask(
                        beforeClosure,
                        delegate.after,
                        delegate.onFinally,
                        delegate.onError,
                        taskDescriptor,
                        taskContext
                )
            }
        } catch (e: ScriptException) {
            throw ModuleException(module, "unable to load ${source.name} due to '${e.message}'")
        }
    }

    override fun createDescriptor(): TaskDescriptor {
        return delegate.taskDescriptor(module, this)
    }
}