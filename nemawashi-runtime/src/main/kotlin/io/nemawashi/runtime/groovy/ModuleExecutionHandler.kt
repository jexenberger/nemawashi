/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime.groovy

import io.nemawashi.api.*
import io.nemawashi.runtime.LocalTarget
import io.nemawashi.runtime.ModuleRegistry
import io.nemawashi.runtime.TaskContextImpl
import io.nemawashi.runtime.server.RemoteTarget

open class ModuleExecutionHandler(val owningModule: NemawashiModule?, val ctx: TaskContextImpl, val target: RunTarget = LocalTarget) : BaseGroovyInterceptable() {

    fun createTaskExecutionHandler(moduleName: String): TaskExecutionHandler {
        val moduleVersion = owningModule?.getDependencyVersion(moduleName) ?: defaultVersion
        val moduleId = ModuleId(moduleName, moduleVersion)
        val targetModule = ModuleRegistry[moduleId] ?: throw ModuleDoesNotExistException(moduleName)
        return createTaskWrapper(owningModule, targetModule, ctx)
    }

    open fun createTaskWrapper(owningModule: NemawashiModule?, targetModule: NemawashiModule, ctx: TaskContextImpl): TaskExecutionHandler {
        return TaskExecutionHandler(owningModule, targetModule, ctx, target)
    }

    override fun invokeMethod(method: String?, parameters: Any?): Any? {
        if (method.equals("runAt")) {
            val url = (parameters as Array<Any?>)[0]
            return ModuleExecutionHandler(owningModule, ctx, RemoteTarget(url.toString()))
        }
        if (ModuleRegistry.systemModule.getByName(method!!) != null) {
            val parms = if (parameters is Array<*>) parameters.toList() else listOf(parameters)
            return createTaskExecutionHandler(ModuleRegistry.systemModule.name).handleMethodMissing(method, parms)

        }
        throw UnsupportedOperationException("$method is not supported")
    }


    override fun setProperty(p0: String?, p1: Any?) {
    }

    override fun getProperty(name: String?): Any {
        return createTaskExecutionHandler(name!!)
    }


}