/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */
package nemawashi

import groovy.lang.Closure
import io.nemawashi.api.*
import io.nemawashi.runtime.TaskContextImpl
import io.nemawashi.runtime.container.TaskScope
import io.nemawashi.runtime.container.TaskScopeId
import io.nemawashi.runtime.groovy.BaseTaskDescriptorDelegate
import io.nemawashi.runtime.groovy.GroovyClosureHandler
import io.nemawashi.util.RuntimeUtil
import io.nemawashi.util.ThreadLocalHandler
import io.nemawashi.util.system
import java.util.*
import java.util.concurrent.Callable
import java.util.concurrent.Future

object nw {

    @JvmStatic
    fun task(closure: Closure<Any>): BaseTaskDescriptorDelegate {
        val delegate = BaseTaskDescriptorDelegate()
        closure.delegate = delegate
        closure.call()
        return delegate
    }

    @JvmStatic
    fun putAt(key:String, value:Any?) {
        val (id, ctx) = resolveCurrentTaskAndContext()
        val taskScopeId = TaskScopeId(ctx, id)
        val current = TaskScope.get(taskScopeId, ctx) as MutableMap<String, Any?>? ?: mutableMapOf()
        current[key] = value
        TaskScope.add(taskScopeId, current)
    }

    @JvmStatic
    fun getAt(key:String) : Any? {
        val (id, ctx) = resolveCurrentTaskAndContext()
        val taskScopeId = TaskScopeId(ctx, id)
        val current = TaskScope.get(taskScopeId, ctx)?.let { it as MutableMap<String, Any?> } ?: return null
        return current[key]
    }

    @JvmStatic
    fun fail(msg: String) {
        val (id, _) = resolveCurrentTaskAndContext()
        val exc = Exception()
        val lineNo = exc.stackTrace.filter {
            it.fileName?.endsWith(".groovy") ?: false
        }.firstOrNull()?.lineNumber ?: -1
        throw TaskFailedException(id, msg, lineNo)
    }

    @JvmStatic
    fun async(closure: Closure<Any?>): Future<Any?> {
        val (id, ctx) = resolveCurrentTaskAndContext()
        val subContext = ctx.subContext(true)
        return system.optimizedExecutor.submit(Callable {
            ThreadLocalHandler.doWithValue(id to subContext) { theContext ->
                GroovyClosureHandler(closure).handleClosureCall(theContext.first, theContext.second)
            }
        })
    }

    private fun resolveCurrentTaskAndContext(): Pair<TaskId, TaskContextImpl> {
        val id = TaskId(TaskType.fromString("_nwInternal::async"), UUID.randomUUID().toString())
        val currentValue = ThreadLocalHandler.currentValue<Pair<TaskId, TaskContextImpl>>()
                ?: throw TaskFailedException(id, "Something went wrong somewhere in the call  chain")
        return currentValue
    }
}
