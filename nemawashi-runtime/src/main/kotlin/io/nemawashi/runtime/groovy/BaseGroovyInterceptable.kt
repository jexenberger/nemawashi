/*
 *  Copyright 2018 Julian Exenberger
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package io.nemawashi.runtime.groovy

import groovy.lang.GroovyInterceptable
import groovy.lang.MetaClass
import groovy.lang.MetaClassImpl

abstract class BaseGroovyInterceptable : GroovyInterceptable{

    internal var metaClass:MetaClass = MetaClassImpl(this::class.java)

    override fun setMetaClass(p0: MetaClass?) {
        metaClass = p0!!
    }

    override fun getMetaClass(): MetaClass {
        return metaClass
    }
}