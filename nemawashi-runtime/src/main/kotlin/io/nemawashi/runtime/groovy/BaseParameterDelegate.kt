/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime.groovy

import groovy.lang.GroovyInterceptable
import io.nemawashi.api.ComponentDefinitionException
import io.nemawashi.api.Type
import io.nemawashi.api.descriptor.ParameterDescriptor
import io.nemawashi.util.transformation.TransformerService
import java.util.*

open class BaseParameterDelegate() : GroovyMapDelegate(), GroovyInterceptable {


    constructor(description: String) : this() {
        map["description"] = description
    }


    override fun invokeMethod(methodName: String?, parameters: Any?): Any {
        val argsList = Arrays.asList(parameters as Array<Any>)
        val arg = (argsList[0] as Array<Any>)[0]
        map[methodName!!] = arg
        return Unit
    }

    override fun setProperty(property: String?, index: Any?) {
        throw UnsupportedOperationException("no indexed properties supported")
    }


    override fun getProperty(property: String?): Any {
        return map[property] ?: throw ComponentDefinitionException(property!!, "Does not exist")
    }


    @Suppress("UNCHECKED_CAST")
    fun parameterDescriptor(name: String) =
            ParameterDescriptor(
                    name = name,
                    description = map["description"]?.toString()
                            ?: throw ComponentDefinitionException(name, "No description defined on parameter '$name'"),
                    type = map["type"] as Type? ?: Type.string,
                    required = map["required"] as Boolean? ?: true,
                    alias = map["alias"]?.toString() ?: "",
                    allowedValues = TransformerService.convertWithNull<Array<String>>(map["allowedValues"]) ?: emptyArray(),
                    regex = map["regex"]?.toString() ?: "",
                    default = map["defaultValue"]?.toString() ?: ""
            )


}