/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime.groovy

import groovy.lang.Closure
import io.nemawashi.api.ComponentDefinitionException
import io.nemawashi.api.NemawashiModule
import io.nemawashi.api.Type
import io.nemawashi.api.descriptor.TaskDescriptor
import io.nemawashi.runtime.dynamic.DynamicTaskProvider
import java.util.*

open class BaseTaskDescriptorDelegate(
        var name: String? = null,
        var returnDescription: String? = null,
        var description: String? = null,
        var parameters: BaseParametersDelegate? = null,
        var script: Closure<Any?>? = null,
        var before: Closure<Any?>? = null,
        var after: Closure<Any?>? = null,
        var onFinally: Closure<Any?>? = null,
        var onError: Closure<Any?>? = null,
        var returnType: Type = Type.unit
) : BaseGroovyInterceptable() {

    val isFunctionalTask: Boolean get() = script != null


    override fun setProperty(param: String?, value: Any?) {
    }

    override fun getProperty(prop: String?): Any {
        return Unit
    }

    @Suppress("UNCHECKED_CAST")
    override fun invokeMethod(nameS: String?, argsList: Any?): Any {
        val name = nameS!!
        val args = Arrays.asList(argsList!! as Array<Any>)
        val arg = (args[0] as Array<Any>)[0]
        when (name) {
            "parameters" -> {
                val delegate = BaseParametersDelegate()
                val closure = arg as Closure<Any?>
                closure.resolveStrategy = Closure.DELEGATE_FIRST
                closure.delegate = delegate
                closure.call()
                parameters = delegate
            }
            "name" -> true
            "returnDescription" -> this.returnDescription = arg.toString()
            "returnType" -> this.returnType = arg as Type
            "description" -> this.description = arg.toString()
            "script" -> this.script = arg as Closure<Any?>
            "before" -> this.before = arg as Closure<Any?>
            "after" -> this.after = arg as Closure<Any?>
            "onFinally" -> this.onFinally = arg as Closure<Any?>
            "onError" -> this.onError = arg as Closure<Any?>
            else -> throw ComponentDefinitionException(name, "Not a recognized configuration parameter")
        }
        return Unit
    }


    fun taskDescriptor(module: NemawashiModule, factory: DynamicTaskProvider): TaskDescriptor {
        val returnDescriptor = returnDescription ?: "No return value specified"
        return TaskDescriptor(
                module,
                name ?: throw ComponentDefinitionException("${module.id}::$name", "No name defined on task"),
                description
                        ?: throw ComponentDefinitionException("${module.id}::$name", "no description defined on task"),
                parameters?.parameters() ?: emptyMap(),
                factory,
                !isFunctionalTask,
                returnType to returnDescriptor
        )
    }


}