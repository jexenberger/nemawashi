package io.nemawashi.runtime

import io.nemawashi.api.CategorisedResource
import io.nemawashi.api.IdStoreException
import io.nemawashi.api.resources.CategorisedType
import java.io.File
import java.io.OutputStream
import java.io.OutputStreamWriter

abstract class BaseCategorisedResourceStore<T>(databaseName: String, basePath: File) where T : CategorisedResource {

    protected val fullPath: File

    init {
        if (!basePath.exists()) {
            basePath.mkdirs()
        }
        if (!basePath.isDirectory) {
            throw IdStoreException(databaseName, "'${basePath.absolutePath}' does not exist or is not a directory")
        }
        fullPath = File(basePath, databaseName)
        if (fullPath.exists() && !fullPath.isDirectory) {
            throw IdStoreException(databaseName, "'${basePath.absolutePath}' already exists, unable to create resource database")
        }
    }

    fun get(type: CategorisedType, id: String): T? {
        val typePath = getPath(type)
        if (!typePath.exists()) {
            return null
        }
        val idPath = File(typePath, id)
        if (!idPath.exists()) {
            return null
        }
        if (idPath.isDirectory) {
            return null
        }
        return loadResource(idPath, type, id)
    }

    abstract fun loadResource(idPath: File, type: CategorisedType, id: String): T

    fun save(resource: T) {
        val typePath = getPath(resource.type)
        if (!typePath.exists()) {
            typePath.mkdirs()
        }
        val idPath = File(typePath, resource.id)
        val bufferedWriter = idPath.outputStream()
        try {
            writeType(bufferedWriter, resource)
            bufferedWriter.flush()
        } finally {
            bufferedWriter.close()
        }
    }


    fun delete(type: CategorisedType, id: String): Boolean {
        val typePath = getPath(type)
        if (!typePath.exists()) {
            return false
        }
        val idPath = File(typePath, id)
        if (!idPath.exists()) {
            return false
        }
        if (idPath.isDirectory) {
            return false
        }
        //dont know why this is not working on my windows build
        return File(idPath.absolutePath).delete()
    }

    internal fun getPath(type: CategorisedType) = File(fullPath, type.pathSnippet.toString())

    abstract fun writeType(bufferedWriter: OutputStream, resource: T)
    fun find(type: String): Collection<String> {
        val file = File(fullPath, type)
        if (!file.isDirectory) {
            return emptyList()
        }
        return file.list().toList()
    }
}