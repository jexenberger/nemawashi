/*
 *  Copyright 2018 Julian Exenberger
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package io.nemawashi.runtime.yaml.snakeyaml

import io.nemawashi.util.crypto.Secret
import org.yaml.snakeyaml.nodes.Node
import org.yaml.snakeyaml.nodes.Tag
import org.yaml.snakeyaml.representer.Represent
import org.yaml.snakeyaml.representer.Representer
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime

class ExtTypeRepresenters : Representer() {
    init {
        this.representers.put(LocalDateTime::class.java, RepresentLocalDateTime())
        this.representers.put(LocalDate::class.java, RepresentLocalDate())
        this.representers.put(LocalTime::class.java, RepresentLocalTime())
        this.representers.put(Secret::class.java, RepresentSecret())
    }

    private inner class RepresentLocalDateTime : Represent {
        override fun representData(data: Any): Node {
            val localDateTime = data as LocalDateTime
            val value = localDateTime.toString()
            return representScalar(Tag("!datetime"), value)
        }
    }

    private inner class RepresentLocalDate : Represent {
        override fun representData(data: Any): Node {
            val localDateTime = data as LocalDate
            val value = localDateTime.toString()
            return representScalar(Tag("!date"), value)
        }
    }

    private inner class RepresentLocalTime : Represent {
        override fun representData(data: Any): Node {
            val localDateTime = data as LocalDate
            val value = localDateTime.toString()
            return representScalar(Tag("!time"), value)
        }
    }

    private inner class RepresentSecret : Represent {
        override fun representData(data: Any): Node {
            val secret = data as Secret
            val value = secret.cipherTextBase64
            return representScalar(Tag("!secret"), value)
        }
    }

}