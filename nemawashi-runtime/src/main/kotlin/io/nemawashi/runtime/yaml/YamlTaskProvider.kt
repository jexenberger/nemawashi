/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime.yaml

import io.nemawashi.api.Task
import io.nemawashi.api.TaskDoesNotExistException
import io.nemawashi.api.TaskId
import io.nemawashi.api.TaskType
import io.nemawashi.api.descriptor.TaskDescriptor
import io.nemawashi.di.api.ComponentId
import io.nemawashi.di.api.Context
import io.nemawashi.runtime.TaskContextImpl
import io.nemawashi.runtime.dynamic.BaseDynamicModule
import io.nemawashi.runtime.dynamic.CompositeTask
import io.nemawashi.runtime.dynamic.CompositeTaskTemplate
import io.nemawashi.runtime.dynamic.DynamicTaskProvider
import io.nemawashi.util.log.LogProvider
import io.nemawashi.util.string.asString
import java.util.*
import javax.activation.DataSource

class YamlTaskProvider(source: DataSource, module: BaseDynamicModule) : DynamicTaskProvider(source, module) {

    override fun get(id: ComponentId, ctx: Context, log: LogProvider): Task {
        return getCompositeTask(id as TaskId, ctx as TaskContextImpl)
    }

    override fun createDescriptor(): TaskDescriptor {
        return YamlTaskBuilder(source.name, module, source.asString()).taskDescriptor(this)
    }

    fun getCompositeTask(id: TaskId, ctx: TaskContextImpl): CompositeTask {
        val taskDescriptor = module.getByName(id.taskType.name)
        taskDescriptor ?: throw TaskDoesNotExistException(id.taskType)
        val name = id.taskType.name
        val newContext = createDynamicTaskContext(ctx)
        val task = compositeTaskTemplates.getOrPut(id.taskType) {
            val template = CompositeTaskTemplate(id.taskType)
            YamlTaskBuilder(name, module, source.asString()).defineTaskTree(template)
            template
        }
        return task.toCompositeTask(id, taskDescriptor, newContext, module)
    }

    companion object {
        val compositeTaskTemplates: MutableMap<TaskType, CompositeTaskTemplate> = Collections.synchronizedMap(mutableMapOf())
    }


}