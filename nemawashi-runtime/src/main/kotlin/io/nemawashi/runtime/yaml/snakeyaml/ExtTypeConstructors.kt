/*
 *  Copyright 2018 Julian Exenberger
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package io.nemawashi.runtime.yaml.snakeyaml

import io.nemawashi.util.crypto.Secret
import org.yaml.snakeyaml.constructor.AbstractConstruct
import org.yaml.snakeyaml.constructor.Constructor
import org.yaml.snakeyaml.nodes.Node
import org.yaml.snakeyaml.nodes.ScalarNode
import org.yaml.snakeyaml.nodes.Tag
import java.time.LocalDateTime
import java.time.LocalTime


class ExtTypeConstructors : Constructor() {

    init {
        this.yamlConstructors.put(Tag("!datetime"), ConstructLocalDateTime())
        this.yamlConstructors.put(Tag("!date"), ConstructLocalDate())
        this.yamlConstructors.put(Tag("!time"), ConstructLocalTime())
        this.yamlConstructors.put(Tag("!secret"), ConstructSecret())
    }


    private inner class ConstructLocalDateTime : AbstractConstruct() {
        override fun construct(node: Node): Any {
            if (node is ScalarNode) {
                val value = constructScalar(node) as String
                return LocalDateTime.parse(value)
            } else {
                throw RuntimeException("node not instance of Scalar node:" + node::class)
            }
        }
    }

    private inner class ConstructLocalDate : AbstractConstruct() {
        override fun construct(node: Node): Any {
            if (node is ScalarNode) {
                val value = constructScalar(node) as String
                return LocalDateTime.parse(value)
            } else {
                throw RuntimeException("node not instance of Scalar node:" + node::class)
            }
        }
    }

    private inner class ConstructLocalTime : AbstractConstruct() {
        override fun construct(node: Node): Any {
            if (node is ScalarNode) {
                val value = constructScalar(node) as String
                return LocalTime.parse(value)
            } else {
                throw RuntimeException("node not instance of Scalar node:" + node::class)
            }
        }
    }

    private inner class ConstructSecret : AbstractConstruct() {
        override fun construct(node: Node): Any {
            if (node is ScalarNode) {
                val value = constructScalar(node) as String
                return Secret.fromBase64(value)
            } else {
                throw java.lang.RuntimeException("node not instance of Scalar node:" + node::class)
            }
        }
    }
}