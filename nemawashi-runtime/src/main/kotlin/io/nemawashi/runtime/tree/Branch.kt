/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime.tree

import io.nemawashi.api.Directive
import io.nemawashi.util.ThreadLocalHandler
import io.nemawashi.util.system
import java.util.concurrent.Callable
import java.util.concurrent.ExecutionException
import java.util.concurrent.Future

abstract class Branch<T>(
        override val id: String,
        val parallel: Boolean = true) : Node<T> where T : ParallelContext<T> {

    val children: List<Node<T>> get() = _childList

    private val _childList: MutableList<Node<T>> = mutableListOf()
    private var internalState: NodeState = NodeState.unrun

    override val state: NodeState
        get() = synchronized(this) { internalState }


    fun add(child: Node<T>) = _childList.add(child)

    @Synchronized
    operator fun plusAssign(child: Node<T>) {
        add(child)
    }

    @Synchronized
    operator fun plus(child: Node<T>): Branch<T> {
        add(child)
        return this
    }

    override fun nodeById(id: String): Node<T>? {
        return super.nodeById(id)?.let { it } ?: run {
            var found: Node<T>? = null
            val children = this.children.iterator()
            while (children.hasNext() && found == null) {
                found = children.next().nodeById(id)
            }
            return found
        }
    }


    fun runBody(ctx: T): NodeState {
        var doAgain = true
        while (doAgain) {
            val pre = preTraversal(ctx)
            if (pre != Directive.continueExecution) {
                return NodeState.completed
            }
            //val subContext = ctx.subContext(clone = false)
            //runChildren(subContext)
            runChildren(ctx)
            val post = postTraversal(ctx)
            if (post != Directive.again) {
                doAgain = false
            }
        }
        return NodeState.completed
    }


    override fun execute(ctx: T): NodeState {
        internalState = NodeState.running
        try {
            enterBranch(ctx)
            internalState = runBody(ctx)
        } catch (e: Exception) {
            internalState = NodeState.failed
            val ex = if (e is ExecutionException) e.cause as Exception else e
            onError(ex, ctx)
        } finally {
            exitBranch(ctx)
        }
        return internalState
    }

    protected open fun runChildren(ctx: T) {
        if (parallel) {
            val futures = mutableListOf<Future<*>>()
            for (node in _childList) {
                futures += system.optimizedExecutor.submit(Callable {
                    ThreadLocalHandler.doWithValue(ctx) {
                        runLeafNode(node, it)
                    }!!
                })
            }
            futures.forEach { it.get() }
            return
        }
        for (node in _childList) {
            runLeafNode(node, ctx)
        }
    }

    private fun runLeafNode(node: Node<T>, ctx: T): Node<T> {
        node.execute(ctx)
        return node
    }

    override fun dump(level: Int) {
        for (i in 0..level) {
            print(" ")
        }
        println(id)
        children.forEach { it.dump(level + 1) }
    }

    override fun traverse(handler: (Node<T>, Node<T>?) -> Unit) {
        handler(this, null)
        children.forEach { handler(it, this) }
    }

    abstract fun preTraversal(ctx: T): Directive
    abstract fun postTraversal(ctx: T): Directive
    abstract fun onError(error: Exception, ctx: T)
    abstract fun enterBranch(ctx: T)
    abstract fun exitBranch(ctx: T)


}