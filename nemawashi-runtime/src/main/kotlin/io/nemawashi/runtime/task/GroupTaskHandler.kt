/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime.task

import io.nemawashi.api.*
import io.nemawashi.api.events.*
import io.nemawashi.api.metamodel.GroupTaskDef
import io.nemawashi.runtime.TaskContextImpl
import io.nemawashi.runtime.container.TaskScope
import io.nemawashi.runtime.container.TaskScopeId
import io.nemawashi.runtime.tree.Branch
import io.nemawashi.util.Timer
import io.nemawashi.util.ifTrue

class GroupTaskHandler(private val taskId: TaskId,
                       parallel: Boolean,
                       val taskDef: GroupTaskDef)
    : Branch<TaskContextImpl>(taskId.toString(), parallel) {


    private val timer: Timer by lazy { Timer() }

    private fun getTask(ctx: TaskContextImpl) = ctx.get<GroupTask>(taskId)
            ?: throw TaskFailedException(taskId, "not defined")


    override fun preTraversal(ctx: TaskContextImpl): Directive {

        return taskDef.condition(ctx.bindings)
                .ifTrue {
                    //init the timer
                    timer
                    val result = getTask(ctx).before(taskId, ctx.bindings)
                    fireEvent(ctx, BeforeGroupTaskEvent(taskId, result.name, result.state), result)
                    result

                } ?: fireEvent(ctx, TaskSkippedEvent(taskId, "Task condition evaluated to false"), Directive.abort)
    }

    private fun fireEvent(ctx: TaskContextImpl, event: NemawashiEvent, directive: Directive): Directive {
        ctx.events.publish(event)
        return directive
    }

    override fun postTraversal(ctx: TaskContextImpl): Directive {
        val result = getTask(ctx).after(taskId, ctx.bindings)
        return fireEvent(ctx, AfterTaskEvent(taskId, result.name, result.state, timer.currentTimeTaken), result)
    }


    override fun onError(error: Exception, ctx: TaskContextImpl) {
        val taskError = TaskError(taskId, error, ctx.bindings)
        ctx.bindings["_error_"] = taskError
        fireEvent(ctx, TaskErrorEvent(taskId, taskError), Directive.done)
        getTask(ctx).onError(taskId, taskError, ctx.bindings)
    }

    override fun enterBranch(ctx: TaskContextImpl) {
    }

    override fun exitBranch(ctx: TaskContextImpl) {
        try {
            getTask(ctx).onFinally(taskId, ctx.bindings)
        } finally {
            TaskScope.release(TaskScopeId(ctx, taskId))
        }
    }

    override fun toString(): String {
        return "GroupTask -> $taskId)"
    }

    override fun runChildren(ctx: TaskContextImpl) {
        val subCtx = ctx.subContext(false)
        super.runChildren(subCtx)
    }
}