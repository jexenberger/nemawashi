/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime.task

import io.nemawashi.api.TaskError
import io.nemawashi.api.TaskId
import io.nemawashi.api.events.TaskErrorEvent
import io.nemawashi.api.metamodel.FunctionalTaskDef
import io.nemawashi.api.metamodel.TaskDef
import io.nemawashi.runtime.TaskContextImpl
import io.nemawashi.runtime.container.TaskScope
import io.nemawashi.runtime.container.TaskScopeId
import io.nemawashi.runtime.tree.Node


class NonGroupTaskHandler(taskId: TaskId, taskDef: TaskDef) : BaseLeafTaskHandler(taskId, taskDef) {

    override fun run(ctx: TaskContextImpl) {
        try {
            val result = taskDef.target.run(taskId, taskDef, ctx)
            if (result !is Unit && taskDef is FunctionalTaskDef && taskDef.assign != null) {
                ctx.bindings[taskDef.assign!!] = result
            }
        } finally {
            TaskScope.release(TaskScopeId(ctx, taskId))
        }
    }

    override fun visitWhenError(error: Exception, leaf: Node<TaskContextImpl>, ctx: TaskContextImpl): Exception {
        val taskError = TaskError.bubble(taskId, error, ctx.bindings)
        ctx.events.publish(TaskErrorEvent(taskId, taskError))
        return taskError
    }

    override fun toString(): String {
        return "SimpleTask -> $taskId)"
    }
}