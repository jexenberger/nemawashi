/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime.task

import io.nemawashi.api.TaskError
import io.nemawashi.api.TaskId
import io.nemawashi.api.events.*
import io.nemawashi.api.metamodel.TaskDef
import io.nemawashi.runtime.TaskContextImpl
import io.nemawashi.runtime.container.TaskScope
import io.nemawashi.runtime.container.TaskScopeId
import io.nemawashi.runtime.tree.Leaf
import io.nemawashi.runtime.tree.Node
import io.nemawashi.runtime.tree.NodeState

abstract class BaseLeafTaskHandler(
        val taskId: TaskId, val taskDef: TaskDef
) : Leaf<TaskContextImpl>(taskId.id) {


    override fun visitSkipped(state: NodeState, leaf: Node<TaskContextImpl>, ctx: TaskContextImpl) {
        ctx.events.publish(TaskSkippedEvent(taskId, "Task condition evaluated to false"))
    }

    override fun visitBeforeLeaf(leaf: Node<TaskContextImpl>, ctx: TaskContextImpl): Boolean {
        ctx.events.publish(BeforeTaskEvent(taskId, "running", TaskState.running))
        val condition = taskDef.condition(ctx.bindings)
        return condition
    }

    override fun visitWhenError(error: Exception, leaf: Node<TaskContextImpl>, ctx: TaskContextImpl): Exception {
        ctx.events.publish(TaskErrorEvent(taskId, TaskError(taskId, error, ctx.bindings)))
        return error
    }

    override fun visitAfterLeaf(state: NodeState, leaf: Node<TaskContextImpl>, ctx: TaskContextImpl, timeTaken: Pair<Long, Unit>) {
        ctx.events.publish(AfterTaskEvent(taskId, "completed", TaskState.completed, timeTaken.first))
    }

    override fun handle(ctx: TaskContextImpl) {
        try {
            TaskDefContext.defn = taskDef
            run(ctx)
        } catch (e: Exception) {
            throw e
        } finally {
            TaskDefContext.clear()
            TaskScope.release(TaskScopeId(ctx, taskId))
        }
    }

    abstract fun run(ctx: TaskContextImpl)


}