package io.nemawashi.runtime.modules.files

import io.nemawashi.api.KotlinModule

class FilesModule : KotlinModule("files","Module to interact with the file service") {

    init {
        add(GetFileReference::class)
        add(SaveFileReference::class)
        add(ListFileReferences::class)
    }

}