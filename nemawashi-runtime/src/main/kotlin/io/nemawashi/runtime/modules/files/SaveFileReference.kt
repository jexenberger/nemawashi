/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime.modules.files

import io.nemawashi.api.*
import io.nemawashi.api.annotations.Parameter
import io.nemawashi.api.annotations.Task
import io.nemawashi.api.resources.CategorisedType
import io.nemawashi.api.services.FileService
import io.nemawashi.di.annotation.Inject
import java.io.ByteArrayInputStream
import java.io.File

@Task(name = "save",
        description = "Returns a reference to a file in file service, either takes a file or an array of bytes to store",
        returnType = Type.fileref,
        returnDescription = "The new created file reference object")
class SaveFileReference(
        @Parameter(description = "id to the resource to define")
        val id: String,
        @Parameter(description = "Name of resource type to define")
        val type: String,
        @Parameter(description = "Reference to a file to copy into the store")
        val file: File?,
        @Parameter(description = "Byte array to store")
        val content: Array<Byte>?,
        @Inject
        val fileService: FileService
) : FunctionalTask<FileRef> {

    override fun evaluate(id: TaskId, ctx: RunContext): FileRef {

        if (file == null && content == null) {
            throw TaskFailedException(id, "Save needs to either have a file reference or an array of bytes to save as content")
        }

        val resourceType = CategorisedType.fromString(type)
        val inputStream = if (file != null) file.inputStream() else ByteArrayInputStream(content?.toByteArray())
        val resource = FileResource(resourceType, this.id, inputStream)
        fileService.save(resource)
        return resource.fileRef
    }
}