/*
 *  Copyright 2018 Julian Exenberger
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package io.nemawashi.runtime.modules.system

import io.nemawashi.api.*
import io.nemawashi.api.annotations.Parameter
import io.nemawashi.api.annotations.Task

@Task(name = "set", description = "Displays a value to the system console", returnType = Type.unit)
class Set(
        @Parameter(description = "value to set for the against the name in the variable", required = false)
        val value: Any?,
        @Parameter(description = "value to set for the against the name in the variable")
        val variable: String,
        @Parameter(description = "Sets the value in a parent scope", default = "false")
        val parent: Boolean = false
) : SimpleTask{

    override fun run(id: TaskId, ctx: RunContext) {
        if (parent) {
            (ctx["_parent"] as RunContext?
                    ?: throw TaskFailedException(id, "configured to set parent but not defined"))[variable] = value
            return
        }
        ctx[variable] = value
    }
}