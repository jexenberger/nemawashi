/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime.modules.system

import io.nemawashi.api.*
import io.nemawashi.api.annotations.Parameter
import io.nemawashi.api.annotations.Task
import io.nemawashi.runtime.TaskContextImpl
import io.nemawashi.util.resolveConstructorForArgs

@Task(name = "new-instance", description = "Creates a new instance of a class, can be used to call classes loaded via the 'jar' task", returnType = Type.any)
class NewInstance(
        @Parameter(description = "Fully qualified class name of the desired type to create a new instance of")
        val type: String,
        @Parameter(description = "Array of values to pass as a constructor, ")
        val args: Array<Any?> = emptyArray()

) : FunctionalTask<Any> {
    override fun evaluate(id: TaskId, ctx: RunContext): Any? {
        val taskContext = (ctx["_ctx"] as TaskContextImpl)
        val type = Class.forName(type, true, taskContext.classLoader)

        val constructor = type.resolveConstructorForArgs(args)
                ?: throw TaskFailedException(id, "unable to resolve constructor for args: ${args.map {
                    it?.let { it::class.java.name } ?: "null"
                }}")

        return if (constructor.parameterCount > 0) constructor.newInstance(*args) else constructor.newInstance()
    }
}