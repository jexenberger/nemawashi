/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime.modules.templating

import io.nemawashi.api.FunctionalTask
import io.nemawashi.api.RunContext
import io.nemawashi.api.TaskId
import io.nemawashi.api.Type
import io.nemawashi.api.annotations.Parameter
import io.nemawashi.api.annotations.Task
import io.nemawashi.api.services.TemplateService
import io.nemawashi.di.annotation.Inject
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.File

@Task(name = "generate",
        description = "Generates output from a MVEL template from a file or string template",
        returnDescription = "The result of the evaluation of the MVEL template",
        returnType = Type.string)
class Generate(
        @Parameter(description = "File or template content")
        val template: String,
        @Parameter(description = "Parameters to generate the template with")
        val parameters: Map<String, Any?> = mutableMapOf(),
        @Parameter(description = "Inject all parameters into the context")
        val injectContext: Boolean = false,
        @Inject
        val templateService: TemplateService
) : FunctionalTask<String> {

    override fun evaluate(id: TaskId, ctx: RunContext): String? {
        val file = File(template)
        val buffer = ByteArrayOutputStream()
        val reader = if (file.isFile) {
            file.inputStream()
        } else {
            ByteArrayInputStream(template.toByteArray())
        }
        val params = mutableMapOf<String, Any?>()
        params.putAll(parameters)
        if (injectContext) {
            ctx.keys.forEach { k -> params[k] = ctx[k] }
        }
        templateService.write(reader, buffer, params)
        return String(buffer.toByteArray())
    }

}