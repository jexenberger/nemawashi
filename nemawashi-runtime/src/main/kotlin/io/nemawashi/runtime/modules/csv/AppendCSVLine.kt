/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime.modules.csv

import io.nemawashi.api.RunContext
import io.nemawashi.api.SimpleTask
import io.nemawashi.api.TaskId
import io.nemawashi.api.Type
import io.nemawashi.api.annotations.Parameter
import io.nemawashi.api.annotations.Task
import io.nemawashi.util.csv.CSVLine

import java.io.File

@Task("append", "Appends a line to a CSV file", returnType = Type.unit)
class AppendCSVLine(
        @Parameter(description = "path to CSV file to append to")
        val file: File,
        @Parameter(description = "Array of values to append to the specified file")
        val line:Array<String>,
        @Parameter(description = "Character to use to concatenate the line", default = CSVLine.defaultSeperator.toString())
        val separator:Char = CSVLine.defaultSeperator,
        @Parameter(description = "Character to use to wrap the value in row, not wrapped if not specified", required = false)
        val quoteCharacter:Char? = null) : SimpleTask {

    override fun run(id: TaskId, ctx: RunContext) {
        val line = CSVLine(line.toList(), separator, quoteCharacter)
        file.appendText("${line.toLine()}\n")
    }

}