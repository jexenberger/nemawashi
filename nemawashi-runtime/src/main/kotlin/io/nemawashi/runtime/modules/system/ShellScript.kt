/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime.modules.system

import io.nemawashi.api.FunctionalTask
import io.nemawashi.api.RunContext
import io.nemawashi.api.TaskId
import io.nemawashi.api.Type
import io.nemawashi.api.annotations.Parameter
import io.nemawashi.api.annotations.Task
import io.nemawashi.util.system

@Task(name = "shell-script", description = "Run a list of commands, returns a keyValue with 'success' to indicate the result and and int array of 'processResults for the result of each command'", returnType = Type.keyValue)
class ShellScript(
        @Parameter(description = "List of commands to execute")
        val script: Array<String>,
        @Parameter(description = "Carry on executing if a command fails, default is false", default = "false")
        val resume: Boolean = false,
        @Parameter(description = "directory to evaluate the command in, default is pwd")
        val dir: String = system.pwd
) : FunctionalTask<Map<String, *>> {
    override fun evaluate(id: TaskId, ctx: RunContext): Map<String, *>? {

        val resultList = mutableListOf<Int>()
        var success = true

        for (s in script) {
            val result = system.shell(s, dir)
            resultList += result.first
            val resultSuccess = result.first == 0
            if (!resultSuccess && !resume) {
                return mapOf(
                        "success" to false,
                        "processResults" to resultList.toIntArray()
                )
            }
            success = success and resultSuccess
        }
        return mapOf(
                "success" to success,
                "processResults" to resultList.toIntArray()
        )
    }
}