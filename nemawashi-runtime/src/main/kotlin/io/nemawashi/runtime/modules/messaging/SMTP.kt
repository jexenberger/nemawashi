/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime.modules.messaging

import io.nemawashi.api.RunContext
import io.nemawashi.api.SimpleTask
import io.nemawashi.api.TaskId
import io.nemawashi.api.Type
import io.nemawashi.api.annotations.Parameter
import io.nemawashi.api.annotations.Task
import io.nemawashi.util.crypto.Secret
import io.nemawashi.util.string.asString
import java.util.*
import javax.activation.DataSource
import javax.mail.Session
import javax.mail.internet.InternetAddress
import javax.mail.internet.MimeMessage


@Task(name = "smtp", description = "Send an e-mail via SMTP", returnType = Type.unit)
class SMTP(
        @Parameter(description = "SMTP service host")
        val host: String,
        @Parameter(description = "Message content to send")
        val message: DataSource,
        @Parameter(description = "Subject to send")
        val subject: String,
        @Parameter(description = "From Address")
        val from: String,
        @Parameter(description = "E-mail destination")
        val to: Array<String>,
        @Parameter(description = "User to use to connect to the SMTP service")
        val user: String? = null,
        @Parameter(description = "Password to use to connect to the SMTP service")
        val password: Secret? = null,
        @Parameter(description = "SMTP service port", default = "25")
        val port: Int = 25,
        @Parameter(description = "Enable start TLS", default = "false")
        val startTLSEnable: Boolean = false,
        @Parameter(description = "Additional properties to use to configure transport layer", required = false)
        val additionalProperties: Map<String, Any?> = emptyMap()

) : SimpleTask {
    override fun run(id: TaskId, ctx: RunContext) {
        val prop = Properties()
        val auth = user != null
        prop["mail.smtp.auth"] = auth
        prop["mail.smtp.starttls.enable"] = startTLSEnable
        additionalProperties.forEach { key, value ->
            prop[key] = value?.toString()
        }
        val session = Session.getDefaultInstance(prop)
        val transport = session.getTransport("smtp")
        val message = MimeMessage(session)
        message.setFrom(InternetAddress(from))
        val toAddresses = to.map { InternetAddress(it) }.toTypedArray()
        message.subject = subject
        message.setContent(this.message.asString(), this.message.contentType)
        try {
            transport.connect(host, port, user, password?.value)
            transport.sendMessage(message, toAddresses)
        } finally {
            transport.close()
        }
    }
}