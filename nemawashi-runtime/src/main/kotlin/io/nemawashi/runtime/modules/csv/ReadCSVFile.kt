/*
 *  Copyright 2018 Julian Exenberger
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package io.nemawashi.runtime.modules.csv

import io.nemawashi.api.*
import io.nemawashi.api.annotations.Parameter
import io.nemawashi.api.annotations.Task
import io.nemawashi.api.annotations.TaskContext
import io.nemawashi.util.csv.CSVLine
import java.io.BufferedReader

import java.io.File

@Task("read", "Reads each line of a CSV file, outputting each line as a keyValue", returnType = Type.unit)
class ReadCSVFile(
        @Parameter(description = "path to CSV file to parse")
        val file: File,
        @Parameter(description = "name of variable to write each line as as keyValue key with the headers, if no headers are specified the position is used as a key")
        val outputVariable: String = "__csv",
        @Parameter(description = "use the first line of the file to to define headers")
        val useFirstLineAsHeader: Boolean = false,
        @Parameter(description = "headers in order of columns in the file", required = false)
        val headers: Array<String> = emptyArray(),
        @TaskContext
        val context: ReadCSVFileContext
) : GroupTask {

    override fun before(id: TaskId, ctx: RunContext): Directive {
        loadFile(id)
        if (context.firstTime) {
            context.csvFile = loadFile(id)
            context.firstTime = false
            val line = context.csvFile?.readLine() ?: return Directive.done
            if (useFirstLineAsHeader) {
                val headers = CSVLine.parseLine(line)
                return readNextLine(headers, ctx)
            }
            val csv = CSVLine(line)
            val headers = if (this.headers.isEmpty()) (0..csv.line.size).map { it.toString() }.toTypedArray() else headers
            context.headers = headers.toList()
            ctx[outputVariable] = csv.mapTo(headers)
            return Directive.continueExecution
        } else {
            return readNextLine(context.headers, ctx)
        }
    }

    private fun readNextLine(headers: List<String>, ctx: RunContext): Directive {
        val nextLine = context.csvFile?.readLine() ?: return Directive.done
        val csvRecord = CSVLine(nextLine).mapTo(headers.toTypedArray())
        ctx[outputVariable] = csvRecord
        context.headers = headers
        return Directive.continueExecution
    }


    private fun loadFile(id: TaskId): BufferedReader {
        if (!file.isFile) {
            throw TaskFailedException(id, "${file.absolutePath} does not exist or is a directory")
        }
        return BufferedReader(file.reader())
    }

    override fun after(id: TaskId, ctx: RunContext): Directive {
        return Directive.again
    }

    override fun onFinally(id: TaskId, ctx: RunContext) {
        this.context.csvFile?.close()
    }
}