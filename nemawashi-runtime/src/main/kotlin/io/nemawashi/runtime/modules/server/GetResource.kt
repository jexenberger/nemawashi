/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime.modules.server

import io.nemawashi.api.*
import io.nemawashi.api.annotations.Parameter
import io.nemawashi.api.annotations.Task
import io.nemawashi.api.resources.Resource
import io.nemawashi.api.server.api.RemoteResource
import io.nemawashi.di.annotation.Inject
import io.nemawashi.runtime.server.RemoteServerDefinition
import io.nemawashi.runtime.server.ServerRegistry

@Task(name = "get-resource", description = "Retrieves a resource from a remote service", returnType = Type.unit)
class GetResource(@Inject val registry: ServerRegistry,
                  @Parameter(description = "Server on which to set the resource") val server: String,
                  @Parameter(description = "resource type") val type: String,
                  @Parameter(description = "resource id") val id: String)
    : FunctionalTask<Resource> {
    override fun evaluate(id: TaskId, ctx: RunContext): Resource? {
        val response = RemoteServerDefinition
                .resolve(server, registry)
                .createRequest("resources/${type}/${id}")
                .get()
        if (response.status != 200) {
            throw TaskFailedException(id, "unable to create resource, service failed with -> ${response.status}:${response.responseMessage}")
        }

        val remoteResource = response.bodyToObject<RemoteResource>()
        return remoteResource.toResource()
    }
}