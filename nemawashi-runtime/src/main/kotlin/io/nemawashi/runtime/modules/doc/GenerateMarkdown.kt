/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime.modules.doc

import io.nemawashi.api.*
import io.nemawashi.api.annotations.Parameter
import io.nemawashi.api.annotations.Task
import io.nemawashi.api.services.TemplateService
import io.nemawashi.di.annotation.Inject
import io.nemawashi.doc.ModuleDoc
import io.nemawashi.runtime.ModuleRegistry
import io.nemawashi.util.globMatches
import java.io.File

@Task(name = "generate-markdown", description = "Generates Github style markdown from all the present module to a defined path", returnType = Type.unit)
class GenerateMarkdown(@Inject val templateService: TemplateService,
                       @Parameter(description = "Directory/Folder to generate the output documentation in")
                       val outputPath: File,
                       @Parameter(description = "List of modules for which documentation will be generated, accepts glob patterns")
                       val modules: Array<String>) : SimpleTask {


    override fun run(id: TaskId, ctx: RunContext) {
        if (outputPath.isFile) {
            throw TaskFailedException(id, "${outputPath.absolutePath} already exists and is a file")
        }
        if (modules.isEmpty()) {
            throw TaskFailedException(id, "No modules defined to generate documentation for")
        }
        outputPath.deleteRecursively()
        outputPath.mkdirs()
        val generatedModules = linkedSetOf<ModuleDoc>()
        val matchedModules = ModuleRegistry.modules
                .filter { module -> modules.filter {  module.toString().globMatches(it) }.isNotEmpty() }
        matchedModules
                .forEach {
                    val moduleDoc = ModuleRegistry[it]!!.documentation
                    generatedModules += moduleDoc
                    val modulePath = "${moduleDoc.module.name}/${moduleDoc.module.versionString}"
                    val moduleFullPath = File(outputPath, modulePath)
                    moduleFullPath.mkdirs()
                    generatedModules += moduleDoc
                    val moduleFile = File(moduleFullPath, "module.md")
                    val moduleCtx = mapOf("moduleDoc" to moduleDoc)
                    generateTemplate(moduleFile, moduleCtx, moduleTemplate)
                    val tasks = ModuleRegistry[it]!!.tasks
                    moduleDoc.tasks.forEach {
                        generateItemTemplate(moduleFullPath, it, "tasks", "taskDoc", taskTemplate, it.name)
                    }
                    moduleDoc.functions.forEach {
                        generateItemTemplate(moduleFullPath, it, "functions", "functionDoc", functionTemplate, it.name)
                    }
                }
        val modulesCtx = mapOf("modules" to generatedModules)
        generateTemplate(File(outputPath, "modules.md"), modulesCtx, modulesTemplate)
    }

    private fun generateTemplate(moduleFile: File, moduleCtx: Map<String, *>, template: String) {
        templateService.write(template, moduleFile.outputStream(), moduleCtx)
        //annoying hack to deal with MVEL expressions leaving an empty line
        val txt = moduleFile.readLines().filter { it.isNotBlank() }.joinToString("\n")
        moduleFile.delete()
        moduleFile.writeText(txt)
    }

    private fun generateItemTemplate(moduleFullPath: File, it: Any, subPath: String, varName: String, template: String, itemName: String) {
        val itemSubPath = File(moduleFullPath, subPath)
        if (!itemSubPath.exists()) {
            itemSubPath.mkdirs()
        }
        val taskPath = File(itemSubPath, "$itemName.md")
        val taskCtx = mapOf(varName to it)
        generateTemplate(taskPath, taskCtx, template)
    }

    companion object {
        val taskTemplate by lazy { GenerateMarkdown::class.java.getResourceAsStream("/io/nemawashi/runtime/modules/doc/task.mvel").bufferedReader().readText() }
        val moduleTemplate by lazy { GenerateMarkdown::class.java.getResourceAsStream("/io/nemawashi/runtime/modules/doc/module.mvel").bufferedReader().readText() }
        val functionTemplate by lazy { GenerateMarkdown::class.java.getResourceAsStream("/io/nemawashi/runtime/modules/doc/function.mvel").bufferedReader().readText() }
        val modulesTemplate by lazy { GenerateMarkdown::class.java.getResourceAsStream("/io/nemawashi/runtime/modules/doc/modules.mvel").bufferedReader().readText() }
    }
}