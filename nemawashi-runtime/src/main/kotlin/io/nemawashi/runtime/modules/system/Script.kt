/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime.modules.system

import io.nemawashi.api.FunctionalTask
import io.nemawashi.api.RunContext
import io.nemawashi.api.TaskId
import io.nemawashi.api.Type
import io.nemawashi.api.annotations.Parameter
import io.nemawashi.api.annotations.Task
import io.nemawashi.di.annotation.Inject
import io.nemawashi.util.script.Language
import io.nemawashi.util.script.ScriptService
import javax.activation.DataSource
import javax.script.Bindings

@Task(name = "script", description = "Run a script either directly as text of from a file", returnDescription = "Result of the script", returnType = Type.any)
class Script(
        @Parameter(description = "Script string or File path")
        val script: DataSource,
        @Parameter(description = "Language of the script", default = "groovy")
        val language: Language = Language.groovy,
        @Inject
        val scriptService: ScriptService) : FunctionalTask<Any?> {

    override fun evaluate(id: TaskId, ctx: RunContext): Any? {
        val bindings = ctx as Bindings
        return scriptService.eval(script.inputStream.reader(), bindings, language.engineName)
    }
}