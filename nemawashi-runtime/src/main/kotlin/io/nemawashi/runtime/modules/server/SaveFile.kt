/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime.modules.server

import io.nemawashi.api.*
import io.nemawashi.api.annotations.Parameter
import io.nemawashi.api.annotations.Task
import io.nemawashi.di.annotation.Inject
import io.nemawashi.runtime.server.RemoteServerDefinition
import io.nemawashi.runtime.server.ServerRegistry
import java.io.File

@Task(name = "save-file", description = "Saves a local file to a remote service", returnType = Type.unit)
class SaveFile(@Inject val registry: ServerRegistry,
               @Parameter(description = "Server on which to save the file") val server: String,
               @Parameter(description = "file type") val type: String,
               @Parameter(description = "Path to file on local system") val path: File,
               @Parameter(description = "file id to use, if empty file name will be used") val id: String? = null) : SimpleTask {

    override fun run(id: TaskId, ctx: RunContext) {
        if (!path.exists()) {
            throw TaskFailedException(id, "${path.absolutePath} does not exist")
        }
        if (path.isDirectory) {
            throw TaskFailedException(id, "${path.absolutePath} is a directory")
        }
        val name = this.id ?: path.name
        val response = RemoteServerDefinition
                .resolve(server, registry)
                .createRequest("files/$type/$name")
                .attachment(path, name = "files")
                .put()
        if (response.status != 200) {
            throw TaskFailedException(id, "unable to create resource, service failed with -> ${response.status}:${response.responseMessage}")
        }
    }
}
