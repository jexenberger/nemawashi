/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime.modules.system

import io.nemawashi.api.*
import io.nemawashi.api.annotations.Parameter
import io.nemawashi.api.annotations.Task
import io.nemawashi.api.annotations.TaskContext

@Task(name = "foreach", description = "iterates over any iterable value", returnType = Type.unit)
class ForEach(
        @Parameter(description = "Item collection over which to iterate, works with any Array or Iterable type")
        val items: Iterator<Any>
) : IterableGroupTask<Any>() {

    override fun initIterator(id: TaskId, ctx: RunContext): Iterator<Any> {
        return items
    }


}