/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime.modules.approvals

import io.nemawashi.api.*
import io.nemawashi.api.annotations.Parameter
import io.nemawashi.api.annotations.Task
import io.nemawashi.api.approvals.ApprovalRequest
import io.nemawashi.api.approvals.ApprovalRequests
import io.nemawashi.di.annotation.Inject
import java.time.LocalDateTime

@Task(name = "request", description = "Request an approval", returnType = Type.unit)
class Request(
        @Inject
        val approvalRequests: ApprovalRequests,
        @Inject
        val taskRequest: TaskRequest,
        @Parameter(description = "Headline for the approval request")
        val headline: String,
        @Parameter(description = "Description for the approval request")
        val description: String,
        @Parameter(description = "List of one or more users to assign to the approval")
        val assignedTo: Array<String>,
        @Parameter(description = "Task type to run if the task is approved")
        val approveTask: TaskType,
        @Parameter(description = "Parameters for the approval task")
        val approveTaskParameters: Map<String, Any?>,
        @Parameter(description = "Task type to run if the task is rejected")
        val rejectTask: TaskType,
        @Parameter(description = "Parameters for the rejection task")
        val rejectTaskParameters: Map<String, Any?>,
        @Parameter(description = "Date time on which this approval request expires")
        val expiresOn: LocalDateTime,
        @Parameter(description = "Set of values to display to the user")
        val displayValues: Map<String, String> = emptyMap()

) : SimpleTask {
    override fun run(id: TaskId, ctx: RunContext) {

        val approvalRequest = ApprovalRequest(
                process = taskRequest.process,
                headline = headline,
                description = description,
                assignedTo = assignedTo,
                approveTask = approveTask,
                approveTaskParameters = approveTaskParameters,
                rejectTask = rejectTask,
                rejectTaskParameters = rejectTaskParameters,
                expiresOn = expiresOn
        )
        approvalRequests.request(approvalRequest)

    }
}