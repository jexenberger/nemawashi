/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime.modules.system

import io.nemawashi.api.*
import io.nemawashi.api.annotations.Parameter
import io.nemawashi.api.annotations.Task
import io.nemawashi.api.annotations.TaskContext

@Task(name = "paginate", description = "Breaks an iterator into a subset of items of a a defined size", returnType = Type.unit)
class Paginate(
        @Parameter(description = "Item collection over which to iterate, works with any Array or Iterable type")
        val items: Iterator<*>,
        @Parameter(description = "Variable for the page, default is '__page'", default = "__page")
        val varName: String,
        @Parameter(description = "Size for each page", default = "10")
        val pageSize: Int = 10,
        @TaskContext
        val page: Page
) : GroupTask {

    override fun before(id: TaskId, ctx: RunContext): Directive {
        if (page.iterator == null) {
            page.iterator = items
            page.pageSize = pageSize
        }
        val items = page.nextPage()
        if (items.size > 0) {
            ctx[varName] = items
            return Directive.continueExecution
        }
        return Directive.done
    }

    override fun after(id: TaskId, ctx: RunContext): Directive = Directive.again

    override fun onFinally(id: TaskId, ctx: RunContext) {
        ctx - varName
    }

}