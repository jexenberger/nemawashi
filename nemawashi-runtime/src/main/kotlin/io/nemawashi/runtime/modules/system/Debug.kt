/*
 *  Copyright 2018 Julian Exenberger
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package io.nemawashi.runtime.modules.system

import io.nemawashi.api.NemawashiSettings
import io.nemawashi.api.RunContext
import io.nemawashi.api.TaskId
import io.nemawashi.api.Type
import io.nemawashi.api.annotations.Parameter
import io.nemawashi.api.annotations.Task
import io.nemawashi.di.annotation.Inject

@Task(name = "debug", description = "Displays a value to the system console if debug is enabled", returnType = Type.unit)
class Debug(
        @Parameter(description = "Value to echo to the Console") value: Any?,
        @Parameter(description = "Colour the output, valid options are 'red', 'green' or 'default'", default = "default")  type: Echo.Display,
        @Inject
        val settings:NemawashiSettings = NemawashiSettings()

) : Echo(value, type){

    override fun run(id: TaskId, ctx: RunContext) {
        if (settings.debug) super.run(id, ctx)
    }
}