/*
 *  Copyright 2018 Julian Exenberger
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package io.nemawashi.runtime.modules.resources

import io.nemawashi.api.FunctionalTask
import io.nemawashi.api.RunContext
import io.nemawashi.api.TaskId
import io.nemawashi.api.Type
import io.nemawashi.api.annotations.Parameter
import io.nemawashi.api.annotations.Task
import io.nemawashi.api.resources.Resource
import io.nemawashi.api.resources.ResourceRepository
import io.nemawashi.api.resources.CategorisedType
import io.nemawashi.di.annotation.Inject

@Task(name = "query", description = "Queries the resources repository against a defined set of parameters", returnType = Type.iterator, returnDescription = "A list of resources that match the parameters, if none are found an empty list is returned")
class QueryResources(
        @Parameter(description = "Name of resource type to define")
        val type: String,
        @Parameter(description = "Parameters against which to query")
        val parameters: Map<String, Any?>,
        @Inject
        val registry: ResourceRepository
) : FunctionalTask<Collection<Resource>> {

    override fun evaluate(id: TaskId, ctx: RunContext): Collection<Resource>? {
        val resourceType = CategorisedType.fromString(type)
        return registry.find(resourceType, parameters)
    }
}