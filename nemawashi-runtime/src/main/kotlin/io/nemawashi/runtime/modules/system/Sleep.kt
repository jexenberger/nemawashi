/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime.modules.system

import io.nemawashi.api.RunContext
import io.nemawashi.api.SimpleTask
import io.nemawashi.api.TaskId
import io.nemawashi.api.Type
import io.nemawashi.api.annotations.Parameter
import io.nemawashi.api.annotations.Task

@Task(name = "sleep", description = "Sleeps for a defined set of milliseconds", returnType = Type.unit)
class Sleep(
        @Parameter(description = "Defined set of milliseconds", default = "1000")
        val duration: Long = 1000
) : SimpleTask {
    override fun run(id: TaskId, ctx: RunContext) {
        Thread.sleep(duration)
    }
}