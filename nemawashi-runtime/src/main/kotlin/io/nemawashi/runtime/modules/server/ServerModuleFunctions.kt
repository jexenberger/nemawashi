/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime.modules.server

import io.nemawashi.api.annotations.ModuleFunction
import io.nemawashi.api.annotations.ModuleFunctionParameter
import io.nemawashi.di.annotation.Inject
import io.nemawashi.runtime.server.RemoteServerDefinition
import io.nemawashi.runtime.server.ServerRegistry

class ServerModuleFunctions {

    @Inject
    lateinit var servers: ServerRegistry

    @ModuleFunction("takes an input reference and tries to resolve the service ")
    fun resolveServer(
            @ModuleFunctionParameter("serverRef", "references of the service that you wish to use ")
            serverRef: String
    ): RemoteServerDefinition {
        return RemoteServerDefinition.resolve(serverRef, servers)
    }


}