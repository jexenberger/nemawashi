/*
 *  Copyright 2018 Julian Exenberger
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package io.nemawashi.runtime.modules.resources

import io.nemawashi.api.RunContext
import io.nemawashi.api.SimpleTask
import io.nemawashi.api.TaskId
import io.nemawashi.api.Type
import io.nemawashi.api.annotations.Parameter
import io.nemawashi.api.annotations.Task
import io.nemawashi.api.resources.Resource
import io.nemawashi.api.resources.CategorisedType
import io.nemawashi.api.resources.WritableResourceRepository
import io.nemawashi.di.annotation.Inject


@Task(name = "define", description = "Defines a resource against a specific type", returnType = Type.unit)
class DefineResource(
        @Parameter(description = "id to the resource to define")
        val id: String,
        @Parameter(description = "Name of resource type to define", required = true)
        val type: String,
        @Parameter(description = "Parameters to define the resource")
        val parameters: Map<String, Any?>,
        @Inject
        val registry: WritableResourceRepository

) : SimpleTask {


    override fun run(id: TaskId, ctx: RunContext) {
        val resourceType = CategorisedType.fromString(type)
        val resource = Resource(resourceType, this.id, parameters)
        registry.save(resource)
    }
}