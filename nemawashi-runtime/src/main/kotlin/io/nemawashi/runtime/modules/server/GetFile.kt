/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime.modules.server

import io.nemawashi.api.*
import io.nemawashi.api.annotations.Parameter
import io.nemawashi.api.annotations.Task
import io.nemawashi.api.resources.CategorisedType
import io.nemawashi.di.annotation.Inject
import io.nemawashi.runtime.server.RemoteServerDefinition
import io.nemawashi.runtime.server.ServerRegistry
import java.io.File

@Task(name = "get-file", description = "Retrieves a file from a service and saves it locally, returns null of not found", returnType = Type.fileref)
class GetFile(@Inject val registry: ServerRegistry,
              @Parameter(description = "Server on which to set the resource") val server: String,
              @Parameter(description = "file type") val type: String,
              @Parameter(description = "file id") val id: String,
              @Parameter(description = "Local path to save file, will save the file id as name if file is directory, overwrites if it exists") val path: File) : FunctionalTask<FileResource> {
    override fun evaluate(id: TaskId, ctx: RunContext): FileResource? {
        val pathToUse = determinePath(path, this.id)
        val response = RemoteServerDefinition
                .resolve(server, registry)
                .createRequest("files/$type/${this.id}")
                .outputFile(pathToUse)
                .get()

        return when (response.status) {
            200 -> FileResource(CategorisedType.fromString(type), this.id, pathToUse.inputStream())
            404 -> null
            else -> throw TaskFailedException(id, "unable to create resource, service failed with -> ${response.status}:${response.responseMessage}")
        }
    }

    fun determinePath(path: File, id: String): File {
        if (path.isFile) {
            return path
        }
        if (path.isDirectory) {
            return File(path, id)
        }
        //path doesn't exist
        return path
    }
}