package io.nemawashi.runtime.modules.messaging

import io.nemawashi.api.KotlinModule

class MessagingModule : KotlinModule("messaging", "Module for sending messages") {


    init {
        add(SMTP::class)
    }
}