package io.nemawashi.runtime.modules.files

import io.nemawashi.api.*
import io.nemawashi.api.annotations.Parameter
import io.nemawashi.api.annotations.Task
import io.nemawashi.api.resources.CategorisedType
import io.nemawashi.api.services.FileService
import io.nemawashi.di.annotation.Inject

@Task(name = "list", description = "returns a list of File references for a specified type", returnType = Type.filerefArray)
class ListFileReferences(
        @Parameter(description = "Name of resource type to define")
        val type: String,
        @Inject
        val fileService: FileService
) : FunctionalTask<Array<FileResource>> {

    override fun evaluate(id: TaskId, ctx: RunContext): Array<FileResource> {
        val resourceType = CategorisedType.fromString(type)
        return fileService.list(resourceType).toTypedArray()
    }
}