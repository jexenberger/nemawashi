/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime.modules.server

import io.nemawashi.api.RunContext
import io.nemawashi.api.SimpleTask
import io.nemawashi.api.TaskId
import io.nemawashi.api.Type
import io.nemawashi.api.annotations.Parameter
import io.nemawashi.api.annotations.Task
import io.nemawashi.di.annotation.Inject
import io.nemawashi.runtime.server.RemoteServerDefinition
import io.nemawashi.runtime.server.ServerRegistry

@Task(name = "add-reference", description = "Adds a reference to the local service registry", returnType = Type.unit)
class AddReference(@Inject val registry: ServerRegistry,
                   @Parameter(description = "Name to use for the service reference") val name: String,
                   @Parameter(description = "URL of the service") val url: String,
                   @Parameter(description = "user to use") val user: String,
                   @Parameter(description = "password to use") val password: String,
                   @Parameter(description = "To validate SSL or not", default = "true") val validateSSL: Boolean = true
) : SimpleTask {

    override fun run(id: TaskId, ctx: RunContext) {
        val definition = RemoteServerDefinition(name, url, user, password, validateSSL)
        registry.save(definition)
    }
}