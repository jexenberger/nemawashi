/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime.approvals.yaml

import io.nemawashi.api.Nemawashi
import io.nemawashi.api.NemawashiProcess
import io.nemawashi.api.TaskType
import io.nemawashi.api.approvals.ApprovalRequest
import io.nemawashi.api.approvals.ApprovalRequestState
import io.nemawashi.api.approvals.ApprovalRequests
import io.nemawashi.api.resources.CategorisedType
import io.nemawashi.api.resources.Resource
import io.nemawashi.api.resources.WritableResourceRepository
import io.nemawashi.di.annotation.Inject
import io.nemawashi.runtime.DefaultParameterCallback
import io.nemawashi.runtime.LocalTarget
import java.time.LocalDateTime

class YamlApprovalRequests(
        @Inject
        val resourceRepository: WritableResourceRepository,
        @Inject
        val nemawashi: Nemawashi
) : ApprovalRequests {

    @Synchronized
    override fun findPendingByUser(userId: String): List<ApprovalRequest> {
        return findByUser(userId).filter { ApprovalRequestState.pending == it.state }
    }

    override fun get(id: String): ApprovalRequest? = resolveResource(id)?.let { fromResource(it) }

    override fun request(request: ApprovalRequest) = resourceRepository.save(toResource(request))

    @Synchronized
    override fun approve(id: String): ApprovalRequest? {
        val resource = resolveResource(id)
        return resource?.let {
            return handleStateChange(
                    resource = it,
                    newState = ApprovalRequestState.approved,
                    rejectionReason = null)
        }
    }

    private fun handleStateChange(resource: Resource, newState: ApprovalRequestState, rejectionReason: String?): ApprovalRequest {
        val updated = resource.cloneWith(mapOf(
                "state" to newState,
                "rejectionReason" to rejectionReason
        ))
        val approvalRequest = fromResource(updated)
        approvalRequest.requestByState?.let {
            val execution = nemawashi.runTask(it, DefaultParameterCallback(), LocalTarget)
            if (approvalRequest.blockTillTaskComplete) {
                execution.getResult()
            }
        }
        val newResource = toResource(approvalRequest)
        resourceRepository.save(newResource)
        return approvalRequest
    }

    @Synchronized
    override fun reject(id: String, reason: String): ApprovalRequest? {
        val resource = resolveResource(id)
        return resource?.let {
            return handleStateChange(
                    resource = it,
                    newState = ApprovalRequestState.rejected,
                    rejectionReason = reason)
        }

    }


    private fun resolveResource(id: String) = resourceRepository.get(resourceType, id)

    @Synchronized
    override fun findByUser(userId: String): List<ApprovalRequest> {
        return resourceRepository
                .find(resourceType)
                .map { fromResource(it) }
                .filter { it.assignedTo.contains(userId) }
    }


    internal fun toResource(approvalRequest: ApprovalRequest): Resource {
        return Resource(
                resourceType,
                approvalRequest.id,
                mapOf(
                        "user" to approvalRequest.process.user,
                        "processId" to approvalRequest.process.id,
                        "processDateTimeCreated" to approvalRequest.process.dateTimeCreated,
                        "headline" to approvalRequest.headline,
                        "description" to approvalRequest.description,
                        "assignedTo" to approvalRequest.assignedTo,
                        "approveTask" to approvalRequest.approveTask.taskName,
                        "approveTaskParameters" to approvalRequest.approveTaskParameters,
                        "rejectTask" to approvalRequest.rejectTask.taskName,
                        "rejectTaskParameters" to approvalRequest.rejectTaskParameters,
                        "state" to approvalRequest.state,
                        "rejectionReason" to approvalRequest.rejectionReason,
                        "expiresOn" to approvalRequest.expiresOn,
                        "blockTillTaskComplete" to approvalRequest.blockTillTaskComplete,
                        "dateTimeRequest" to approvalRequest.dateTimeRequest,
                        "displayValues" to approvalRequest.displayValues
                ))
    }

    internal fun fromResource(resource: Resource): ApprovalRequest {
        return ApprovalRequest(
                process = NemawashiProcess(
                        id = resource["processId"] as String,
                        user = resource["user"] as String,
                        dateTimeCreated = resource["processDateTimeCreated"] as LocalDateTime
                ),
                headline = resource["headline"] as String,
                description = resource["description"] as String,
                assignedTo = resource["assignedTo"]?.let { (it as List<String>).toTypedArray() } ?: emptyArray(),
                approveTask = TaskType.fromString(resource["approveTask"]!!.toString()),
                approveTaskParameters = resource["approveTaskParameters"] as Map<String, Any?>,
                rejectTask = TaskType.fromString(resource["rejectTask"]!!.toString()),
                rejectTaskParameters = resource["rejectTaskParameters"] as Map<String, Any?>,
                state = resource["state"] as ApprovalRequestState,
                rejectionReason = resource["rejectionReason"]?.toString(),
                id = resource.id,
                expiresOn = resource["expiresOn"] as LocalDateTime,
                blockTillTaskComplete = resource["blockTillTaskComplete"] as Boolean,
                dateTimeRequest = resource["dateTimeRequest"] as LocalDateTime,
                displayValues = resource["displayValues"] as Map<String, String>? ?: emptyMap()
        )
    }

    companion object {
        val resourceType = CategorisedType("nwInternal", "approvals")
    }


}