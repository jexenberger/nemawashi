/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime

import io.nemawashi.api.*
import io.nemawashi.api.metamodel.TaskDef
import io.nemawashi.di.api.*
import io.nemawashi.di.event.EventDispatcher
import io.nemawashi.runtime.container.ParameterDependency
import io.nemawashi.runtime.container.TaskContextDependency
import io.nemawashi.runtime.container.TaskScopeId
import io.nemawashi.runtime.services.NemawashiScriptingService
import io.nemawashi.runtime.tree.ParallelContext
import io.nemawashi.util.log.JDKLogProvider
import io.nemawashi.util.log.LogProvider
import io.nemawashi.util.script.ScriptService
import io.nemawashi.util.system
import org.xeustechnologies.jcl.JarClassLoader
import java.io.File
import java.time.LocalDateTime

class TaskContextImpl(
        override val taskRequest: TaskRequest,
        override val id: String = system.nextId(),
        override val scriptingService: ScriptService = NemawashiScriptingService(),
        log: LogProvider = JDKLogProvider(),
        ctx: MutableMap<ComponentId, ComponentDefinition<*>> = mutableMapOf(),
        values: MutableMap<String, Any> = mutableMapOf(),
        events: EventDispatcher = EventDispatcher(),
        override val bindings: ScopedDependencyBindings = ScopedDependencyBindings(),
        val taskScope: MutableMap<TaskScopeId, Any?> = mutableMapOf()) : ApplicationContext(ctx, values, log = log, events = events), ParallelContext<TaskContextImpl>, TaskContext {

    override val classLoader: ClassLoader by lazy {
        JarClassLoader(Thread.currentThread().contextClassLoader)
    }

    override val dateTimeCreated = LocalDateTime.now()
    override val process: NemawashiProcess get() = taskRequest.process



    init {
        bindings["_ctx"] = this
        bindings["_process"] = process
        TaskContextImpl += this
        addInstance(scriptingService) withId TypeId(ScriptService::class) into this
    }


    fun registerTask(def: TaskDef, callingModule: NemawashiModule? = null) {
        val descriptor = TaskRegistry.resolve(def.id.taskType, callingModule)
                ?: throw TaskDoesNotExistException(def.id.taskType)
        bind(descriptor.factory) withId def.id toScope ScopeType.prototype.name into this
        addInstance(def) withId io.nemawashi.runtime.TaskDefId(def.id) into this
    }


    @Synchronized
    override fun subContext(cloneBindings: Boolean): TaskContextImpl {

        val bindings = if (cloneBindings) this.bindings.clone() else this.bindings.subBindings()

        return TaskContextImpl(
                taskRequest = this.taskRequest,
                ctx = ctx,
                bindings = bindings,
                values = values,
                events = events,
                log = this.log,
                taskScope = this.taskScope
        )
    }


    @Synchronized
    override fun cloneWithoutBindings() = TaskContextImpl(
            taskRequest = this.taskRequest,
            ctx = ctx,
            values = values,
            events = events,
            log = this.log,
            taskScope = this.taskScope
    )

    @Synchronized
    fun loadClasses(path: File) {
        (this.classLoader as org.xeustechnologies.jcl.JarClassLoader).add(path.absolutePath)
    }



    companion object {

        private val runningContexts: MutableMap<String, TaskContextImpl> = mutableMapOf()
        private val threadContext = ThreadLocal<RunContext>()

        init {
            addDependencyHandler(ParameterDependency())
            addDependencyHandler(TaskContextDependency())
        }

        @Synchronized
        private operator fun plusAssign(ctx: TaskContextImpl) {
            runningContexts[ctx.id] = ctx
        }

        operator fun get(id: String) = runningContexts[id]

        @Synchronized
        fun release(id: String) {
            runningContexts.remove(id)
        }

        fun set(ctx: RunContext) {
            threadContext.set(ctx)
        }

        fun currentContext() = threadContext.get()

        fun unset() {
            threadContext.remove()
        }

        fun <T, K> runWithContext(ctx: RunContext, handler: (RunContext) -> T?): T? {
            try {
                set(ctx)
                return handler(ctx)
            } finally {
                unset()
            }
        }

    }

}