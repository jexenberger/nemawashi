/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime

import io.nemawashi.di.api.bind

class FunctionsObjectProxy(val ctx: TaskContextImpl) : Map<String, Any> {


    override val entries: Set<Map.Entry<String, Any>>
        get() = scriptObjectCache.entries
    override val keys: Set<String>
        get() = scriptObjectCache.keys
    override val size: Int
        get() = scriptObjectCache.size
    override val values: Collection<Any>
        get() = scriptObjectCache.values

    override fun containsKey(key: String): Boolean {
        val found = scriptObjectCache.containsKey(key)

        return if (!found) {
            ModuleRegistry.modules.find { it.name.equals(key) }?.let { true } ?: false
        } else {
            found
        }
    }

    override fun containsValue(value: Any): Boolean {
        return scriptObjectCache.containsValue(value)
    }

    override fun get(key: String): Any? {
        val existing = scriptObjectCache[key]
        if (existing != null) {
            return existing
        }
        val module = ModuleRegistry.getLatestVersion(key)
        return module?.let {
            it.scriptObject?.let {
                val id = "_fn.${module.id}"
                bind(it) withId id into ctx
                val scriptObject = ctx.get<Any>(id)!!
                synchronized(scriptObjectCache) {
                    scriptObjectCache[key] = scriptObject
                }
                scriptObject
            }
        }
    }

    override fun isEmpty() = scriptObjectCache.isEmpty()

    companion object {
        val scriptObjectCache = mutableMapOf<String, Any>()
    }
}