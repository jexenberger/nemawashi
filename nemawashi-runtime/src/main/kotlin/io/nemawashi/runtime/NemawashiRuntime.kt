/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime

import groovy.lang.GString
import io.nemawashi.api.*
import io.nemawashi.api.approvals.ApprovalRequests
import io.nemawashi.api.descriptor.ParameterDescriptor
import io.nemawashi.api.metamodel.ParameterDef
import io.nemawashi.api.resources.ResolvingResourceRegistry
import io.nemawashi.api.resources.ResourceRepository
import io.nemawashi.api.resources.WritableResourceRepository
import io.nemawashi.api.services.FileService
import io.nemawashi.api.services.TemplateService
import io.nemawashi.di.api.addInstance
import io.nemawashi.di.api.addType
import io.nemawashi.di.event.EventHandler
import io.nemawashi.doc.ModuleDoc
import io.nemawashi.runtime.approvals.yaml.YamlApprovalRequests
import io.nemawashi.runtime.dynamic.BaseDynamicModule
import io.nemawashi.runtime.dynamic.SingleFileModule
import io.nemawashi.runtime.files.FileServiceDataSource
import io.nemawashi.runtime.files.FileSystemFileService
import io.nemawashi.runtime.resources.yaml.YamlResourceRepository
import io.nemawashi.runtime.server.ServerRegistry
import io.nemawashi.runtime.server.yaml.YamlServerRegistry
import io.nemawashi.runtime.services.MvelTemplateService
import io.nemawashi.runtime.services.NemawashiScriptingService
import io.nemawashi.util.log.LogProvider
import io.nemawashi.util.script.ScriptService
import io.nemawashi.util.string.SourceFactory
import io.nemawashi.util.system
import io.nemawashi.util.transformation.LambdaTransformer
import io.nemawashi.util.transformation.TransformerService
import org.codehaus.groovy.runtime.GStringImpl
import java.io.File
import java.io.FileNotFoundException
import java.util.*

class NemawashiRuntime(val settings: NemawashiSettings = NemawashiSettings()) : Nemawashi {

    override val fileService: FileService by lazy { FileSystemFileService("files", settings.resourceRepositoryPath) }

    override val runningTasks: Collection<TaskExecution>
        get() = Collections.unmodifiableCollection(runtimeRunningTasks.values)

    override val modules get() = ModuleRegistry.modules

    override val eventHandlers = Collections.synchronizedSet(mutableSetOf<EventHandler<*>>())

    override val resources: WritableResourceRepository  by lazy { ResolvingResourceRegistry(YamlResourceRepository("global", settings.resourceRepositoryPath)) }

    override val approvalRequests: ApprovalRequests by lazy { YamlApprovalRequests(resources, this) }

    private val runLog: LogProvider = settings.runLog
    private val processLog: ProcessLog = settings.processLog
    private val runtimeRunningTasks: MutableMap<String, TaskExecution> = mutableMapOf()

    init {
        TransformerService.addConverter(String::class, TaskType::class, LambdaTransformer<String, TaskType> {
            TaskType.fromString(it)
        })
        TransformerService.addConverter(GString::class, String::class, LambdaTransformer<GString, String> {
            it.toString()
        })
        TransformerService.addConverter(GStringImpl::class, String::class, LambdaTransformer<GStringImpl, String> {
            it.toString()
        })
        TransformerService.addConverter(TaskType::class, String::class, LambdaTransformer<TaskType, String> {
            it.taskName
        })
        TransformerService.addConverter(String::class, FileResource::class, LambdaTransformer<String, FileResource> {
            fileService.get(FileRef.fromString(it))
                    ?: throw FileNotFoundException("Unable to resolve $it in fileService")
        })
        TransformerService.addConverter(FileResource::class, String::class, LambdaTransformer<FileResource, String> {
            it.text
        })
        SourceFactory[Type.fileref.name] = { FileServiceDataSource(it, this.fileService) }

        runLog.traceWithTime("LOAD MODULE REGISTRY") {
            ModuleRegistry.loadDefinedYamlModules(settings.yamlModulePath, runLog)
        }
    }

    override fun supportsDynamicType(file: File) = BaseDynamicModule.supports(file)

    override fun taskDoc(name: TaskType) = this.moduleDoc(name.module)?.taskDoc(name.name)

    override fun shutdown() {
        system.optimizedExecutor.shutdownNow()
    }

    fun resolveParameter(type: ParameterDescriptor, existingParameters: Map<String, Any?>, callback: ParameterCallback): Any? {
        val existing = existingParameters[type.name]
        return existing?.let { it } ?: (callback.capture(type) ?: type.default)
    }

    override fun runTask(request: TaskRequest, callback: ParameterCallback, runTarget: RunTarget): TaskExecution {
        val executionImpl = createTaskExecution(request, callback, runTarget)
        runtimeRunningTasks[executionImpl.executionId] = executionImpl
        return executionImpl.run()
    }

    internal fun createTaskExecution(request: TaskRequest, callback: ParameterCallback, runTarget: RunTarget): TaskExecutionImpl {
        val theModule = ModuleRegistry[request.module]
                ?: throw IllegalArgumentException("${request.module} is not defined")
        val taskDescriptor = theModule[TaskType(theModule.id, request.task)]
                ?: throw IllegalArgumentException("Task '${request.task}' does not exist on Module '${request.module}'")

        val taskParams = if (request.parameters.isEmpty()) {
            taskDescriptor.parameters.map { (k, v) ->
                val paramValue = resolveParameter(v, existingParameters = request.parameters, callback = callback)
                k to ParameterDef(k, paramValue)
            }.toMap()
        } else request.parameters.map { (k, v) -> k to ParameterDef(k, v) }.toMap()
        val id = TaskId(taskDescriptor.type, "<<ROOT>>")
        val streamContext = createContext(request)
        val executionImpl = TaskExecutionImpl(
                runningTaskRegistry = runtimeRunningTasks,
                request = request,
                taskId = id,
                taskParams = taskParams,
                taskDescriptor = taskDescriptor,
                streamContext = streamContext,
                processLog = this.processLog,
                executor = system.optimizedExecutor,
                runTarget = runTarget
        )
        return executionImpl
    }

    fun createContext(taskRequest: TaskRequest): TaskContextImpl {
        val streamContext = TaskContextImpl(
                taskRequest = taskRequest,
                log = runLog,
                ctx = mutableMapOf(),
                values = mutableMapOf()
        )
        addInstance(taskRequest) withId TaskRequest::class into streamContext
        addInstance(settings) withId NemawashiSettings::class into streamContext
        addInstance(resources) withId WritableResourceRepository::class into streamContext
        addInstance(resources) withId ResourceRepository::class into streamContext
        addInstance(fileService) withId FileService::class into streamContext
        addInstance(this) withId Nemawashi::class into streamContext
        addInstance(approvalRequests) withId ApprovalRequests::class into streamContext
        addType<ScriptService>(NemawashiScriptingService::class) withId ScriptService::class into streamContext
        addType<TemplateService>(MvelTemplateService::class) withId TemplateService::class into streamContext
        addType<ServerRegistry>(YamlServerRegistry::class) withId ServerRegistry::class into streamContext
        val functionObjects = FunctionsObjectProxy(streamContext)
        streamContext.bindings["_fn"] = functionObjects
        eventHandlers.forEach {
            streamContext.events.register(it)
        }
        return streamContext
    }


    override fun runTask(module: ModuleId, task: String, parameters: Map<String, Any?>, runTarget: RunTarget): TaskExecution {
        val request = TaskRequest(
                module = module,
                task = task,
                parameters = parameters
        )
        return runTask(request, DefaultParameterCallback(), runTarget)
    }

    override fun runTask(file: File, parameters: Map<String, Any?>, callback: ParameterCallback, runTarget: RunTarget): TaskExecution {
        val module = SingleFileModule(file)
        ModuleRegistry += module
        val descriptor = module.taskDescriptor
        val request = TaskRequest(
                module = module.id,
                task = descriptor.name,
                parameters = parameters
        )
        return runTask(request, callback, runTarget)
    }

    override fun moduleDoc(name: ModuleId): ModuleDoc? {
        return if (name.isDefaultVersion)
            ModuleRegistry.getLatestVersion(name.name)?.documentation
        else
            ModuleRegistry[name]?.documentation
    }

    companion object {
        init {
            TransformerService.addConverter(String::class, FileRef::class, transformer = LambdaTransformer<String, FileRef> {
                FileRef.fromString(it)
            })
            TransformerService.addConverter(FileRef::class, String::class, transformer = LambdaTransformer<FileRef, String> {
                it.toString()
            })
        }
    }
}
