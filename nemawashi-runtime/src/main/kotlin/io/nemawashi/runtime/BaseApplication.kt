/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime

import io.nemawashi.api.Nemawashi
import io.nemawashi.api.NemawashiSettings
import io.nemawashi.di.api.ApplicationContext
import io.nemawashi.di.api.addInstance
import io.nemawashi.di.api.addType
import io.nemawashi.di.event.EventHandler
import io.nemawashi.util.log.LogProvider
import io.nemawashi.util.log.NOOPLogger
import io.nemawashi.util.system
import java.util.concurrent.ExecutorService

abstract class BaseApplication(log: LogProvider = NOOPLogger(), val taskExecutor: ExecutorService = system.optimizedExecutor) : ApplicationContext(log = log) {

    fun initRuntimeContainer(settings: NemawashiSettings, eventHandlers: Set<EventHandler<*>>) {
        addInstance(eventHandlers) withId "eventHandlers" into this
        addInstance(settings) withId NemawashiSettings::class into this
        addType<NemawashiRuntimeFactory>(NemawashiRuntimeFactory::class) withId Nemawashi::class into this
    }
}