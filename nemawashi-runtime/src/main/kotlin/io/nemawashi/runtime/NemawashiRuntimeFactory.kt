/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime

import io.nemawashi.api.Nemawashi
import io.nemawashi.api.NemawashiSettings
import io.nemawashi.di.annotation.Inject
import io.nemawashi.di.annotation.Qualified
import io.nemawashi.di.api.ComponentId
import io.nemawashi.di.api.Context
import io.nemawashi.di.api.Factory
import io.nemawashi.di.event.EventHandler
import io.nemawashi.util.log.LogProvider

class NemawashiRuntimeFactory(
        @Inject val nemawashiSettings: NemawashiSettings,
        @Inject @Qualified("eventHandlers") val eventHandlers: Set<EventHandler<*>>

) : Factory<Nemawashi> {

    override fun get(id: ComponentId, ctx: Context, log: LogProvider): Nemawashi {
        val get = NemawashiRuntime(nemawashiSettings)
        get.eventHandlers.addAll(eventHandlers)
        return get
    }

}