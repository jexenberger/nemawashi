/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime

import io.nemawashi.api.NemawashiSettings
import io.nemawashi.api.RunContext
import io.nemawashi.api.resources.WritableResourceRepository
import io.nemawashi.di.api.DependencyInjectionBindings
import io.nemawashi.di.api.TypeId
import io.nemawashi.runtime.server.ServerRegistry
import io.nemawashi.util.system

class ScopedDependencyBindings(
        m: MutableMap<String, Any?> = mutableMapOf(),
        val parent: ScopedDependencyBindings? = null) : DependencyInjectionBindings(m), RunContext {

    private var ctx: TaskContextImpl? = null


    init {
        init()
    }

    private fun init() {
        put("_os", system)
        put("_env", system.env)
        put("_properties", system.props)
        put("_parent", parent)
    }

    override fun put(key: String, value: Any?) = m.put(key, value)

    override fun <T> getAs(key: String) = get(key)?.let { it as T }

    override fun minus(key: String): Any? {
        return m.remove(key)
    }

    fun subBindings(): ScopedDependencyBindings {
        return ScopedDependencyBindings(mutableMapOf(), this)
    }

    override fun get(key: String): Any? {
        if (systemObjects.containsKey(key) && ctx != null) {
            return ctx?.get(systemObjects[key]!!)
        }
        return m.get(key) ?: parent?.get(key)
    }

    override fun containsValue(value: Any?): Boolean {
        if (!m.containsValue(value) && !systemObjects.containsValue(value)) {
            return parent?.containsValue(value) ?: false
        }
        return true
    }

    override fun containsKey(key: String): Boolean {
        if (!m.containsKey(key) && !systemObjects.containsKey(key)) {
            return parent?.containsKey(key) ?: false
        }
        return true
    }

    @Synchronized
    fun clone(): ScopedDependencyBindings {
        val mutableMap = mutableMapOf<String, Any?>()
        this.keys.forEach {
            mutableMap[it] = this.get(it)
        }
        return ScopedDependencyBindings(mutableMap, null)
    }


    override fun isEmpty(): Boolean {
        return m.isEmpty() && parent?.isEmpty() ?: true
    }

    override val size: Int
        get() = super.size + (parent?.size ?: 0)

    override val entries: MutableSet<MutableMap.MutableEntry<String, Any?>>
        get() {
            val entries = mutableSetOf<MutableMap.MutableEntry<String, Any?>>()
            entries.addAll(parent?.entries ?: emptySet())
            entries.addAll(m.entries)
            return entries
        }
    override val keys: MutableSet<String>
        get() {
            val keys = mutableSetOf<String>()
            keys.addAll(systemObjects.keys)
            keys.addAll(parent?.keys ?: mutableSetOf())
            keys.addAll(super.keys)
            return keys
        }
    override val values: MutableCollection<Any?>
        get() {
            val values = mutableSetOf<Any?>()
            values.addAll(systemObjects.values)
            values.addAll(parent?.values ?: mutableSetOf())
            values.addAll(super.values)
            return values
        }

    companion object {
        private val systemObjects = mapOf(
                "_resources" to TypeId(WritableResourceRepository::class),
                "_settings" to TypeId(NemawashiSettings::class),
                "_servers" to TypeId(ServerRegistry::class)
        )
    }


}