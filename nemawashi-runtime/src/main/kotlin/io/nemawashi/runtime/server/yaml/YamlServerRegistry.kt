/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime.server.yaml

import io.nemawashi.api.resources.WritableResourceRepository
import io.nemawashi.di.annotation.Inject
import io.nemawashi.runtime.server.RemoteServerDefinition
import io.nemawashi.runtime.server.ServerRegistry

class YamlServerRegistry(@Inject val resourceRepository: WritableResourceRepository) : ServerRegistry {

    override fun get(name: String): RemoteServerDefinition? =
            resourceRepository.get(RemoteServerDefinition.resourceType, name)?.let {
                RemoteServerDefinition.fromResource(it)
            }

    override fun save(server: RemoteServerDefinition) {
        resourceRepository.save(server.toResource())
    }
}