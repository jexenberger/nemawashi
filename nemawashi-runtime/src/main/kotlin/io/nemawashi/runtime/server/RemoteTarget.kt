/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime.server

import com.fasterxml.jackson.module.kotlin.readValue
import io.nemawashi.api.*
import io.nemawashi.api.metamodel.TaskDef
import io.nemawashi.api.server.api.RemoteExecution
import io.nemawashi.doc.TaskDoc
import io.nemawashi.util.Json
import io.nemawashi.util.script.ScriptService

class RemoteTarget(val serverRef: String) : RunTarget {


    override val name: String = serverRef

    private fun getServer(registry: ServerRegistry) = RemoteServerDefinition.resolve(serverRef, registry)


    override fun run(taskId: TaskId, taskDef: TaskDef, ctx: TaskContext): Any? {

        val scriptService = ctx.get<ScriptService>(ScriptService::class)
                ?: throw IllegalStateException("No '${ScriptService::class}' registered in context")
        val serverRegistry = ctx.get<ServerRegistry>(ServerRegistry::class)
                ?: throw IllegalStateException("No '${ServerRegistry::class}' registered in context")
        val server = getServer(serverRegistry)
        val parameters = taskDef.parameters.map {
            val value = scriptService.evalIfScript<Any?>(it.value.valueDefn, ctx.bindings, "mvel")
            it.key to value
        }.toMap()
        val taskDoc = retrieveMetaData(server, taskId, taskDef, ctx.bindings)
        return invokeTask(server, ctx, parameters, taskId, taskDef, ctx.bindings)?.let { value ->
            return@let taskDoc.returnType?.let {
                val convert = Type.typeForString(it.first)?.convert<Any?>(value)
                convert ?: throw TaskFailedException(taskId, "$it not recognised return type")
            } ?: Unit
        }
    }


    fun retrieveMetaData(server: RemoteServerDefinition, taskId: TaskId, taskDef: TaskDef, ctx: RunContext): TaskDoc {
        val response = createRequest(server, taskDef, "metadata").get()
        return response.if2xx { status, body -> Json.objectMapper.readValue<TaskDoc>(body) }
                ?: throw TaskFailedException(taskId, if (response.status == 404) "Task ${taskDef.id} not found on service" else "Failed with: ${response.status}:${response.responseMessage}")

    }

    private fun invokeTask(server: RemoteServerDefinition, streamContext: TaskContext, parameters: Map<String, Any?>, taskId: TaskId, taskDef: TaskDef, ctx: RunContext): Any? {
        val remoteExecution = RemoteExecution(
                process = streamContext.process,
                parameters = parameters
        )

        val response = createRequest(server, taskDef, "executions").body(Json.objectMapper.writeValueAsString(remoteExecution)).post()
        when (response.status) {
            201 -> {
                val value = Json.objectMapper.readValue<RemoteExecution>(response.body)
                return value.returnValue
            }
            else -> throw TaskFailedException(taskId, response.responseMessage)
        }
    }

    private fun createRequest(server: RemoteServerDefinition, taskDef: TaskDef, operation: String) =
            server.createRequest(createPath(taskDef, operation))

    private fun createPath(taskDef: TaskDef, operation: String) =
            "tasks/${taskDef.id.taskType.taskName}/$operation"


}