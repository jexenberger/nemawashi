/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime.server

import io.nemawashi.api.resources.Resource
import io.nemawashi.api.resources.CategorisedType
import io.nemawashi.util.rest.Request
import java.net.URL

data class RemoteServerDefinition(val name: String,
                                  val url: String,
                                  val user: String?,
                                  val password: String?,
                                  val validateSSL: Boolean = false) {


    fun toResource() =
            Resource(resourceType, name, mapOf(
                    "name" to name,
                    "url" to url,
                    "user" to user,
                    "password" to password,
                    "validateSSL" to validateSSL
            ))

    fun request(path: String) = createRequest(path, mapOf(), mapOf())

    fun createRequest(path: String, headers: Map<String, String> = mapOf(), queryParams: Map<String, String> = mapOf()): Request {
        val request = Request(
                url = url,
                path = path,
                contentType = "application/json",
                validateHostName = validateSSL,
                validateSSL = validateSSL
        )
        queryParams.forEach { (key, value) ->
            request.parm(key, value)
        }
        headers.forEach { (key, value) ->
            request.header(key, value)
        }
        return applyAuthentication(request)
    }


    fun applyAuthentication(request: Request): Request {
        return if (user != null && password != null)
            request.basicAuth(user, password)
        else
            request
    }


    companion object {

        val resourceType = CategorisedType("nwInternal", "servers")

        fun fromUrl(url: String): RemoteServerDefinition {
            val targetUrl = URL(url)
            val name = targetUrl.host
            val userInfo = targetUrl.userInfo?.let { it.split(":") } ?: emptyList()
            //we only do if there is both a username and password
            val user = if (userInfo.size > 1) userInfo[0] else null
            val password = if (userInfo.size > 1) userInfo[1] else null
            val validateSSL = targetUrl.query?.let { qStr ->
                val split = qStr.split("&")
                if (split.isEmpty()) {
                    return@let true
                }
                val qParms = split.map {
                    val str = it.split("=")
                    str[0] to str[1]
                }.toMap()
                qParms["validateSSL"]?.toBoolean() ?: true
            } ?: true

            val path = if (targetUrl.path.isNotEmpty()) targetUrl.path else ""
            val port = if (targetUrl.port == -1) "" else ":${targetUrl.port}"

            val newUrl = "${targetUrl.protocol}://${targetUrl.host}${port}${path}"
            return RemoteServerDefinition(name, newUrl, user, password, validateSSL)
        }

        fun resolve(ref: String, registry: ServerRegistry) =
                registry.get(ref) ?: RemoteServerDefinition.fromUrl(ref)

        fun fromResource(resource: Resource): RemoteServerDefinition {
            return RemoteServerDefinition(
                    resource.getValue<String>("name")!!,
                    resource.getValue<String>("url")!!,
                    resource.getValue<String>("user"),
                    resource.getValue<String>("password"),
                    resource.getValue<Boolean>("validateSSL")!!
            )
        }
    }

}