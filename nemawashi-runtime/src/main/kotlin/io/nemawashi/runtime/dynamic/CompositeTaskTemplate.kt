/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime.dynamic

import io.nemawashi.api.Directive
import io.nemawashi.api.NemawashiModule
import io.nemawashi.api.TaskId
import io.nemawashi.api.TaskType
import io.nemawashi.api.descriptor.TaskDescriptor
import io.nemawashi.runtime.TaskContextImpl
import io.nemawashi.runtime.task.NonGroupTaskHandler
import io.nemawashi.runtime.tree.Branch

class CompositeTaskTemplate(
        val type: TaskType,
        var errorTask: NonGroupTaskHandler? = null,
        var finallyTask: NonGroupTaskHandler? = null,
        var returnVal: String? = null
) : Branch<TaskContextImpl>(type.taskName, false) {

    override fun preTraversal(ctx: TaskContextImpl) = Directive.abort

    override fun postTraversal(ctx: TaskContextImpl) = Directive.abort

    override fun onError(error: Exception, ctx: TaskContextImpl) {
    }

    override fun enterBranch(ctx: TaskContextImpl) {
    }

    override fun exitBranch(ctx: TaskContextImpl) {
    }

    fun toCompositeTask(id: TaskId, descriptor: TaskDescriptor, ctx: TaskContextImpl, module: NemawashiModule): CompositeTask {
        val task = CompositeTask(id, descriptor, ctx, this)
        task.registerTask(module)
        return task
    }

}