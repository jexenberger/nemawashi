/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime.dynamic

import io.nemawashi.api.TaskFailedException
import io.nemawashi.api.FunctionalTask
import io.nemawashi.api.RunContext
import io.nemawashi.api.TaskId
import io.nemawashi.api.metamodel.TaskDef
import io.nemawashi.runtime.TaskContextImpl
import io.nemawashi.runtime.TaskDefId

interface DynamicFunctionalTask : FunctionalTask<Any>, DynamicTaskMixin {

    val nestedContext: TaskContextImpl


    fun executeTask(id: TaskId, context: TaskContextImpl, parentContext: TaskContextImpl, taskDef: TaskDef): Any? {
        populateContext(taskDef, id, context, parentContext, descriptor)
        return runTask(id, context)
    }


    override fun evaluate(id: TaskId, ctx: RunContext): Any? {
        val context = ctx["_ctx"] as TaskContextImpl?
                ?: throw TaskFailedException(id, "context not loaded in Task Context")
        val taskDef = context.get<TaskDef>(TaskDefId(id))
                ?: throw TaskFailedException(id, "Task Definition not found for Task")
        try {
            return this.executeTask(id, nestedContext, context, taskDef)
        } finally {
            TaskContextImpl.release(nestedContext.id)
        }
    }

    fun runTask(id: TaskId, ctx: TaskContextImpl): Any?

}