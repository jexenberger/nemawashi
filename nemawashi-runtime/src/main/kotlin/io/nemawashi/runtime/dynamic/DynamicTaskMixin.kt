/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime.dynamic

import io.nemawashi.api.ComponentDefinitionException
import io.nemawashi.api.TaskId
import io.nemawashi.api.descriptor.TaskDescriptor
import io.nemawashi.api.metamodel.TaskDef
import io.nemawashi.runtime.TaskContextImpl
import io.nemawashi.util.transformation.TransformerService

interface DynamicTaskMixin {

    val descriptor: TaskDescriptor


    fun populateContext(taskDef: TaskDef, id: TaskId, context: TaskContextImpl, parentContext: TaskContextImpl, taskDescriptor: TaskDescriptor) {
        taskDescriptor.parameters.forEach { k, v ->
            val paramDefn = taskDef.parameters[k]
            if (v.required && paramDefn == null) {
                throw ComponentDefinitionException(id.stringId, "Required parameter '$k' is missing")
            }
            val evaledResult = context.scriptingService.evalIfScript<Any?>(paramDefn?.valueDefn, parentContext.bindings)?.let {
                TransformerService.convertWithNull<Any>(it, v.type.typeMapping)
            }
            v.isValid(evaledResult).right?.let {
                throw ComponentDefinitionException(id.toString(), it.toStringWithDescriptions())
            }
            context.bindings[k] = evaledResult
        }
    }



}