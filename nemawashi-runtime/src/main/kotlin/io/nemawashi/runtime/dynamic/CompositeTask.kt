/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime.dynamic

import io.nemawashi.api.Directive
import io.nemawashi.api.NemawashiModule
import io.nemawashi.api.TaskError
import io.nemawashi.api.TaskId
import io.nemawashi.api.descriptor.TaskDescriptor
import io.nemawashi.runtime.TaskContextImpl
import io.nemawashi.runtime.task.GroupTaskHandler
import io.nemawashi.runtime.task.NonGroupTaskHandler
import io.nemawashi.runtime.tree.Branch
import io.nemawashi.runtime.tree.Node

open class CompositeTask(
        val taskId: TaskId,
        override val descriptor: TaskDescriptor,
        override val nestedContext: TaskContextImpl,
        val template: CompositeTaskTemplate) : Branch<TaskContextImpl>(taskId.toString(), false), DynamicFunctionalTask {

    init {
        template.children.forEach { this.add(it) }
    }

    override fun runTask(id: TaskId, ctx: TaskContextImpl): Any? {
        this.execute(ctx)
        return template.returnVal?.let {
            ctx.bindings.get(it)
        }
    }


    override fun preTraversal(ctx: TaskContextImpl) = Directive.continueExecution

    override fun postTraversal(ctx: TaskContextImpl) = Directive.done

    override fun onError(error: Exception, ctx: TaskContextImpl) {
        val taskError = if (error is TaskError) error else TaskError(taskId, error, ctx.bindings)
        ctx.bindings["_error_"] = taskError
        template.errorTask?.let { runTask(ctx, it) } ?: throw taskError
    }

    override fun enterBranch(ctx: TaskContextImpl) {
    }

    override fun exitBranch(ctx: TaskContextImpl) {
        runTask(ctx, template.finallyTask)
    }


    fun registerTask(module: NemawashiModule, value: Node<TaskContextImpl> = this) {
        when (value) {
            //root task
            is CompositeTask -> {
                value.template.errorTask?.let {
                    nestedContext.registerTask(it.taskDef, module)
                }
                value.template.finallyTask?.let {
                    nestedContext.registerTask(it.taskDef, module)
                }
                value.children.forEach { registerTask(module, it) }
            }
            is GroupTaskHandler -> {
                nestedContext.registerTask(value.taskDef, module)
                value.children.forEach { registerTask(module, it) }
            }
            is NonGroupTaskHandler -> {
                nestedContext.registerTask(value.taskDef, module)
            }
            else -> throw IllegalStateException("cant handle ${value::class.simpleName}")
        }
    }


    private fun runTask(ctx: TaskContextImpl, targetTask: NonGroupTaskHandler?) {
        targetTask?.execute(ctx)
    }

    override fun toString(): String {
        return "CompositeTask(taskId=$taskId)"
    }


}