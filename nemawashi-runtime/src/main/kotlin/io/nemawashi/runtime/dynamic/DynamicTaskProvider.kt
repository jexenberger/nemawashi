/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime.dynamic

import io.nemawashi.api.Task
import io.nemawashi.api.descriptor.TaskDescriptor
import io.nemawashi.di.api.Factory
import io.nemawashi.runtime.TaskContextImpl
import javax.activation.DataSource

abstract class DynamicTaskProvider(val source: DataSource, val module: BaseDynamicModule) : Factory<Task> {


    @Suppress("UNCHECKED_CAST")
    fun createDynamicTaskContext(ctx: TaskContextImpl): TaskContextImpl {
        val newContext = ctx.cloneWithoutBindings()
        newContext.bindings[modulePathVariable] = module.modulePath
        newContext.bindings[moduleProperties] = module.properties
        newContext.bindings[taskModuleVariable] = module
        newContext.bindings[taskFilePathVariable] = source.name

        ctx.bindings.keys
                .filter { !excludedVariablesFromCopy.contains(it) }
                .filter { it.startsWith("_") }
                .forEach { newContext.bindings[it] = ctx.bindings[it] }
        return newContext
    }

    abstract fun createDescriptor() : TaskDescriptor

    companion object {

        const val modulePathVariable = "__modulePath"
        const val taskFilePathVariable = "__taskFilePath"
        const val taskModuleVariable = "__taskModule"
        const val moduleProperties = "__moduleProperties"
        val excludedVariablesFromCopy = listOf(modulePathVariable, taskFilePathVariable, "_ctx", "_os", "_env", "_properties", "_parent")
    }

}