/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime.dynamic

import io.nemawashi.api.*
import io.nemawashi.api.metamodel.TaskDef
import io.nemawashi.runtime.TaskContextImpl
import io.nemawashi.runtime.TaskDefId

interface DynamicGroupTask : GroupTask, DynamicTaskMixin {

    val nestedContext: TaskContextImpl

    override fun before(id: TaskId, ctx: RunContext): Directive {
        setupContext(ctx, id)
        return handleBefore(id, ctx)
    }

    fun setupContext(ctx: RunContext, id: TaskId) {
        val context = ctx["_ctx"] as TaskContextImpl?
                ?: throw TaskFailedException(id, "context not loaded in Task Context")
        val taskDef = context.get<TaskDef>(TaskDefId(id))
                ?: throw TaskFailedException(id, "Task Definition not found for Task")
        populateContext(taskDef, id, nestedContext, context, descriptor)
    }

    override fun after(id: TaskId, ctx: RunContext): Directive {
        setupContext(ctx, id)
        return handleAfter(id, ctx)
    }

    override fun onError(id: TaskId, error: TaskError, ctx: RunContext) {
        setupContext(ctx, id)
        return handleOnError(id, ctx, error)
    }

    override fun onFinally(id: TaskId, ctx: RunContext) {
        handleOnFinally(id, ctx)
        TaskContextImpl.release(nestedContext.id)
    }

    fun handleOnFinally(id: TaskId, ctx: RunContext)
    fun handleAfter(id: TaskId, ctx: RunContext) : Directive
    fun handleOnError(id: TaskId, ctx: RunContext, error: TaskError)
    fun handleBefore(id: TaskId, ctx: RunContext): Directive
}