/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime.dynamic

import io.nemawashi.api.NemawashiModule
import io.nemawashi.runtime.groovy.GroovyTaskProvider
import io.nemawashi.runtime.yaml.YamlTaskProvider
import io.nemawashi.util.string.FileDataSource
import java.io.File

abstract class BaseDynamicModule(val modulePath: String) : NemawashiModule {

    override val extensible: Boolean = true

    abstract val properties: Map<String, Any?>

    abstract fun createScriptObjects(): Map<String, Any>

    fun resolveProvider(file: File) = providers[file.extension]?.let { it(file, this) }

    fun supportedType(file: File) = supports(file)


    companion object {
        val providers = mapOf<String, (File, BaseDynamicModule) -> DynamicTaskProvider>(
                "yaml" to { file, module -> YamlTaskProvider(FileDataSource(file), module) },
                "yml" to { file, module -> YamlTaskProvider(FileDataSource(file), module) },
                "groovy" to { file, module -> GroovyTaskProvider(FileDataSource(file), module) }
        )

        fun supports(file: File) = providers.containsKey(file.extension)
    }

}