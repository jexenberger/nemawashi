/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime.dynamic

import de.skuzzle.semantic.Version
import io.nemawashi.api.TaskType
import io.nemawashi.api.defaultVersion
import io.nemawashi.api.descriptor.TaskDescriptor
import io.nemawashi.di.api.Factory
import io.nemawashi.doc.FunctionDoc
import java.io.File
import java.util.*

class SingleFileModule(val file: File) : BaseDynamicModule(resolvePath(file)) {

    override val scriptObject: Factory<Any>? = null
    override val properties: Map<String, Any?> get() = emptyMap()
    override val description: String get() = taskDescriptor.description
    override val name: String  by lazy {
        file.parentFile?.absolutePath ?: "./"
    }

    override val version: Version get() = defaultVersion
    override val scriptObjectDocumentation = emptyList<FunctionDoc>()
    override val extensible: Boolean = false
    val taskDescriptor: TaskDescriptor

    init {
        taskDescriptor = resolveProvider(file)?.createDescriptor() ?: throw UnsupportedOperationException("${file.absolutePath} is not a supported extension")
    }

    override fun createScriptObjects() = emptyMap<String, Any>()


    override val tasks: Set<TaskType> = Collections.singleton(taskDescriptor.type).toSet()

    override fun get(name: TaskType) = taskDescriptor

    companion object {
        fun resolvePath(file:File) : String {
            val path = file.parentFile?.absolutePath
            return if (path == null) "./" else path
        }
    }
}