/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime.dynamic

import de.skuzzle.semantic.Version
import io.nemawashi.api.*
import io.nemawashi.api.descriptor.TaskDescriptor
import io.nemawashi.di.api.LambdaFactory
import io.nemawashi.doc.FunctionDoc
import io.nemawashi.runtime.ModuleRegistry
import io.nemawashi.runtime.yaml.DynamicModuleDescriptor
import io.nemawashi.runtime.yaml.snakeyaml.ExtTypeConstructors
import io.nemawashi.runtime.yaml.snakeyaml.ExtTypeRepresenters
import org.yaml.snakeyaml.Yaml
import java.io.File
import java.io.FileReader
import java.util.*
import kotlin.reflect.KClass
import kotlin.reflect.full.createInstance


@Suppress("UNCHECKED_CAST")
class DefinedDynamicModule(val path: File) : BaseDynamicModule(path.absolutePath) {


    val descriptor: DynamicModuleDescriptor
    internal val taskDescriptorCache: MutableMap<TaskType, TaskDescriptor> = Collections.synchronizedMap(mutableMapOf())

    internal val taskList: Set<Pair<TaskType, File>> by lazy { loadTaskTypes() }
    override val description: String get() = descriptor.description
    override val name: String get() = descriptor.name ?: path.name
    override val properties: Map<String, Any?> get() = descriptor.properties

    override val version: Version get() = descriptor.version?.let { Version.parseVersion(it) } ?: defaultVersion
    override val scriptObject by lazy {
        loadScriptObject()?.let {
            LambdaFactory({ id, ctx -> it })
        }
    }
    override val tasks: Set<TaskType> by lazy { taskList.map { it.first }.toSet() }
    override val scriptObjectDocumentation: Collection<FunctionDoc>
        get() = scriptClass?.let { NemawashiModule.functionsDocumentationFromType(it, this) } ?: emptyList()
    private val scriptClass: KClass<*>?

    init {

        if (!path.isDirectory) {
            throw ModuleDoesNotExistException(path.name)
        }
        scriptClass = parseScriptClass()
        descriptor = load()
    }


    @Synchronized
    override fun get(name: TaskType): TaskDescriptor? {
        val type = taskList.find { it.first.equals(name) } ?: return null
        val descriptor = taskDescriptorCache[name]
        val load = descriptor?.let { it.dateTimeCreated <= type.second.lastModified() } ?: true
        if (load) {
            val sourceFile = type.second
            val provider = resolveProvider(sourceFile)
                    ?: //not supported
                    return null
            val dynamicTaskDescriptor = provider.createDescriptor()
            taskDescriptorCache[name] = dynamicTaskDescriptor
        }
        return taskDescriptorCache[name]
    }


    private fun loadScriptObject(): Any? {
        return scriptClass?.createInstance()
    }

    private fun parseScriptClass(): KClass<*>? {
        //until Groovy startup time improves
        return null
    }

    private fun loadTaskTypes(): Set<Pair<TaskType, File>> {
        return this.path.listFiles().filter {
            supportedType(it)
        }.map {
            TaskType(this.id, it.nameWithoutExtension) to it
        }.toSet()
    }


    private fun load(): DynamicModuleDescriptor {
        val load = Yaml(ExtTypeConstructors(), ExtTypeRepresenters()).load(FileReader(File(path, "module.conf"))) as Map<String, Any?>
        return DynamicModuleDescriptor(
                name = load["name"] as String?,
                description = load["description"] as String? ?: "<no description>",
                version = load["version"] as String?,
                properties = load["properties"] as Map<String, Any?>? ?: emptyMap()
        )
    }

    override fun createScriptObjects(): MutableMap<String, Any> {
        val scriptObjects = dependencies
                .map { ModuleRegistry[it] ?: throw ModuleException(this, "has dependency on ${it} but does not exist") }
                .map { mod ->
                    mod.scriptObject?.let { mod.name to it }
                }.filterNotNull().toMap()
        val withThis = mutableMapOf<String, Any>()
        withThis.putAll(scriptObjects)
        this.scriptObject?.let { withThis[this.name] = it }
        return withThis
    }

}