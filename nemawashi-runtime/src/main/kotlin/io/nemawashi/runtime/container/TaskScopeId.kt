/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime.container

import io.nemawashi.api.TaskId
import io.nemawashi.di.api.ComponentId
import io.nemawashi.runtime.TaskContextImpl

data class TaskScopeId(val ctx: TaskContextImpl, val taskId: TaskId) : ComponentId {
    override val stringId = "$ctx->$taskId"

    override fun toString() = stringId

    fun set(value: Any?) {
        ctx.taskScope[this] = value
    }

    fun get() = ctx.taskScope[this]


    fun release() {
        ctx.taskScope.remove(this)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is TaskScopeId) return false

        if (taskId != other.taskId) return false

        return true
    }

    override fun hashCode(): Int {
        return taskId.hashCode()
    }


}