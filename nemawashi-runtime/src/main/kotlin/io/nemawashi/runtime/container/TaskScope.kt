/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime.container

import io.nemawashi.di.api.ComponentId
import io.nemawashi.di.api.Context
import io.nemawashi.di.api.Scope
import io.nemawashi.di.api.Scopes

object TaskScope : Scope {

    init {
        Scopes += this
    }

    override val id: String = "taskScope"


    @Synchronized
    override fun add(id: ComponentId, instance: Any) {
        val taskScopeId = id as TaskScopeId
        taskScopeId.set(instance)
    }

    override fun get(id: ComponentId, ctx: Context) = (id as TaskScopeId).get()

    @Synchronized
    fun release(id: TaskScopeId) {
        id.release()
    }

}