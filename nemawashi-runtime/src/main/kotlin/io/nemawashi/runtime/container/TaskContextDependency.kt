/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime.container

import io.nemawashi.api.TaskId
import io.nemawashi.api.annotations.TaskContext
import io.nemawashi.di.api.*

class TaskContextDependency : AnnotationDependency<TaskContext>(TaskContext::class)  {
    override fun <T> resolve(annotation: TaskContext, target: DependencyTarget, ctx: Context): T? {
        val streamCtx = ctx as io.nemawashi.runtime.TaskContextImpl
        val id = io.nemawashi.runtime.container.TaskScopeId(streamCtx, target.id as TaskId)
        if (!ctx.hasComponent(id)) {
            addType<T>(target.targetType) withId(id) toScope io.nemawashi.runtime.container.TaskScope.id into (ctx as DefinableContext)
        }
        return ctx.get<T>(id)!!
    }
}