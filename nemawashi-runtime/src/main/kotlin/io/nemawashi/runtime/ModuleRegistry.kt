/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime

import io.nemawashi.api.ModuleId
import io.nemawashi.api.NemawashiModule
import io.nemawashi.runtime.dynamic.DefinedDynamicModule
import io.nemawashi.runtime.modules.approvals.ApprovalsModule
import io.nemawashi.runtime.modules.csv.CSVModule
import io.nemawashi.runtime.modules.doc.DocumentationModule
import io.nemawashi.runtime.modules.files.FilesModule
import io.nemawashi.runtime.modules.http.HttpModule
import io.nemawashi.runtime.modules.json.JsonModule
import io.nemawashi.runtime.modules.messaging.MessagingModule
import io.nemawashi.runtime.modules.resources.ResourcesModule
import io.nemawashi.runtime.modules.server.ServerModule
import io.nemawashi.runtime.modules.system.SystemModule
import io.nemawashi.runtime.modules.templating.TemplateModule
import io.nemawashi.runtime.services.ServiceModuleLoader
import io.nemawashi.util.log.LogProvider
import io.nemawashi.util.system
import java.io.File
import java.util.concurrent.Future


object ModuleRegistry {


    internal val __MODULES: MutableMap<ModuleId, NemawashiModule> = mutableMapOf()
    internal val systemModule = SystemModule()

    val systemModules = setOf(
            systemModule,
            ResourcesModule(),
            HttpModule(),
            TemplateModule(),
            JsonModule(),
            ServerModule(),
            DocumentationModule(),
            ApprovalsModule(),
            CSVModule(),
            FilesModule(),
            MessagingModule()
    )

    val modules: Set<ModuleId> get() = __MODULES.keys.toSet()

    init {
        loadSystemModules()
        load()
    }

    private fun loadSystemModules() {
        systemModules.forEach {
            this += it
        }
    }

    private fun load() {
        val modules = ServiceModuleLoader.loadModules()
        __MODULES.putAll(modules.map { it.id to it }.toMap())
    }

    @JvmStatic
    fun loadDefinedYamlModules(modulePaths: String, log: LogProvider) {
        val allLoaded = mutableListOf<Future<*>>()
        val paths = log.traceWithTime("LOAD MODULES PATHS") {
            system.parsePathPath(modulePaths)
                    .map { File(it) }
                    .filter { it.isDirectory }
        }

        log.trace("LOOKING FOR MODULES IN ${system.pwd}.")
        val pwdModule = File(system.pwdPath, "module.conf")
        if (pwdModule.exists() && pwdModule.isFile) {
            allLoaded += system.optimizedExecutor.submit {
                log.traceWithTime("LOAD LOCAL MODULE") {
                    ModuleRegistry += DefinedDynamicModule(system.pwdPath)
                    log.trace("LOADED MODULE -> $pwdModule")
                }
            }
        }
        log.traceWithTime("RESOLVE MODULES") {
            for (path in paths) {
                log.trace("CHECKING $path...")
                log.traceWithTime("RESOLVE MODULES IN DIRECTORY : $path") {
                    allLoaded += system.optimizedExecutor.submit {
                        loadPath(path, log)
                    }
                }
            }
        }
        log.traceWithTime("WAITING FOR MODULES TO LOAD") {
            allLoaded.forEach { it.get() }
        }
        log.trace("MODULE REGISTRY COMPLETE")
    }

    private fun loadPath(path: File, log: LogProvider) {
        if (!path.exists() || path.isFile) {
            log.trace("$path DOES NOT EXIST OR IS NOT A FILE")
            return
        }
        val nested = path.listFiles { f, _ -> f.isDirectory }
        for (nestedPath in nested) {
            log.trace("LOOKING FOR MODULE IN $nestedPath")
            val conf = File(nestedPath, "module.conf")
            if (!conf.exists()) {
                log.trace("SKIPPING $nestedPath")
                continue
            }
            log.traceWithTime("LOAD OF MODULE : $nestedPath") {
                ModuleRegistry += DefinedDynamicModule(nestedPath)
            }
        }
    }

    @Synchronized
    operator fun plusAssign(module: NemawashiModule) {
        __MODULES[module.id] = module
    }

    fun getLatestVersion(name: String): NemawashiModule? {
        return __MODULES
                .entries
                .filter { it.key.name.equals(name) }
                .sortedByDescending { it.value.version }
                .firstOrNull()?.value
    }

    operator fun get(name: ModuleId): NemawashiModule? {
        return if (name.isDefaultVersion) getLatestVersion(name.name) else __MODULES[name]
    }


}