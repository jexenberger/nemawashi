package io.nemawashi.runtime.files

import io.nemawashi.api.FileRef
import io.nemawashi.api.FileResource
import io.nemawashi.api.services.FileService
import io.nemawashi.util.rest.Multipart
import java.io.File
import java.io.FileNotFoundException
import javax.activation.DataSource

class FileServiceDataSource(val ref: String, val service: FileService) : DataSource {

    val resource: FileResource

    init {
        resource = service.get(FileRef.fromString(ref))
                ?: throw FileNotFoundException("$ref cannot be found in file store")
    }

    override fun getName() = resource.fileRef.toString()

    override fun getOutputStream() = throw UnsupportedOperationException("Cannot write to resources")

    override fun getInputStream() = resource.inputStream

    override fun getContentType() = Multipart.contentTypeFromFile(File(resource.fileRef.toString()))
}