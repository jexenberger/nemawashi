/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime.files

import io.nemawashi.api.FileRef
import io.nemawashi.api.FileResource
import io.nemawashi.api.resources.CategorisedType
import io.nemawashi.api.services.FileService
import io.nemawashi.runtime.BaseCategorisedResourceStore
import java.io.File
import java.io.OutputStream

class FileSystemFileService(databaseName: String, basePath: File) :
        BaseCategorisedResourceStore<FileResource>(databaseName, basePath),
        FileService {

    override fun get(type: FileRef) = get(type.type, type.id)

    override fun list(type: CategorisedType): List<FileResource> {
        val path = getPath(type)
        if (!path.isDirectory) {
            return emptyList()
        }
        return path.list().map {
            FileResource(type, it, File(path, it).inputStream())
        }
    }

    override fun loadResource(idPath: File, type: CategorisedType, id: String): FileResource {
        return FileResource(type, id, idPath.inputStream())
    }

    override fun writeType(bufferedWriter: OutputStream, resource: FileResource) {
        resource.stream(bufferedWriter)
    }

}