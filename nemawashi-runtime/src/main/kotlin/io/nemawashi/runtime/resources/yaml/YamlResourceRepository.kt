/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.runtime.resources.yaml

import io.nemawashi.api.resources.CategorisedType
import io.nemawashi.api.resources.Resource
import io.nemawashi.api.resources.WritableResourceRepository
import io.nemawashi.runtime.BaseCategorisedResourceStore
import io.nemawashi.runtime.yaml.snakeyaml.ExtTypeConstructors
import io.nemawashi.runtime.yaml.snakeyaml.ExtTypeRepresenters
import org.yaml.snakeyaml.Yaml
import java.io.File
import java.io.FileReader
import java.io.OutputStream
import java.io.OutputStreamWriter

class YamlResourceRepository(databaseName: String, basePath: File) :
        BaseCategorisedResourceStore<Resource>(databaseName, basePath),
        WritableResourceRepository {


    private val yaml = Yaml(ExtTypeConstructors(), ExtTypeRepresenters())


    @Suppress("UNCHECKED_CAST")
    override fun loadResource(idPath: File, type: CategorisedType, id: String): Resource {
        val fileReader = FileReader(idPath)
        val resourceData = yaml.load(fileReader) as Map<String, Any?>
        fileReader.close()
        return Resource(type, id, resourceData)
    }

    override fun find(type: CategorisedType): Collection<Resource> {
        val typePath = File(fullPath, type.pathSnippet.toString())
        if (!typePath.exists()) {
            return emptyList()
        }
        val files = typePath.listFiles { file: File -> file.isFile }.sortedBy { it.name }
        return files.map { loadResource(it, type, it.name) }
    }

    override fun find(type: CategorisedType, attributes: Map<String, Any?>): Collection<Resource> {
        return find(type).filter {
            it.properties.map { (k, v) ->
                attributes[k]?.equals(v) ?: true
            }.fold(true) { a, b -> a and b}
        }
    }

    override fun writeType(bufferedWriter: OutputStream, resource: Resource) {
        bufferedWriter.write(yaml.dump(resource.properties).toByteArray())
    }


}
