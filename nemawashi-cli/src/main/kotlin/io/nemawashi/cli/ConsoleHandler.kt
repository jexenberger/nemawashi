/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.cli

import io.nemawashi.api.NemawashiRuntimeException
import io.nemawashi.api.TaskFailedException
import io.nemawashi.api.TaskId
import io.nemawashi.api.events.AfterTaskEvent
import io.nemawashi.api.events.BeforeGroupTaskEvent
import io.nemawashi.api.events.TaskErrorEvent
import io.nemawashi.api.events.TaskEvent
import io.nemawashi.di.event.EventHandler
import io.nemawashi.util.io.console.Console
import io.nemawashi.util.io.console.bold
import io.nemawashi.util.io.console.decorate

class ConsoleHandler(val enableDebug: Boolean) : EventHandler<TaskEvent> {

    fun formatTaskId(id: TaskId) = "${bold(id.id).padEnd(40)} -> [${decorate(id.taskType.taskName, Console.ANSI_GREEN)}]"

    override fun onEvent(event: TaskEvent) {
        when (event) {
            is BeforeGroupTaskEvent -> {
                val display = "${formatTaskId(event.taskId)}..."
                Console.display(display).newLine()
            }
            is AfterTaskEvent -> {
                val taskString = formatTaskId(event.taskId).padEnd(110, '.')
                val display = "$taskString ${bold("(TOOK " + event.timeTaken + "ms)")}"
                Console.display(display)
                        .newLine()

            }
            is TaskErrorEvent -> {

                val cause = event.error.exception


                val msg = if (cause is NemawashiRuntimeException) {
                    cause.msg
                } else {
                    event.error.message
                }

                val lineNo = if (cause is TaskFailedException) {
                    if (cause.lineNo > 0) cause.lineNo else cause.taskId.lineFormatId?.lineNo ?: "<UNKNOWN>"
                } else {
                    "<UNKNOWN>"
                }

                Console.display(decorate("TASK FAILED: ${event.taskId}", Console.REVERSED, Console.ANSI_RED))
                        .newLine()
                        .display("LINE: $lineNo")
                        .newLine()
                        .display("---")
                        .newLine()
                        .display(decorate(msg, Console.BOLD))
                        .newLine()
                        .display("---")
                        .newLine()
                if (enableDebug) {
                    Console.display(event.error.rootCause).newLine()
                }
            }
        }

    }
}