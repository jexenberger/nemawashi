/*
 *  Copyright 2018 Julian Exenberger
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package io.nemawashi.cli

import io.nemawashi.api.ModuleId
import io.nemawashi.api.Nemawashi
import io.nemawashi.api.TaskType
import io.nemawashi.di.annotation.Inject
import io.nemawashi.di.annotation.Value
import io.nemawashi.doc.ModuleDoc
import io.nemawashi.doc.TaskDoc
import io.nemawashi.util.io.console.Console
import io.nemawashi.util.io.console.bold
import io.nemawashi.util.io.console.decorate
import io.nemawashi.util.stringOf

class HelpCommandlet(
        @Inject
        val nemawashi: Nemawashi,
        @Value("task.ref")
        val task: String
) : Commandlet {

    override fun run() {
        Console.newLine()
        when (task) {
            "" -> {
                headerAndDescription("Help Options", "List of available functions to get help on (nw help [option])")
                Console.newLine()
                        .newLine()
                        .display("pptions:")
                        .newLine()

                displayListLine("modules", "Displays the list of available modules")
                displayListLine("[module name]", "Displays details about a specific module")
                displayListLine("[task]", "Displays details about a specific task")
            }
            "run" -> {
                headerAndDescription("run", "Runs a specified task")
                Console.newLine()
                        .newLine()
                        .display("Arguments:")
                        .newLine()
                displayListLine("[task]", "Task to run in format [module@version::task] eg sys@1.0.0::echo, omitting the version eg: sys::echo will run the latest")
                displayListLine("-R, --runAt", "Runs the task on a specified service, the service can be a URL endpoint or a pre configured service")
                displayListLine("-I, --input", "An input parameter to be passed to the task in [parameter]=[value] format, eg -I value=hello")
            }
            "modules" -> {
                Console.display("available modules:").newLine()
                nemawashi.modules.forEach { id ->
                    nemawashi.moduleDoc(id)?.let {
                        Console.space(2)
                                .display(bold(id.toString()).padEnd(30))
                                .display("  ")
                                .display(it.description)
                                .newLine()

                    } ?: throw IllegalStateException("Module '$id' has no documentation???")
                }
            }
            else -> {
                if (task.contains("::")) {
                    val type = TaskType.fromString(task)
                    nemawashi.moduleDoc(type.module)?.let {
                        it.taskDoc(type.name)?.let {
                            displayTask(type, it)
                        }
                    }
                } else {
                    val module = ModuleId.fromString(task)
                    if (module.isDefaultVersion) {
                        nemawashi.modulesByName(module.name).forEach {
                            displayModule(it, nemawashi.moduleDoc(it)!!)
                        }
                    } else {
                        nemawashi.moduleDoc(module)?.let { displayModule(module, it) }
                    }
                }
            }

        }
        Console.newLine().newLine()

    }


    fun displayTask(type: TaskType, doc: TaskDoc) {
        headerAndDescription(type.taskName, doc.description)
        Console
                .newLine()
                .display("parameters:")
                .newLine()
        doc.parameters.forEach {
            displayListLine(it.name, it.description)
            displaySubListLine("required:  ${it.required}")
            displaySubListLine("type:      ${it.type}")
            it.pattern?.let {
                if (it.isNotEmpty()) displaySubListLine("regex:      ${it}")
            }
            it.default?.let {
                if (it.isNotEmpty()) displaySubListLine("default:    ${it}")
            }
        }
        Console.newLine()
        doc.returnType?.let {
            Console.display(heading("Return type:")).newLine()
            displayListLine("Type", it.first)
            displayListLine("Description", it.second)
        }
        Console.newLine()
    }

    fun displayModule(id: ModuleId, doc: ModuleDoc) {
        headerAndDescription(id.toString(), doc.description)

        Console.newLine()
                .display("tasks:")
                .newLine()
        doc.tasks.forEach { task ->
            displayListLine(task.name, task.description)
            Console.newLine()
        }
        Console.newLine()
        if (doc.functions.isNotEmpty()) {
            Console.display("functions:").newLine()
            doc.functions.forEach { func ->
                displayListLine(func.name, func.description)
                func.params.forEach { (name, description) ->
                    displaySubListLine("${name.padEnd(10)}: $description")
                }
                Console.newLine()
            }
        }
    }

    private fun heading(heading: String) = decorate(heading, Console.BOLD)

    private fun displayListLine(value: String, description: String) {
        Console.space(2)
                .display(value.padEnd(30))
                .display(":")
                .space()
                .display(description)
                .newLine()
    }

    private fun displaySubListLine(description: String) {
        Console.display("".padEnd(33))
                .display("-")
                .space()
                .display(description)
                .newLine()
    }

    private fun headerAndDescription(id: String, desc: String) {
        Console.display(heading(id))
                .newLine()
                .display(stringOf(id.length, '='))
                .newLine()
                .display(desc)
                .newLine()
    }
}