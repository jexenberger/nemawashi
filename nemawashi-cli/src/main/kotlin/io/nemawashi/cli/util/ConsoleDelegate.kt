/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.cli.util

import io.nemawashi.api.Type
import io.nemawashi.api.descriptor.ParameterDescriptor
import io.nemawashi.util.Either
import io.nemawashi.util.crypto.Secret
import io.nemawashi.util.io.console.Console
import io.nemawashi.util.io.console.bold
import java.util.*

object ConsoleDelegate {

    fun capture(descriptor: ParameterDescriptor): Any? {
        fun isValid(required: Boolean, value: Any?) = if (required)  value != null else true

        val prompt = if (descriptor.required) "Required: " else ":"
        val default = if (!descriptor.default.isNullOrBlank()) "default value: '${descriptor.default}'" else ""
        do {
            Console.display("Enter")
                    .space()
                    .display(bold(descriptor.name))
                    .space()
                    .display("(")
                    .display(descriptor.description)
                    .display(")")
                    .space()
                    .display(default)
                    .newLine()
                    .display(bold(prompt))
                    .space()


            val readVal = captureValue(descriptor)?.let { it } ?: descriptor.defaultValue
            if (!isValid(descriptor.required, readVal)) {
                Console.display(bold("${descriptor.name} is a required field")).newLine()
            } else {
                return readVal
            }
        } while (true)
    }

    fun captureSecret(): Any? {
        do {
            val a = Console.getSecret() ?: return null
            if (a.isBlank()) {
                return null
            }
            Console.display(bold("Re-enter :"))
            val b = Console.getSecret()
            if (Objects.equals(a, b)) {
                return a
            }
        } while (true)

    }

    fun captureValue(type: ParameterDescriptor): Any? {
        return when (type.type) {
            Type.keyValue -> captureMap()
            Type.secret -> captureSecret()?.let { Secret(it.toString()) }
            else -> Console.getNullForBlank()
        }
    }

    fun captureMap(): Map<String, Any?>? {
        val keyValues = mutableListOf<Pair<String, Any?>>()
        var keyValue: Either<Boolean, Pair<String, Any?>>
        do {
            keyValue = captureKeyValue()
            keyValue.mapR { keyValues.add(it) }
        } while (keyValue is Either.Right)
        val map = keyValues.toMap()
        return if (map.isNotEmpty()) map else null
    }

    fun captureKeyValue(): Either<Boolean, Pair<String, Any?>> {
        fun captureKey(): String? {
            Console.newLine().display(bold("Key:")).space().display("(press ctrl-X and then enter to exit) : ")
            return Console.getNullForBlank()
        }

        var key: String? = null
        while (key == null) {
            key = captureKey()
            if (24 == key?.toByteArray()?.get(0)?.toInt() ?: null) {
                return Either.left(false)
            }
        }

        Console.display(bold("Value:")).space()
        val value = Console.getNullForBlank()
        return Either.right(key to value)
    }

}