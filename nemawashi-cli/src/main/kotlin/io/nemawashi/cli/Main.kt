/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

@file:JvmName("Main")

package io.nemawashi.cli

import com.xenomachina.argparser.ArgParser
import com.xenomachina.argparser.ShowHelpException
import io.nemawashi.util.io.console.Console
import io.nemawashi.util.log.JDKLogProvider
import io.nemawashi.util.log.Level
import io.nemawashi.util.log.NOOPLogger
import io.nemawashi.util.script.Eval
import io.nemawashi.util.stripQuotes
import io.nemawashi.util.system
import sun.misc.Unsafe
import java.util.concurrent.Callable
import javax.script.ScriptEngine

fun disableIllegalAccessWarnings() {
    try {

        val theUnsafe = Unsafe::class.java.getDeclaredField("theUnsafe")
        theUnsafe.isAccessible = true
        val u = theUnsafe.get(null) as Unsafe

        val cls = Class.forName("jdk.internal.module.IllegalAccessLogger")
        val logger = cls.getDeclaredField("logger")
        u.putObjectVolatile(cls, u.staticFieldOffset(logger), null)
    } catch (e: Exception) {
        // ignore
    }

}

fun main(rawArgs: Array<String>) {
    val args = rawArgs.map { it.stripQuotes() }.toTypedArray()
    val log = if (args.contains("-X") || args.contains("--trace")) JDKLogProvider(Level.trace) else  {
        disableIllegalAccessWarnings()
        NOOPLogger()
    }
    //hack to make groovy spin up in the background
    system.optimizedExecutor.submit(Callable<ScriptEngine> { Eval.eval("1+1", emptyMap(), Eval.engineByName("groovy")) })
    val app = CliApp(ArgParser(args), log)
    try {
        app.run()
    } catch (e:ShowHelpException) {
        e.toString()
    }


}