/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.cli

import io.nemawashi.api.*
import io.nemawashi.api.descriptor.ParameterDescriptor
import io.nemawashi.cli.util.ConsoleDelegate
import io.nemawashi.di.annotation.Inject
import io.nemawashi.di.annotation.Value
import io.nemawashi.util.SystemException
import io.nemawashi.util.Timer
import io.nemawashi.util.io.console.Console
import io.nemawashi.util.io.console.bold
import io.nemawashi.util.io.console.decorate
import io.nemawashi.util.io.console.warn
import io.nemawashi.util.stackDump
import java.io.File
import java.util.concurrent.ExecutionException

class RunTaskCommandlet(
        @Inject
        val nemawashi: Nemawashi,
        @Value("task.ref")
        val task: String,
        @Value("input.parameters")
        val inputParameters: Map<String, Any?>,
        @Value("enable.debug")
        val debug: Boolean,
        @Value("run.target")
        val runAt: RunTarget
) : Commandlet, ParameterCallback {
    override fun capture(descriptor: ParameterDescriptor): Any? {
        return ConsoleDelegate.capture(descriptor)
    }

    override fun run() {
        try {

            val (time, _) = Timer.run {
                if (nemawashi.supportsDynamicType(File(task))) {
                    runFile()
                } else {
                    runTask()
                }
            }
            Console.display(bold("DONE - Took ${time}ms")).newLine()

        } catch (e: Exception) {
            displayError(e)
        }
    }

    private fun runTask() {
        val taskType = TaskType.fromString(task)
        val taskRequest = TaskRequest(
                module = taskType.module,
                task = taskType.name,
                parameters = inputParameters
        )
        val execution = nemawashi.runTask(
                request = taskRequest,
                callback = this,
                runTarget = runAt
        )
        val result = execution.getResult()
        if (result !is Unit) {
            Console.display(bold(result)).newLine()
        }
        displayOutput(execution.ctx)
    }

    private fun displayOutput(result: Map<String, Any?>) {
        if (debug) {
            result.entries.forEach { (k, v)  ->
                Console.display(bold(k)
                        .padEnd(20))
                        .display(":")
                        .display(v.toString())
                        .newLine()
            }
        }
    }

    private fun runFile() {
        val file = File(task)
        if (!file.exists() || file.isDirectory) {
            Console.display(warn("$task is not a file")).newLine()
            return
        }
        val execution = nemawashi.runTask(file, inputParameters, this, runAt)
        val result = execution.getResult()
        if (result !is Unit) {
            Console.display(bold(result)).newLine()
        }
        displayOutput(execution.ctx)
    }

    private fun displayError(e: Exception) {
        if (e is TaskError) {
            return
        }
        if (e is ExecutionException && e.cause is TaskError) {
            return
        }
        Console.display(decorate("ERROR:", Console.ANSI_RED, Console.REVERSED))
                .space()
                .display(bold(e.message!!))
                .newLine()
        if (this.debug) {
            Console.display(decorate(e.stackDump, Console.ANSI_RED, Console.REVERSED)).newLine()
        }
    }





}