/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.cli

import com.xenomachina.argparser.ArgParser
import com.xenomachina.argparser.InvalidArgumentException
import com.xenomachina.argparser.default
import com.xenomachina.argparser.mainBody
import io.nemawashi.api.NemawashiSettings
import io.nemawashi.di.api.StringId
import io.nemawashi.di.api.addInstance
import io.nemawashi.di.api.addType
import io.nemawashi.runtime.BaseApplication
import io.nemawashi.runtime.LocalTarget
import io.nemawashi.runtime.server.RemoteTarget
import io.nemawashi.util.OS
import io.nemawashi.util.io.console.Console
import io.nemawashi.util.io.console.decorate
import io.nemawashi.util.log.LogProvider
import io.nemawashi.util.log.NOOPLogger
import io.nemawashi.util.stripQuotes
import java.util.*


class CliApp(val args: ArgParser, log: LogProvider = NOOPLogger(), val reuse: Boolean = false) : BaseApplication(log) {

    val command: String by args
            .positional(name = "COMMAND", help = "command to execute, can be one of : ${CliApp.commandListString}")
            .addValidator { if (!CliApp.commandList.contains(value)) throw InvalidArgumentException("command can be one of : ${CliApp.commandListString}") }

    val task: String by args.positional(name = "COMMAND_OPTION", help = "Parameter for command option, type 'nw help' for individual commands").default("")
    val inputParms by args.adding("-I", "--input", help = "input parameter (format: [name]=[items]") {
        val parts = this.split("=")
        if (parts.size > 1) {
            Pair(parts[0], parts[1].stripQuotes())
        } else {
            Pair(this, null)
        }
    }
    val settingsFile by args.storing("-F", "--settings-file", help = "path to properties file to use for settings").default(NemawashiSettings.defaultSettingsFile)
    val settings by args.adding("-S", "--setting", help = "override values in settings file or default value (format: [setting key]=[value])") {
        val parts = this.split("=")
        if (parts.size > 1) {
            Pair(parts[0], parts[1].stripQuotes())
        } else {
            Pair(this, null)
        }
    }

    val modulePath by args.storing("-M", "--modules", help = "Path separated set of directories which contain YAML modules").default(NemawashiSettings.defaultModulePath)
    val trace by args.flagging("-X", "--trace", help = "Enable trace output").default(false)
    val debug by args.flagging("-D", "--debug", help = "Run with debug output").default(false)
    val runAt by args.storing("-R", "--runAt", help = "Run at a specific service") {
        RemoteTarget(this)
    }.default(LocalTarget)

    val nemawashiSettings: NemawashiSettings by lazy {

        val overrides = settings.toMap(Properties())
        overrides += "yamlModulePath" to modulePath
        overrides += "trace" to trace
        overrides += "debug" to debug
        val settings = NemawashiSettings.fromSettingsFile(
                file = settingsFile.toString(),
                overrides = overrides
        )
        settings ?: NemawashiSettings()
    }

    private fun startContainer(parameters: MutableList<Pair<String, String?>>, eventHandlers: Set<ConsoleHandler>): Commandlet {
        val parms = parameters.toTypedArray().toMap()
        setValue("task.ref", this.task)
        setValue("enable.debug", debug)
        setValue("input.parameters", parms)
        setValue("run.target", runAt)

        addInstance(args) withId StringId("args") into this

        initRuntimeContainer(nemawashiSettings, eventHandlers)

        commands.forEach { type, cmd ->
            addType<Commandlet>(cmd) withId StringId(type) into this
        }
        return log.traceWithTime("INITIALISE COMMANDLET $command") {
            get<Commandlet>(command) ?: throw IllegalStateException("$command is not valid")
        }
    }

    fun run() = mainBody("nw") {
        task
        inputParms
        val commandlet = log.traceWithTime("START CONTAINER") {
            startContainer(inputParms, setOf(ConsoleHandler(this.trace)))
        }
        try {
            if (!CliApp.commands.containsKey(command)) {
                Console.display(
                        decorate(
                                "'$command' is not a recognised command, must be one of:",
                                Console.BOLD,
                                Console.ANSI_RED)).newLine()
                CliApp.commands.keys.forEach {
                    Console.tab()
                            .display(decorate("- $it", Console.BOLD, Console.ANSI_RED))
                            .newLine()
                }

            } else {
                commandlet.run()
            }
        } finally {
            if (!reuse) {
                OS.os().optimizedExecutor.shutdownNow()
            }
        }

    }

    companion object {
        val commands = mapOf(
                "run" to RunTaskCommandlet::class,
                "help" to HelpCommandlet::class
        )

        val commandList = commands.keys
        val commandListString = commands.keys.joinToString(", ")
    }
}
