/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */


import io.nemawashi.api.TaskFailedException
import io.nemawashi.api.Type
import nemawashi.nw
import groovy.json.JsonBuilder

nw.task {

    name "addserver"
    description "Adds a remote service reference to the service registry"

    parameters {
        name "Reference name to use when using the service"
        url "URL for the service"
        user "User for the service"
        password "Password for the service"
    }

    script { taskId, ctx ->

        JsonBuilder builder = new JsonBuilder()
        builder {
            name userName
            password userPassword.value
            roles userRoles
        }

        def serverRef = _fn.server.resolveServer(server)

        def response = serverRef.request("users").body(builder.toString()).post()
        if (response.status != 200) {
            throw new TaskFailedException(taskId, "service failed with response -> ${response.status}: ${response.responseMessage}".toString())
        }
    }
}