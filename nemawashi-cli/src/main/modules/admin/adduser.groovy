/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */


import io.nemawashi.api.TaskFailedException
import io.nemawashi.api.Type
import nemawashi.nw
import groovy.json.JsonBuilder

nw.task {

    name "adduser"
    description "Add a user to a Nemawashi Server"

    parameters {
        userId "User ID of the user to add"
        userName "Name of the user"
        userPassword {
            description "Password of the user to add"
            type Type.secret
        }
        userRoles {
            description "List of roles to assign to the user"
            type Type.stringArray
        }
        server {
            description "Server reference to do add the user to"
            defaultValue "localhost"
        }
    }

    script { taskId, ctx ->

        JsonBuilder builder = new JsonBuilder()
        builder {
            userId userId
            name userName
            password userPassword.value
            roles userRoles
        }

        def serverRef = _fn.server.resolveServer(server)

        def response = serverRef.request("users").body(builder.toString()).post()
        if (response.status != 201) {
            throw new TaskFailedException(taskId, "service failed with response -> ${response.status}: ${response.responseMessage}".toString())
        }
    }
}