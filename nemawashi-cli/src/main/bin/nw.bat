@echo off

REM  Nemawashi version
set VERSION=1.0-SNAPSHOT

REM  Root path of Nemawashi installation folder
set NW_HOME_DIR=""

REM Java executable path
set JAVA=""

REM Java properties to pass
set JAVA_PROPS=""

REM Executable
set JAR_FILE=nemawashi-cli
set EXECUTABLE=%JAR_FILE%-%VERSION%.jar

REM  Setup Java path
if DEFINED NW_JAVA_HOME (
    set JAVA="%NW_JAVA_HOME%\bin\java"
) ELSE (
    if DEFINED JAVA_HOME (
        set JAVA="%JAVA_HOME%\bin\java"
    ) ELSE (
        set JAVA=java
    )
)

set candidate=%~dp0
REM  Setup nemawashi home
if DEFINED NW_HOME (
    set NW_HOME_DIR=%NW_HOME%
) ELSE (
    for %%a in ("%candidate:~0,-1%") do SET "NW_HOME_DIR=%%~dpa"
)


REM jvm parameters
set STARTUP_TUNING=-Xshare:auto -XX:TieredStopAtLevel=1 -XX:CICompilerCount=1 -XX:+UseSerialGC

REM Memory settings
set MEM_OPTS=

REM  Set nemawashi working home
set JAVA_PROPS=-Dnw.installation.folder=%NW_HOME_DIR%

REM set module path
set MODULE_PATH=%NW_HOME_DIR%\modules

REM  setup arguments
set ARGS=""
IF [%1] EQU [] (
	set ARGS=--help
) ELSE (
    set ARGS=%*
)

CMD /C %JAVA% %STARTUP_TUNING% %MEM_OPTS% %JAVA_PROPS% -p %MODULE_PATH% -m io.nemawashi.cli/io.nemawashi.cli.Main %ARGS%