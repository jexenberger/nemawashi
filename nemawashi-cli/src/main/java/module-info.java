import io.nemawashi.api.NemawashiModule;
import io.nemawashi.api.NemawashiServiceLoaderFactory;

open module io.nemawashi.cli {
    uses NemawashiModule;

    requires jdk.unsupported;
    requires io.nemawashi.di;

    requires io.nemawashi.util;
    requires io.nemawashi.api;
    requires io.nemawashi.runtime;
    requires io.nemawashi.server;

    requires kotlin.stdlib;
    requires kotlin.reflect;
    requires kotlinx.coroutines.core;
    requires kotlinx.coroutines.core.common;
    requires kotlinx.coroutines.jdk8;

    requires java.scripting;

    requires kotlin.argparser;
    requires semantic.version;
    //Snakeyaml tries to taskDescriptor dates as java.sql.Date
    requires java.sql;


}
