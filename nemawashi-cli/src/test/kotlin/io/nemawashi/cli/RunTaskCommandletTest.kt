/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.cli

import io.nemawashi.api.NemawashiSettings
import io.nemawashi.runtime.LocalTarget
import io.nemawashi.runtime.NemawashiRuntime
import org.junit.jupiter.api.Test

class RunTaskCommandletTest {

    /*
    @Test
    internal fun testRunTaskBaseTask() {
        val tsk = RunTaskCommandlet(Nemawashi.get(), "sys::echo", mapOf("value" to 1), true)
        tsk.evaluate()
    }*/

    @Test
    internal fun testRunFileBasedTask() {
        val tsk = RunTaskCommandlet(NemawashiRuntime(NemawashiSettings()), "src/test/resources/sample.yaml", emptyMap(), true, LocalTarget)
        tsk.run()
    }

}