/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.cli

import com.xenomachina.argparser.ArgParser
import org.junit.jupiter.api.Test

class CliAppTest {
    @Test
    internal fun testRun() {
        CliApp(ArgParser(arrayOf("run", "sys::echo", "-Ivalue=hello world")), reuse = true).run()
    }

    @Test
    internal fun testRunHelp() {
        CliApp(ArgParser(arrayOf("help", "modules")), reuse = true).run()
    }

     /*Cant run this test as the exception calls System.exit
    @Test
    internal fun testRunInvalidCommand() {
        try {
            CliApp(ArgParser(arrayOf("qwerty")), reuse = true).run()
            fail("should have thrown an error")
        } catch (e:Exception) {
            //should fail
        }
    }*/

    @Test
    internal fun testRunStandaloneFile() {
        CliApp(ArgParser(arrayOf("run", "src/test/resources/hello-world.yaml", "-X")), reuse = true).run()
    }

    @Test
    internal fun testRunNemwashiTask() {
        CliApp(ArgParser(arrayOf("run", "nemawashi::submit-task", "-I","assignee=jexenberger","-I","message=test")), reuse = true).run()
    }

    @Test
    internal fun testRunGroovyGroupTask() {
        CliApp(ArgParser(arrayOf("run", "/home/julian/nemawashi/nemawashi-runtime/src/test/resources/samplemodule/samplegrouptask.groovy", "-X","-Istart=10", "-Iend=15")), reuse = true).run()
    }

    @Test
    internal fun testSimpleGroupTask() {
        CliApp(ArgParser(arrayOf("run", "foreach", "-Iitems=1,2,3")), reuse = true).run()
    }


    @Test
    internal fun testRunRemoteTask() {
        CliApp(ArgParser(arrayOf("run", "sys::script", "-X", "-R", "https://admin:admin@localhost:7000?validateSSL=false", "-Iscript=1+1")), reuse = true).run()

    }




}