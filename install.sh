#!/bin/sh

cp -r nemawashi-cli/target/nemawashi-cli-1.0-SNAPSHOT-nemawashi ~/

echo "export NW_HOME=~/nemawashi-cli-1.0-SNAPSHOT-nemawashi" >> ~/.bashrc
echo "export PATH=~/nemawashi-cli-1.0-SNAPSHOT-nemawashi/bin:$PATH" >> ~/.bashrc
