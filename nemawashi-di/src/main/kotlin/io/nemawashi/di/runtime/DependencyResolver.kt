/*
 *  Copyright 2018 Julian Exenberger
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package io.nemawashi.di.runtime

import io.nemawashi.di.annotation.Eval
import io.nemawashi.di.annotation.Inject
import io.nemawashi.di.annotation.Value
import io.nemawashi.di.api.AnnotationDependency
import io.nemawashi.di.api.Context
import io.nemawashi.di.api.Dependency
import io.nemawashi.di.api.DependencyTarget
import java.util.*
import kotlin.reflect.KClass


object DependencyResolver {


    private val RESOLVERS: Stack<Dependency> = Stack()
    private val ANNOTATION_RESOLVERS: MutableMap<KClass<out Annotation>, AnnotationDependency<out Annotation>> = mutableMapOf()

    init {
        ANNOTATION_RESOLVERS[Inject::class] = InjectionDependency()
        ANNOTATION_RESOLVERS[Value::class] = ValueDependency()
        ANNOTATION_RESOLVERS[Eval::class] = EvalDependency()
        RESOLVERS.push(TypedDependency())
        RESOLVERS.push(EvalDependency())
    }

    fun getDependency(call: DependencyTarget, ctx: Context) : Dependency? {

        val result = ANNOTATION_RESOLVERS.values.find {
            call.supports(it)
        }
        if (result != null) {
            return result
        }

        return RESOLVERS.reversed().find {
            call.supports(it)
        }
    }

    fun add(dependency: Dependency) : DependencyResolver {
        if (dependency is AnnotationDependency<*>) {
            ANNOTATION_RESOLVERS[dependency.annotation] = dependency
            return this
        }
        RESOLVERS.push(dependency)
        return this
    }


}