module io.nemawashi.di {

    exports io.nemawashi.di.annotation;
    exports io.nemawashi.di.api;
    exports io.nemawashi.di.event;
    exports io.nemawashi.di.sample;

    requires transitive io.nemawashi.util;

    requires kotlin.stdlib;
    requires kotlin.reflect;

    requires java.scripting;

}