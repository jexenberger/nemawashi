/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */


open module io.nemawashi.api {

    requires io.nemawashi.di;

    requires transitive io.nemawashi.util;

    requires kotlin.stdlib;
    requires kotlin.reflect;

    requires java.scripting;
    requires semantic.version;
    requires jackson.annotations;
    requires java.activation;


    exports io.nemawashi.doc;
    exports io.nemawashi.api;
    exports io.nemawashi.api.annotations;
    exports io.nemawashi.api.approvals;
    exports io.nemawashi.api.resources;
    exports io.nemawashi.api.services;
    exports io.nemawashi.api.events;
    exports io.nemawashi.api.metamodel;
    exports io.nemawashi.api.descriptor;
    exports io.nemawashi.api.server.api;


    uses io.nemawashi.api.NemawashiModule;
    uses io.nemawashi.api.NemawashiServiceLoaderFactory;

}