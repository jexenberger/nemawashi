/*
 *  Copyright 2018 Julian Exenberger
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package io.nemawashi.api

import io.nemawashi.di.api.ComponentId
import io.nemawashi.util.system
import java.lang.IllegalArgumentException
import java.util.*

data class TaskId(val taskType: TaskType, val id: String = UUID.randomUUID().toString()) : ComponentId {

    constructor(taskType: TaskType, id: LineFormatId) : this(taskType, id.toString())

    override val stringId: String
        get() = "${taskType.taskName}${id}"

    val lineFormatId: LineFormatId?
        get() {
            return try {
                LineFormatId.fromString(this.id)
            } catch (e: IllegalArgumentException) {
                null
            }
        }

    override fun toString(): String {
        return stringId
    }


    companion object {
        fun fromString(taskString: String, id: String = system.nextId()) = TaskId(TaskType.fromString(taskString), id)
    }


}