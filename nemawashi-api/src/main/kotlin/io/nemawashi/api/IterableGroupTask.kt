package io.nemawashi.api

import io.nemawashi.api.annotations.Parameter

abstract class IterableGroupTask<T> : GroupTask {

    @Parameter(description = "Variable name of the current value of the iterator, default is '__var'", default = "__var")
    var varName: String = "__var"
    @Parameter(description = "Variable name to hold the iterator, default is '__iteratorVar'", default = "__iteratorVar")
    var iteratorVarName: String = "__iteratorVar"


    fun setEvaluation(it: Iterator<T>, ctx: RunContext): Directive {
        return if (it.hasNext()) {
            ctx[varName] = it.next()
            ctx[iteratorVarName] = it
            Directive.continueExecution
        } else {
            Directive.done
        }
    }

    abstract fun initIterator(id:TaskId, ctx: RunContext) : Iterator<T>

    override fun before(id: TaskId, ctx: RunContext): Directive {
        val existing = ctx[iteratorVarName] as Iterator<T>?
        if (existing != null) {
            @Suppress("UNCHECKED_CAST")
            return setEvaluation(existing, ctx)
        }
        val iterator = initIterator(id, ctx)
        ctx[iteratorVarName] = iterator
        return setEvaluation(iterator, ctx)
    }


    override fun after(id: TaskId, ctx: RunContext): Directive = Directive.again


    override fun onFinally(id: TaskId, ctx: RunContext) {
        ctx - varName
        ctx - iteratorVarName
    }

}