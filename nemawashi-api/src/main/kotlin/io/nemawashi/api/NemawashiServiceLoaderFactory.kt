/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.api

import java.util.*

interface NemawashiServiceLoaderFactory {

    fun get(settings: NemawashiSettings): Nemawashi

    fun get(): Nemawashi

    fun runContext(): RunContext


    companion object {
        fun get(): NemawashiServiceLoaderFactory {
            val serviceLoader = ServiceLoader.load<NemawashiServiceLoaderFactory>(io.nemawashi.api.NemawashiServiceLoaderFactory::class.java)
            return serviceLoader.findFirst().orElseThrow { IllegalArgumentException("No default implementation found") }
        }
    }

}