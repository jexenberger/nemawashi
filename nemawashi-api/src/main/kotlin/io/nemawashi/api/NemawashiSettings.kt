/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.api

import io.nemawashi.util.OS
import io.nemawashi.util.config.properties.toClass
import io.nemawashi.util.log.FileJDKLogProvider
import io.nemawashi.util.log.Level
import io.nemawashi.util.log.LogProvider
import io.nemawashi.util.system
import io.nemawashi.util.transformation.TransformerService
import java.io.File
import java.util.*
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

data class NemawashiSettings(
        val yamlModulePath: String = defaultModulePath,
        val executor: ExecutorService = system.optimizedExecutor,
        val debug: Boolean = false,
        val trace: Boolean = false,
        val resourceRepositoryPath: File = defaultResourcesPath,
        val secretStorePath: File = defaultSecretStorePath,
        val globalKeyPath: File = defaultKeyPath,
        val runLogFile: String = defaultRunLogPath,
        val settingsPath: String = defaultSettingsPath,
        val settingsFile: String = defaultSettingsFile,
        val processLog: ProcessLog = ProcessLog,
        val port: Int = defaultPort,
        val enableSecurity: Boolean = defaultEnableSecurity,
        val trustStore: File? = null,
        val trustStorePassword: String? = null,
        val keyStore: File? = null,
        val keyStorePassword: String? = null,
        val uxPath: String? = null
) {

    val runLog: LogProvider

    init {
        val level = if (trace) Level.trace else if (debug) Level.debug else Level.info
        val echo = debug || trace
        runLog = FileJDKLogProvider(level, echo = echo, loggingFile = runLogFile)
        keyStore?.let {
            System.setProperty("javax.net.ssl.keyStore", it.absolutePath)
            keyStorePassword?.let {
                System.setProperty("javax.net.ssl.keyStorePassword", it)
            }
        }
        trustStore?.let {
            System.setProperty("javax.net.ssl.trustStore", it.absolutePath)
            trustStorePassword?.let {
                System.setProperty("javax.net.ssl.trustStorePassword", it)
            }
        }
    }


    companion object {

        val defaultSettingsPath = ".nw"
        val defaultNwPath = "${OS.os().homeDir}/$defaultSettingsPath"
        val defaultSettingsFile = "${defaultNwPath}/settings.properties"
        val defaultModulePath = system.pathString("_modules", "${defaultNwPath}/_modules")
        val defaultResourcesPath = File("${defaultNwPath}/resources")
        val defaultSecretStorePath = File("${defaultNwPath}/secretstore.yaml")
        val defaultKeyPath = File("${defaultNwPath}/globalkey.p12")
        val defaultRunLogPath = "${OS.os().homeDir}/.runlog"
        val defaultPort = 7000
        val defaultEnableSecurity = true

        fun fromSettingsFile(file: String = defaultSettingsFile,
                             overrides: Properties? = null,
                             processLog: ProcessLog = ProcessLog): NemawashiSettings? {
            val file = File(file)
            if (file.exists()) {
                val propFile = Properties()
                propFile.load(file.reader())
                val props = overrides?.let {
                    propFile.putAll(it)
                    propFile
                } ?: propFile
                return createFromProperties(props, file, processLog)
            }
            return overrides?.let {
                createFromProperties(overrides, file, processLog)
            }
        }

        private fun createFromProperties(props: Properties, file: File, processLog: ProcessLog): NemawashiSettings {

            return props.toClass<NemawashiSettings>(
                    "executor" to { p: Properties ->
                        props.getProperty("threadPoolSize")
                                ?.let { Executors.newFixedThreadPool(TransformerService.convert(it) as Int) }
                                ?: system.optimizedExecutor
                    }
            )
        }


    }
}

