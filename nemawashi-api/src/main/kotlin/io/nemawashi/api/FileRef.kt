/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.api

import io.nemawashi.api.resources.CategorisedType

data class FileRef(val type: CategorisedType, val id: String) {

    val typeTag = Type.fileref

    companion object {
        fun fromString(string: String): FileRef {
            val split = string.split("/")
            if (split.size != 2) {
                throw IllegalArgumentException("'$string' should be in the formation [category]::[type]/[id], eg 'applications::sales-server/application.properties'")
            }
            val type = CategorisedType.fromString(split[0])
            return FileRef(type, split[1])
        }
    }

    override fun toString(): String {
        return "$type/$id"
    }

}