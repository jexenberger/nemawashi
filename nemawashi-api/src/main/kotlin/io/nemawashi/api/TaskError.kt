/*
 *  Copyright 2018 Julian Exenberger
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package io.nemawashi.api

import io.nemawashi.util.SystemException
import io.nemawashi.util.rootCause
import io.nemawashi.util.stackDump
import java.lang.Error

class TaskError(val taskId: TaskId, val exception: Exception, val runContext: RunContext)
    : SystemException("$taskId -> '$exception'", exception) {
    val rootCause: String = exception.rootCause.stackDump

    companion object {
        fun bubble(taskId: TaskId, exception: Exception, runContext: RunContext): TaskError {
            if (exception is Error) {
                throw exception
            }
            return when (exception) {
                is TaskError -> exception
                is NemawashiRuntimeException -> TaskError(taskId, exception.rootCause as Exception, runContext)
                else -> TaskError(taskId, exception, runContext)
            }
        }
    }
}