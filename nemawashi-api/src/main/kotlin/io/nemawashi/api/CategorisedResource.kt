package io.nemawashi.api

import io.nemawashi.api.resources.CategorisedType

interface CategorisedResource {

    val type: CategorisedType
    val id:String
}