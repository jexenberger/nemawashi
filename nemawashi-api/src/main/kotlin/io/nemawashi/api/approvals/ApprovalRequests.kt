/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.api.approvals

interface ApprovalRequests {


    fun get(id: String): ApprovalRequest?

    fun request(request: ApprovalRequest)

    fun approve(id: String): ApprovalRequest?

    fun reject(id: String, reason: String): ApprovalRequest?

    fun findByUser(userId: String): List<ApprovalRequest>

    fun findPendingByUser(userId: String): List<ApprovalRequest>


}