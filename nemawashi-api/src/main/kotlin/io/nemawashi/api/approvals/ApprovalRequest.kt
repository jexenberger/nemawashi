/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.api.approvals

import io.nemawashi.api.NemawashiProcess
import io.nemawashi.api.TaskRequest
import io.nemawashi.api.TaskType
import io.nemawashi.util.system
import java.time.LocalDateTime

data class ApprovalRequest(
        val process: NemawashiProcess,
        val headline: String,
        val description: String,
        val assignedTo: Array<String>,
        val approveTask: TaskType,
        val approveTaskParameters: Map<String, Any?>,
        val rejectTask: TaskType,
        val rejectTaskParameters: Map<String, Any?>,
        val expiresOn: LocalDateTime,
        val rejectionReason: String? = null,
        val state: ApprovalRequestState = ApprovalRequestState.pending,
        val id: String = system.nextId(),
        val blockTillTaskComplete: Boolean = true,
        val dateTimeRequest: LocalDateTime = LocalDateTime.now(),
        val displayValues: Map<String, String> = emptyMap()


) {

    val requestByState: TaskRequest?
        get() {
            return when (state) {
                ApprovalRequestState.approved -> createRequest(approveTask, approveTaskParameters)
                ApprovalRequestState.rejected -> createRequest(rejectTask, rejectTaskParameters)
                else -> null
            }
        }

    private fun createRequest(taskType: TaskType, parameters: Map<String, Any?>): TaskRequest {
        return TaskRequest(
                module = taskType.module,
                task = taskType.name,
                parameters = parameters,
                process = process
        )
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ApprovalRequest

        if (process != other.process) return false
        if (headline != other.headline) return false
        if (description != other.description) return false
        if (!assignedTo.contentEquals(other.assignedTo)) return false
        if (approveTask != other.approveTask) return false
        if (approveTaskParameters != other.approveTaskParameters) return false
        if (rejectTask != other.rejectTask) return false
        if (rejectTaskParameters != other.rejectTaskParameters) return false
        if (expiresOn != other.expiresOn) return false
        if (rejectionReason != other.rejectionReason) return false
        if (state != other.state) return false
        if (id != other.id) return false
        if (blockTillTaskComplete != other.blockTillTaskComplete) return false
        if (dateTimeRequest != other.dateTimeRequest) return false
        if (displayValues != other.displayValues) return false

        return true
    }

    override fun hashCode(): Int {
        var result = process.hashCode()
        result = 31 * result + headline.hashCode()
        result = 31 * result + description.hashCode()
        result = 31 * result + assignedTo.contentHashCode()
        result = 31 * result + approveTask.hashCode()
        result = 31 * result + approveTaskParameters.hashCode()
        result = 31 * result + rejectTask.hashCode()
        result = 31 * result + rejectTaskParameters.hashCode()
        result = 31 * result + expiresOn.hashCode()
        result = 31 * result + (rejectionReason?.hashCode() ?: 0)
        result = 31 * result + state.hashCode()
        result = 31 * result + id.hashCode()
        result = 31 * result + blockTillTaskComplete.hashCode()
        result = 31 * result + dateTimeRequest.hashCode()
        result = 31 * result + displayValues.hashCode()
        return result
    }


}