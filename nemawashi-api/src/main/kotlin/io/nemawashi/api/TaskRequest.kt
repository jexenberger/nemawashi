/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.api

import io.nemawashi.util.system
import java.time.LocalDateTime
import java.time.LocalDateTime.now

data class TaskRequest(
        val module: ModuleId,
        val task: String,
        val parameters: Map<String, Any?>,
        val target: RunTarget? = null,
        val id: String = system.nextId(),
        val dateTime: LocalDateTime = now(),
        val user: String = system.user,
        val process: NemawashiProcess = NemawashiProcess()


) {

    val taskType: TaskType by lazy { TaskType(module, task) }

    constructor(task: String,
                target: RunTarget? = null,
                parameters: Map<String, Any?> = emptyMap()
    ) : this(module = TaskType.fromString(task).module,
            task = TaskType.fromString(task).name,
            target = target,
            parameters = parameters
    )
}

