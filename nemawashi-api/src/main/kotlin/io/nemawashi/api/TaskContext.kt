/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.api

import io.nemawashi.di.api.Context
import io.nemawashi.util.script.ScriptService
import java.time.LocalDateTime

interface TaskContext : Context {
    val taskRequest: TaskRequest
    val id: String
    val scriptingService: ScriptService
    override val bindings: RunContext
    val dateTimeCreated: LocalDateTime?
    val process: NemawashiProcess
    val classLoader: ClassLoader

    fun subContext(cloneBindings: Boolean): TaskContext

    fun cloneWithoutBindings(): TaskContext

}