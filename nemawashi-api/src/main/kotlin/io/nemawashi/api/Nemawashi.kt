/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.api

import io.nemawashi.api.approvals.ApprovalRequests
import io.nemawashi.api.resources.WritableResourceRepository
import io.nemawashi.api.services.FileService
import io.nemawashi.di.event.EventHandler
import io.nemawashi.doc.ModuleDoc
import io.nemawashi.doc.TaskDoc

import java.io.File

interface Nemawashi {

    val modules: Set<ModuleId>
    val resources: WritableResourceRepository
    val approvalRequests: ApprovalRequests
    val fileService: FileService

    fun runTask(request: TaskRequest, callback: ParameterCallback, runTarget: RunTarget): TaskExecution
    fun runTask(module: ModuleId, task: String, parameters: Map<String, Any?>, runTarget: RunTarget): TaskExecution
    fun runTask(file: File, parameters: Map<String, Any?>, callback: ParameterCallback, runTarget: RunTarget): TaskExecution

    fun moduleDoc(name: ModuleId): ModuleDoc?
    fun taskDoc(name: TaskType): TaskDoc?

    fun supportsDynamicType(file: File): Boolean

    fun shutdown()

    val eventHandlers: MutableSet<EventHandler<*>>

    val runningTasks: Collection<TaskExecution>

    fun modulesByName(name: String): Set<ModuleId> = modules.filter { it.name.equals(name) }.toSet()

    operator fun plusAssign(handler: EventHandler<*>) {
        eventHandlers += handler
    }

}