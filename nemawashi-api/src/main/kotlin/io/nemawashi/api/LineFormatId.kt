package io.nemawashi.api

import java.lang.IllegalArgumentException

class LineFormatId(val sourceModuleId: ModuleId, val fileName:String, val lineNo:Int, val threadId:String) {

    override fun toString(): String {
        return "[${sourceModuleId}\$$fileName\$$lineNo\$$threadId]"
    }

    companion object {
        fun fromString(str:String) : LineFormatId {
            if (!str.startsWith("[") && !str.endsWith("]")) {
                throw IllegalArgumentException("$str is not a valid LineFormatId format")
            }
            val parts = str.split("\$")
            if (parts.size != 4) {
                throw IllegalArgumentException("$str is not a valid LineFormatId format")
            }
            val sourceModuleId = ModuleId.fromString(parts[0].replace("[",""))
            val fileName = parts[1].replace("[","")
            val lineNo = parts[2].toInt()
            val threadId = parts[3].replace("]", "")
            return LineFormatId(sourceModuleId, fileName, lineNo, threadId)
        }
    }
}