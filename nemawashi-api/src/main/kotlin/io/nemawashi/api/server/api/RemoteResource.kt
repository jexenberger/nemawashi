/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.api.server.api

import io.nemawashi.api.Type
import io.nemawashi.api.resources.Resource
import io.nemawashi.api.resources.CategorisedType
import io.nemawashi.util.crypto.Secret

data class RemoteResource(
        val type: String,
        val id: String,
        val properties: List<ResourceProperty>
) {


    fun toResource() = Resource(CategorisedType.fromString(type), id, properties.map { it.name to it.type.convert<Any?>(it.value) }.toMap())


    companion object {
        fun fromResource(resource: Resource): RemoteResource {
            val properties = resource.properties.map {
                val type = it.value?.let { Type.typeForClass(it::class) ?: Type.any } ?: Type.any
                val value = when (it.value) {
                    is Secret -> it.value.toString()
                    else -> it.value
                }
                ResourceProperty(it.key, type, value)
            }
            return RemoteResource(resource.type.toString(), resource.id, properties)
        }
    }

}