/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.api

import io.nemawashi.api.resources.CategorisedType
import io.nemawashi.util.io.IOFunctions
import java.io.InputStream
import java.io.OutputStream

class FileResource(override val type: CategorisedType, override val id: String, val inputStream: InputStream) : CategorisedResource {


    val fileRef: FileRef by lazy {
        FileRef(type, id)
    }

    val text : String by lazy {
        try {
            val result = String(IOFunctions.readFully(inputStream))
            result
        } finally {
            if (inputStream.markSupported()) {
                inputStream.reset()
            }
        }
    }

    fun stream(target: OutputStream) {
        IOFunctions.pipe(inputStream, target)
        target.flush()
    }

}