import io.nemawashi.api.Type

/*
 *  Copyright 2018 Julian Exenberger
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */


task {

    description : "Standard Greeter task which takes your name then greets"

    parameters : {
        name : {
            description "Name of person that you want to greet"
            type Type.string
            required true
        }
    }

    returns: "Name of the person that was entered"

    script: {
        _sys.echo(value: "hello $name")
        1.times {
            _sys.echo(value: "hello $it")
        }
        def result =_http.get(
            uri: "https://www.google.com"
        )
        return result.status
    }
}