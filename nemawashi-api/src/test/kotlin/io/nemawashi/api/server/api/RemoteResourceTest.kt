/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.api.server.api

import io.nemawashi.api.resources.Resource
import io.nemawashi.api.resources.CategorisedType
import io.nemawashi.util.crypto.Secret
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class RemoteResourceTest {


    @Test
    internal fun testToResource() {
        val rs = Resource(CategorisedType("test", "test"), "test123", mapOf(
                "a" to "hello world",
                "b" to arrayOf(1, 2, 3),
                "c" to mapOf<String, Any>("hello" to "world"),
                "d" to setOf(true, false, true),
                "e" to Secret("hello world"))
        )
        val resource = RemoteResource.fromResource(rs)
        assertEquals(5, resource.properties.size)
        val newResource = resource.toResource()
        assertEquals(rs, newResource)

    }
}