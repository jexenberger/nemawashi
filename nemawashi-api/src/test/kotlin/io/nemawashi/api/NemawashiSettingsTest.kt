/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.api

import io.nemawashi.util.system
import org.junit.jupiter.api.Test
import java.io.FileReader
import java.util.*
import kotlin.reflect.full.declaredMemberProperties
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

class NemawashiSettingsTest {

    private val settingsFile = "src/test/resources/samplesettings.properties"
    private val emptyFile = "src/test/resources/emptysettings.properties"

    private val properties: Properties by lazy {
        val properties = Properties()
        properties.load(FileReader(settingsFile))
        properties
    }

    @Test
    internal fun testFromSettingsFile() {

        val settings = NemawashiSettings.fromSettingsFile(file = settingsFile)
        assertNotNull(settings)

        NemawashiSettings::class.declaredMemberProperties.forEach {
            properties[it.name]?.let { value ->
                val testString = it.get(settings).toString()
                //filter out paths of Windows
                if (!system.unixVariant && testString.indexOf("\\") == -1) {
                    assertEquals(value, testString, it.name)
                }
            }
        }
    }

    @Test
    internal fun testFromSettingsFileWithOverride() {
        val overrides = mapOf("globalKeyPath" to "qwerty.fredperte")
        val props = Properties()
        props.putAll(overrides)
        val settings = NemawashiSettings.fromSettingsFile(file = settingsFile, overrides = props)
        assertNotNull(settings)
        assertEquals("qwerty.fredperte", settings.globalKeyPath.path)
    }

    @Test
    internal fun testEmptySettingsFile() {
        val settings = NemawashiSettings.fromSettingsFile(file = emptyFile)
        assertNotNull(settings)
        assertEquals(NemawashiSettings.defaultKeyPath, settings.globalKeyPath)
        assertEquals(NemawashiSettings.defaultModulePath, settings.yamlModulePath)
    }

}