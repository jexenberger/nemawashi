## generate-markdown
### Description
 Generates Github style markdown from all the present module to a defined path
### Parameters
#### modules
|Property|Value|
|--------|-----|
|Description|List of modules for which documentation will be generated, accepts glob patterns|
|Type|stringArray|
|Required|true|
|Allowed pattern||
|Default||
---
#### outputPath
|Property|Value|
|--------|-----|
|Description|Directory/Folder to generate the output documentation in|
|Type|path|
|Required|true|
|Allowed pattern||
|Default||
---