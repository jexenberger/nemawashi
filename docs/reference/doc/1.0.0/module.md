## doc
#### Version
1.0.0
#### Description
Module to generate documentation from tasks
#### Tasks
|Task|Description|
|----|-----------|
|[generate-markdown](tasks/generate-markdown.md)|Generates Github style markdown from all the present module to a defined path|