## request
### Description
 Request an approval
### Parameters
#### approveTask
|Property|Value|
|--------|-----|
|Description|Task type to run if the task is approved|
|Type|taskType|
|Required|true|
|Allowed pattern||
|Default||
---
#### approveTaskParameters
|Property|Value|
|--------|-----|
|Description|Parameters for the approval task|
|Type|keyValue|
|Required|true|
|Allowed pattern||
|Default||
---
#### assignedTo
|Property|Value|
|--------|-----|
|Description|List of one or more users to assign to the approval|
|Type|stringArray|
|Required|true|
|Allowed pattern||
|Default||
---
#### description
|Property|Value|
|--------|-----|
|Description|Description for the approval request|
|Type|string|
|Required|true|
|Allowed pattern||
|Default||
---
#### expiresOn
|Property|Value|
|--------|-----|
|Description|Date time on which this approval request expires|
|Type|datetime|
|Required|true|
|Allowed pattern||
|Default||
---
#### headline
|Property|Value|
|--------|-----|
|Description|Headline for the approval request|
|Type|string|
|Required|true|
|Allowed pattern||
|Default||
---
#### rejectTask
|Property|Value|
|--------|-----|
|Description|Task type to run if the task is rejected|
|Type|taskType|
|Required|true|
|Allowed pattern||
|Default||
---
#### rejectTaskParameters
|Property|Value|
|--------|-----|
|Description|Parameters for the rejection task|
|Type|keyValue|
|Required|true|
|Allowed pattern||
|Default||
---