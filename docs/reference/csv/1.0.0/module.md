## csv
#### Version
1.0.0
#### Description
Module for working with CSV files
#### Tasks
|Task|Description|
|----|-----------|
|[append](tasks/append.md)|Appends a line to a CSV file|
|[read](tasks/read.md)|Reads each line of a CSV file, outputting each line as a keyValue|