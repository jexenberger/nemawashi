## append
### Description
 Appends a line to a CSV file
### Parameters
#### file
|Property|Value|
|--------|-----|
|Description|path to CSV file to append to|
|Type|path|
|Required|true|
|Allowed pattern||
|Default||
---
#### line
|Property|Value|
|--------|-----|
|Description|Array of values to append to the specified file|
|Type|stringArray|
|Required|true|
|Allowed pattern||
|Default||
---
#### quoteCharacter
|Property|Value|
|--------|-----|
|Description|Character to use to wrap the value in row, not wrapped if not specified|
|Type|char|
|Required|false|
|Allowed pattern||
|Default||
---
#### separator
|Property|Value|
|--------|-----|
|Description|Character to use to concatenate the line|
|Type|char|
|Required|true|
|Allowed pattern||
---