## read
### Description
 Reads each line of a CSV file, outputting each line as a keyValue
### Parameters
#### file
|Property|Value|
|--------|-----|
|Description|path to CSV file to parse|
|Type|path|
|Required|true|
|Allowed pattern||
|Default||
---
#### headers
|Property|Value|
|--------|-----|
|Description|headers in order of columns in the file|
|Type|stringArray|
|Required|false|
|Allowed pattern||
|Default||
---
#### outputVariable
|Property|Value|
|--------|-----|
|Description|name of variable to write each line as as keyValue key with the headers, if no headers are specified the position is used as a key|
|Type|string|
|Required|true|
|Allowed pattern||
|Default||
---
#### useFirstLineAsHeader
|Property|Value|
|--------|-----|
|Description|use the first line of the file to to define headers|
|Type|boolean|
|Required|true|
|Allowed pattern||
|Default||
---