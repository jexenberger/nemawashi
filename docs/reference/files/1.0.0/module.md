## files
#### Version
1.0.0
#### Description
Module to interact with the file service
#### Tasks
|Task|Description|
|----|-----------|
|[get](tasks/get.md)|Returns a reference to a file in file service|
|[list](tasks/list.md)|returns a list of File references for a specified type|
|[save](tasks/save.md)|Returns a reference to a file in file service, either takes a file or an array of bytes to store|