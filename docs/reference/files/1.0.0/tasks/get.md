## get
### Description
 Returns a reference to a file in file service
### Task Return
|Property|Value|
|--------|-----|
|Type | fileref|
|Description | |
### Parameters
#### id
|Property|Value|
|--------|-----|
|Description|id to the resource to define|
|Type|string|
|Required|true|
|Allowed pattern||
|Default||
---
#### type
|Property|Value|
|--------|-----|
|Description|Name of resource type to define|
|Type|string|
|Required|true|
|Allowed pattern||
|Default||
---