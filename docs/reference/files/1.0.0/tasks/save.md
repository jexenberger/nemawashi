## save
### Description
 Returns a reference to a file in file service, either takes a file or an array of bytes to store
### Task Return
|Property|Value|
|--------|-----|
|Type | fileref|
|Description | The new created file reference object|
### Parameters
#### content
|Property|Value|
|--------|-----|
|Description|Byte array to store|
|Type|byteArray|
|Required|true|
|Allowed pattern||
|Default||
---
#### file
|Property|Value|
|--------|-----|
|Description|Reference to a file to copy into the store|
|Type|path|
|Required|true|
|Allowed pattern||
|Default||
---
#### id
|Property|Value|
|--------|-----|
|Description|id to the resource to define|
|Type|string|
|Required|true|
|Allowed pattern||
|Default||
---
#### type
|Property|Value|
|--------|-----|
|Description|Name of resource type to define|
|Type|string|
|Required|true|
|Allowed pattern||
|Default||
---