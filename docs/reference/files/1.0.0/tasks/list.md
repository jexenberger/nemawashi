## list
### Description
 returns a list of File references for a specified type
### Task Return
|Property|Value|
|--------|-----|
|Type | filerefArray|
|Description | |
### Parameters
#### type
|Property|Value|
|--------|-----|
|Description|Name of resource type to define|
|Type|string|
|Required|true|
|Allowed pattern||
|Default||
---