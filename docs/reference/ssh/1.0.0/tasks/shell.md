## shell
### Description
 Executes a command against an existing sshSession, return the output
### Task Return
|Property|Value|
|--------|-----|
|Type | string|
|Description | Result of shell task|
### Parameters
#### cmd
|Property|Value|
|--------|-----|
|Description|Command to execute on the shell|
|Type|string|
|Required|true|
|Allowed pattern||
|Default||
---
#### echo
|Property|Value|
|--------|-----|
|Description|Echo output to the console|
|Type|boolean|
|Required|true|
|Allowed pattern||
|Default||
---
#### sessionVar
|Property|Value|
|--------|-----|
|Description|Name of variable which holds SSH Session, default is '$ssh'|
|Type|string|
|Required|true|
|Allowed pattern||
|Default||
---