## scp-to
### Description
 SCPs a local file to the remote filesystem
### Parameters
#### host
|Property|Value|
|--------|-----|
|Description|Host name or ip|
|Type|string|
|Required|true|
|Allowed pattern||
|Default||
---
#### hostsFile
|Property|Value|
|--------|-----|
|Description|Known hosts file|
|Type|path|
|Required|false|
|Allowed pattern||
---
#### keyFile
|Property|Value|
|--------|-----|
|Description|Key to use for authentication if using SSH key authentication|
|Type|path|
|Required|true|
|Allowed pattern||
|Default||
---
#### keyFilePassword
|Property|Value|
|--------|-----|
|Description|Password for key file|
|Type|string|
|Required|false|
|Allowed pattern||
|Default||
---
#### password
|Property|Value|
|--------|-----|
|Description|Host password (not required if key file is set)|
|Type|string|
|Required|false|
|Allowed pattern||
|Default||
---
#### port
|Property|Value|
|--------|-----|
|Description|Host port|
|Type|int|
|Required|true|
|Allowed pattern||
---
#### src
|Property|Value|
|--------|-----|
|Description|Source file to copy from remote SSH server|
|Type|string|
|Required|true|
|Allowed pattern||
|Default||
---
#### strictHostChecking
|Property|Value|
|--------|-----|
|Description|Flag to enable strict host checking, default is 'true'|
|Type|boolean|
|Required|true|
|Allowed pattern||
---
#### target
|Property|Value|
|--------|-----|
|Description|Target path to copy file to on local filesystem|
|Type|string|
|Required|true|
|Allowed pattern||
|Default||
---
#### user
|Property|Value|
|--------|-----|
|Description|Host user|
|Type|string|
|Required|true|
|Allowed pattern||
|Default||
---