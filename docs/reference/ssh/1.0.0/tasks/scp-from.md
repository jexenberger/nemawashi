## scp-from
### Description
 SCPs a file from a remote SSH server to the local filesystem
### Parameters
#### host
|Property|Value|
|--------|-----|
|Description|Host name or ip|
|Type|string|
|Required|true|
|Allowed pattern||
|Default||
---
#### hostsFile
|Property|Value|
|--------|-----|
|Description|Known hosts file|
|Type|path|
|Required|false|
|Allowed pattern||
---
#### keyFile
|Property|Value|
|--------|-----|
|Description|Key to use for authentication if using SSH key authentication|
|Type|path|
|Required|true|
|Allowed pattern||
|Default||
---
#### keyFilePassword
|Property|Value|
|--------|-----|
|Description|Password for key file|
|Type|string|
|Required|false|
|Allowed pattern||
|Default||
---
#### overwrite
|Property|Value|
|--------|-----|
|Description|Overwrite existing target file if it already exists, default is 'false'|
|Type|boolean|
|Required|true|
|Allowed pattern||
|Default||
---
#### password
|Property|Value|
|--------|-----|
|Description|Host password (not required if key file is set)|
|Type|string|
|Required|false|
|Allowed pattern||
|Default||
---
#### port
|Property|Value|
|--------|-----|
|Description|Host port|
|Type|int|
|Required|true|
|Allowed pattern||
---
#### src
|Property|Value|
|--------|-----|
|Description|Source file to copy|
|Type|string|
|Required|true|
|Allowed pattern||
|Default||
---
#### strictHostChecking
|Property|Value|
|--------|-----|
|Description|Flag to enable strict host checking, default is 'true'|
|Type|boolean|
|Required|true|
|Allowed pattern||
---
#### target
|Property|Value|
|--------|-----|
|Description|Target folder/name copy the file to|
|Type|string|
|Required|true|
|Allowed pattern||
|Default||
---
#### user
|Property|Value|
|--------|-----|
|Description|Host user|
|Type|string|
|Required|true|
|Allowed pattern||
|Default||
---