## session
### Description
 Group tasks manages an SSH session, can be used in conjuction with shell task
### Parameters
#### emulation
|Property|Value|
|--------|-----|
|Description|Character to split line, default is '\n'|
|Type|string|
|Required|true|
|Allowed pattern||
---
#### eol
|Property|Value|
|--------|-----|
|Description|Character to split line, default is '\n'|
|Type|string|
|Required|true|
|Allowed pattern||
---
#### host
|Property|Value|
|--------|-----|
|Description|Host name or ip|
|Type|string|
|Required|true|
|Allowed pattern||
|Default||
---
#### hostsFile
|Property|Value|
|--------|-----|
|Description|Known hosts file|
|Type|path|
|Required|false|
|Allowed pattern||
---
#### keyFile
|Property|Value|
|--------|-----|
|Description|Key to use for authentication if using SSH key authentication|
|Type|path|
|Required|true|
|Allowed pattern||
|Default||
---
#### keyFilePassword
|Property|Value|
|--------|-----|
|Description|Password for key file|
|Type|string|
|Required|false|
|Allowed pattern||
|Default||
---
#### password
|Property|Value|
|--------|-----|
|Description|Host password (not required if key file is set)|
|Type|string|
|Required|false|
|Allowed pattern||
|Default||
---
#### port
|Property|Value|
|--------|-----|
|Description|Host port|
|Type|int|
|Required|true|
|Allowed pattern||
---
#### prompt
|Property|Value|
|--------|-----|
|Description|Prompt to look for default is '$'|
|Type|string|
|Required|true|
|Allowed pattern||
---
#### sessionVar
|Property|Value|
|--------|-----|
|Description|name of variable to set default is '__ssh'|
|Type|string|
|Required|true|
|Allowed pattern||
---
#### strictHostChecking
|Property|Value|
|--------|-----|
|Description|Flag to enable strict host checking, default is 'true'|
|Type|boolean|
|Required|true|
|Allowed pattern||
---
#### user
|Property|Value|
|--------|-----|
|Description|Host user|
|Type|string|
|Required|true|
|Allowed pattern||
|Default||
---