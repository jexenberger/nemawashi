## ssh
#### Version
1.0.0
#### Description
Module for working with SSH
#### Tasks
|Task|Description|
|----|-----------|
|[exec](tasks/exec.md)|Executes a command against an SSH server|
|[scp-from](tasks/scp-from.md)|SCPs a file from a remote SSH server to the local filesystem|
|[scp-to](tasks/scp-to.md)|SCPs a local file to the remote filesystem|
|[session](tasks/session.md)|Group tasks manages an SSH session, can be used in conjuction with shell task|
|[shell](tasks/shell.md)|Executes a command against an existing sshSession, return the output|