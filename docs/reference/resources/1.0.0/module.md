## resources
#### Version
1.0.0
#### Description
Module for interacting with resources
#### Tasks
|Task|Description|
|----|-----------|
|[define](tasks/define.md)|Defines a resource against a specific type|
|[get](tasks/get.md)|Defines a resource against a specific type with an id, if the type and id combination exist the existing resource is overwritten|
|[query](tasks/query.md)|Queries the resources repository against a defined set of parameters|