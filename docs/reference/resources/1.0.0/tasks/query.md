## query
### Description
 Queries the resources repository against a defined set of parameters
### Task Return
|Property|Value|
|--------|-----|
|Type | iterator|
|Description | A list of resources that match the parameters, if none are found an empty list is returned|
### Parameters
#### parameters
|Property|Value|
|--------|-----|
|Description|Parameters against which to query|
|Type|keyValue|
|Required|true|
|Allowed pattern||
|Default||
---
#### type
|Property|Value|
|--------|-----|
|Description|Name of resource type to define|
|Type|string|
|Required|true|
|Allowed pattern||
|Default||
---