## define
### Description
 Defines a resource against a specific type
### Parameters
#### id
|Property|Value|
|--------|-----|
|Description|id to the resource to define|
|Type|string|
|Required|true|
|Allowed pattern||
|Default||
---
#### parameters
|Property|Value|
|--------|-----|
|Description|Parameters to define the resource|
|Type|keyValue|
|Required|true|
|Allowed pattern||
|Default||
---
#### type
|Property|Value|
|--------|-----|
|Description|Name of resource type to define|
|Type|string|
|Required|true|
|Allowed pattern||
|Default||
---