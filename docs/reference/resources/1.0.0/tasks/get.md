## get
### Description
 Defines a resource against a specific type with an id, if the type and id combination exist the existing resource is overwritten
### Task Return
|Property|Value|
|--------|-----|
|Type | resource|
|Description | The returned resource, null if not found|
### Parameters
#### id
|Property|Value|
|--------|-----|
|Description|id to the resource to define|
|Type|string|
|Required|true|
|Allowed pattern||
|Default||
---
#### type
|Property|Value|
|--------|-----|
|Description|Name of resource type to define|
|Type|string|
|Required|true|
|Allowed pattern||
|Default||
---