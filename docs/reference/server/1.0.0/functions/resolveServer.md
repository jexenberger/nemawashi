## resolveServer
#### Description
takes an input reference and tries to resolve the server 
#### Parameters
|Name|Description|
|----|-----------|
|serverRef|references of the server that you wish to use |