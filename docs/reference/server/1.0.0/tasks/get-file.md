## get-file
### Description
 Retrieves a file from a server and saves it locally, returns null of not found
### Task Return
|Property|Value|
|--------|-----|
|Type | fileref|
|Description | |
### Parameters
#### id
|Property|Value|
|--------|-----|
|Description|file id|
|Type|string|
|Required|true|
|Allowed pattern||
|Default||
---
#### path
|Property|Value|
|--------|-----|
|Description|Local path to save file, will save the file id as name if file is directory, overwrites if it exists|
|Type|path|
|Required|true|
|Allowed pattern||
|Default||
---
#### server
|Property|Value|
|--------|-----|
|Description|Server on which to set the resource|
|Type|string|
|Required|true|
|Allowed pattern||
|Default||
---
#### type
|Property|Value|
|--------|-----|
|Description|file type|
|Type|string|
|Required|true|
|Allowed pattern||
|Default||
---