## add-reference
### Description
 Adds a reference to the local server registry
### Parameters
#### name
|Property|Value|
|--------|-----|
|Description|Name to use for the server reference|
|Type|string|
|Required|true|
|Allowed pattern||
|Default||
---
#### password
|Property|Value|
|--------|-----|
|Description|password to use|
|Type|string|
|Required|true|
|Allowed pattern||
|Default||
---
#### url
|Property|Value|
|--------|-----|
|Description|URL of the server|
|Type|string|
|Required|true|
|Allowed pattern||
|Default||
---
#### user
|Property|Value|
|--------|-----|
|Description|user to use|
|Type|string|
|Required|true|
|Allowed pattern||
|Default||
---
#### validateSSL
|Property|Value|
|--------|-----|
|Description|To validate SSL or not|
|Type|boolean|
|Required|true|
|Allowed pattern||
---