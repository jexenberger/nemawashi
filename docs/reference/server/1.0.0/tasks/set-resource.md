## set-resource
### Description
 Sets a remote resource against a server
### Parameters
#### resource
|Property|Value|
|--------|-----|
|Description|Resource to set|
|Type|keyValue|
|Required|true|
|Allowed pattern||
|Default||
---
#### server
|Property|Value|
|--------|-----|
|Description|Server on which to set the resource|
|Type|string|
|Required|true|
|Allowed pattern||
|Default||
---