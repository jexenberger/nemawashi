## get-resource
### Description
 Retrieves a resource from a remote server
### Task Return
|Property|Value|
|--------|-----|
|Type | unit|
|Description | |
### Parameters
#### id
|Property|Value|
|--------|-----|
|Description|resource id|
|Type|string|
|Required|true|
|Allowed pattern||
|Default||
---
#### server
|Property|Value|
|--------|-----|
|Description|Server on which to set the resource|
|Type|string|
|Required|true|
|Allowed pattern||
|Default||
---
#### type
|Property|Value|
|--------|-----|
|Description|resource type|
|Type|string|
|Required|true|
|Allowed pattern||
|Default||
---