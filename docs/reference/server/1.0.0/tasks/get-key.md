## get-key
### Description
 Retrieves a server key and saves it locally
### Parameters
#### path
|Property|Value|
|--------|-----|
|Description|Path to which to save the key, default is the system key|
|Type|path|
|Required|true|
|Allowed pattern||
|Default||
---
#### server
|Property|Value|
|--------|-----|
|Description|Server on which to set the resource|
|Type|string|
|Required|true|
|Allowed pattern||
|Default||
---