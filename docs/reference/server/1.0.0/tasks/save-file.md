## save-file
### Description
 Saves a local file to a remote server
### Parameters
#### id
|Property|Value|
|--------|-----|
|Description|file id to use, if empty file name will be used|
|Type|string|
|Required|true|
|Allowed pattern||
|Default||
---
#### path
|Property|Value|
|--------|-----|
|Description|Path to file on local system|
|Type|path|
|Required|true|
|Allowed pattern||
|Default||
---
#### server
|Property|Value|
|--------|-----|
|Description|Server on which to save the file|
|Type|string|
|Required|true|
|Allowed pattern||
|Default||
---
#### type
|Property|Value|
|--------|-----|
|Description|file type|
|Type|string|
|Required|true|
|Allowed pattern||
|Default||
---