## server
#### Version
1.0.0
#### Description
Module for interacting with a Nemawashi server
#### Tasks
|Task|Description|
|----|-----------|
|[add-reference](tasks/add-reference.md)|Adds a reference to the local server registry|
|[get-file](tasks/get-file.md)|Retrieves a file from a server and saves it locally, returns null of not found|
|[get-key](tasks/get-key.md)|Retrieves a server key and saves it locally|
|[get-resource](tasks/get-resource.md)|Retrieves a resource from a remote server|
|[save-file](tasks/save-file.md)|Saves a local file to a remote server|
|[set-resource](tasks/set-resource.md)|Sets a remote resource against a server|
#### Functions
|Task|Description|
|----|-----------|
|[resolveServer](functions/resolveServer.md)|takes an input reference and tries to resolve the server |