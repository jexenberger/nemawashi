## basicAuth
#### Description
Generates a basic auth string from the passed parameters
#### Parameters
|Name|Description|
|----|-----------|
|user|User|
|password|Password|