## escapeURL
#### Description
Escapes a string into a URL encoded string
#### Parameters
|Name|Description|
|----|-----------|
|string|String to encode|