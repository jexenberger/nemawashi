## http
#### Version
1.0.0
#### Description
module to perform HTTP operations
#### Tasks
|Task|Description|
|----|-----------|
|[get](tasks/get.md)|Performs an HTTP GetFileReference|
|[post](tasks/post.md)|Performs an HTTP Post|
|[put](tasks/put.md)|Performs an HTTP Put|
#### Functions
|Task|Description|
|----|-----------|
|[basicAuth](functions/basicAuth.md)|Generates a basic auth string from the passed parameters|
|[escapeURL](functions/escapeURL.md)|Escapes a string into a URL encoded string|