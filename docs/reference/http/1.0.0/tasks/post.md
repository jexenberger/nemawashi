## post
### Description
 Performs an HTTP Post
### Task Return
|Property|Value|
|--------|-----|
|Type | keyValue|
|Description | returns the 'status', 'message', 'body' and 'headers' as keyValue|
### Parameters
#### body
|Property|Value|
|--------|-----|
|Description|Body to submit with request|
|Type|string|
|Required|false|
|Allowed pattern||
|Default||
---
#### encodeQueryParameters
|Property|Value|
|--------|-----|
|Description|Encode the Query parameters (if any), default is true|
|Type|boolean|
|Required|false|
|Allowed pattern||
---
#### headers
|Property|Value|
|--------|-----|
|Description|Headers for the HTTP operation|
|Type|keyValue|
|Required|false|
|Allowed pattern||
|Default||
---
#### outputFile
|Property|Value|
|--------|-----|
|Description|Path of an output file to dump result to|
|Type|path|
|Required|false|
|Allowed pattern||
|Default||
---
#### parameters
|Property|Value|
|--------|-----|
|Description|Parameters for the HTTP operation|
|Type|keyValue|
|Required|false|
|Allowed pattern||
|Default||
---
#### proxyHost
|Property|Value|
|--------|-----|
|Description|Proxy server to use for the request (can be set in system or globally in ~/.nw/proxy.settings)|
|Type|string|
|Required|false|
|Allowed pattern||
|Default||
---
#### proxyPassword
|Property|Value|
|--------|-----|
|Description|Proxy password to use for the request (can be set in system or globally in ~/.nw/proxy.settings)|
|Type|string|
|Required|false|
|Allowed pattern||
|Default||
---
#### proxyPort
|Property|Value|
|--------|-----|
|Description|Proxy port to use for the request (can be set in system or globally in ~/.nw/proxy.settings)|
|Type|int|
|Required|false|
|Allowed pattern||
---
#### proxyUser
|Property|Value|
|--------|-----|
|Description|Proxy user to use for the request (can be set in system or globally in ~/.nw/proxy.settings)|
|Type|string|
|Required|false|
|Allowed pattern||
|Default||
---
#### uri
|Property|Value|
|--------|-----|
|Description|URI to call|
|Type|string|
|Required|true|
|Allowed pattern||
|Default||
---
#### validateHostName
|Property|Value|
|--------|-----|
|Description|Validate Hostname on SSL connection|
|Type|boolean|
|Required|false|
|Allowed pattern||
---
#### validateSSL
|Property|Value|
|--------|-----|
|Description|Validate SSL connection|
|Type|boolean|
|Required|false|
|Allowed pattern||
---