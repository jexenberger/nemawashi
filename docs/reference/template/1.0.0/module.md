## template
#### Version
1.0.0
#### Description
Module for generating mustache template
#### Tasks
|Task|Description|
|----|-----------|
|[generate](tasks/generate.md)|Generates output from a MVEL template from a file or string template|