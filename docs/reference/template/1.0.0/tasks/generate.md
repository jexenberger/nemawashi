## generate
### Description
 Generates output from a MVEL template from a file or string template
### Task Return
|Property|Value|
|--------|-----|
|Type | string|
|Description | The result of the evaluation of the MVEL template|
### Parameters
#### injectContext
|Property|Value|
|--------|-----|
|Description|Inject all parameters into the context|
|Type|boolean|
|Required|true|
|Allowed pattern||
|Default||
---
#### parameters
|Property|Value|
|--------|-----|
|Description|Parameters to generate the template with|
|Type|keyValue|
|Required|true|
|Allowed pattern||
|Default||
---
#### template
|Property|Value|
|--------|-----|
|Description|File or template content|
|Type|string|
|Required|true|
|Allowed pattern||
|Default||
---