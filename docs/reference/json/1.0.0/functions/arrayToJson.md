## arrayToJson
#### Description
Takes an array type and writes it in JSON format, returns the value in a String
#### Parameters
|Name|Description|
|----|-----------|
|array|array of values |