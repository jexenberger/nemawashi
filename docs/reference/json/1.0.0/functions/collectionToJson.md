## collectionToJson
#### Description
Takes a collection which is Iterable in JSON format, returns the value in a String
#### Parameters
|Name|Description|
|----|-----------|
|collection|collection of values|