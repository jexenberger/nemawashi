## to-json
### Description
 Turns a Map of Maps into a JSON string
### Task Return
|Property|Value|
|--------|-----|
|Type | string|
|Description | the input variable as a json string|
### Parameters
#### input
|Property|Value|
|--------|-----|
|Description|set of parameters to dump|
|Type|keyValue|
|Required|true|
|Allowed pattern||
|Default||
---