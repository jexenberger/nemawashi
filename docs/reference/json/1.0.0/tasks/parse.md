## parse
### Description
 Parses a json string into a Map of Maps or List of Maps
### Task Return
|Property|Value|
|--------|-----|
|Type | any|
|Description | The deserialized Object|
### Parameters
#### json
|Property|Value|
|--------|-----|
|Description|JSON string to parse|
|Type|string|
|Required|true|
|Allowed pattern||
|Default||
---