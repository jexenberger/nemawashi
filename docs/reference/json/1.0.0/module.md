## json
#### Version
1.0.0
#### Description
Module for processing json
#### Tasks
|Task|Description|
|----|-----------|
|[parse](tasks/parse.md)|Parses a json string into a Map of Maps or List of Maps|
|[to-json](tasks/to-json.md)|Turns a Map of Maps into a JSON string|
#### Functions
|Task|Description|
|----|-----------|
|[arrayToJson](functions/arrayToJson.md)|Takes an array type and writes it in JSON format, returns the value in a String|
|[collectionToJson](functions/collectionToJson.md)|Takes a collection which is Iterable in JSON format, returns the value in a String|