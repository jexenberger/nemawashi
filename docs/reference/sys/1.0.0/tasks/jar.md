## jar
### Description
 Loads a jar file and makes it available in scripts
### Parameters
#### path
|Property|Value|
|--------|-----|
|Description|Path or a jar file or a directory contains a set of jar files.|
|Type|path|
|Required|true|
|Allowed pattern||
|Default||
---