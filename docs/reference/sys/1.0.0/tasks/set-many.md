## set-many
### Description
 Batch sets a set of variables
### Parameters
#### parent
|Property|Value|
|--------|-----|
|Description|Sets the value in a parent scope|
|Type|boolean|
|Required|true|
|Allowed pattern||
---
#### variables
|Property|Value|
|--------|-----|
|Description|a keyValue map of variables to set|
|Type|keyValue|
|Required|true|
|Allowed pattern||
|Default||
---