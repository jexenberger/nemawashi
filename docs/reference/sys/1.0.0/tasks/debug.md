## debug
### Description
 Displays a value to the system console if debug is enabled
### Parameters
#### type
|Property|Value|
|--------|-----|
|Description|Colour the output, valid options are 'red', 'green' or 'default'|
|Type|enumeration|
|Required|true|
|Allowed pattern|red | green | default|
---
#### value
|Property|Value|
|--------|-----|
|Description|Value to echo to the Console|
|Type|any|
|Required|true|
|Allowed pattern||
|Default||
---