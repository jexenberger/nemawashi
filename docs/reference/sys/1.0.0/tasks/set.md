## set
### Description
 Displays a value to the system console
### Parameters
#### parent
|Property|Value|
|--------|-----|
|Description|Sets the value in a parent scope|
|Type|boolean|
|Required|true|
|Allowed pattern||
---
#### value
|Property|Value|
|--------|-----|
|Description|value to set for the against the name in the variable|
|Type|any|
|Required|false|
|Allowed pattern||
|Default||
---
#### variable
|Property|Value|
|--------|-----|
|Description|value to set for the against the name in the variable|
|Type|string|
|Required|true|
|Allowed pattern||
|Default||
---