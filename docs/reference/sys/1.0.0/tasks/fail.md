## fail
### Description
 Fails the current task execution with a message
### Parameters
#### message
|Property|Value|
|--------|-----|
|Description|message to display|
|Type|string|
|Required|true|
|Allowed pattern||
|Default||
---