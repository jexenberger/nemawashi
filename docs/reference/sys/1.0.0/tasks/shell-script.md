## shell-script
### Description
 Run a list of commands, returns a keyValue with 'success' to indicate the result and and int array of 'processResults for the result of each command'
### Task Return
|Property|Value|
|--------|-----|
|Type | keyValue|
|Description | |
### Parameters
#### dir
|Property|Value|
|--------|-----|
|Description|directory to evaluate the command in, default is pwd|
|Type|string|
|Required|true|
|Allowed pattern||
|Default||
---
#### resume
|Property|Value|
|--------|-----|
|Description|Carry on executing if a command fails, default is false|
|Type|boolean|
|Required|true|
|Allowed pattern||
---
#### script
|Property|Value|
|--------|-----|
|Description|List of commands to execute|
|Type|stringArray|
|Required|true|
|Allowed pattern||
|Default||
---