## new-instance
### Description
 Creates a new instance of a class, can be used to call classes loaded via the 'jar' task
### Task Return
|Property|Value|
|--------|-----|
|Type | any|
|Description | |
### Parameters
#### args
|Property|Value|
|--------|-----|
|Description|Array of values to pass as a constructor, |
|Type|anyArray|
|Required|true|
|Allowed pattern||
|Default||
---
#### type
|Property|Value|
|--------|-----|
|Description|Fully qualified class name of the desired type to create a new instance of|
|Type|string|
|Required|true|
|Allowed pattern||
|Default||
---