## foreach
### Description
 iterates over any iterable value
### Parameters
#### items
|Property|Value|
|--------|-----|
|Description|Item collection over which to iterate, works with any Array or Iterable type|
|Type|iterator|
|Required|true|
|Allowed pattern||
|Default||
---
#### iteratorVarName
|Property|Value|
|--------|-----|
|Description|Variable name to hold the iterator, default is '__iteratorVar'|
|Type|string|
|Required|true|
|Allowed pattern||
---
#### varName
|Property|Value|
|--------|-----|
|Description|Variable name of the current value of the iterator, default is '__var'|
|Type|string|
|Required|true|
|Allowed pattern||
---