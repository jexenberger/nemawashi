## echo
### Description
 Displays a value to system out
### Parameters
#### type
|Property|Value|
|--------|-----|
|Description|Colour the output, valid options are 'red', 'green' or 'default'|
|Type|enumeration|
|Required|true|
|Allowed pattern|red | green | default|
---
#### value
|Property|Value|
|--------|-----|
|Description|Value to echo to system out|
|Type|any|
|Required|false|
|Allowed pattern||
|Default||
---