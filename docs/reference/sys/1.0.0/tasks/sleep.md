## sleep
### Description
 Sleeps for a defined set of milliseconds
### Parameters
#### duration
|Property|Value|
|--------|-----|
|Description|Defined set of milliseconds|
|Type|long|
|Required|true|
|Allowed pattern||
---