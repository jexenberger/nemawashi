## paginate
### Description
 Breaks an iterator into a subset of items of a a defined size
### Parameters
#### items
|Property|Value|
|--------|-----|
|Description|Item collection over which to iterate, works with any Array or Iterable type|
|Type|iterator|
|Required|true|
|Allowed pattern||
|Default||
---
#### pageSize
|Property|Value|
|--------|-----|
|Description|Size for each page|
|Type|int|
|Required|true|
|Allowed pattern||
---
#### varName
|Property|Value|
|--------|-----|
|Description|Variable for the page, default is '__page'|
|Type|string|
|Required|true|
|Allowed pattern||
---