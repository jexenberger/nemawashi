## shell
### Description
 Runs a command against the system shell, 'bin/sh' in *nix and cmd.exe on windows
### Task Return
|Property|Value|
|--------|-----|
|Type | keyValue|
|Description | returns a keyValue with return code mapped to 'result' and the script output mapped to 'output'|
### Parameters
#### cmd
|Property|Value|
|--------|-----|
|Description|command to evaluate|
|Type|string|
|Required|true|
|Allowed pattern||
|Default||
---
#### dir
|Property|Value|
|--------|-----|
|Description|directory to evaluate the command in, default is pwd|
|Type|string|
|Required|true|
|Allowed pattern||
|Default||
---
#### echo
|Property|Value|
|--------|-----|
|Description|Echo output to the screen, default it true|
|Type|boolean|
|Required|true|
|Allowed pattern||
|Default||
---