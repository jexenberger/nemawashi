## group
### Description
 Groups a set of related tasks together, allows the tasks to be executed conditionally or in parallel
### Parameters