## script
### Description
 Run a script either directly as text of from a file
### Task Return
|Property|Value|
|--------|-----|
|Type | any|
|Description | Result of the script|
### Parameters
#### language
|Property|Value|
|--------|-----|
|Description|Language of the script|
|Type|enumeration|
|Required|true|
|Allowed pattern|javascript | mvel | groovy|
---
#### script
|Property|Value|
|--------|-----|
|Description|Script string or File path|
|Type|string|
|Required|true|
|Allowed pattern||
|Default||
---