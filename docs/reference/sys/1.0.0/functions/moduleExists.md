## moduleExists
#### Description
Checks if a module exists with a version string
#### Parameters
|Name|Description|
|----|-----------|
|name|Name of the module to check|
|version|Version in string format to check|