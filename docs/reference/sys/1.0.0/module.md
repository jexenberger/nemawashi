## sys
#### Version
1.0.0
#### Description
Core system module
#### Tasks
|Task|Description|
|----|-----------|
|[debug](tasks/debug.md)|Displays a value to the system console if debug is enabled|
|[echo](tasks/echo.md)|Displays a value to system out|
|[fail](tasks/fail.md)|Fails the current task execution with a message|
|[foreach](tasks/foreach.md)|iterates over any iterable value|
|[group](tasks/group.md)|Groups a set of related tasks together, allows the tasks to be executed conditionally or in parallel|
|[jar](tasks/jar.md)|Loads a jar file and makes it available in scripts|
|[new-instance](tasks/new-instance.md)|Creates a new instance of a class, can be used to call classes loaded via the 'jar' task|
|[paginate](tasks/paginate.md)|Breaks an iterator into a subset of items of a a defined size|
|[script](tasks/script.md)|Run a script either directly as text of from a file|
|[set](tasks/set.md)|Displays a value to the system console|
|[set-many](tasks/set-many.md)|Batch sets a set of variables|
|[shell](tasks/shell.md)|Runs a command against the system shell, 'bin/sh' in *nix and cmd.exe on windows|
|[shell-script](tasks/shell-script.md)|Run a list of commands, returns a keyValue with 'success' to indicate the result and and int array of 'processResults for the result of each command'|
|[sleep](tasks/sleep.md)|Sleeps for a defined set of milliseconds|
#### Functions
|Task|Description|
|----|-----------|
|[moduleExists](functions/moduleExists.md)|Checks if a module exists with a version string|