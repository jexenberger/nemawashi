## Modules
|Name|Description|
|----|-----------|
|[sys@1.0.0](sys/1.0.0/module.md)|Core system module|
|[resources@1.0.0](resources/1.0.0/module.md)|Module for interacting with resources|
|[http@1.0.0](http/1.0.0/module.md)|module to perform HTTP operations|
|[template@1.0.0](template/1.0.0/module.md)|Module for generating mustache template|
|[json@1.0.0](json/1.0.0/module.md)|Module for processing json|
|[server@1.0.0](server/1.0.0/module.md)|Module for interacting with a Nemawashi server|
|[doc@1.0.0](doc/1.0.0/module.md)|Module to generate documentation from tasks|
|[approvals@1.0.0](approvals/1.0.0/module.md)|This module is used to create and manage approvals|
|[csv@1.0.0](csv/1.0.0/module.md)|Module for working with CSV files|
|[files@1.0.0](files/1.0.0/module.md)|Module to interact with the file service|
|[ssh@1.0.0](ssh/1.0.0/module.md)|Module for working with SSH|