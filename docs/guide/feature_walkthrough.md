## Feature Walkthrough

### Tasks
In Nemawashi a task is how all functionality is organised and executed. Tasks can in turn also call other tasks and thus can 
build up complex pieces of functionality. A task can literally be any type of functionality that the user requires to do.

Tasks can be authored in any of the following:
* Kotlin - Nemawashi's native language
* Groovy - Apache super flexible and dynamic scripting language for the JVM
* YAML - Nemawashi's YAML based DSL which can be used to simple readable task creation.

Tasks authored in any of the approaches are interchangeable. A YAML task can call a Kotlin task, that can call a Groovy task and vicae versa.

Furthermore there are three different types of tasks that can be created
* Simple Task - This is a simple task that can be used to execute a piece of functionality
* Functional Task - Like a simple task but returns a value
* Group Task - A task meant to provide functionality for a group of tasks.An example is something like iterating over a result set or managing a session.

All tasks have must have well defined parameters, these are introspected by the Nemawashi runtime. Parameters must be one of the supported [types](types.md).

Nemawashi also provides a set of [built-in tasks](../reference/modules.md) which come bundled out of the box.

### Resources
Resources are unstructured key-value type records which can be stored against a defined category and type. Resources provide a database for storing any type of data that you may need during the execution of tasks. 

Nemawashi provides a set of built in tasks which can be used to [maintain and query Resources](../reference/resources/1.0.0/module.md).

### Approvals
Approvals are a simple user driven Approve/Decline requests that can used when user input is required to perform a task. This can be used im a scenario where you need a person to approve or reject some of other action. 

For example you may have a task to create a new instance in AWS EC2. However since this has a potential financial impact you may want to first have a superior approve the request.

Approvals then give you the ability to create simple workflows.

### Server
Nemawashi can be run as a standalone script type application from the command line. However it can also be run as a server. This allows you to create an appliance for running tasks which against a shared central instance. 

The server provides the following benefits:
* An API for accessing the servers Resource store, meaning configuration can be shared across an organization.
* Shared functionality accessible from a single point, without the need to distribute task definitions across the organisation n
* A lightweight web based UX for executing tasks



 