## Standard Types
Standard types that are supported in the Nemawashi Runtime

### Groovy script usage
When using Groovy scripts, the type is imported as the Enumeration `io.nemawashi.api.Type`

### Type list
|Type|Yaml Name|Description|
|----|---------|-----------|
|any|any|Any type|
|iterable|iterable|Any type which can be iterated  over|
|string|string|Standard string|
|char|char|Standard character|
|int|int|32 bit signed integer value|
|long|long|64 bit signed integer value|
|boolean|boolean|true,false, yes, no, on, off|
|float|float|32 bit signed floating point value|
|double|double|64 bit signed floating point value|
|path|path|Reference to a file on the file system|
|enumeration|enum|Fixed enumerated value|
|anyArray|any[]|Array of any types|
|fileArray|path[]|Array of file types|
|stringArray|string[]|Array of string types|
|intArray|int[]|Array of int types|
|byteArray|byte[]|Array of int types|
|longArray|long[]|Array of long types|
|floatArray|float[]|Array of float types|
|doubleArray|double[]|Array of double types|
|iterator|iterator|Enumeration type|
|keyValue|keyValue|Set of key=value mappings|
|resource|resource|Resource stored in the resource registry|
|fileref|fileref|Reference to a file in the file store registry|
|filerefArray|fileref[]|List of file store references|
|secret|secret|A value which needs to be securely stored|
|date|date|A date value|
|time|time|A time value|
|datetime|datetime|A timestamp value|
|regex|regex|A regular expression value for matching|
|taskType|taskType|Nemawashi task type|
|unit|unit|void|
