### Nemawashi quick start

Once you've installed Nemawashi, open your command line and type:

```
nw
```

This will launch Nemawashi and display the basic usage and options.

Now we are ready to do something useful. Nemawashi is based around the concept of tasks. A task is simply a packaged unit of execution which takes zero or more defined parameters and executes some functionality.

Lets run a task in Nemawashi to simply echo 'Hello' on the display. To do this we need to run a task. We will use one of the build in tasks 'echo' to display hello world world to the screen.

Once again on your command line type:

```
nw run sys::echo
```

When you are promoted enter `Hello world`. You should see the following output with `Hello world` being displayed

```
Enter value (Value to echo to the Console) default value: ''
Required: : Hello world
Hello world
=== 1 -> [sys@LATEST::echo] ( TOOK 80ms)
DONE - Took 6551ms
```


What happened here? To run a task you to need to call the `run` command. The parameter for the run command is the task you want to run in this case `sys::echo`. The format of this string is `[module name]::[task name]`. In the case `sys` is the core system module which contains a set of low level built-in tasks and `echo` is the name of the task inside the system module that we want to execute.

Now that we have run a task, lets create our own task using Nemawashi's YAML based automation language.

First lets create a directory called 'greeter' and cd into it.

Now open a text editor and enter the following:

```
description: A simple greeter task
parameters:
    - name:
        description: Name of the person that you want to greet

tasks:
    - sys::echo:
        value: ${'Hello ' + name}
```

Once you are done, save the this as 'greeter.yaml' and then on your command line run:

```
nw run greeter.yaml
```

You will be prompted to enter the name and then you will get the following output:

```
Enter name (Name of the person that you want to greet)
Required: : John
Hello John
=== ./::greeter.yaml@7 -> [sys@LATEST::echo] ( TOOK 141ms)
=== 1 -> [./@LATEST::greeter] ( TOOK 2848ms)
DONE - Took 6484ms
```


What have we done here. We have declared a simple task which takes a single parameter `name` and then calls the echo task to where the `name` parameter is concatenated with _Hello_ using the expression `${'Hello ' + name}` which is passed to the `value` parameter which then displays the result of the expression.

You will note we had to put a `description` attribute at the top of the task, as well as a description for the parameter. The task would not have compiled otherwise. This is because one of the Nemawashi's principles is that documentation should be directly baked into the tasks and to to make them self describing.

The last thing we going to do for this tutorial is to turn the greeter task into a module.

inside your directory, open a text editor and enter the following.

```
description: A simple greeter module
```


Now save the file inside the directory as 'module.conf'

Now once again from the command line run:

```
nw run greeter::greeter
```


You will once again be prompted for the name and once entered you will have something along the lines of:

```
Enter name (Name of the person that you want to greet)
Required: : John
Hello John
=== greeter::greeter.yaml@7 -> [sys@LATEST::echo] ( TOOK 142ms)
=== 1 -> [greeter@LATEST::greeter] ( TOOK 2865ms)
DONE - Took 7011ms
```

Congratulations you have written and executed your first Nemawashi task and module.