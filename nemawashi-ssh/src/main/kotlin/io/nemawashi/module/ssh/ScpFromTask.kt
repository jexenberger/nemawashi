package io.nemawashi.module.ssh

import io.nemawashi.api.*
import io.nemawashi.api.annotations.Parameter
import io.nemawashi.api.annotations.Task
import java.io.File

@Task("scp-from", description = "SCPs a file from a remote SSH service to the local filesystem", returnType = Type.unit)
class ScpFromTask : BaseSSHTask(), SimpleTask {

    @Parameter(description = "Source file to copy")
    var src = ""

    @Parameter(description = "Target folder/name copy the file to")
    var target = ""

    @Parameter(description = "Overwrite existing target file if it already exists, default is 'false'")
    var overwrite = false

    override fun run(id: TaskId, ctx: RunContext)  {
        val file = File(target)
        if (!file.exists() && !file.mkdirs()) {
            throw TaskFailedException(id, "Target $target cannot be created as a directory")
        }
        if (file.isFile && !overwrite) {
            throw TaskFailedException(id, "$target already exists and overwrite disabled")
        }
        doInSession(id) { session ->
            session.scpFrom(target, src)?.let { throw TaskFailedException(id, "Unable to scp -> $it") }
        }
    }

}