package io.nemawashi.module.ssh

import io.nemawashi.api.*
import io.nemawashi.api.annotations.Parameter
import io.nemawashi.api.annotations.Task
import java.io.File

@Task("scp-to", description = "SCPs a local file to the remote filesystem", returnType = Type.unit)
class ScpToTask : BaseSSHTask(), SimpleTask {

    @Parameter(description = "Source file to copy from remote SSH service")
    var src = ""

    @Parameter(description = "Target path to copy file to on local filesystem")
    var target = ""


    override fun run(id: TaskId, ctx: RunContext) {
        val file = File(src)
        if (!file.exists()) {
            throw TaskFailedException(id, "$src does not exist")
        }
        doInSession(id) { session ->
            session.scpTo(src, target)?.let { throw TaskFailedException(id, "Unable to copy to -> $it") }
        }
    }
}