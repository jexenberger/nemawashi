package io.nemawashi.module.ssh

import io.nemawashi.api.*
import io.nemawashi.api.annotations.Parameter
import io.nemawashi.api.annotations.Task
import io.nemawashi.util.io.console.Console
import io.nemawashi.util.ssh.SSHSession

@Task("shell", description = "Executes a command against an existing sshSession, return the output", returnType = Type.string, returnDescription = "Result of shell task")
class ShellTask : FunctionalTask<String> {


    @Parameter(description = "Name of variable which holds SSH Session, default is '\$ssh'")
    var sessionVar: String = "\$ssh"

    @Parameter(description = "Command to execute on the shell")
    var cmd: String = ""

    @Parameter(description = "Echo output to the console")
    var echo: Boolean = true


    override fun evaluate(id: TaskId, ctx: RunContext): String? {
        if (!ctx.containsKey(sessionVar)) {
            throw TaskFailedException(id, "$sessionVar is not present in context")
        }
        val session = ctx[sessionVar]!!
        if (!(session is SSHSession)) {
            throw TaskFailedException(id, "$sessionVar is not an instance of ${SSHSession::class.qualifiedName}")
        }
        var buffer = ""
        session.shell(cmd) {
            buffer += it
            if (echo) {
                Console.display(it)
            }
        }
        return buffer
    }
}