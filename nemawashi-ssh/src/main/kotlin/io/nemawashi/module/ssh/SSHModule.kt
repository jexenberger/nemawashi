package io.nemawashi.module.ssh

import io.nemawashi.api.KotlinModule
import io.nemawashi.api.NemawashiModule

class SSHModule : KotlinModule("ssh", description = "Module for working with SSH") {


    init {
        add(ExecTask::class)
        add(ScpFromTask::class)
        add(ScpToTask::class)
        add(ShellTask::class)
        add(SSHSessionGroupTask::class)
    }

    companion object {
        @JvmStatic
         fun provider(): NemawashiModule {
            return SSHModule()
        }
    }
}

