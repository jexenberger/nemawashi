package io.nemawashi.module.ssh

import io.nemawashi.api.*
import io.nemawashi.api.annotations.Parameter
import io.nemawashi.api.annotations.Task
import io.nemawashi.util.ssh.SSHSessionBuilder

@Task("session", description = "Group tasks manages an SSH session, can be used in conjuction with shell task", returnType = Type.unit)
class SSHSessionGroupTask : BaseSSHTask(), GroupTask {

    @Parameter(description = "name of variable to set default is '__ssh'", default = "__ssh")
    var sessionVar: String = "__ssh"

    @Parameter(description = "Character to split line, default is '\\n'", default = "\n")
    var eol: String = "\n"


    @Parameter(description = "Character to split line, default is '\\n'", default = "vt100")
    var emulation: String = "vt100"

    @Parameter(description = "Prompt to look for default is '\$'", default = "\$")
    var prompt: String = "\$"

    override fun additionalSettings(builder: SSHSessionBuilder) {
        builder.keepAlive()
                .emulation(emulation)
                .eol(eol)
                .prompt(prompt)
    }

    override fun before(id: TaskId, ctx: RunContext): Directive {
        val session = startSession(id)
        if (session == null) {
            ctx[sessionVar] = this.session!!
        }
        session?.let {
            throw TaskFailedException(id, "Unable to create SSH session -> $it")
        }
        return Directive.continueExecution
    }

    override fun after(id: TaskId, ctx: RunContext) = Directive.done

    override fun onFinally(id: TaskId, ctx: RunContext) {
        shutdown()
    }

    override fun onError(id: TaskId, error: TaskError, ctx: RunContext) {
        shutdown()
        super.onError(id, error, ctx)
    }

}