package io.nemawashi.module.ssh

import io.nemawashi.api.TaskFailedException
import io.nemawashi.api.TaskId
import io.nemawashi.api.annotations.Parameter
import io.nemawashi.util.ssh.SSHSession
import io.nemawashi.util.ssh.SSHSessionBuilder
import java.io.File

open class BaseSSHTask {

    @Parameter(description = "Host name or ip")
    var host: String = ""
    @Parameter(description = "Host port", default = "22")
    var port: Int = 22
    @Parameter(description = "Host user")
    var user: String = ""
    @Parameter(description = "Host password (not required if key file is set)", required = false)
    var password: String? = null
    @Parameter(description = "Known hosts file", default = "~/.ssh/known_hosts", required = false)
    var hostsFile: File? = null
    @Parameter(description = "Key to use for authentication if using SSH key authentication")
    var keyFile: File? = null
    @Parameter(description = "Password for key file", required = false)
    var keyFilePassword: String? = null

    @Parameter(description = "Flag to enable strict host checking, default is 'true'", default = "true")
    var strictHostChecking: Boolean = true

    var session: SSHSession? = null

    protected fun startSession(id: TaskId): String? {
        val builder = SSHSessionBuilder(user, host, port)
                .strictHostChecking(strictHostChecking)
        hostsFile?.let { builder.hostsFile = it.absolutePath }
        when {
            keyFile != null -> builder.keyFile(keyFile!!, keyFilePassword)
            password != null -> builder.password(password!!)
            else -> throw TaskFailedException(id, "Has neither a password or a key file specified")
        }
        additionalSettings(builder)
        return builder.session.whenL { session = it }.right
    }

    open fun additionalSettings(builder: SSHSessionBuilder) {

    }

    protected fun shutdown() {
        session?.disconnect()
    }


    protected fun <T> doInSession(id: TaskId, handler: (SSHSession) -> T?): T? {
        try {
            return startSession(id)?.let {
                throw TaskFailedException(id, "Unable to create SSH Session -> $it")
            } ?: session?.use(handler)
        } finally {
            session?.close()
        }
    }

}