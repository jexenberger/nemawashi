package io.nemawashi.module.ssh

import io.nemawashi.api.FunctionalTask
import io.nemawashi.api.RunContext
import io.nemawashi.api.TaskId
import io.nemawashi.api.Type
import io.nemawashi.api.annotations.Parameter
import io.nemawashi.api.annotations.Task
import io.nemawashi.util.io.console.Console

@Task("exec", description = "Executes a command against an SSH service", returnType = Type.string, returnDescription = "Result of exec")
class ExecTask : BaseSSHTask(), FunctionalTask<String> {


    @Parameter(description = "Command to execute")
    var cmd: String = ""

    @Parameter(description = "Echo output to screen")
    var echo: Boolean = true


    override fun evaluate(id: TaskId, ctx: RunContext): String? {
        return doInSession(id) {
            var buffer = ""
            it.exec(cmd) { line ->
                buffer += line + "\n"
                if (echo) {
                    Console.display(line).newLine()
                }
            }
            buffer
        }
    }
}