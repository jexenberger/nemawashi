package io.nemawashi.module.ssh

import io.nemawashi.BaseSSHTest
import io.nemawashi.api.Directive
import io.nemawashi.api.MapRunContext
import io.nemawashi.api.TaskId
import io.nemawashi.api.TaskType
import io.nemawashi.setupTask
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class SSHSessionGroupTaskTest : BaseSSHTest() {

    @Test
    internal fun testRun() {
        val task = SSHSessionGroupTask()
        val context = MapRunContext()
        val taskId = TaskId(TaskType(SSHModule().id, "test"), "test")

        setupTask(task)

        try {
            val directive = task.before(taskId, context)
            assertEquals(Directive.continueExecution, directive)
            assertTrue { context.containsKey(task.sessionVar) }
            assertTrue { task.session?.connected ?: false }
        } finally {
            task.onFinally(taskId, context)
        }
        assertFalse { task.session?.connected ?: false }
    }
}