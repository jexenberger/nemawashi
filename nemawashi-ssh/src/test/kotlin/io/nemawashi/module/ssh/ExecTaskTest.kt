package io.nemawashi.module.ssh

import io.nemawashi.BaseSSHTest
import io.nemawashi.api.MapRunContext
import io.nemawashi.api.TaskId
import io.nemawashi.api.TaskType
import io.nemawashi.setupTask
import org.junit.jupiter.api.Test
import kotlin.test.assertNotNull

class ExecTaskTest : BaseSSHTest() {


    @Test
    internal fun testEvaluate() {
        val task = ExecTask()
        setupTask(task)

        task.cmd = "ls"

        val context = MapRunContext()
        val taskId = TaskId(TaskType(SSHModule().id, "test"), "test")
        val result = task.evaluate(taskId, context)
        assertNotNull(result)

    }

}