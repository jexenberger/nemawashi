package io.nemawashi.module.ssh

import io.nemawashi.BaseSSHTest
import io.nemawashi.api.MapRunContext
import io.nemawashi.api.TaskId
import io.nemawashi.api.TaskType
import io.nemawashi.util.ssh.SSHSessionBuilder
import org.junit.jupiter.api.Test
import kotlin.test.assertNotNull

class ShellTaskTest: BaseSSHTest() {

    @Test
    internal fun testEvaluate() {
        val context = MapRunContext()
        val taskId = TaskId(TaskType(SSHModule().id, "test"), "test")
        val session = SSHSessionBuilder(settings.sshUser, settings.sshHost, port = 20022)
                .password(settings.sshPassword)
                .hostsFile(settings.sshKnownHosts)
                .strictHostChecking(false)
                .emulation("vt100")
                .keepAlive()
                .prompt("\$")
                .eol("\n")
                .session.left

        val shellTask = ShellTask()
        shellTask.cmd = "ls"
        shellTask.sessionVar = "test"
        context["test"] = session
        val result = shellTask.evaluate(taskId, context)
        assertNotNull(result)

    }
}