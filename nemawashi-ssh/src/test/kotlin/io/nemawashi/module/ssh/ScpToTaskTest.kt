package io.nemawashi.module.ssh

import io.nemawashi.BaseScpSSHTest
import io.nemawashi.api.MapRunContext
import io.nemawashi.api.TaskId
import io.nemawashi.api.TaskType
import io.nemawashi.setupTask
import io.nemawashi.util.system
import org.junit.jupiter.api.Test
import java.io.File
import java.util.*
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class ScpToTaskTest : BaseScpSSHTest() {

    @Test
    internal fun testRun() {
        val context = MapRunContext()
        val taskId = TaskId(TaskType(SSHModule().id, "test"), "test")
        val task = ScpToTask()

        setupTask(task)
        val file = File.createTempFile("ssh", "testtask")
        file.deleteOnExit()
        var uuid = UUID.randomUUID().toString()
        for (i in 1..1000) {
            uuid += UUID.randomUUID().toString()
        }
        file.appendText(uuid)
        task.src = file.absolutePath
        task.target = system.homeDir
        task.run(taskId, context)

        val targetFile = File(system.homeDir, file.name)
        targetFile.deleteOnExit()
        assertTrue { targetFile.exists() }
        assertEquals(uuid, targetFile.readText())

    }
}