/*
 *  Copyright 2018 Julian Exenberger
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package io.nemawashi

import io.nemawashi.module.ssh.BaseSSHTask
import io.nemawashi.util.ssh.MockSSHServer
import io.nemawashi.util.ssh.SSHServer
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach

abstract class BaseSSHInteractiveTest {

    val settings: SSHServer = SSHServer()


    @BeforeEach
    open fun setUp() {

        MockSSHServer.start(interactive = true)
    }

    @AfterEach
    open fun tearDown() {
        MockSSHServer.stop()
    }

    fun setupTask(task: BaseSSHTask) {
        task.host = settings.sshHost
        task.user = settings.sshUser
        task.password = settings.sshPassword
        task.hostsFile = settings.sshKnownHosts
        task.strictHostChecking = false
        task.port = 20022
    }
}