module io.nemawashi.di.test {

    exports io.nemawashi.di.sample.repo;
    exports io.nemawashi.di.sample.model;

    requires transitive io.nemawashi.util;
    requires io.nemawashi.di;

    requires kotlin.stdlib;
    requires kotlin.reflect;

    requires java.scripting;

}