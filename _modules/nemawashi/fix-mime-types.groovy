/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */


import nemawashi.nw
import java.io.File

nw.task {

    description "Creates the mime type mappings for the server, for a cut and paste job of the interwebs"

    parameters {
    }

    script {

        def file = new File("${__modulePath}/files/mime-types.properties")
        def targetFile = new File("nemawashi-server/src/main/resources/mime-types.properties")
        if (targetFile.exists()) {
            targetFile.delete()
        }

        file.eachLine {
            println(it)
            def parts = it.split("\t")
            def type = parts[0]?.trim()
            def mimeType = parts[1]?.trim()
            type = type.substring(1)
            targetFile.append("$type=$mimeType\n")
        }


    }

}