/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

import java.io.File

import io.nemawashi.api.Type
import nemawashi.nw

nw.task {

    description "Generates markdown for the standard types in the build and adds into the documentation"

    script {

        def file = new File('docs/guide/types.md')
        file.delete()
        def buffer = "## Standard Types\n"
        buffer += "Standard types that are supported in the Nemawashi Runtime\n\n"
        buffer += "### Groovy script usage\n"
        buffer += "When using Groovy scripts, the type is imported as the Enumeration `io.nemawashi.api.Type`\n\n"
        buffer += "### Type list\n"
        buffer += "|Type|Yaml Name|Description|\n"
        buffer += "|----|---------|-----------|\n"

        Type.values().each { type ->
            buffer += "|${type.name()}|${type.stringDescriptor}|${type.description}|\n"
        }

        println(buffer)
        file.append(buffer)




    }
}