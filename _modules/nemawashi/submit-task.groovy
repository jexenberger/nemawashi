/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

import io.nemawashi.api.Type
import nemawashi.nw

nw.task {

    name "submit-task"
    description "Submit a task change by creating sucessfully completing a build and doing a merge request"

    parameters {
        message "GIT commit message"
        assignee "Gitlab user to assign the Merge Request to"
    }

    script {

        def settings = $nw.resources.get(
                id: 'mygitlabconfig',
                type: 'nemawashi::gitlabconfig'
        )

        if (settings == null) {
            $nw.fail(message: 'Gitlab configuration not found please run nemawashi::setup')
        }

        def userId = $nw.gitlab.get_userid(token: settings.token, project: settings.project, user: assignee)
        def buildResult = $nw.shell(cmd: 'mvn clean install -B', echo: true)

        if (buildResult.output.indexOf('BUILD FAILURE') > -1) {
            $nw.fail(message: 'BUILD FAILED!')
        }

        def branch = $nw.shell(cmd: 'git symbolic-ref --short HEAD', echo: true)

        def issue = branch.output.split('-')[0]

        $nw.doc.generate_markdown(outputPath: 'docs/reference', modules: [
                "sys*",
                "doc*",
                "http*",
                "json*",
                "resources*",
                "server*",
                "ssh*",
                "template*",
                "approvals*",
                "files*",
                "csv*"
        ])

        $nw.nemawashi.generate_type_reference()

        def gitScript = $nw.sys.shell_script(script: [
                'git add --all',
                "git commit -m '$issue  --- $message'",
                'git push'
        ])


        //fail if the push wasn't sucessfull
        if (!gitScript.success && gitScript.processResults[2] != 1 ) {
            for (def i : gitScript.processResults) {
                $nw.sys.echo(value: "-> $i")
            }
            $nw.sys.fail(message: "unable to commit and push code $gitScript")
        }

        $nw.gitlab.create_mr(
                token: settings.token,
                assignee: userId,
                project: settings.project,
                src: branch.output,
                target: 'master',
                title: "PR for issue - $issue",
                description: message,
                milestone: settings.milestone
        )

    }


}