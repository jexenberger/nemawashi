/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.module.mongodb

import com.mongodb.client.MongoCollection
import com.mongodb.client.MongoIterable
import com.mongodb.client.model.Aggregates
import io.nemawashi.api.TaskFailedException
import io.nemawashi.api.TaskId
import io.nemawashi.api.Type
import io.nemawashi.api.annotations.Parameter
import io.nemawashi.api.annotations.Task
import io.nemawashi.api.annotations.TaskContext
import io.nemawashi.api.services.TemplateService
import io.nemawashi.di.annotation.Inject
import org.bson.BsonDocument
import org.bson.BsonValue
import org.bson.Document
import org.bson.RawBsonDocument
import org.bson.conversions.Bson

@Task("aggregate", description = "Group task which runs a aggregationPipeline and then iterates over the result set", returnType = Type.unit)
class Aggregate(
        @Parameter(description = "collection to aggregationPipeline")
        collection: String,
        @Parameter(description = "list of aggregation stages, use stage as such 'project' or 'group' as key names for relevant stage")
        stages: Map<String, String>,
        @Parameter(description = "name of variable to get the current mongo session '__mongo'")
        sessionVariableName: String = defaultSessionName,
        @Parameter(description = "database to use for the aggregationPipeline", default = "local")
        database: String = "local",
        @Parameter(description = "keyValue of parameters to pass the aggregationPipeline")
        val parameters: Map<String, Any?> = emptyMap(),
        @TaskContext
        queryContext: MongoQueryContext,
        @Inject
        val templateService: TemplateService

) : BaseMongoLookup<Map<String, String>>(
        collection = collection,
        sessionVariableName = sessionVariableName,
        database = database,
        query = stages,
        queryContext = queryContext) {

    override fun createQuery(id:TaskId, mongoCollection: MongoCollection<Document>, queryString: Map<String, String>?): MongoIterable<Document> {
        val actualMap = queryString!!
        val stagesList = actualMap.map { (k, v) ->
            val name = if (!k.startsWith("\$")) "\$$k" else k
            BsonDocument(name, BsonDocument.parse(templateService.writeToString(v, parameters)))
        }
        return mongoCollection.aggregate(stagesList.toMutableList())
    }

}