/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.module.mongodb

import com.mongodb.MongoClientSettings
import com.mongodb.MongoCredential
import com.mongodb.ServerAddress
import com.mongodb.client.MongoClient
import com.mongodb.client.MongoClients
import io.nemawashi.api.*
import io.nemawashi.api.annotations.Parameter
import io.nemawashi.api.annotations.Task
import java.util.logging.Level
import java.util.logging.Logger

const val defaultSessionName = "__mongo"


@Task("session", description = "Group task which provides a MongoClient object within the scope of this group task", returnType = Type.unit)
class MongoDbSession(
        @Parameter(description = "List of servers with their port, default is localhost: 27017")
        val servers: Map<String, String> = mapOf("localhost" to "27017"),
        @Parameter(description = "User to use to connect to mongo")
        val user: String? = null,
        @Parameter(description = "Password to use to connect to mongo")
        val password: String = "",
        @Parameter(description = "Authentication database")
        val authenticationData: String = "admin",
        @Parameter(description = "name of variable to set default is '__mongo'")
        val varName:String = defaultSessionName,
        @Parameter(description = "enable SSL for the mongo connection")
        val enableSSL:Boolean = false
) : GroupTask {


    override fun before(id: TaskId, ctx: RunContext): Directive {
        val mongoClient = MongoClients.create(
                createSettings()
        )
        ctx[varName] = mongoClient
        return Directive.continueExecution
    }

    private fun createSettings(): MongoClientSettings {
        val credential = user?.let { MongoCredential.createCredential(user, authenticationData, password.toCharArray()) }
        val settingsBuilder = MongoClientSettings
                .builder()
                .applyToClusterSettings {
                    val servers = servers.map { (host, port) -> ServerAddress(host, port.toInt()) }
                    it.hosts(servers)
                }
                .applyToSslSettings {
                    it.enabled(enableSSL)
                }
        credential?.let { settingsBuilder.credential(credential) }
        return settingsBuilder.build()
    }



    override fun after(id: TaskId, ctx: RunContext) = Directive.done

    override fun onFinally(id: TaskId, ctx: RunContext) {
        ctx.getAs<MongoClient>(varName)?.let {
            ctx - varName
            try {
                it.close()
            } finally {

            }
        }
    }

    companion object {
        init {
            val mongoLogger = Logger.getLogger( "org.mongodb.driver.cluster" );
            mongoLogger.level = Level.SEVERE;
        }
    }
}