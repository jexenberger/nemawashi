/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.module.mongodb

import com.mongodb.client.MongoClient
import com.mongodb.client.MongoCollection
import com.mongodb.client.MongoCursor
import com.mongodb.client.MongoIterable
import io.nemawashi.api.*
import org.bson.Document

abstract class BaseMongoLookup<T>(
        val collection: String,
        val queryContext: MongoQueryContext,
        val query: T? = null,
        val database: String = "local",
        val sessionVariableName: String = defaultSessionName) : IterableGroupTask<Document>() {

    override fun initIterator(id:TaskId, ctx: RunContext) = createMongoQuery(ctx, id)

    private fun createMongoQuery(ctx: RunContext, id: TaskId): MongoCursor<Document> {
        val session = ctx.getAs<MongoClient>(sessionVariableName)
                ?: throw TaskFailedException(id, "expected a mongo session instance for variable: '$sessionVariableName'")
        val sessionDatabase = session.getDatabase(database)
        val mongoCollection = sessionDatabase.getCollection(collection)
        val resultSet = createQuery(id, mongoCollection, query)

        val resultSetIterable = resultSet.iterator()
        return resultSetIterable
    }

    abstract fun createQuery(id:TaskId, mongoCollection: MongoCollection<Document>, query: T?): MongoIterable<Document>
}