/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.module.mongodb

import com.mongodb.client.MongoClient
import com.mongodb.client.MongoCollection
import io.nemawashi.api.TaskFailedException
import io.nemawashi.api.RunContext
import io.nemawashi.api.SimpleTask
import io.nemawashi.api.TaskId
import io.nemawashi.api.annotations.Parameter
import io.nemawashi.api.services.TemplateService
import io.nemawashi.di.annotation.Inject
import org.bson.Document

abstract class BaseWriteTask(@Parameter(description = "Json value to insert, can be a template value")
                             val json: String, @Parameter(description = "collection to query")
                             val collection: String, @Inject
                             val templateService: TemplateService, @Parameter(description = "database to use for the query", default = "local")
                             val database: String = "local", @Parameter(description = "name of variable to get the current mongo session '__mongo'")
                             val sessionVariableName: String = defaultSessionName) : SimpleTask {
    override fun run(id: TaskId, ctx: RunContext) {
        val session = ctx.getAs<MongoClient>(sessionVariableName)
                ?: throw TaskFailedException(id, "excpected a mongo session instance for variable: '$sessionVariableName'")
        val sessionDatabase = session.getDatabase(database)
        val mongoCollection = sessionDatabase.getCollection(collection)
        val record = templateService.writeToString(json, ctx)
        handleWrite(id, ctx, mongoCollection, record)
    }

    abstract fun handleWrite(id: TaskId, ctx: RunContext, mongoCollection: MongoCollection<Document>, record: String)


}