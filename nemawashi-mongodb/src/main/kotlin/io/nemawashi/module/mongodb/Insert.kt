/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.module.mongodb

import com.mongodb.client.MongoClient
import com.mongodb.client.MongoCollection
import io.nemawashi.api.*
import io.nemawashi.api.annotations.Parameter
import io.nemawashi.api.annotations.Task
import io.nemawashi.api.services.TemplateService
import io.nemawashi.di.annotation.Inject
import org.bson.Document

@Task("insert", description = "Inserts a document into a specified collection", returnType = Type.unit)
class Insert(
        @Parameter(description = "Json value to insert, can be a template value")
        json:String,
        @Parameter(description = "collection to query")
        collection: String,
        @Parameter(description = "name of variable to get the current mongo session '__mongo'")
        sessionVariableName: String = defaultSessionName,
        @Parameter(description = "database to use for the query", default = "local")
        database: String = "local",
        @Inject
        templateService: TemplateService
) : BaseWriteTask(
        json = json,
        collection = collection,
        sessionVariableName = sessionVariableName,
        database = database,
        templateService = templateService) {

    override fun handleWrite(id:TaskId, ctx: RunContext, mongoCollection: MongoCollection<Document>, record: String) {
        mongoCollection.insertOne(Document.parse(record))
    }

}