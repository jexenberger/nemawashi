/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.module.mongodb

import com.mongodb.client.FindIterable
import com.mongodb.client.MongoClient
import com.mongodb.client.MongoCollection
import io.nemawashi.api.*
import io.nemawashi.api.annotations.Parameter
import io.nemawashi.api.annotations.Task
import io.nemawashi.api.annotations.TaskContext
import io.nemawashi.api.services.TemplateService
import io.nemawashi.di.annotation.Inject
import org.bson.Document

@Task("find", description = "Group task which runs a query and then iterates over the result set", returnType = Type.unit)
class Find(
        @Parameter(description = "collection to query")
        collection: String,
        @Parameter(description = "name of variable to get the current mongo session '__mongo'")
        sessionVariableName: String = defaultSessionName,
        @Parameter(description = "database to use for the query", default = "local")
        database: String = "local",
        @Parameter(description = "query to run, this value supports templates, leaving empty will query whole collection")
        query: String? = null,
        @Parameter(description = "projection to add to query string (if specified)")
        val projection: String? = null,
        @Parameter(description = "keyValue of parameters to pass the query")
        val parameters: Map<String, Any?> = emptyMap(),
        @Parameter(description = "assign a count to the result, rather an iterating over the query results")
        count: Boolean = false,
        @TaskContext
        queryContext: MongoQueryContext,
        @Inject
        val templateService: TemplateService

) : BaseMongoLookup<String>(
        collection = collection,
        sessionVariableName = sessionVariableName,
        database = database,
        query = query,
        queryContext = queryContext) {

    override fun createQuery(id:TaskId, mongoCollection: MongoCollection<Document>, queryString: String?): FindIterable<Document> {
        val queryToUse = queryString?.let { templateService.writeToString(it, parameters) } ?: "{}"
        val query = Document.parse(queryToUse)
        val resultSet = mongoCollection.find(query)

        if (projection != null) {
            resultSet.projection(Document.parse(projection))
        }
        return resultSet
    }

}