/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */


import io.nemawashi.api.NemawashiModule;
import io.nemawashi.module.mongodb.MongoDbModule;

open module io.nemawashi.mongodb {

    requires io.nemawashi.di;
    requires io.nemawashi.api;

    requires transitive io.nemawashi.util;

    requires kotlin.stdlib;
    requires kotlin.reflect;

    requires java.scripting;
    requires semantic.version;

    requires org.mongodb.bson;
    requires org.mongodb.driver.core;
    requires org.mongodb.driver.sync.client;
    requires java.logging;
    
    
    provides NemawashiModule with MongoDbModule;

}