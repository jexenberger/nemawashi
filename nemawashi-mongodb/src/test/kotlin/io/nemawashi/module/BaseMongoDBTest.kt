/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.module

import com.mongodb.client.MongoClient
import com.mongodb.client.MongoClients
import com.mongodb.client.MongoCollection
import com.mongodb.client.MongoDatabase
import de.flapdoodle.embed.mongo.MongodExecutable
import de.flapdoodle.embed.mongo.MongodProcess
import de.flapdoodle.embed.mongo.MongodStarter
import de.flapdoodle.embed.mongo.config.MongoCmdOptionsBuilder
import de.flapdoodle.embed.mongo.config.MongodConfigBuilder
import de.flapdoodle.embed.mongo.config.Net
import de.flapdoodle.embed.mongo.distribution.Version
import de.flapdoodle.embed.process.runtime.Network
import org.bson.Document
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.TestInstance

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
abstract class BaseMongoDBTest {
    var mongod: MongodProcess? = null
    var mongodExecutable: MongodExecutable? = null


    private fun startUp() {
        val starter = MongodStarter.getDefaultInstance()
        val bindIp = "localhost"
        val port = 12345
        val mongodConfig = MongodConfigBuilder()
                .version(Version.Main.PRODUCTION)
                .cmdOptions(MongoCmdOptionsBuilder()
                        .build())
                .net(Net(bindIp, port, Network.localhostIsIPv6()))
                .build()

        mongodExecutable = starter.prepare(mongodConfig)
        mongod = mongodExecutable!!.start()!!
    }


    fun doWithLocal(db:String, collection:String, handler:(MongoDatabase, MongoCollection<Document>, MongoClient) -> Unit) {
        var client: MongoClient? = null
        try {
            client = MongoClients.create("mongodb://localhost:12345")
            val database = client.getDatabase(db)
            val collection = database.getCollection(collection)
            handler(database, collection, client)
        } finally {
            client?.close()
        }

    }


    @AfterAll
    internal fun tearDown() {
        teadDownDb()
        mongod!!.stop()
        mongodExecutable!!.stop()
    }

    @BeforeAll
    internal fun setUp() {
        startUp()
        initialiseDB()
    }


    open fun initialiseDB() {

    }

    open fun teadDownDb() {

    }




}