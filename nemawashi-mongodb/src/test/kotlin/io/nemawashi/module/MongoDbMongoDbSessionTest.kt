/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.module

import com.mongodb.MongoSecurityException
import com.mongodb.client.MongoClient
import com.mongodb.client.model.CreateCollectionOptions
import de.flapdoodle.embed.mongo.MongodExecutable
import de.flapdoodle.embed.mongo.MongodProcess
import de.flapdoodle.embed.mongo.MongodStarter
import de.flapdoodle.embed.mongo.config.MongoCmdOptionsBuilder
import de.flapdoodle.embed.mongo.config.MongodConfigBuilder
import de.flapdoodle.embed.mongo.config.Net
import de.flapdoodle.embed.mongo.distribution.Version
import de.flapdoodle.embed.process.runtime.Network
import io.nemawashi.api.MapRunContext
import io.nemawashi.api.TaskId
import io.nemawashi.module.mongodb.MongoDbSession
import org.junit.jupiter.api.*
import java.lang.IllegalStateException
import kotlin.test.assertEquals
import kotlin.test.assertTrue
import kotlin.test.expect


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class MongoDbMongoDbSessionTest : BaseMongoDBTest(){




    @Test
    internal fun testBefore() {

        val mongoDbSession = MongoDbSession(
                servers = mapOf("localhost" to "12345")
        )
        val ctx = MapRunContext()
        val id = TaskId.fromString("test::test")
        mongoDbSession.before(id, ctx)
        assertTrue { ctx.containsKey(mongoDbSession.varName) }
        val client = ctx.getAs<MongoClient>(mongoDbSession.varName)!!
        val database = client.getDatabase("test")
        database.createCollection("test")
        val dbs = client.listDatabaseNames().count()
        assertTrue { dbs > 0 }
        mongoDbSession.onFinally(id, ctx)
        try {
            client.listDatabaseNames().count()
            fail("should have been closed")
        } catch (e:IllegalStateException) {

        }

    }


    @Test()
    internal fun testBeforeWithAuthentication() {
        assertThrows<MongoSecurityException> {
            val mongoDbSession = MongoDbSession(
                    servers = mapOf("localhost" to "12345"),
                    user = "admin",
                    password = "admin"
            )
            val ctx = MapRunContext()
            mongoDbSession.before(TaskId.fromString("test::test"), ctx)
            assertTrue { ctx.containsKey(mongoDbSession.varName) }
            val client = ctx.getAs<MongoClient>(mongoDbSession.varName)!!
            client.listDatabaseNames().forEach { println(it) }
            try {
                client.close()
            } catch (e: Exception) {

            }
        }


    }


}