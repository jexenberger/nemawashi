/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.module

import com.mongodb.client.model.Filters.eq
import io.mockk.every
import io.mockk.mockkClass
import io.nemawashi.api.MapRunContext
import io.nemawashi.api.TaskId
import io.nemawashi.api.services.TemplateService
import io.nemawashi.module.mongodb.Insert
import io.nemawashi.module.mongodb.defaultSessionName
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

class InsertTest : BaseMongoWithDBTest() {


    @Test
    internal fun testRun() {
        val ctx = MapRunContext()
        val templateService = mockkClass(TemplateService::class, relaxed = true)
        val document = "{ 'one': 1, 'two': 2}"
        every { templateService.writeToString(document, ctx) } returns document
        val insert = Insert(
                json = document,
                templateService = templateService,
                collection = "to_insert",
                database = database
        )

        doWithLocal(database, "to_insert") { db, collection, client ->
            ctx[defaultSessionName] = client
            insert.run(TaskId.fromString("test::test"), ctx)
            val result = collection.find(eq("one", 1)).first()
            assertNotNull(result)
            assertEquals(1, result?.getInteger("one"))
            assertEquals(2, result?.getInteger("two"))
        }
    }
}