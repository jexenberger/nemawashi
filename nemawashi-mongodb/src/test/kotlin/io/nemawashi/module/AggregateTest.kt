/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.module

import io.mockk.every
import io.mockk.mockkClass
import io.nemawashi.api.Directive
import io.nemawashi.api.MapRunContext
import io.nemawashi.api.TaskId
import io.nemawashi.api.services.TemplateService
import io.nemawashi.module.mongodb.Aggregate
import io.nemawashi.module.mongodb.MongoDbSession
import io.nemawashi.module.mongodb.MongoQueryContext
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertTrue

class AggregateTest : BaseMongoWithDBTest() {

    private fun checkDocuments(find: Aggregate, id: TaskId, ctx: MapRunContext, expectedAmt: Int) {
        var cnt = 0
        while (find.before(id, ctx) == Directive.continueExecution) {
            cnt += 1
            val document = ctx.getAs<Map<String, Any?>>(find.varName)
            println(document)
            assertNotNull(document)
            assertTrue { document.containsKey("_id") }
            assertTrue { document.containsKey("sumVals") }
        }
        assertEquals(expectedAmt, cnt)
    }

    @Test
    internal fun testFindBefore() {
        val ctx = MapRunContext()
        val id = TaskId.fromString("test::test")
        val find = createAggregate(id, ctx, "{ \"theValue\": { \$gt: 10 ,  \$lte : 50 } }", "{'theValue' : 1}")

        checkDocuments(find, id, ctx, 1)
    }



    private fun createAggregate(id: TaskId, ctx: MapRunContext, query: String?, projection: String?): Aggregate {
        val queries = mapOf<String, String>(
                "group" to "{ _id: 'theName',  total: { \$sum : '\$theValue'} }",
                "project" to "{ sumVals: { \$sum : '\$vals'} }"
        )
        val templateService = mockkClass(TemplateService::class, relaxed = true)
        queries.forEach { t, u ->
            every {   templateService.writeToString(u, any()) } returns u
        }
        val mongoDbSession = MongoDbSession(servers = mapOf("localhost" to "12345"))
        val queryContext = MongoQueryContext()
        mongoDbSession.before(id, ctx)
        val find = Aggregate(
                collection = "the_cool_collection",
                stages = queries,
                database = "qwerty",
                queryContext = queryContext,
                templateService = templateService
        )
        return find
    }

}