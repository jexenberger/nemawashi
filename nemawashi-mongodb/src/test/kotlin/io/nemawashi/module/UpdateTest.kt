/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.module

import com.mongodb.client.model.Filters.eq
import com.mongodb.client.model.Updates
import io.mockk.every
import io.mockk.mockkClass
import io.nemawashi.api.MapRunContext
import io.nemawashi.api.TaskId
import io.nemawashi.api.services.TemplateService
import io.nemawashi.module.mongodb.Update
import io.nemawashi.module.mongodb.defaultSessionName
import org.bson.Document
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

class UpdateTest : BaseMongoWithDBTest() {

    override fun initialiseDB() {
        val docs = Updates.combine(Updates.set("stars", 0), Updates.currentDate("lastModified"))
        println(docs)
        doWithLocal(database, "to_update") { db, coll, d ->
            coll.insertOne(Document("one", 1).append("two", 2))
        }
    }

    @Test
    internal fun testRun() {
        val ctx = MapRunContext()
        val templateService = mockkClass(TemplateService::class, relaxed = true)
        val query = "{ \"one\": 1}"
        val document = "{ \$set: {'two' : 3 }}"
        every { templateService.writeToString(eq(document), ctx) } returns document
        every { templateService.writeToString(eq(query), ctx) } returns query
        val update = Update(
                query = query,
                json = document,
                templateService = templateService,
                collection = "to_update",
                database = database
        )

        doWithLocal(database, "to_update") { db, collection, client ->
            ctx[defaultSessionName] = client
            update.run(TaskId.fromString("test::test"), ctx)
            val result = collection.find(eq("one", 1)).first()
            assertNotNull(result)
            assertEquals(1, result.getInteger("one"))
            assertEquals(3, result.getInteger("two"))
        }
    }

    @Test
    internal fun testRunReplace() {
        val ctx = MapRunContext()
        val templateService = mockkClass(TemplateService::class, relaxed = true)
        val query = "{ \"one\": 1}"
        val document = "{ 'one' : 1, 'two' : 3 }}"
        every { templateService.writeToString(eq(document), ctx) } returns document
        every { templateService.writeToString(eq(query), ctx) } returns query
        val update = Update(
                query = query,
                json = document,
                templateService = templateService,
                collection = "to_update",
                database = database,
                replace = true
        )

        doWithLocal(database, "to_update") { db, collection, client ->
            ctx[defaultSessionName] = client
            update.run(TaskId.fromString("test::test"), ctx)
            val result = collection.find(eq("one", 1)).first()
            assertNotNull(result)
            assertEquals(1, result.getInteger("one"))
            assertEquals(3, result.getInteger("two"))
        }
    }
}