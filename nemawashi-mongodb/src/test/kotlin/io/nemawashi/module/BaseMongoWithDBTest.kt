/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.module

import com.mongodb.client.MongoClient
import com.mongodb.client.MongoClients
import com.mongodb.client.MongoDatabase
import com.mongodb.client.model.Updates.*
import org.bson.Document

abstract class BaseMongoWithDBTest : BaseMongoDBTest() {

    val collectionName = "the_cool_collection"
    val database = "qwerty"

    override fun initialiseDB() {
        doWithLocal(database, collectionName) { _, collection, _ ->
            for (i in 1..100) {
                collection.insertOne(Document(mapOf("theValue" to i, "theName" to "Name$i", "vals" to listOf(1, 2, 3))))
            }
        }

    }


    override fun teadDownDb() {
        doWithLocal(database, collectionName) { db, _, _ ->
            db.drop()
        }
    }


}