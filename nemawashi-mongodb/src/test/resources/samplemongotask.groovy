/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */


import groovy.json.JsonBuilder
import nemawashi.nw


nw.task {
    name "samplemongotask"
    description "A sample mongo task which tests integration with the tasks"


    script { taskId, ctx ->


        def inserted = 0
        def updated = 0
        def queried = 0

        $nw.echo(value: "Started task...")
        $nw.mongodb.session(servers: ["localhost": "12345"]) {
            $nw.echo(value: "Running in MongoDB Session...")
            100.times { x ->
                def builder = new JsonBuilder()
                builder {
                    customerId x
                    name "John the ${x}th"
                }
                $nw.mongodb.insert(
                    collection: 'a_bunch_of_documents',
                    json: builder.toPrettyString()
                )
                inserted++
            }
            def queryBuilder = new JsonBuilder()
            def queryString = "{ 'customerId' : { \$gt : 10 } }"
            queryBuilder {
                customerId: 50

            }
            $nw.mongodb.find(
                    collection: 'a_bunch_of_documents',
                    query: queryString,
                    varName: '__mongoDocument'
            ) {
                queried++
                println __mongoDocument
                def builder = new JsonBuilder()
                def id = __mongoDocument.customerId
                builder {
                    customerId id
                    name "John the ${__mongoDocument.customerId}th"
                    updated: true
                    scores: id * 2
                }
                $nw.mongodb.update(
                    query: "{ 'customerId' : $id }",
                    collection: 'a_bunch_of_documents',
                    json: builder.toPrettyString(),
                    replace: true
                )
                updated++
            }
            if (inserted == 0 || updated == 0 || queried == 0) {
                nw.fail taskId, "Nothing happened"
            } else {
                $nw.echo(value: "SUCCESS inserted -> ($inserted),  updated -> ($updated), queried -> ($queried)")
            }

        }


    }
}