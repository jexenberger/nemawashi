# Nemawashi

Nemawashi is a general purpose open source automation tool which makes it quick and
easy to build powerfull re-usable automations. 

Furthermore it features a declaritive model for building automations. This allows Nemawashi automations to automatically to be accessed via several channels namely:
* Command Line interface
* Web API
* Web UX

[Nemawashi] (https://en.wikipedia.org/wiki/Nemawashi) is a Japanse concept which means _Laying the groundwork_. Nemawashi provides that framework which gives you the tools to create powerfull automations.

## What it's for
N

## What it does
Nemawashi basically allows you to quickly compose and integrate many of the various tools and utilities available into a set of re-usable tasks.

![Nemawashi](docs/images/nemawashi_overview.png)

## Getting started
Nemawashi current has no release packages and must be built from source

### Pre-requisites
* Java JDK 9 or later
* Apache Maven

### Building and setup
* Make sure your `JAVA_HOME` environment variable is setup to point to your JDK 9 (or later) installation
* Checkout the source code from this project
* From the checked out directory run `mvn clean install`
* On *Nix systems
  * run `./install.sh` from the checked out source directory after installation
* On Windows systems
  * unzip the `nemawashi-cli-*-nemawashi.zip` into a target directory and set up the following:
  * `%NW_HOME%` environment variable to point to the root of the unzipped folder
  * `%PATH%` to point to `%NW_HOME$\bin`

## User Guide
* [Quick start tutorial](docs/quick_tutorial/quickstart_tutorial.md)
* [Nemawashi User Guide](docs/guide/index.md)

## Standard Module Reference Documentation
* [Standard Module Reference Documentation](docs/reference/modules.md)

## Concepts
Everything in Nemawashi is a Task. A Task is a unit of automation which allows you to accomplish some activity. Tasks can take a set of parameters and optionally return a value, Tasks can also work across a group of nested tasks. Tasks can be authored either in Nemawashi's native Kotlin or in Nemawashi YAML based automation language. Finally Tasks can be grouped together into a module which can be distributed and re-used

### Tasks

#### Simple Tasks
Simple tasks are simple units of automation which take a set of parameters and execute some automation activity. Tasks can be authored either in Kotlin or in YAML.

#### Functional Tasks
Functional tasks are units of automation wchih take a set of parameters, execute some automation activity and then return a value. Tasks can be authored either in Kotlin or in YAML.

#### Group Tasks
Group tasks are tasks which work across a set of nested tasks. For example Nemawashi's ssh module contains a session tasks which mtaintains an SSH session that sub tasks can act upon

### Automation Language
A YAML based language which allows you to orchestrate a set of tasks to accomplish some automation activity. These orchestrations can then in turn be exposed as tasks

### Modules
Modules provide a way to package set of Tasks together. Modules can be authored either in YAML to group together a set of YAML based Automation language tasks, or they can be authored in Kotlin to group together a set of Kotlin based tasks

## License
Nemawashi is Open Source, licensed under the [Apache 2.0 license] (https://www.apache.org/licenses/LICENSE-2.0)