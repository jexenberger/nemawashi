/*
 *  Copyright 2018 Julian Exenberger
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package io.nemawashi.util.log

import org.junit.jupiter.api.Test
import java.io.File

class JDKLogProviderTest {


    @Test
    internal fun testInfo() {
        val loggingFile = File("target/test_log1.log")
        val logger = FileJDKLogProvider(Level.info, loggingFile = loggingFile.absolutePath)
        logger.info("hello world")
        val logText = loggingFile.readText()
        println(logText)
        assert(logText.contains("hello world"))

    }


    @Test
    internal fun testError() {
        val loggingFile = File("target/test_log2.log")
        val logger = FileJDKLogProvider(Level.info, loggingFile = loggingFile.absolutePath)
        logger.level = Level.error
        logger.error("hello world error", IllegalStateException("borked"))
        logger.error("xxx_error_xxx")
        val logText = loggingFile.readText()
        println(logText)
        assert(logText.contains("hello world error"))
        assert(logText.contains("xxx_error_xxx"))

    }

    @Test
    internal fun testWarn() {
        val loggingFile = File("target/test_log3.log")
        val logger = FileJDKLogProvider(Level.info, loggingFile = loggingFile.absolutePath)

        logger.level = Level.warn
        logger.warn("hello world warn")
        val logText = loggingFile.readText()
        println(logText)
        assert(logText.contains("hello world warn"))

    }

    @Test
    internal fun testTrace() {
        val loggingFile = File("target/test_log4.log")
        val logger = FileJDKLogProvider(Level.info, loggingFile = loggingFile.absolutePath)

        logger.level = Level.trace
        logger.trace("hello world trace")
        val logText = loggingFile.readText()
        println(logText)
        assert(logText.contains("hello world trace"))

    }
}