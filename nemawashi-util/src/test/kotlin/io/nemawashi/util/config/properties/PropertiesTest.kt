/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.util.config.properties

import org.junit.jupiter.api.Test
import java.io.File
import java.util.*
import kotlin.test.assertNotNull

class PropertiesTest {


    @Test
    internal fun toClass() {
        val properties = Properties()
        properties.load(File("src/test/resources/sampleproperty.properties").reader())
        val sampleProperty = properties.toClass<SampleProperty>(
                "f" to { p: Properties -> 1 to 3 }
        )
        assertNotNull(sampleProperty)
        println(sampleProperty)
    }
}