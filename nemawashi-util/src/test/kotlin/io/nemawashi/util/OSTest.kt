/*
 *  Copyright 2018 Julian Exenberger
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package io.nemawashi.util

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertTrue

class OSTest {
    @Test
    fun testOs() {
        //flaky test, how do we check if the right is returned across different platforms
        val os = OS.os()
        assertNotNull(os)
    }

    @Test
    fun testProperties() {
        assertEquals(System.getProperty("os.version"), OS.os().version)
        assertEquals(System.getProperty("os.arch"), OS.os().architecture)
    }

    @Test
    fun testPwd() {
        assertEquals(System.getProperty("user.dir"), OS.os().pwd)
    }

    @Test
    fun testHomeDir() {
        assertEquals(System.getProperty("user.home"), OS.os().homeDir)
    }

    @Test
    fun testPathString() {
        val os = OS.os()
        val pathString = os.pathString("hello", "world")
        if (os.unixVariant) {
            assertEquals("hello:world", pathString)
        } else {
            assertEquals("hello;world", pathString)
        }
    }

    @Test
    fun testFsString() {
        val os = OS.os()
        val pathString = os.fsString("hello", "world")
        if (os.unixVariant) {
            assertEquals("hello/world", pathString)
        } else {
            assertEquals("hello\\world", pathString)
        }

    }

    @Test
    fun testFsStringWithRoot() {
        val os = OS.os()
        val pathString = os.fsString("hello", "world", absolute = true)
        if (os.unixVariant) {
            assertEquals("/hello/world", pathString)
        } else {
            assertEquals("C:\\hello\\world", pathString)
        }
    }

    @Test
    fun testEnvironment() {
        assertNotNull(OS.os().env)
    }

    @Test
    fun testProps() {
        assertNotNull(OS.os().props)
    }

    @Test
    fun testRun() {
        val cmd = if (OS.os().unixVariant) "whoami" else "git --version"
        var called = false
        val buffer = StringBuffer()
        val res = OS.os().shell(cmd) {
            called = true
            buffer.append(it)
        }
        println(buffer.toString()   )
        assertTrue { called }
        assertEquals(0, res)
    }

    @Test
    fun testRunStringOutput() {
        if (OS.os().unixVariant) {
            val (res, str) = OS.os().shell("whoami")
            assertTrue { str.length > 0 }
            assertEquals(0, res)
            println(str)
        }
    }

    @Test
    internal fun testExpandPath() {
        val os = OS.os()
        val expandPath = os.expandPath("~/theDir/theSubDir")
        val expected = system.fsString(system.homeDir, "theDir", "theSubDir")
        assertEquals(expected, expandPath)

        val env = os.env
        val envVar = if (system.unixVariant) "HOME"  else "TEMP"
        val pathWithEnv = "\$$envVar/theDir/theSubDir"
        val varExpected = system.fsString(env[envVar]!!.toString(),"theDir", "theSubDir")
        assertEquals(varExpected, os.expandPath(pathWithEnv))
    }
}

