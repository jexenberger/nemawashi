package io.nemawashi.util.string

import org.junit.jupiter.api.Test
import java.io.File
import javax.activation.URLDataSource
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class SourceFactoryTest {


    @Test
    internal fun testCreateFile() {
        val dataSource = SourceFactory.toSource("##file:src/test/resources/nested.yml")
        assertTrue { dataSource is FileDataSource }
        assertTrue { dataSource.name.endsWith("src/test/resources/nested.yml") }
    }

    @Test
    internal fun testCreateOrdinaryString() {
        val dataSource = SourceFactory.toSource("hello world")
        assertTrue { dataSource is StringDataSource }
        assertEquals("hello world", dataSource.asString())
    }

    @Test
    internal fun testCreateURL() {
        val file = File("src/test/resources/nested.yml")
        val dataSource = SourceFactory.toSource("##url:${file.toURI().toURL()}")
        assertTrue { dataSource is URLDataSource }
        assertTrue { dataSource.name.endsWith("src/test/resources/nested.yml") }
    }

    @Test
    internal fun testEscape() {
        val file = File("src/test/resources/nested.yml")
        val unescaped = "##url:${file.toURI().toURL()}"
        val str = "\\$unescaped"
        val dataSource = SourceFactory.toSource(str)
        assertTrue { dataSource is StringDataSource }
        assertEquals(unescaped, dataSource.asString())
    }
}