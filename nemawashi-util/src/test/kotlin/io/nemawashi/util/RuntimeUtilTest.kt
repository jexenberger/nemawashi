/*
 *  Copyright 2018 Julian Exenberger
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package io.nemawashi.util

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

class RuntimeUtilTest {


    @Test
    internal fun testExecutingLine() {
        val (file, classType, lineNumber) = RuntimeUtil.executingLine
        assertEquals("${this::class.simpleName}.kt", file)
        assertEquals(this::class.qualifiedName, classType)
        assertEquals(27, lineNumber)
    }

    @Test
    internal fun testFindByFilter() {
        val fileName = "${this::class.simpleName}.kt"
        val line = RuntimeUtil.findByFilter { it.fileName.equals(fileName) }
        assertNotNull(line)
        assertEquals(fileName, line?.file)
        assertEquals(this::class.qualifiedName, line?.className)
        assertEquals(36, line?.lineNumber)
    }
}