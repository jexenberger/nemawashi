/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.util

import org.junit.jupiter.api.Test
import java.lang.reflect.Constructor
import kotlin.test.assertNotNull

class ClassTest {


    @Test
    internal fun testResolveConstructorFromArgs() {
        val args: Array<Any?> = arrayOf(100, 100.0f, "hello")
        val constructor: Constructor<SampleClass>? = SampleClass::class.java.resolveConstructorForArgs(args)
        assertNotNull(constructor)
        constructor.newInstance(100, 100.0f, "hello")
        constructor.newInstance(*args)
    }

    @Test
    internal fun testResolveConstructorFromArgsWhenNoArgs() {
        val constructor: Constructor<SampleClass>? = SampleClass::class.java.resolveConstructorForArgs()
        assertNotNull(constructor)
        constructor.newInstance()
    }

    @Test
    internal fun testResolveConstructorFromArgsWhenNull() {
        val args: Array<Any?> = arrayOf(null)
        val constructor: Constructor<SampleClass>? = SampleClass::class.java.resolveConstructorForArgs(args)
        assertNotNull(constructor)
        constructor.newInstance(null)
    }
}