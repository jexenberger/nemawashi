/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.util.log

object Log : LogProvider {

    override var level: Level
        get() = provider.level
        @Synchronized
        set(value) {
            provider.level = value
        }

    @set:Synchronized
    var provider: LogProvider = FileJDKLogProvider(Level.info, loggingFile = ".runlog")

    override fun info(message: Any?) {
        provider.info(message)
    }

    override fun warn(message: Any?) {
        provider.warn(message)
    }

    override fun debug(message: Any?) {
        provider.debug(message)
    }

    override fun trace(message: Any?) {
        provider.trace(message)
    }

    override fun error(message: Any?, error: Exception?) {
        provider.error(message, error)
    }


}