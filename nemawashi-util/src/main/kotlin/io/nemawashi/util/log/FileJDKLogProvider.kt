/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.util.log

import java.util.logging.FileHandler
import java.util.logging.Logger
import java.util.logging.SimpleFormatter

class FileJDKLogProvider(_level: Level, var echo: Boolean = false, name: String = "NEMAWASHI", val loggingFile: String) : JDKLogProvider(_level, name) {


    override val logger: Logger by lazy {
        val logger = Logger.getLogger(this.name)
        if (!echo) {
            logger.useParentHandlers = false
        }
        logger.handlers.forEach {
            logger.removeHandler(it)
        }
        val handler = FileHandler(loggingFile, false)
        handler.formatter = SimpleFormatter()
        logger.addHandler(handler)
        setLevel(logger)
        logger
    }

}