/*
 *  Copyright 2018 Julian Exenberger
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package io.nemawashi.util.log

import io.nemawashi.util.Timer
import java.util.*

interface LogProvider {


    var level: Level


    fun info(message: Any?)
    fun warn(message: Any?)
    fun debug(message: Any?)
    fun trace(message: Any?)
    fun error(message: Any?, error: Exception? = null)

    fun <T> traceWithTime(operationName: String, handler: () -> T): T {
        val event = UUID.randomUUID()
        trace(">> START($event) - $operationName")
        val (time, res) = Timer.run {
            handler()
        }
        trace(">> END($event) - ${operationName.padEnd(40)} -> ${time}ms")
        return res
    }

}