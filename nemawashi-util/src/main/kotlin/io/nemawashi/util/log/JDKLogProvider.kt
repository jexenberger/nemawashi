/*
 *  Copyright 2018 Julian Exenberger
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package io.nemawashi.util.log

import java.util.logging.ConsoleHandler
import java.util.logging.Logger

open class JDKLogProvider(protected var _level: Level = Level.info, val name: String = "CODESTREAM") : LogProvider {

    protected open val logger: Logger by lazy {
        val log = Logger.getLogger(name)
        log.useParentHandlers = false
        log.addHandler(ConsoleHandler())
        setLevel(log)
        log
    }

    override var level: Level
        get() = _level
        @Synchronized
        set(value) {
            _level = value
            setLevel(logger)
        }


    protected fun setLevel(log: Logger) {
        val targetLevel = levels[_level]!!
        log.level = targetLevel
        log.handlers.forEach {
            it.level = targetLevel
        }
        log.parent.level = targetLevel
    }

    override fun info(message: Any?) {
        logger.info("$message")
    }

    override fun warn(message: Any?) {
        logger.warning("$message")
    }

    override fun debug(message: Any?) {
        logger.fine("$message")
    }

    override fun trace(message: Any?) {
        logger.finer("$message")
    }

    override fun error(message: Any?, error: Exception?) {
        if (error != null) {
            logger.log(java.util.logging.Level.SEVERE, "$message", error)
        } else {
            logger.severe("$message")
        }
    }

    companion object {
        val levels: Map<Level, java.util.logging.Level> = mapOf(
                Level.info to java.util.logging.Level.INFO,
                Level.error to java.util.logging.Level.SEVERE,
                Level.warn to java.util.logging.Level.WARNING,
                Level.debug to java.util.logging.Level.FINE,
                Level.trace to java.util.logging.Level.ALL
        )
    }
}