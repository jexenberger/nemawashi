/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.util

import java.io.File
import java.util.concurrent.TimeUnit


fun Any?.stringify(): String = this.toString().trim()

fun stringOf(size:Int = 1, charToUse: Char = ' ') = (1..size).map { charToUse }.joinToString(separator = "")


fun String.exec(dir: File = File(System.getProperty("user.dir")),
                timeout: Long = 60,
                timeUnit: TimeUnit = TimeUnit.MINUTES): Pair<Int, String> {
    val parts = this.split(" ")
    val process = ProcessBuilder(parts)
            .redirectOutput(ProcessBuilder.Redirect.INHERIT)
            .redirectError(ProcessBuilder.Redirect.INHERIT)
            .directory(dir)
            .start()
    val output = process.inputStream.bufferedReader().readText()
    val completed = process.waitFor(timeout, timeUnit)
    if (!completed) {
        return Pair(-1, "TIMEOUT")
    }
    val result = process.exitValue()
    return Pair(result, output)
}

fun String.exec(dir: File = File(System.getProperty("user.dir")),
                timeout: Long = 60,
                timeUnit: TimeUnit = TimeUnit.MINUTES,
                handler: (String) -> Unit): Int {
    val parts = this.split(" ").map { it.trim() }
    return system.exec(parts.toTypedArray(),
            dir = dir,
            timeout = timeout,
            timeUnit = timeUnit,
            handler = handler)
}

fun String.stripQuotes() : String {
    val starts = (this.startsWith("'") || this.startsWith("\""))
    val ends = (this.endsWith("'") || this.endsWith("\""))
    return if (ends && starts) this.substring(1, this.length-1) else this
}



fun String.globMatches( glob: String): Boolean {
    var glob = glob
    var rest: String? = null
    val pos = glob.indexOf('*')
    if (pos != -1) {
        rest = glob.substring(pos + 1)
        glob = glob.substring(0, pos)
    }

    if (glob.length > this.length)
        return false

    // handle the part up to the first *
    for (i in 0 until glob.length)
        if (glob[i] != '?' && !glob.substring(i, i + 1).equals(this.substring(i, i + 1), ignoreCase = true))
            return false

    // recurse for the part after the first *, if any
    if (rest == null) {
        return glob.length == this.length
    } else {
        for (i in glob.length..this.length) {
            if (this.substring(i).globMatches(rest))
                return true
        }
        return false
    }
}

fun String.splitCamelCase(): String {
    val split = this.split(Strings.camelCaseRegex)

    return split.map { it.capitalize() }.joinToString(" ")

}

object Strings {

    val camelCaseRegex = "(?<!(^|[A-Z]))(?=[A-Z])|(?<!^)(?=[A-Z][a-z])".toRegex()

    fun isEmpty(str: String?): Boolean {
        return (str == null || str.trim().equals(""))
    }
}