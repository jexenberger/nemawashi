/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.util

object ThreadLocalHandler {

    private val currentThreadLocal = ThreadLocal<Any>()


    fun <T> set(value: T) {
        currentThreadLocal.set(value)
    }

    fun unset() {
        currentThreadLocal.remove()
    }

    @Suppress("UNCHECKED_CAST")
    fun <T> currentValue(): T? {
        return currentThreadLocal.get() as T?
    }


    fun <T, K> doWithValue(handler: (T) -> K?): K? {
        val currentValue = currentValue<T>() ?: throw IllegalStateException("ThreadLocal is unset")
        return handler(currentValue)
    }

    fun <T, K> doWithValue(value: T, handler: (T) -> K?): K? {
        try {
            set(value)
            return handler(value)
        } finally {
            unset()
        }
    }


}