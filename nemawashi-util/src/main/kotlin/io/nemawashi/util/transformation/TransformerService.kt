/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.util.transformation

import io.nemawashi.util.SystemException
import io.nemawashi.util.crypto.Secret
import io.nemawashi.util.string.SourceFactory
import io.nemawashi.util.string.asString
import io.nemawashi.util.system
import java.io.File
import java.math.BigDecimal
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import javax.activation.DataSource
import kotlin.reflect.KClass
import kotlin.reflect.full.isSubclassOf
import kotlin.reflect.full.isSuperclassOf

@Suppress("UNCHECKED_CAST")
object TransformerService {

    val formatDateTime = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")
    val formatDate = DateTimeFormatter.ofPattern("yyyy-MM-dd")

    internal val CONVERSION_REGISTRY: MutableMap<Pair<KClass<*>, KClass<*>>, TypeTransformer<*, *>> = mutableMapOf()


    init {
        addConverter(CharSequence::class, Secret::class, LambdaTransformer<CharSequence, Secret> { Secret(it.toString()) })
        addConverter(CharSequence::class, DataSource::class, LambdaTransformer<CharSequence, DataSource> { SourceFactory.toSource(it.toString()) })
        addConverter(DataSource::class, CharSequence::class, LambdaTransformer<DataSource, CharSequence> { it.asString() })
        addConverter(Secret::class, CharSequence::class, LambdaTransformer<Secret, CharSequence> { it.value })
        addConverter(CharSequence::class, File::class, LambdaTransformer<CharSequence, File> { File(system.expandPath(it.toString())) })
        addConverter(Collection::class, Array<Int>::class, LambdaTransformer<List<Any>, Array<Int>> {
            it.map { value -> TransformerService.convert<Int>(value) }.toTypedArray()
        })
        addConverter(Iterable::class, Iterator::class, LambdaTransformer<Iterable<Any>, Iterator<Any>> { it.iterator() })
        addConverter(CharSequence::class, Iterator::class, LambdaTransformer<CharSequence, Iterator<Any>> {
            it.split(",").map { it.trim() }.iterator()
        })
        addConverter(Array<Any>::class, Iterator::class, LambdaTransformer<Array<Any>, Iterator<Any>> { it.iterator() })
        addConverter(Array<CharSequence>::class, Iterator::class, LambdaTransformer<Array<CharSequence>, Iterator<CharSequence>> { it.iterator() })
        addConverter(Array<Int>::class, Iterator::class, LambdaTransformer<Array<Int>, Iterator<Int>> { it.iterator() })
        addConverter(Array<Double>::class, Iterator::class, LambdaTransformer<Array<Double>, Iterator<Double>> { it.iterator() })
        addConverter(Array<Short>::class, Iterator::class, LambdaTransformer<Array<Short>, Iterator<Short>> { it.iterator() })
        addConverter(Array<Byte>::class, Iterator::class, LambdaTransformer<Array<Byte>, Iterator<Byte>> { it.iterator() })
        addConverter(Array<Boolean>::class, Iterator::class, LambdaTransformer<Array<Boolean>, Iterator<Boolean>> { it.iterator() })
        addConverter(Array<Float>::class, Iterator::class, LambdaTransformer<Array<Float>, Iterator<Float>> { it.iterator() })
        addConverter(Array<Long>::class, Iterator::class, LambdaTransformer<Array<Long>, Iterator<Long>> { it.iterator() })
        addConverter(File::class, CharSequence::class, LambdaTransformer<File, CharSequence> { it.absolutePath })
        addConverter(CharSequence::class, Long::class, LambdaTransformer<CharSequence, Long> { it.toString().toLong() })
        addConverter(CharSequence::class, Int::class, LambdaTransformer<CharSequence, Int> { it.toString().toInt() })
        addConverter(CharSequence::class, Byte::class, LambdaTransformer<CharSequence, Byte> { it.toString().toByte() })
        addConverter(CharSequence::class, Short::class, LambdaTransformer<CharSequence, Short> { it.toString().toShort() })
        addConverter(CharSequence::class, Float::class, LambdaTransformer<CharSequence, Float> { it.toString().toFloat() })
        addConverter(CharSequence::class, Double::class, LambdaTransformer<CharSequence, Double> { it.toString().toDouble() })
        addConverter(CharSequence::class, Char::class, LambdaTransformer<CharSequence, Char> { it.toString().toCharArray()[0] })
        addConverter(CharSequence::class, Boolean::class, LambdaTransformer<CharSequence, Boolean> { Transformations.toBoolean(it) })
        addConverter(CharSequence::class, BigDecimal::class, LambdaTransformer<CharSequence, BigDecimal> { BigDecimal(it.toString()) })
        addConverter(CharSequence::class, Regex::class, LambdaTransformer<CharSequence, Regex> { it.toString().toRegex() })
        addConverter(CharSequence::class, LocalDate::class, LambdaTransformer<CharSequence, LocalDate> { LocalDate.parse(it, formatDate) })
        addConverter(CharSequence::class, LocalDateTime::class, LambdaTransformer<CharSequence, LocalDateTime> { Transformations.toDateTime(it.toString()) })
        addConverter(CharSequence::class, Array<String>::class, LambdaTransformer<CharSequence, Array<String>> { sequence ->
            sequence.split(",").map { it.trim() }.toTypedArray()
        })
        addConverter(CharSequence::class, Array<Float>::class, LambdaTransformer<CharSequence, Array<Float>> {
            it.split(",").map { it.trim().toFloat() }.toTypedArray()
        })
        addConverter(CharSequence::class, Array<Double>::class, LambdaTransformer<CharSequence, Array<Double>> {
            it.split(",").map { it.trim().toDouble() }.toTypedArray()
        })
        addConverter(CharSequence::class, Array<BigDecimal>::class, LambdaTransformer<CharSequence, Array<BigDecimal>> {
            it.split(",").map { BigDecimal(it.trim()) }.toTypedArray()
        })
        addConverter(CharSequence::class, Array<File>::class, LambdaTransformer<CharSequence, Array<File>> {
            it.split(",").map { File(it.trim()) }.toTypedArray()
        })
        addConverter(CharSequence::class, Array<Boolean>::class, LambdaTransformer<CharSequence, Array<Boolean>> {
            it.split(",").map { Transformations.toBoolean(it.trim()) }.toTypedArray()
        })
        addConverter(CharSequence::class, Array<Int>::class, LambdaTransformer<CharSequence, Array<Int>> {
            it.split(",").map { it.trim().toInt() }.toTypedArray()
        })
        addConverter(CharSequence::class, Array<Long>::class, LambdaTransformer<CharSequence, Array<Long>> {
            it.split(",").map { it.trim().toLong() }.toTypedArray()
        })
        addConverter(CharSequence::class, Array<Short>::class, LambdaTransformer<CharSequence, Array<Short>> {
            it.split(",").map { it.trim().toShort() }.toTypedArray()
        })
        addConverter(CharSequence::class, Array<Byte>::class, LambdaTransformer<CharSequence, Array<Byte>> {
            it.split(",").map { it.trim().toByte() }.toTypedArray()
        })
        addConverter(CharSequence::class, Collection::class, LambdaTransformer<CharSequence, Collection<CharSequence>> {
            it.split(",").map { it.trim() }
        })
        addConverter(CharSequence::class, Array<Any>::class, LambdaTransformer<CharSequence, Array<Any>> {
            it.split(",").map { it.trim() }.toTypedArray()
        })
        addConverter(CharSequence::class, Any::class, LambdaTransformer<CharSequence, Any> { it })
        addConverter(Number::class, Long::class, LambdaTransformer<Number, Long> { it.toLong() })
        addConverter(Number::class, Int::class, LambdaTransformer<Number, Int> { it.toInt() })
        addConverter(Number::class, Byte::class, LambdaTransformer<Number, Byte> { it.toByte() })
        addConverter(Number::class, Short::class, LambdaTransformer<Number, Short> { it.toShort() })
        addConverter(Number::class, Boolean::class, LambdaTransformer<Number, Boolean> { Transformations.toBoolean(it) })
        addConverter(Number::class, Float::class, LambdaTransformer<Number, Float> { it.toFloat() })
        addConverter(Number::class, Double::class, LambdaTransformer<Number, Double> { it.toDouble() })
        addConverter(Number::class, BigDecimal::class, LambdaTransformer<Number, BigDecimal> { BigDecimal(it.toString()) })
        addConverter(LocalDate::class, CharSequence::class, LambdaTransformer<LocalDate, CharSequence> { formatDate.format(it) })
        addConverter(LocalDateTime::class, CharSequence::class, LambdaTransformer<LocalDateTime, CharSequence> { formatDateTime.format(it) })
        addConverter(Regex::class, CharSequence::class, LambdaTransformer<Regex, CharSequence> { it.toPattern().pattern()!! })
        addConverter(Any::class, CharSequence::class, LambdaTransformer<Any, CharSequence> { it.toString() })
        addConverter(Char::class, Boolean::class, LambdaTransformer<Char, Boolean> { Transformations.toBoolean(it) })
        addConverter(Array<CharSequence>::class, Collection::class, LambdaTransformer<Array<CharSequence>, Collection<CharSequence>> {
            it.toList()
        })

        addConverter(ByteArray::class, Array<Byte>::class, LambdaTransformer<ByteArray, Array<Byte>> {
            it.toTypedArray()
        })
        addConverter(Array<Byte>::class, ByteArray::class, LambdaTransformer<Array<Byte>, ByteArray> {
            it.toByteArray()
        })

        addConverter(Collection::class, Array<String>::class, LambdaTransformer<Collection<*>, Array<String>> {
            it.toTypedArray().map { it?.toString() ?: "" }.toTypedArray()
        })


        addConverter(Map::class, Map::class, LambdaTransformer<Map<CharSequence, Any?>, Map<CharSequence, Any?>> {
            it
        })
    }


    internal val conversionRegistry: MutableMap<Pair<KClass<*>, KClass<*>>, TypeTransformer<*, *>>
        get() = CONVERSION_REGISTRY

    fun addConverter(source: KClass<*>, target: KClass<*>, transformer: TypeTransformer<*, *>) {
        val key = source to target
        conversionRegistry.put(key, transformer)
    }

    fun findWideningTransformer(source: KClass<*>, target: KClass<*>): TypeTransformer<*, *>? {


        val typeTransformer = conversionRegistry[source to target]
        if (typeTransformer != null) {
            return typeTransformer
        }
        //do a widening search
        val transformerKey = conversionRegistry
                .keys
                .filter { (first, second) ->
                    val sourceAssignable = first.isSuperclassOf(source)
                    val targetAssignable = second.isSuperclassOf(target) || second.equals(target)
                    val fits = sourceAssignable && targetAssignable
                    fits
                }
                .firstOrNull()
        return conversionRegistry[transformerKey]
    }

    inline fun <reified K> convertWithNull(instance: Any?, typeHint: KClass<*>? = null): K? {
        return instance?.let {
            convert(it, typeHint)
        }
    }


    fun isCollectionType(a: KClass<*>): Boolean {
        val collection = isCollection(a)
        val iterable = isIterableType(a)
        return collection || iterable
    }


    private fun isIterableType(a: KClass<*>) = a.equals(Iterable::class) || a.isSubclassOf(Iterable::class)

    private fun isCollection(a: KClass<*>) = a.equals(Collection::class) || a.isSubclassOf(Collection::class)

    fun areBothCollectionTypes(a: KClass<*>, b: KClass<*>): Boolean {
        return isCollectionType(a) && isCollectionType(b)
    }

    @SuppressWarnings("UNCHECKED_CAST")
    inline fun <reified K> convert(instance: Any, typeHint: KClass<*>? = null): K {
        val type = typeHint?.let { it } ?: K::class
        //special case with enum as we always need the absolute concrete type
        if (type.equals(Any::class)) {
            return instance as K
        }
        if (instance is CharSequence && type.java.isEnum) {
            @Suppress("UPPER_BOUND_VIOLATED", "UNCHECKED_CAST")
            return (java.lang.Enum.valueOf<Any>(type.java as Class<Any>, instance.toString()) as K)
        }


        if (type.isInstance(instance)) {
            return instance as K
        }
        @SuppressWarnings("UNCHECKED_CAST")
        val transfomer = findWideningTransformer(instance::class, type) as TypeTransformer<Any, K>?
        return transfomer?.transform(instance)
                ?: throw SystemException("unable to convert from ${instance::class.qualifiedName} to ${type.qualifiedName}")
    }


}