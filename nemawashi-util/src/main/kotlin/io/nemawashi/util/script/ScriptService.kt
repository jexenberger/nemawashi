/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.util.script

import java.io.File
import java.io.Reader
import java.util.*
import javax.script.Bindings
import javax.script.Compilable
import javax.script.CompiledScript
import kotlin.reflect.KClass

interface ScriptService {


    fun loadClass(source: File, parentLoader: ClassLoader, language: String = Language.defaultName): KClass<*>

    fun <T> compile(source: Reader, parentLoader: ClassLoader, language: String = Language.defaultName) = Eval.compile(source, Eval.engineByName(language))

    fun <T> eval(source: Reader, bindings: Map<String, Any?>, language: String): T {
        return Eval.eval(source, bindings, scriptEngine = Eval.engineByName(language))
    }

    @Suppress("UNCHECKED_CAST")
    fun <T> eval(source: String, bindings: Bindings, checkForScript: Boolean, language: String, defaultValue: () -> T): T? {
        val isScriptString = Eval.isScriptString(source)
        if (checkForScript && !isScriptString) return defaultValue()
        val script = if (isScriptString) Eval.extractScriptString(source) else source
        val engine = Eval.engineByName(language) as Compilable
        val toEval = ScriptService.scriptCache.getOrPut(script) {
            engine.compile(script)
        }
        return toEval.eval(bindings) as T
    }

    fun <T> evalIfScript(source: Any?, bindings: Bindings, language: String = Language.defaultName): T? {
        if (source == null) {
            return null
        }
        val candidateScript = source.toString()
        return eval(candidateScript, bindings, true, language) {
            @Suppress("UNCHECKED_CAST")
            source as T
        }
    }

    companion object {
        private val scriptCache: MutableMap<Any, CompiledScript> = Collections.synchronizedMap(mutableMapOf())

    }
}