/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.util.config.properties

import io.nemawashi.util.transformation.TransformerService
import java.util.*
import kotlin.reflect.KClass
import kotlin.reflect.full.primaryConstructor

inline fun <reified T> Properties.toClass(vararg converters: Pair<String, (Properties) -> Any?>): T {
    val converterMap = converters.toMap()
    val converter = { name: String, type: KClass<*> ->
        converterMap[name] ?: { props ->
            TransformerService.convertWithNull(props[name], type)
        }
    }


    val type = T::class as KClass<*>
    assert(type.isData) { "only supports Kotlin data classes" }
    val parameters = type.primaryConstructor!!.parameters

    val structorArgs = parameters.map {
        val propertyValue = converter(it.name!!, it.type.classifier as KClass<*>)(this)
        it to propertyValue
    }.filter { (_, value) ->
        (value != null)
    }.toMap()
    return type.primaryConstructor!!.callBy(structorArgs) as T
}