/*
 *  Copyright 2018 Julian Exenberger
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package io.nemawashi.util.string

import java.net.URL
import javax.activation.DataSource
import javax.activation.URLDataSource

object SourceFactory {


    val types = mutableMapOf<String, (String) -> DataSource>()


    init {
        types["file"] = { FileDataSource(it) }
        types["url"] = { URLDataSource(URL(it)) }
    }

    operator fun set(type: String, handler: (String) -> DataSource) {
        types[type] = handler
    }

    fun toSource(str: String): DataSource {
        //check for escape
        if (str.contains("##") && str.startsWith("\\##")) {
            return StringDataSource(str.substring(1))
        }
        val keys = types.filterKeys { str.startsWith("##$it:") }
        if (keys.isEmpty()) {
            return StringDataSource(str)
        }
        val (key, handler) = keys.iterator().next()
        val ref = str.substring("##$key:".length)
        return handler(ref)
    }




}