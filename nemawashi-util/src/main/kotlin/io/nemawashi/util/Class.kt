/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.util

import java.lang.ClassCastException
import java.lang.reflect.Constructor


internal val primitiveMap = mapOf<String, Class<*>>(
        "int" to java.lang.Integer::class.java,
        "float" to java.lang.Float::class.java,
        "long" to java.lang.Long::class.java,
        "double" to java.lang.Double::class.java,
        "boolean" to java.lang.Boolean::class.java,
        "short" to java.lang.Short::class.java,
        "byte" to java.lang.Byte::class.java
)

fun java.lang.Class<*>.resolveWrapperClass(candidatePrimitive:Class<*>) : Class<*>? {
    if (!candidatePrimitive.isPrimitive) {
        return null;
    }
    return primitiveMap[candidatePrimitive.simpleName]
}

fun <T> java.lang.Class<T>.resolveConstructorForArgs(args: Array<Any?> = emptyArray()): Constructor<T>? {

    return this.constructors
            .filter { it.parameterCount == args.size }
            .filter {
                it.parameterTypes
                        .mapIndexed { index, clazz ->
                            val arg = args[index]
                            //allow nulls
                            arg?.let { theArg ->
                                return@let try {
                                    val typeToCheck = resolveWrapperClass(clazz) ?: clazz
                                    typeToCheck.isAssignableFrom(theArg::class.java)
                                } catch (e: ClassCastException) {
                                    false
                                }
                            } ?: !clazz.isPrimitive

                        }
                        .fold(true) { a, b -> a and b }
            }
            .firstOrNull() as Constructor<T>?
}

