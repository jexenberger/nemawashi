/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.util

import java.util.*
import java.util.regex.Pattern


/**
 * Implementation of Rails'
 * [Inflections](http://api.rubyonrails.org/classes/ActiveSupport/CoreExtensions/String/Inflections.html)
 * to handle singularization and pluralization of 'Rails strings'.
 *
 * Copied from [rogueweb](http://code.google.com/p/rogueweb/)'s port of Rails to Java.
 *
 */
class Inflection @JvmOverloads constructor(private val pattern: String, private val replacement: String? = null, private val ignoreCase: Boolean = true) {

    /**
     * Does the given word match?
     * @param word The word
     * @return True if it matches the inflection pattern
     */
    fun match(word: String): Boolean {
        var flags = 0
        if (ignoreCase) {
            flags = flags or Pattern.CASE_INSENSITIVE
        }
        return Pattern.compile(pattern, flags).matcher(word).find()
    }

    /**
     * Replace the word with its pattern.
     * @param word The word
     * @return The result
     */
    fun replace(word: String): String {
        var flags = 0
        if (ignoreCase) {
            flags = flags or Pattern.CASE_INSENSITIVE
        }
        return Pattern.compile(pattern, flags).matcher(word).replaceAll(replacement)
    }

    companion object {
        private val plural = ArrayList<Inflection>()
        private val singular = ArrayList<Inflection>()
        private val uncountable = ArrayList<String>()

        init {
            // plural is "singular to plural form"
            // singular is "plural to singular form"
            plural("$", "s")
            plural("s$", "s")
            plural("(ax|test)is$", "$1es")
            plural("(octop|vir)us$", "$1i")
            plural("(alias|status)$", "$1es")
            plural("(bu)s$", "$1ses")
            plural("(buffal|tomat)o$", "$1oes")
            plural("([ti])um$", "$1a")
            plural("sis$", "ses")
            plural("(?:([^f])fe|([lr])f)$", "$1$2ves")
            plural("(hive)$", "$1s")
            plural("([^aeiouy]|qu)y$", "$1ies")
            //plural("([^aeiouy]|qu)ies$", "$1y");
            plural("(x|ch|ss|sh)$", "$1es")
            plural("(matr|vert|ind)ix|ex$", "$1ices")
            plural("([m|l])ouse$", "$1ice")
            plural("^(ox)$", "$1en")
            plural("(quiz)$", "$1zes")

            singular("s$", "")
            singular("(n)ews$", "$1ews")
            singular("([ti])a$", "$1um")
            singular("((a)naly|(b)a|(d)iagno|(p)arenthe|(p)rogno|(s)ynop|(t)he)ses$", "$1$2sis")
            singular("(^analy)ses$", "$1sis")
            singular("([^f])ves$", "$1fe")
            singular("(hive)s$", "$1")
            singular("(tive)s$", "$1")
            singular("([lr])ves$", "$1f")
            singular("([^aeiouy]|qu)ies$", "$1y")
            singular("(s)eries$", "$1eries")
            singular("(m)ovies$", "$1ovie")
            singular("(x|ch|ss|sh)es$", "$1")
            singular("([m|l])ice$", "$1ouse")
            singular("(bus)es$", "$1")
            singular("(o)es$", "$1")
            singular("(shoe)s$", "$1")
            singular("(cris|ax|test)es$", "$1is")
            singular("(octop|vir)i$", "$1us")
            singular("(alias|status)es$", "$1")
            singular("^(ox)en", "$1")
            singular("(vert|ind)ices$", "$1ex")
            singular("(matr)ices$", "$1ix")
            singular("(quiz)zes$", "$1")

            // irregular
            irregular("person", "people")
            irregular("man", "men")
            irregular("child", "children")
            irregular("sex", "sexes")
            irregular("move", "moves")

            uncountable("equipment")
            uncountable("information")
            uncountable("rice")
            uncountable("money")
            uncountable("species")
            uncountable("series")
            uncountable("fish")
            uncountable("sheep")

            //Collections.reverse(singular);
            //Collections.reverse(plural);
        }

        private fun plural(pattern: String, replacement: String) {
            plural.add(0, Inflection(pattern, replacement))
        }

        private fun singular(pattern: String, replacement: String) {
            singular.add(0, Inflection(pattern, replacement))
        }

        private fun irregular(s: String, p: String) {
            plural("(" + s.substring(0, 1) + ")" + s.substring(1) + "$", "$1" + p.substring(1))
            singular("(" + p.substring(0, 1) + ")" + p.substring(1) + "$", "$1" + s.substring(1))
        }

        private fun uncountable(word: String) {
            uncountable.add(word)
        }

        /**
         * Return the pluralized version of a word.
         * @param word The word
         * @return The pluralized word
         */
        fun pluralize(word: String): String {
            if (Inflection.isUncountable(word)) {
                return word
            }

            for (inflection in plural) {
                if (inflection.match(word)) {
                    return inflection.replace(word)
                }
            }
            return word
        }

        /**
         * Return the singularized version of a word.
         * @param word The word
         * @return The singularized word
         */
        fun singularize(word: String): String {
            if (Inflection.isUncountable(word)) {
                return word
            }

            for (inflection in singular) {
                //System.out.println(word + " matches " + inflection.pattern + "? (ignore case: " + inflection.ignoreCase + ")");
                if (inflection.match(word)) {
                    //System.out.println("match!");
                    return inflection.replace(word)
                }
            }
            return word
        }

        /**
         * Return true if the word is uncountable.
         * @param word The word
         * @return True if it is uncountable
         */
        fun isUncountable(word: String): Boolean {
            for (w in uncountable) {
                if (w.equals(word, ignoreCase = true)) return true
            }
            return false
        }

        fun humanize(lowerCaseAndUnderscoredWords: String): String {
            var result = lowerCaseAndUnderscoredWords.trim { it <= ' ' }
            if (result.isEmpty()) return ""
            // Remove a trailing "_id" token
            result = result.replace("_id$".toRegex(), "")
            // Remove all of the tokens that should be removed
            result = result.replace("_+".toRegex(), " ")
            return result
        }
    }
}