/*
 *  Copyright 2018 Julian Exenberger
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

module io.nemawashi.util {
    exports io.nemawashi.util;
    exports io.nemawashi.util.io;
    exports io.nemawashi.util.log;
    exports io.nemawashi.util.csv;
    exports io.nemawashi.util.script;
    exports io.nemawashi.util.crypto;
    exports io.nemawashi.util.rest;
    exports io.nemawashi.util.string;
    exports io.nemawashi.util.transformation;
    exports io.nemawashi.util.io.console;
    exports io.nemawashi.util.config.properties;

    requires java.base;
    requires java.scripting;
    requires java.logging;

    requires org.bouncycastle.provider;
    requires org.bouncycastle.pkix;
    requires mvel2;

    requires kotlin.stdlib;
    requires kotlin.reflect;

    requires org.codehaus.groovy;
    requires org.codehaus.groovy.jsr223;
    requires org.codehaus.groovy.json;
    requires com.fasterxml.jackson.databind;
    requires com.fasterxml.jackson.module.kotlin;
    requires com.fasterxml.jackson.datatype.jsr310;
    requires java.activation;


}