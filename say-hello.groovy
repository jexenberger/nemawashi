import nemawashi.nw

nw.task {

  description "A hello world"

  parameters {
     name "The name you need to use"
  }

  script {
    println "hello $name"
  }

}
