var routes = [
    { path: '/', component: login},
    { path: '/login', component: login},
    { path: '/files', component: files},
    { path: '/task-search', component: taskSearch},
    { path: '/user-tasks', component: landingPage},
    { path: '/task-executor/:task', component: taskExecutor },
    { path: '/approvals/:id', component: approvals }
]

var router = new VueRouter({ routes })

var app = new Vue({
            router: router
     }).$mount('#app')