const toggleTemplate = `
    <div>
        &nbsp;&nbsp;&nbsp;
        <b-form-checkbox  v-bind:id="parameter.name" v-model="runParameters[parameter.name]">
        </b-form-checkbox>
    </div>
`

Vue.component('toggle', {
    props: {
        parameter: Object,
        runParameters: Object
    },
    created() {
        this.runParameters[this.parameter.name] = (this.parameter['default'] == 'true')
    },
    template: toggleTemplate
})