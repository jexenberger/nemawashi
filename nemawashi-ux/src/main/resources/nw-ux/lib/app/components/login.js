const loginTemplate = `
         <div align="center">
          <br/>
          <br/>
          <br/>
          <b-card
             header="Login"
             img-top
             header-bg-variant="dark"
             header-text-variant="white"
             style="max-width: 50rem;">
            <b-form>
              <b-form-group>
                        <b-form-input id="loginUserId" type="text" placeholder="User name" v-model="user"/>
              </b-form-group>
              <b-form-group>
                        <b-form-input id="loginPassword" type="password" placeholder="Password" v-model="password"/>
              </b-form-group>
              <div align="right">
                <b-button variant="outline-primary" v-on:click="login()">Login</b-button>
              </div>
            </b-form>
          </b-card>
         </div>
`

const login = Vue.extend({
    template: loginTemplate,
    data: function() {
        return {
            messages: [],
            user: null,
            password: null
        }
    },
    methods: {
        login: function() {
            return session.login(this.user, this.password, this.$root)
                .then(response => {
                    this.$router.push('/user-tasks')

                })
                .catch(error => {
                    console.log(error)
                    this.messages = ["Unable to log in"]
                })
        }
    },
    computed: {
         show: function() {
            return store.loggedIn()
         }
    },
    created: function() {
        session.logout()
    }
})