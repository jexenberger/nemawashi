const arrayTemplate = `
         <div>
            <b-table  v-bind:items="tableData" v-bind:fields="fields" borderless bordered hover>
              <template slot="value" slot-scope="row">{{row.item.value}}</template>
              <template slot="action" slot-scope="row">
                 <b-button size="small" variant="danger"  v-on:click="removeValue(row.item.id)">Remove</b-button>
              </template>
            </b-table>
            <div>
               <b-form inline>
                    <b-form-input v-bind:type="dataType" placeholder="Enter Value" v-model="value"/>
                    &nbsp;
                    <b-button type="primary" v-on:click="addValue()">Add</b-button>
               </b-form>
            </div>
         </div>
`

Vue.component('array', {
    props: {
        parameter: Object,
        runParameters: Object
    },
    data: function () {
      return {
        key: '',
        value: '',
        values: [],
        rowSelection: [],
        showAdd: false,
        fields: [
            { key: 'value', label: 'Value'},
            { key: 'action', label: '' }
        ]
      }
    },
    template: arrayTemplate,
    computed: {
        dataType: function() {
            var typeToUse = typeMap[this.parameter.type]
            if (typeToUse == null) {
                return "string"
            }
            return typeToUse.type
        },
        tableData: function() {
            var arr = []
            for (i in this.values) {
                var struct = {
                    id: i,
                    value: this.values[i]
                }
                arr.push(struct)
            }
            return arr
        }
    },
    methods: {
        addValue: function() {
            this.values.push(this.value)
            this.runParameters[this.parameter.name] = this.values
            this.key = ''
            this.value = ''
        },
        removeValue: function(idx) {
            this.values.splice(idx, 1)
            this.runParameters[this.parameter.name] = this.value

        }
    }
})