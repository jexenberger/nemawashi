Vue.component('secret', {
    props: {
        parameter: Object,
        runParameters: Object
    },
    template: `
           <b-container>
            <b-form inline>
             <b-form-input
                class="uk-input"
                type="password"
                v-on:keyup="setStyle()"
                v-bind:id="parameter.name"
                v-bind:required="parameter.required"
                v-bind:placeholder="parameter.description"
                v-model="runParameters[parameter.name]">
             </b-form-input>
             <b-form-input
                v-bind:class="matches"
                type="password"
                v-bind:required="parameter.required"
                v-on:keyup="setStyle()"
                v-bind:placeholder="'Confirm value for ' + parameter.name"
                v-model="checkPassword">
             </b-form-input>
           <b-form>
        </b-container>
    `,
    data: function() {
        return {
            checkPassword: '',
            matches: 'uk-input'
        }
    },
    methods: {
        setStyle: function() {
            var matchStyle = (this.runParameters[this.parameter.name] == this.checkPassword) ? 'uk-form-success' : 'uk-form-danger'
            var style = 'uk-input ' +  matchStyle
            this.matches = style
            return style
        }
    }
})