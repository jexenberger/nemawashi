const filesTemplate = `
       <b-card
            header="Files"
            img-top
            header-bg-variant="dark"
            header-text-variant="white"
            img-alt="Tasks"
            img-top
            tag="article"
            class="mb-2">
                   <b-form>
                          <b-input-group class="mt-3">
                            <b-form-input v-model="filter"/>
                            <b-input-group-append>
                              <b-button v-on:click="search()">Search</b-button>
                            </b-input-group-append>
                          </b-input-group>
                    </b-form>
                    <b-card title="Files">
                           <b-table v-bind:fields="fileFields" v-bind:items="files"  borderless bordered hover>
                             <template slot="href" slot-scope="row"><b-link v-on:click="display(row.item.href, row.item.title)">{{row.item.href}}</b-link></template>
                           </b-table>
                    </b-card>
                    <br/>
        </b-card>
`

const files = Vue.extend( {
    template: filesTemplate,
    data: function() {
        return {
            files: [],
            filter: null,
            fileFields: [
                { key: 'title', label: 'Name'},
                { key: 'href', label: 'Link'},
                { key: 'type', label: 'Type' }
            ],
        }
    },
    methods: {

        search: function() {
            api.getFiles(this.filter)
                .then(resp => {
                    this.files = resp.content
                })
        },

        display: function(href, id) {
            const parts = href.split('/')
            const type = parts[2]
            api.getFileContent(type, id, false)
                .then(resp => {
                    return resp.blob()
                })
                .then(data => {
                    downloadFile(data, id)
                })
        }

    }
})