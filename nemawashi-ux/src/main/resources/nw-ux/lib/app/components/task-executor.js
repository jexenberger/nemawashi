const taskExecutorTemplate = `
       <b-card
            v-bind:header="name"
            v-bind:sub-title="'Module: '+ module"
            header-bg-variant="dark"
            header-text-variant="white"
            tag="article">
            <b-card-text>
                  <b-container fluid>
                     <b-row>
                        <b-col>
                           {{ description}}
                        </b-col>
                        <b-col md="auto">
                           <div align="right"><b-badge pill variant="info">v{{version}}</b-badge><b-spinner v-if="spinner"></b-spinner></div>
                        </b-col>
                     </b-row>
                  </b-container>
            </b-card-text>

            <b-form>
                    <h5>Parameters</h5>
                    <b-form-checkbox
                        id="checkbox1"
                        v-model="showRequiredOnly">
                        Show only required fields
                    </b-form-checkbox>
                    <hr/>
                    <fieldset>
                      <div id="form-fields" v-for="(parameter, key) in parameters" v-bind:key="parameter.name">
                        <b-form-group
                            v-bind:id="parameter.name + '_label'"
                            v-bind:label="parameter.label"
                            v-bind:label-for="parameter.name"
                            v-bind:description="parameter.description"
                            v-if="displayFields(parameter.required)"
                            label-cols-sm="3"
                            label-cols-lg="3">
                                <dropdown v-if="parameter.type == 'enumeration'" v-bind:parameter="parameter" v-bind:runParameters="runParameters"></dropdown>
                                <keyvalue v-else-if="parameter.type == 'keyValue'" v-bind:parameter="parameter" v-bind:runParameters="runParameters"></keyvalue>
                                <toggle v-else-if="parameter.type == 'boolean'" v-bind:parameter="parameter" v-bind:runParameters="runParameters"></toggle>
                                <secret v-else-if="parameter.type == 'secret'" v-bind:parameter="parameter" v-bind:runParameters="runParameters"></secret>
                                <datetime v-else-if="parameter.type == 'datetime'" v-bind:parameter="parameter" v-bind:runParameters="runParameters"></datetime>
                                <array v-else-if="parameter.type.endsWith('Array') || parameter.type == 'iterator'" v-bind:parameter="parameter" v-bind:runParameters="runParameters" v-bind:type="parameter.type"></array>
                                <string v-else v-bind:parameter="parameter" v-bind:runParameters="runParameters"></string>
                        </b-form-group>
                        <br v-if="displayFields(parameter.required)"/>
                      </div>
                    </fieldset>
                </b-form>
                <div slot="footer">
                    <task-runner
                        v-bind:runParameters="runParameters"
                        v-bind:parameterTypes="parameterTypes"
                        v-bind:taskId="taskId"
                        v-bind:returnType="returnType">
                    </task-runner>
                </div>
      </b-card>
`

var taskExecutor = Vue.extend({
    props: ['targettask'],
    template: taskExecutorTemplate,
    data: function() {
            return  {
                showOutput: false,
                showRequiredOnly: false,
                name: '',
                version: '',
                module: '',
                description: '',
                output: '',
                parameters: [],
                runParameters: {},
                parameterTypes: {},
                returnType: null,
             }
    },
    computed: {
           taskId: function() {
                return this.$route.params.task
           }
       },
    methods: {
         loading: function() {
            this.spinner = true
         },
         stopLoading: function() {
            this.spinner = false
         },
         displayFields: function(required) {
            return !this.showRequiredOnly || (this.showRequiredOnly && required)
         }
    },
    created() {
        this.loading()
        api.getTaskMetaData(this.taskId)
            .then(response => {
                const data = response.content
                this.stopLoading()
                this.name = data.name;
                this.version = data.version;
                this.module = data.module;
                this.description = data.description;
                this.parameters = data.parameters;
                if (data.returnType != null) {
                    this.returnType = data.returnType.first
                }
                for (key in this.parameters) {
                    var parameter = this.parameters[key]
                    this.runParameters[parameter.name] = parameter.default
                    this.parameterTypes[parameter.name] = parameter
                }

            })
            .catch(error => {
                const message = (error.status == 404) ? "Unable to find "+this.taskId : error.message
                this.output = message
                this.stopLoading()
            })

    }

})