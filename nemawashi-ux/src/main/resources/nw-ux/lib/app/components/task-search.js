const taskSearchTemplate = `
      <vk-card>
      <vk-card-title>Find a task</vk-card-title>
        <input v-model="taskId" placeholder="Task ID"><br/>
        <button v-on:click="goto">Run task</button>
      </vk-card>

`

const taskSearch = Vue.extend({
    template: taskSearchTemplate,
    data: function() {
            return  {
                taskId: '',
            }
    },
       methods: {
            goto: function() {
                this.$router.push('/task-executor/' + this.taskId)
            }
       }

})