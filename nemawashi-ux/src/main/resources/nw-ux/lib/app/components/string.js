const stringTemplate = `
             <b-form-input
                v-bind:type="type"
                v-bind:id="parameter.name"
                v-model="runParameters[parameter.name]">
             </b-form-input>
`

Vue.component('string', {
    props: {
        parameter: Object,
        runParameters: Object
    },
    template: stringTemplate,
    computed: {
        type: function() {
            var typeToUse = typeMap[this.parameter.type]
            if (typeToUse == null) {
                return "string"
            }
            return typeToUse.type
        }
    }
})