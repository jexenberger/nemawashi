const approvalsTemplate = `
       <b-card
            v-bind:header="headline"
            header-bg-variant="dark"
            header-text-variant="white"
            tag="article">
            <b-card-text>
                  {{description}}
            </b-card-text>
      <b-container fluid>
        <b-row class="mb-1">
            <b-col cols="4">Requested by</b-col>
            <b-col><code>{{user}}</code></b-col>
        </b-row>
        <b-row class="mb-1">
            <b-col cols="4">Date requested:</b-col>
            <b-col><code>{{dateTimeCreated}}</code></b-col>
        </b-row>
      </b-container>
      <hr/>
      <b-form @submit.prevent>
          <fieldset>
              <div>
                  <label for="rejectionReasonText"></label>
                  <b-form-textarea id="rejectionReasonText" rows="5" v-bind:placeholder="rejectionReasonPlaceholder" v-model="rejectionReason">
                  </b-form-textarea>
              </div>
          </fieldset>
      </b-form>
      <div slot="footer">
        <b-button-group>
            <b-button variant="success" v-on:click="approve(approvalId)">Approve</b-button>
            <b-button variant="danger" v-on:click="reject(approvalId)">Reject</b-button>
        </b-button-group>
      </div>
    </b-card>
`

const approvals = Vue.extend( {
    template: approvalsTemplate,
    data: function() {
        return {
            approval: null,
            rejectionReason: null,
            rejectionReasonPlaceholder: 'rejection reason',
        }
    },
    computed: {
        validation: function() {
            this.rejectionReason != null || this.rej
        },
        approvalId: function() {
            return this.$route.params.id
        },
        headline: function() {
            return (this.approval != null) ? this.approval.headline : ''
        },
        description: function() {
            return (this.approval != null) ? this.approval.description : ''
        },
        user: function() {
            return (this.approval != null) ? this.approval.process.user : ''
        },
        dateTimeCreated: function() {
            return (this.approval != null) ? this.approval.process.dateTimeCreated : ''
        }
    },
    methods: {
        approve: function(id) {
            api.approveApproval(id)
            .then(response => {
                this.closeWindow()
            })
            .catch(error => {
                window.alert('there was a problem approving '+id)
            })
        },
        reject: function(id) {
            if (this.rejectionReason == null || this.rejectionReason.trim() == '') {
                this.rejectionReasonPlaceholder = 'Rejection reason is required if you reject the task'
                return
            }
            api.rejectApproval(id, this.rejectionReason)
            .then(response => {
                this.closeWindow()
            })
            .catch(error => {
                window.alert('there was a problem rejecting '+id)
            })
        },
        closeWindow: function() {
            this.approval = null
            this.$root.$emit('refresh-approvals')
            this.$router.push('/user-tasks')
        }
    },
    created: function() {
         api.getApproval(this.approvalId)
         .then(response => {
            this.approval = response.content
         })
    }
})