Vue.component('datetime', {
    props: {
        parameter: Object,
        runParameters: Object
    },
    template: `
             <b-form-input
                type="datetime-local"
                v-bind:id="parameter.name"
                v-bind:required="parameter.required"
                v-model="runParameters[parameter.name]"
                step=1>
             </b-form-input>
    `
})