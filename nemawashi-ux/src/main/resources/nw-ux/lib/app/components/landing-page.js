const landingPageTemplate = `
 <b-card
     header="Tasks"
     img-top
     header-bg-variant="dark"
     header-text-variant="white"
     img-alt="Tasks"
     img-top
     tag="article"
     class="mb-2">
            <b-form>
                 <b-form-input type="text" placeholder="Search" v-model="filter"/>
             </b-form>
            <div v-for="module in filteredModules">
                <b-card v-bind:title="module.module.name">
                    <b-card-text>{{module.description}}</b-card-text>
                    <div slot="header">{{module.module.name}}&nbsp;&nbsp;&nbsp;<b-badge pill variant="info">v{{ module.module.versionString  }}</b-badge></div>
                    <b-table v-bind:items="module.tasks" v-bind:fields="taskFields" borderless bordered hover>
                      <template slot="name" slot-scope="row"><b>{{row.item.name}}</b></template>
                      <template slot="description" slot-scope="row"> {{row.item.description}} </template>
                      <template slot="actions" slot-scope="row">
                        <b-button variant="outline-success" v-on:click="runTask(row.item.taskType.taskName)">Run</b-button>
                      </template>
                    </b-table>
                </b-card>
                <br/>
            </div>
            <div slot="footer">
                <b-spinner v-if="!loaded" label="Spinning" />
            </div>
 </b-card>
`


var landingPage = Vue.extend({
    template: landingPageTemplate,
    data: function() {
        return {
            loaded: false,
            taskFields: [
                { key: 'name', label: 'Task'},
                { key: 'description', label: 'Description'},
                { key: 'actions', label: '' }
            ],
            visibility: {},
            modules: [],
            filter: ''
        }
    },
    computed: {
        show: function() {
            return show.loggedIn()
        },
        filteredModules: function(){
            if (this.filter != '') {
                const theFilter = this.filter
                var selectedModules = []
                for (m in this.modules) {
                    const theModule = this.modules[m]
                    var tasks = theModule.tasks.filter(function(task) {
                        return task.name.startsWith(theFilter)
                    })
                    if (tasks.length > 0) {
                        selectedModules.push({
                            module: theModule.module,
                            description: theModule.description,
                            tasks: tasks
                        })
                    }
                }
                return selectedModules
            }
            return this.modules
        }
    },
    methods: {
        runTask: function(taskType) {
            this.$router.push('/task-executor/' + taskType)
        }
    },
    created() {
        session.loadUserTasks(this.$root)
            .then(response => {
                this.modules = session.allowedTasks

                for (m in this.modules) {
                    const theModule = this.modules[m]
                    this.visibility[theModule.name] = false
                }
                this.loaded = true
            })

    }
})