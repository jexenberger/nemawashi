Vue.component('dropdown', {
    props: {
        parameter: Object,
        runParameters: Object
    },
    computed: {
        dropDownList: function() {
            var list = this.parameter.pattern.split('|')
            return list.map(item => {
                return { value: item.trim(), text: item }
            })
        }
    },
    template: `
            <b-form-select class="uk-select"
                    v-bind:id="parameter.name"
                    v-model="runParameters[parameter.name]"
                    v-bind:required="parameter.required"
                    v-bind:options="dropDownList">
            </b-form-select>
    `
})