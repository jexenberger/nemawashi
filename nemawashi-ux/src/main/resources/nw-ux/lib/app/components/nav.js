const navTemplate = `
<div>
  <b-navbar toggleable="lg" variant="dark" type="dark">
    <b-navbar-brand href="#/user-tasks">
        <img src="tree.png" alt="Nemawashi" height="32" width="32"/>
        Nemawashi
    </b-navbar-brand>

    <b-navbar-toggle target="nav_collapse" />

    <b-collapse is-nav id="nav_collapse">
      <b-navbar-nav class="ml-auto">
        <b-nav-item href="#/files">Files</b-nav-item>
        <b-nav-item href="#/user-tasks">Tasks</b-nav-item>
        <b-nav-item-dropdown  v-if="pendingApprovals" right>
          <!-- Using button-content slot -->
          <template slot="button-content">Approvals&nbsp;<b-badge pill variant="light">{{noPendingApprovals}}</b-badge></template>
          <b-dropdown-item v-for="approval in approvals" v-on:click="selectApproval(approval.id)">{{approval.headline}}</b-dropdown-item>
        </b-nav-item-dropdown>
        <b-nav-item-dropdown right>
          <!-- Using button-content slot -->
          <template slot="button-content">{{ user }}</template>
          <b-dropdown-item v-on:click="logout()">Logout</b-dropdown-item>
        </b-nav-item-dropdown>
      </b-navbar-nav>
    </b-collapse>
  </b-navbar>
   <b-container fluid>
            <div>
               <br/>
               <br/>
               <br/>
               <router-view></router-view>
            </div>
   </b-container>
</div>`

Vue.component('nw-nav-bar', {
    template: navTemplate,
    data: function() {
        return {
            approvals : [],
            loggedIn : false,
            user: '',
            showNav: false,
            approvalsPoll: null,
            showApproval: false,
            selectedApproval: null,
        }
    },
    computed: {
        pendingApprovals: function() {
            return this.noPendingApprovals > 0
        },
        noPendingApprovals: function() {
            return this.approvals == null ? 0 : this.approvals.length
        }
    },
    mounted() {
        this.$root.$on('login', (user) => {
          this.loggedIn = true
          this.user = this.calcUser()
          this.approvalsPoll = window.setInterval(this.getUserApprovals, 60000)
        })
        this.$root.$on('logout', () => {
            this.cancelPoll()
        })
        this.$root.$on('refresh-approvals', () => {
            this.getUserApprovals()
        })
    },
    methods: {
        selectApproval: function(approval) {
            console.debug('received approval request')
            this.$router.push('/approvals/' + approval)
        },
        calcUser: function() {
             return (session.loggedIn()) ? session.user : ''
        },
        logout: function() {
            session.logout(this.$root)
            this.loggedIn = false
            this.$router.push('/')
            this.toggle()
            this.cancelPoll()
        },
        toggle: function() {
            this.showNav = !this.showNav
        },
        cancelPoll: function() {
            if (this.approvalsPoll != null) {
                window.clearInterval(this.approvalsPoll)
            }
        },
        getUserApprovals() {
            api.getUserApprovals()
                .then(data => {
                    this.approvals = data.content
                })
                .catch(error => {
                    if (error.status == "401") {
                        this.cancelPoll()
                    }
                })
        }

    },
    created() {
        if (session.loggedIn()) {
            this.$router.push('/user-tasks')
        } else {
            this.$router.push('/login')
        }
        this.getUserApprovals()
    }
})