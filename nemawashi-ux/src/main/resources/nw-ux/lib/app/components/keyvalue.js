const keyValueTemplate = `
        <div>
            <b-table  v-bind:items="tableData" v-bind:fields="fields" borderless bordered hover>
              <template slot="key" slot-scope="row">{{row.item.id}}</template>
              <template slot="value" slot-scope="row">{{row.item.value}}</template>
              <template slot="action" slot-scope="row">
                 <b-button size="small" variant="danger"  v-on:click="removeKeyValue(row.item.id)">Remove</b-button>
              </template>
            </b-table>
            <div>
               <b-form inline>
                    <label class="sr-only" for="enterKey">Key</label>
                    <b-form-input class="mb-2 mr-sm-2 mb-sm-0" id="enterKey"  placeholder="Enter Key" v-model="key"/>
                    <label class="sr-only" for="enterValue">Value</label>
                    <b-form-input class="mb-2 mr-sm-2 mb-sm-0" id="enterValue" placeholder="Enter Value" v-model="value"/>
                    <b-button type="primary" v-on:click="addKeyValue()">Add</b-button>
               </b-form>
            </div>
       </div>
`

Vue.component('keyvalue', {
    props: {
        parameter: Object,
        runParameters: Object
    },
    data: function () {
      return {
        key: '',
        value: '',
        values: {},
        showAdd: false,
        fields: [
            { key: 'key', label: 'Key'},
            { key: 'value', label: 'Value'},
            { key: 'action', label: '' }
        ]
      }
    },
    template: keyValueTemplate,
    computed: {

        tableData: function() {
            var arr = []
            for (i in this.values) {
                var struct = {
                    id: i,
                    value: this.values[i]
                }
                arr.push(struct)
            }
            return arr
        }
    },
    methods: {
        addKeyValue: function() {
            Vue.set(this.values, this.key, this.value)
            this.runParameters[this.parameter.name] = this.values
            this.key = ''
            this.value = ''
            console.log(this.runParameters)
        },
        removeKeyValue: function(key) {
            Vue.delete(this.values, key)
            this.runParameters[this.parameter.name] = this.values
        }
    }
})