const showResultsTemplate = `
            <div>

                    <b-container fluid>
                        <b-row>
                          <b-col>
                                <b-button v-on:click="runTask" variant="outline-primary">Run</b-button>

                          </b-col>
                          <b-col>
                            <div align="right">
                              <b-button v-if="taskStatus == 'success'" variant="success" v-b-modal="'resultsModal'">Completed</b-button>
                              <b-button variant="danger" v-if="taskStatus == 'error'" v-b-modal="'resultsModal'">Task failed</b-button>
                              <b-spinner v-if="spinner"></b-spinner>
                            </div>
                          </b-col>
                        </b-row>
                    </b-container>
                    <b-modal size="xl" id="resultsModal" title="Task output" scrollable ok-only>
                      <b-tabs>
                          <b-tab v-if="isValidationFailed" title="Output" active>
                                <h4>Missing or incorrect parameters passed</h4>
                                <b-table v-bind:items="output.validations" v-bind:fields="validationFields">
                                </b-table>
                          </b-tab>
                          <b-tab v-if="isFailed" title="Output" active>
                             <br/>
                             <br/>
                             <div>
                                  <json-tree v-bind:data="output" ></json-tree>
                             </div>
                          </b-tab>
                          <b-tab v-if="taskStatus == 'success'" title="Output" active>
                             <br/>
                             <br/>
                             <div>
                                <json-tree v-if="isOutputJson" v-bind:data="output.returnValue" ></json-tree>
                                <code v-if="!isOutputJson">{{ output.returnValue }}</code>
                             </div>
                          </b-tab>
                          <b-tab v-if="taskStatus == 'success'" title="Process Information">
                             <br/>
                             <br/>
                             <b-container fluid>
                                <b-row class="mb-1">
                                    <b-col cols="2">ID</b-col>
                                    <b-col><code>{{output.process.id}}</code></b-col>
                                </b-row>
                                <b-row class="mb-1">
                                    <b-col cols="2">User</b-col>
                                    <b-col><code>{{output.process.user}}</code></b-col>
                                </b-row>
                                <b-row class="mb-1">
                                    <b-col cols="2">Timestamp</b-col>
                                    <b-col><code>{{output.process.dateTimeCreated}}</code></b-col>
                                </b-row>
                             </b-container>
                          </b-tab>
                          <b-tab title="Task Parameters">
                               <br/>
                               <br/>
                               <div>
                                  <json-tree v-bind:data="displayParameters" ></json-tree>
                               </div>
                          </b-tab>
                      </b-tabs>
                      <div v-if="isFileOutput">
                        <br/>
                        <br/>
                        <b-button v-on:click="downloadFile(output.returnValue.type.qualifiedName,output.returnValue.id)">Download result file</b-button>
                      </div>
                    </b-modal>
            </div>
`

Vue.component('task-runner', {
    template: showResultsTemplate,
    props: {
        runParameters: Object,
        parameterTypes: Object,
        taskId: String,
        returnType: String
    },
    data: function() {
        return {
            taskStatus: 'undefined',
            output: '',
            spinner: false,
            validationFields: [
               { key: 'first', label: 'Parameter', sortable: true, sortDirection: 'desc' },
               { key: 'second', label: 'Message', sortable: true, class: 'text-center' }
            ],
            displayParameters: {}
        }
    },
    computed: {
           isFileOutput() {
                return this.taskStatus == 'success' && this.returnType == 'fileref'
           },
           isOutputJson() {
                if (this.output == '') {
                    return false
                }
                try {
                    JSON.stringify(this.output.returnValue)
                    return true
                } catch (err) {
                    return false
                }
           },
           isTaskFailed() {
                return this.isValidationFailed || this.isFailed
           },
           isValidationFailed() {
                return this.taskStatus == 'error' && this.output.code == 'VALIDATION'
           },
           isFailed() {
                return this.taskStatus == 'error' && this.output.code != 'VALIDATION'
           }
       },
    methods: {
         loading: function() {
            this.spinner = true
         },
         stopLoading: function() {
            this.spinner = false
         },
         processParameters: function() {
            var parms = {}
            for (key in this.runParameters) {
                const type = this.parameterTypes[key]
                currentValue = this.runParameters[key]
                if (currentValue != '' || (currentValue == '' && type.type == 'string' && type.required)) {
                    parms[key] = currentValue
                } else {
                    parms[key] = null
                }
            }
            return parms
         },
         runTask: function(event) {
             this.taskStatus = 'undefined'
             this.output = ''
             this.loading()
             const parameters = this.processParameters(this.runParameters)
             api.executeTask(this.taskId, parameters, session.userId)
                 .then(data => {
                     this.taskStatus = 'success'
                     this.stopLoading()
                     this.output = data.content
                     this.displayParameters = parameters
                  })
                 .catch(error => {
                     this.taskStatus = 'error'
                     this.output = error
                     this.stopLoading()
                     this.displayParameters = parameters
                  });
         },
         downloadFile: function(type, id) {
             api.getFileContent(type, id, false)
                 .then(resp => {
                     return resp.blob()
                 })
                 .then(data => {
                    downloadFile(data, id)
                 })
         }
    }
})