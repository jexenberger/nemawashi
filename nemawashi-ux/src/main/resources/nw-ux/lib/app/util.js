window.$cookies.config('2d')

const typeMap = {
    any: {type: "string"},
    int: {type: "number" },
    long: {type: "number" },
    float: {type: "number"},
    double: {type: "number"},
    path: {type: "string"},
    path: {type: "string"},
    intArray: {type: "number"},
    longArray: {type: "number"},
    floatArray: {type: "number"},
    doubleArray: {type: "number"},
    date: { type: "date"},
    datetime: { type: "datetime-local"},
    time: {type:"time"}
}


/*
    any("any", Any::class, "Any type", false, true),
    iterable("iterable", Iterable::class, "Any type which can be iterated  over", true, false),
    string("string", String::class, "Standard string", false, true),
    int("int", Int::class, "32 bit signed integer value"),
    long("long", Long::class, "64 bit signed integer value"),
    boolean("boolean", Boolean::class, "true,false, yes, no"),
    float("float", Float::class, "32 bit signed floating point value"),
    double("double", Double::class, "64 bit signed integer value"),
    path("path", File::class, "Reference to a file on the file system"),
    enumeration("enum", Enum::class, "Fixed enumerated value"),
    anyArray("any[]", Array<Any>::class, "Array of any types", true),
    fileArray("path[]", Array<File>::class, "Array of file types", true),
    stringArray("string[]", Array<String>::class, "Array of string types", true, true),
    intArray("int[]", Array<Int>::class, "Array of int types", true),
    longArray("long[]", Array<Long>::class, "Array of long types", true),
    floatArray("float[]", Array<Float>::class, "Array of float types", true),
    doubleArray("double[]", Array<Double>::class, "Array of double types", true),
    iterator("iterator", Iterator::class,"Enumeration type", true),
    keyValue("keyValue", Map::class, "Set of key=value mappings"),
    resource("resource", Resource::class, "Resource stored in the resource registry"),
    secret("secret", Secret::class, "A value which needs to be securely stored"),
    date("date", LocalDate::class, "A date value"),
    time("time", LocalTime::class, "A time value"),
    datetime("datetime", LocalDateTime::class, "A timestamp value"),
    regex("regex", Regex::class, "A regular expression value for matching"),
    unit("unit", Unit::class, "void");
const typeMap = {
    any: { handler:'string'},
    iterable: { handler:'array'},
    iterator: { handler:'array'},
    string: {handler:'string'},
    int: { numeric : true, integral: true, handler:'string',regex:/^-?\d+$/, message:' must be in integer value'},
    long: { integral: true, handler:'string', regex:/^-?\d+$/, message:' must be in integer value'},
    boolean: {  handler:'toggle'},
    float: {  integral: false, handler:'string', regex:/^-?\d+\.?\d*$/, message:' must be a floating point value'},
    double: {  integral: false, handler:'string', regex:/^-?\d+\.?\d*$/, message:' must be a floating point value'},
    path: { handler:'string'},
    enumeration: { handler:'array'},
    anyArray: { handler:'array'},
    fileArray: { handler:'array'},
    stringArray: { handler:'array'},
    intArray: { numeric : true, integral: true,handler:'array', regex:/^-?\d+$/, message:' must be in integer value'},
    longArray: { numeric : true, integral: true, handler:'array', regex:/^-?\d+$/, message:' must be an integer value'},
    floatArray: { numeric : true, integral: false, handler:'array', regex:/^-?\d+\.?\d*$/, message:' must be a floating point value'},
    doubleArray: { numeric : true, integral: false, handler:'array', regex:/^-?\d+\.?\d*$/, message:' must be a floating point value'},
    keyValue: { handler:'keyvalue'},
    resource: { handler:'string'},
    date: { handler:'string'}
}*/


function handleResponse(response) {
    return response.json()
        .then((json) => {
            if (!response.ok) {
                const error = Object.assign({}, json, {
                    status: response.status,
                    content: response.statusText
                })
                return Promise.reject(error)
            }
            return {
                status: response.status,
                content:    json
            }
        })
}


function postData(url = "", data = {}) {
  // Default options are marked with *
    return fetch(url, {
        method: "POST", // *GET, POST, PUT, DELETE, etc.
        cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
        headers: {
            "Authorization": session.authorization(),
            // "Content-Type": "application/x-www-form-urlencoded",
        },
        body: JSON.stringify(data), // body data type must match "Content-Type" header
    })
    .then(handleResponse)

}

function putData(url = "", data = {}) {
  // Default options are marked with *
    return fetch(url, {
        method: "PUT", // *GET, POST, PUT, DELETE, etc.
        cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
        headers: {
            "Authorization": session.authorization(),
            // "Content-Type": "application/x-www-form-urlencoded",
        },
        body: JSON.stringify(data), // body data type must match "Content-Type" header
    })
    .then(handleResponse)

}

function getRawData(url) {
     console.log("GET: "+url)
   // Default options are marked with *
     return fetch(url, {
         method: "GET", // *GET, POST, PUT, DELETE, etc.
         cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
         headers: {
             "Authorization": session.authorization(),
             // "Content-Type": "application/x-www-form-urlencoded",
         }// body data type must match "Content-Type" header
     })
}

function getData(url = "") {
     const promise = getRawData(url).then(handleResponse); // parses response to JSON
     return promise
}


function downloadFile(data, id) {
      const url = window.URL.createObjectURL(data);
      const link = document.createElement('a');
      link.href = url;
      link.setAttribute('download', id); //or any other extension
      document.body.appendChild(link);
      link.click();
}

const session = {
  debug: true,
  userTasks: [],
  refresher: null,
  state: {
    user: null,
    password: null,
    allowedTasks: null
  },
  login: function(user, password, events) {
    if (this.debug) console.log('login triggered with', user)
    this.user = user
    this.password = password
    return this.loadUserTasks(events)
  },
  logout: function(events) {
    window.$cookies.remove('__user__')
    if (this.debug) console.log('logout')
    this.user = null
    this.password = null
    this.allowedTasks = null
    if (events != null) {
        events.$emit('logout')
    }
  },
  loadUserTasks: function(events) {
    //yes this is a circular dependency...
    return api.getUserTasks()
          .then(response => {
              events.$emit('login', this.user)
              window.$cookies.set('__user__', this.token())
              this.allowedTasks = response.content
          })
  },
  userId: function() {
    return this.user
  },
  authorization: function() {
    return "Basic " + this.token()
  },
  token: function() {
    return (window.btoa(this.user + ":" + this.password))
  },
  loggedIn: function() {
    var token = window.$cookies.get('__user__', token)
    if (token != null) {
        var userPass = window.atob(token)
        var parts = userPass.split(':')
        this.user = parts[0]
        this.password = parts[1]
    }
    return this.user != null && !(typeof this.user === 'undefined')
  }
}