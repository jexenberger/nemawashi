const api = {

    executeTask : function(taskId, parameters, userId) {
        const url = '/tasks/' + taskId + '/executions';
        const execution = {
            process : {
                id : 1,
                user: userId,
                dateTimeCreated: new Date().toISOString()
            },
            parameters: parameters,
            returnValue: ''
        }
        return postData(url, execution)
    },

    getTaskMetaData: function(taskId) {
        const url = '/tasks/' + taskId + '/metadata'
        return getData(url)
    },

    getUserApprovals: function() {
        const url = '/approvals'
        return getData(url)
    },

    getApproval: function(id) {
        const url = '/approvals/' + id
        return getData(url)
    },

    getUserTasks: function() {
        const url = '/allowed-tasks'
        return getData(url)
    },

    rejectApproval: function(id, reason) {
        const url = '/approvals/rejections'
        var rejection = {
            id: id,
            reason: reason
        }
        return putData(url, rejection)
    },

    approveApproval: function(id, reason) {
        const url = '/approvals/approvals'
        var approval = {
            id: id
        }
        return putData(url, approval)
    },

    getFileContent: function(type, id, base64) {
        const url = '/files/' + type + '/' + id + '?base64=' + base64
        return getRawData(url)
    },

    getFiles : function(type) {
        const url = '/files/' + type
        return getData(url)
    }




}