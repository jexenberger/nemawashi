/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

import io.nemawashi.api.NemawashiModule;

open module io.nemawashi.server {
    uses NemawashiModule;

    requires jdk.unsupported;
    requires io.nemawashi.di;

    requires io.nemawashi.util;
    requires io.nemawashi.api;
    requires io.nemawashi.runtime;
    requires io.nemawashi.ux;

    requires kotlin.stdlib;
    requires kotlin.reflect;

    requires java.scripting;

    requires semantic.version;
    //Snakeyaml tries to taskDescriptor dates as java.sql.Date
    requires java.sql;

    requires io.javalin;
    requires slf4j.api;
    requires kotlin.argparser;
    requires com.fasterxml.jackson.databind;
    requires com.fasterxml.jackson.module.kotlin;
    requires com.fasterxml.jackson.datatype.jsr310;
    requires org.eclipse.jetty.server;
    requires org.eclipse.jetty.util;
}
