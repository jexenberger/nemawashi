package io.nemawashi.server

import io.javalin.Javalin
import io.javalin.staticfiles.Location
import io.nemawashi.api.NemawashiSettings
import io.nemawashi.api.TaskError
import io.nemawashi.api.ValidationErrors
import io.nemawashi.di.api.ApplicationContext
import io.nemawashi.server.api.Auth
import io.nemawashi.server.api.view.ApplicationError
import io.nemawashi.server.security.Roles
import io.nemawashi.server.security.User
import io.nemawashi.server.security.UserRepository
import io.nemawashi.util.system
import org.eclipse.jetty.server.Server
import org.eclipse.jetty.server.ServerConnector
import org.eclipse.jetty.util.ssl.SslContextFactory
import java.util.concurrent.ExecutionException
import javax.net.ssl.SSLContext

data class NemawashiServerSettings(val ctx: ApplicationContext, val nemawashiSettings: NemawashiSettings) {


    fun create(): Javalin {
        val auth = ctx.get<Auth>(Auth::class)!!
        val javalin = Javalin.create()
        if (nemawashiSettings.enableSecurity) {
            javalin.accessManager(auth::accessManager)
        } else {
            javalin.accessManager(auth::noAuth)
        }

        javalin.server {
            val server = Server()
            val connector = if (nemawashiSettings.keyStore != null) {
                val sslContextFactory = SslContextFactory()
                sslContextFactory.sslContext = SSLContext.getDefault()
                val sslConnector = ServerConnector(server, sslContextFactory)
                sslConnector
            } else {
                ServerConnector(server)
            }
            connector.port = nemawashiSettings.port
            server.connectors = arrayOf(connector)
            server
        }

        nemawashiSettings.uxPath?.let {
            javalin.enableStaticFiles(it, Location.EXTERNAL)
        } ?: javalin.enableStaticFiles("${system.nwHomeDir}/nw-ux", Location.EXTERNAL)


        javalin
                .disableStartupBanner()
                .enableCorsForOrigin("*")
                .exception(ExecutionException::class.java) { e, ctx ->
                    if (e.cause is TaskError) {
                        val taskError = e.cause as TaskError
                        if (taskError.exception is ValidationErrors) {
                            val validations = taskError.exception as ValidationErrors
                            val messages = validations.validationErrors.map { it.reference to it.description }
                            ctx.status(400).json(ApplicationError("VALIDATION", validations.message!!, messages.toTypedArray()))
                            return@exception
                        }
                    }
                    ctx.status(500).json(ApplicationError("TASK_FAILED", e.message!!))
                }
                .exception(Exception::class.java) { e, ctx ->
                    ctx.status(500).json(ApplicationError("SERVER_ERROR", e.message!!))
                }
                .start(nemawashiSettings.port)
        return javalin
    }


    fun initialiseUsersAndServers() {
        ctx.get<UserRepository>(UserRepository::class)?.let {
            if (it.get("admin") == null) {
                it.save(User("admin", "admin", "Admin user", listOf(Roles.admin), listOf("*")))
            }
            it.get("admin")!!
        } ?: throw IllegalStateException("No user repository!!")
    }


}