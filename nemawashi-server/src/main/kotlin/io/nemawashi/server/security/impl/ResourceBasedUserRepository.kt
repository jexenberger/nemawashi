/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.server.security.impl

import io.nemawashi.api.resources.Resource
import io.nemawashi.api.resources.CategorisedType
import io.nemawashi.api.resources.WritableResourceRepository
import io.nemawashi.di.annotation.Inject
import io.nemawashi.server.security.Roles
import io.nemawashi.server.security.User
import io.nemawashi.server.security.UserRepository
import io.nemawashi.util.crypto.Secret
import java.time.LocalDateTime
import java.time.LocalDateTime.now

class ResourceBasedUserRepository(@Inject val resourceRepository: WritableResourceRepository) : UserRepository {


    override fun authenticate(userId: String, password: String): User? {

        val pair = userCache[userId]
        return get(userId)?.let {
             if (it.checkPassword(password)) it else null
        }

    }

    override fun save(user: User) {
        val resource = mapToResource(user)
        resourceRepository.save(resource)
        removeUser(user.userId)

    }

    private fun mapToResource(user: User): Resource {
        val resource = Resource(CATEGORISED_TYPE, user.userId, mapOf(
                "userId" to user.userId,
                "name" to user.name,
                "password" to Secret(user.password),
                "roles" to user.roles,
                "allowedTasks" to user.allowedTasks
        ))
        return resource
    }

    private fun mapToUser(user: Resource): User {
        val password = Secret.fromBase64(user["password"]!!.toString())
        val mappedUser = User(
                userId = user["userId"]!!.toString(),
                password = password.value,
                name = user["name"]?.toString(),
                roles = (user["roles"]!! as List<Roles>),
                allowedTasks = (user["allowedTasks"]?.let { it as List<String> } ?: emptyList())
        )
        return mappedUser
    }

    override fun list(): Collection<User> = resourceRepository.find(CATEGORISED_TYPE).map(this::mapToUser)

    override fun remove(userId: String) {
        resourceRepository.delete(CATEGORISED_TYPE, userId)
    }

    override fun get(userId: String): User? {
        val existing = getUser(userId)
        if (existing != null) {
            return existing
        }

        val user = resourceRepository[CATEGORISED_TYPE, userId]?.let { mapToUser(it) }
        return user?.let { putUser(user) }
    }

    companion object {
        val CATEGORISED_TYPE: CategorisedType by lazy {
            CategorisedType("nwInternal", "user")
        }

        internal fun isUserCached(id: String) = userCache.containsKey(id)
        internal fun clearCache() {
            userCache.clear()
        }

        private fun getUser(id: String): User? {
            cleanCache()
            val user = userCache[id]
            return user?.let { putUser(it.first) }
        }

        private fun putUser(user: User): User {
            synchronized(userCache) {
                userCache[user.userId] = user to now()
            }
            return user
        }

        private fun removeUser(id: String) {
            synchronized(userCache) {
                userCache.remove(id)
            }
        }

        private fun cleanCache() {
            for (key in userCache.keys) {
                val (user, lastTimeAccessed) = userCache[key]!!
                if (lastTimeAccessed.isBefore(now().minusMinutes(15))) {
                    synchronized(userCache) {
                        userCache.remove(key)
                    }
                }
            }
        }



        private val userCache: MutableMap<String, Pair<User, LocalDateTime>> = mutableMapOf()
    }
}