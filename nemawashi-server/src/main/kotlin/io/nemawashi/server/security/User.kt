/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.server.security

import io.nemawashi.api.TaskType
import io.nemawashi.util.globMatches

data class User(val userId: String, val password: String, val name: String?, val roles: List<Roles>, val allowedTasks: List<String> = emptyList()) {

    constructor(userId: String, password: String, name: String?, vararg role: Roles) : this(userId, password, name, role.toList())

    fun toWireSafeUser(): User {
        return User(this.userId, "", name, this.roles)
    }

    fun checkPassword(password: String) : Boolean = this.password.equals(password)

    fun isAllowedToExecuteTask(taskType: TaskType): Boolean {
        if (this.allowedTasks.isEmpty()) {
            return false
        }
        return this.allowedTasks
                .map { taskType.taskName.globMatches(it) }
                .fold(false) { a, b -> a or b }
    }

}