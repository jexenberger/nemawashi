/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.server

import io.javalin.Javalin
import io.javalin.apibuilder.ApiBuilder.*
import io.javalin.security.SecurityUtil.roles
import io.nemawashi.di.annotation.Inject
import io.nemawashi.server.api.ApiHandler
import io.nemawashi.server.security.Roles

class Router(@Inject val api: ApiHandler, @Inject val app: Javalin) {


    fun configure() {
        app.routes {
            path("/allowed-tasks") {
                get(api::getUserTasks, roles(Roles.anyOne))
            }
            path("/tasks") {
                path(":tasktype") {
                    path("executions") {
                        post(api::runTask, roles(Roles.admin, Roles.taskExecutor))
                    }
                    path("metadata") {
                        get(api::getTaskMetadata, roles(Roles.admin, Roles.taskExecutor))
                    }

                }
            }
            path("/approvals") {
                get(api::getUserApprovals, roles(Roles.admin, Roles.approver))
                path(":id") {
                    get(api::getApproval, roles(Roles.admin, Roles.approver))
                }
                path("rejections") {
                    put(api::rejectApproval, roles(Roles.admin, Roles.approver))
                }
                path("approvals") {
                    put(api::approvalApproval, roles(Roles.admin, Roles.approver))
                }
            }
            path("/users") {
                get(api::getUsers, roles(Roles.admin))
                path(":userid") {
                    get(api::getUser, roles(Roles.admin))
                }
                post(api::addUser, roles(Roles.admin))
            }
            path("/resources") {
                path(":resourcetype") {
                    get(api::getResources, roles(Roles.admin, Roles.resourceViewer))
                    path(":id") {
                        get(api::getResource, roles(Roles.admin, Roles.resourceViewer))
                    }
                }
                post(api::saveResource, roles(Roles.admin, Roles.resourceWriter))
            }
            path("/files") {
                path(":resourcetype") {
                    get(api::getFiles, roles(Roles.admin, Roles.fileViewer))
                    path(":id") {
                        get(api::getFileContent, roles(Roles.admin, Roles.fileViewer))
                        put(api::saveFile, roles(Roles.admin, Roles.fileWriter))
                    }
                }
            }
        }

    }

}