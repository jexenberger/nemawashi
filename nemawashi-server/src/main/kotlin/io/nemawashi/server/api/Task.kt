package io.nemawashi.server.api

import io.nemawashi.api.defaultVersion

data class Task(val module: String, val version: String, val task: String) {

    constructor(module: String, task: String) : this(module = module, task = task, version = defaultVersion.toString())
}