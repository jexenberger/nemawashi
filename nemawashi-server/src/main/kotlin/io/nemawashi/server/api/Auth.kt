/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.server.api

import io.javalin.Context
import io.javalin.Handler
import io.javalin.security.Role
import io.nemawashi.di.annotation.Inject
import io.nemawashi.server.security.Roles
import io.nemawashi.server.security.User
import io.nemawashi.server.security.UserRepository



class Auth(@Inject val userRepository: UserRepository) {


    fun accessManager(handler: Handler, ctx: Context, permittedRoles: Set<Role>) {
        when {
            permittedRoles.contains(Roles.anyOne) -> handler.handle(ctx)
            ctx.userRoles.any { it in permittedRoles } -> handler.handle(ctx)
            else -> ctx.status(401).json("Unauthorized")
        }
    }

    fun noAuth(handler: Handler, ctx: Context, permittedRoles: Set<Role>) {
        handler.handle(ctx)
    }


    fun basicAuthCredentials(ctx: Context): User? {
        return ctx.basicAuthCredentials()?.let { (username, password) ->
            userRepository.authenticate(userId = username, password = password)
        }
    }


    // get roles from userRolesMap after extracting username/password from basic-auth header
    val Context.userRoles: Collection<Roles>
        get() = this.user?.roles ?: emptyList()

    // get roles from userRolesMap after extracting username/password from basic-auth header
    private val Context.user get() = basicAuthCredentials(this)


}