/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.server.api

import io.javalin.Context
import io.nemawashi.api.*
import io.nemawashi.api.descriptor.ParameterDescriptor
import io.nemawashi.api.resources.CategorisedType
import io.nemawashi.api.resources.WritableResourceRepository
import io.nemawashi.api.server.api.Approval
import io.nemawashi.api.server.api.Rejection
import io.nemawashi.api.server.api.RemoteExecution
import io.nemawashi.api.server.api.RemoteResource
import io.nemawashi.di.annotation.Inject
import io.nemawashi.di.annotation.Value
import io.nemawashi.doc.ModuleDoc
import io.nemawashi.runtime.LocalTarget
import io.nemawashi.runtime.ModuleRegistry
import io.nemawashi.server.api.view.ApplicationError
import io.nemawashi.server.api.view.AtomLink
import io.nemawashi.server.security.User
import io.nemawashi.server.security.UserRepository
import io.nemawashi.util.system
import io.nemawashi.util.transformation.TransformerService
import java.io.File
import java.util.*

class ApiHandler(
        @Inject
        val nemawashi: Nemawashi,
        @Inject
        val users: UserRepository,
        @Inject
        val resources: WritableResourceRepository,
        @Value("mime-types")
        val mimeTypes: Properties = Properties()
) : ParameterCallback {
    override fun capture(descriptor: ParameterDescriptor): Any? {
        //cant do anything, return null
        return null
    }


    fun runTask(ctx: Context) {
        val type = TaskType.fromString(ctx.pathParam("tasktype"))

        val user = ctx.user(this.users)
        if (user == null) {
            ctx.status(401).json(ApplicationError("NOT_AUTHENTICATED", "No user"))
            return
        }
        if (!user.isAllowedToExecuteTask(type)) {
            ctx.status(403).json(ApplicationError("NOT_AUTHORISED", "Not allowed"))
        }
        val execution = ctx.body<RemoteExecution>()
        val taskRequest = TaskRequest(
                module = type.module,
                task = type.name,
                id = system.nextId(),
                user = user.userId,
                process = execution.process,
                parameters = execution.parameters
        )
        val taskResult = nemawashi.runTask(taskRequest, this, runTarget = LocalTarget)
        execution.returnValue = taskResult.getResult()
        ctx.status(201).json(execution)
    }

    fun getTaskMetadata(ctx: Context) {
        val type = TaskType.fromString(ctx.pathParam("tasktype"))
        val taskDoc = nemawashi.taskDoc(type)
        taskDoc?.let { ctx.json(it) }
                ?: ctx.status(404).json(ApplicationError("NOT_FOUND", "Unable to find task : '${type.taskName}'"))
    }

    fun addUser(ctx: Context) {
        val user = ctx.body<User>()
        users.save(user)
        ctx.status(201)
                .json(user.toWireSafeUser())
                .header("Location", "/users/${user.userId}")
    }

    fun getUser(ctx: Context) {
        val id = ctx.pathParam("userid")
        users.get(id)?.let {
            ctx.json(it.toWireSafeUser())
        } ?: ctx.status(404)

    }


    fun getApproval(ctx: Context) {
        val user = ctx.user(this.users)!!
        val id = ctx.pathParam("id")
        nemawashi.approvalRequests.get(id)?.let {
            if (!it.assignedTo.contains(user.userId)) {
                ctx.status(403)
                return@let
            }
            ctx.status(202).json(it)
        }


    }

    fun getUserApprovals(ctx: Context) {
        val user = ctx.user(this.users)
        user?.let {
            val approvalList = nemawashi.approvalRequests.findPendingByUser(it.userId)
            ctx.status(200)
                    .json(approvalList)
        } ?: ctx.status(404)
    }

    fun rejectApproval(ctx: Context) {
        val user = ctx.user(this.users)!!
        val rejection = ctx.body<Rejection>()
        nemawashi.approvalRequests.get(rejection.id)?.let {
            if (!it.assignedTo.contains(user.userId)) {
                ctx.status(403)
                return@let
            }
            nemawashi.approvalRequests.reject(it.id, rejection.reason)?.let {
                ctx.status(200).json(rejection)
            } ?: ctx.status(404)
        } ?: ctx.status(404)
    }

    fun approvalApproval(ctx: Context) {
        val user = ctx.user(this.users)!!
        val approval = ctx.body<Approval>()
        nemawashi.approvalRequests.get(approval.id)?.let {
            if (!it.assignedTo.contains(user.userId)) {
                ctx.status(403)
                return@let
            }
            nemawashi.approvalRequests.approve(it.id)?.let {
                ctx.status(200).json(approval)
            } ?: ctx.status(404)
        } ?: ctx.status(404)
    }

    fun getUserTasks(ctx: Context) {
        val user = ctx.user(this.users)
        user?.let { theUser ->
            val allowedTasks = mutableListOf<ModuleDoc>()
            ModuleRegistry.modules.forEach { module ->
                val theModule = nemawashi.moduleDoc(module)!!
                val matchedTasks = theModule.tasks.filter { theUser.isAllowedToExecuteTask(it.taskType) }
                if (matchedTasks.isNotEmpty()) {
                    allowedTasks.add(theModule.copyWithNewTasks(matchedTasks))
                }
            }
            ctx.json(allowedTasks)
        } ?: ctx.status(404)


    }

    fun getUsers(ctx: Context) {
        ctx.json(users.list().map { it.toWireSafeUser() })
    }

    fun getResources(ctx: Context) {
        val type = CategorisedType.fromString(ctx.pathParam("resourcetype"))
        ctx.json(resources.find(type).map { RemoteResource.fromResource(it) })
    }

    fun getResource(ctx: Context) {
        val (type, id) = resolveTypeAndId(ctx)
        resources.get(type, id)?.let { ctx.json(RemoteResource.fromResource(it)) } ?: ctx.status(404)
    }

    fun saveResource(ctx: Context) {
        val resource = ctx.body<RemoteResource>()
        resources.save(resource.toResource())
        ctx.status(201).header("Location", "/resources/${resource.type}/${resource.id}").json(resource)
    }


    fun saveFile(ctx: Context) {
        val (type, id) = resolveTypeAndId(ctx)
        ctx.uploadedFiles("files").forEach {
            nemawashi.fileService.save(FileResource(type, id, it.content))
        }
    }

    private fun resolveTypeAndId(ctx: Context): Pair<CategorisedType, String> {
        val type = CategorisedType.fromString(ctx.pathParam("resourcetype"))
        val id = ctx.pathParam("id")
        return Pair(type, id)
    }

    fun getFileContent(ctx: Context) {
        val (type, id) = resolveTypeAndId(ctx)
        nemawashi.fileService.get(type, id)?.let {
            val extension = File(it.id).extension
            val type = this.mimeTypes[extension]?.toString() ?: "application/octet-stream"

            val encoding = ctx.queryParam("base64", "false") ?: "false"
            val useBase64 = TransformerService.convert<Boolean>(encoding)
            if (useBase64) {
                val bytes = it.inputStream.readAllBytes()
                val base64Data = Base64.getEncoder().encode(bytes)
                val output = "data:$type;base64;${String(base64Data)}"
                ctx.status(200)
                        .result(output)
            } else {
                ctx.status(200)
                        .contentType(type)
                        .result(it.inputStream)
            }
        }
    }

    fun getFiles(ctx: Context) {
        val type = CategorisedType.fromString(ctx.pathParam("resourcetype"))
        ctx.json(nemawashi.fileService.list(type).map {
            val extension = File(it.id).extension
            val mimeType = this.mimeTypes[extension]?.toString() ?: "application/octet-stream"
            AtomLink("entry", "/files/$type/${it.id}", mimeType, it.id)
        })
    }


}