/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.server

import com.xenomachina.argparser.ArgParser
import com.xenomachina.argparser.default
import io.javalin.Javalin
import io.javalin.json.JavalinJackson
import io.nemawashi.api.Nemawashi
import io.nemawashi.api.NemawashiSettings
import io.nemawashi.api.ParameterCallback
import io.nemawashi.api.descriptor.ParameterDescriptor
import io.nemawashi.api.resources.WritableResourceRepository
import io.nemawashi.di.api.addType
import io.nemawashi.di.api.bind
import io.nemawashi.di.api.load
import io.nemawashi.runtime.BaseApplication
import io.nemawashi.server.api.ApiHandler
import io.nemawashi.server.api.Auth
import io.nemawashi.server.security.UserRepository
import io.nemawashi.server.security.impl.ResourceBasedUserRepository
import io.nemawashi.util.Json
import io.nemawashi.util.log.LogProvider
import io.nemawashi.util.stripQuotes
import java.util.*


class ServerApp(args: ArgParser, override val log: LogProvider) : BaseApplication(log), ParameterCallback {


    val settingsFile by args.storing("-F", "--settings-file", help = "path to properties file to use for settings").default(NemawashiSettings.defaultSettingsFile)
    val settings by args.adding("-S", "--setting", help = "override values in settings file or default value (format: [setting key]=[value])") {
        val parts = this.split("=")
        if (parts.size > 1) {
            Pair(parts[0], parts[1].stripQuotes())
        } else {
            Pair(this, null)
        }
    }

    val modulePath by args.storing("-M", "--modules", help = "Path separated set of directories which contain YAML modules").default(NemawashiSettings.defaultModulePath)
    val trace by args.flagging("-X", "--trace", help = "Enable trace output").default(false)
    val debug by args.flagging("-D", "--debug", help = "Run with debug output").default(false)

    val nemawashiSettings: NemawashiSettings by lazy {

        val overrides = settings.toMap(Properties())
        overrides += "yamlModulePath" to modulePath
        overrides += "trace" to trace
        overrides += "debug" to debug
        val settings = NemawashiSettings.fromSettingsFile(
                file = settingsFile,
                overrides = overrides
        )
        settings ?: NemawashiSettings()
    }


    override fun capture(descriptor: ParameterDescriptor): Any? {
        return null
    }


    fun run() {

        val serverSettings = NemawashiServerSettings(this, nemawashiSettings)

        JavalinJackson.configure(Json.objectMapper)
        initRuntimeContainer(nemawashiSettings, emptySet())
        this.setValue("mime-types", load("mime-types.properties").properties)
        addType<ApiHandler>(ApiHandler::class) into this
        addType<Router>(Router::class) into this
        bind { id, theContext ->
            theContext.get<Nemawashi>(Nemawashi::class)!!.resources
        } withId WritableResourceRepository::class into this
        addType<UserRepository>(ResourceBasedUserRepository::class) withId UserRepository::class into this
        addType<Auth>(Auth::class) into this
        bind { id, theContext ->
            serverSettings.create()
        } withId Javalin::class into this

        serverSettings.initialiseUsersAndServers()

        val router = get<Router>(Router::class)!!
        router.configure()
    }

}