/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.server.api

import io.javalin.Context
import io.mockk.every
import io.mockk.mockk
import io.nemawashi.api.Nemawashi
import io.nemawashi.api.NemawashiProcess
import io.nemawashi.api.NemawashiSettings
import io.nemawashi.api.TaskType
import io.nemawashi.api.approvals.ApprovalRequest
import io.nemawashi.api.resources.WritableResourceRepository
import io.nemawashi.api.server.api.Approval
import io.nemawashi.api.server.api.Rejection
import io.nemawashi.api.server.api.RemoteExecution
import io.nemawashi.api.server.api.RemoteResource
import io.nemawashi.runtime.NemawashiRuntime
import io.nemawashi.server.security.User
import io.nemawashi.server.security.UserRepository
import io.nemawashi.util.system
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.io.File
import java.time.LocalDateTime

class ApiHandlerTest {

    val dbPath: File
    val settings: NemawashiSettings
    val nemawashi: Nemawashi

    private val approvalRequest: ApprovalRequest = ApprovalRequest(
            process = NemawashiProcess(),
            headline = "The headline",
            description = "The description of the headling",
            assignedTo = arrayOf("yuri", "jim"),
            approveTask = TaskType.fromString("sys::script"),
            approveTaskParameters = mapOf("script" to "1 + 1"),
            rejectTask = TaskType.fromString("sys::shell-script"),
            rejectTaskParameters = mapOf("script" to listOf("echo 'no so well'", "echo 'done'")),
            expiresOn = LocalDateTime.now().plusDays(24)
    )


    init {
        dbPath = File("${system.homeDir}/.serverPath")
        if (dbPath.exists()) {
            dbPath.deleteRecursively()
        }
        settings = NemawashiSettings(resourceRepositoryPath = dbPath)
        nemawashi = NemawashiRuntime(settings)
    }

    @BeforeEach
    internal fun setUp() {
        dbPath.mkdirs()
    }

    @AfterEach
    internal fun tearDown() {
        dbPath.deleteRecursively()
        dbPath.delete()
    }


    @Test
    internal fun testRunTask() {
        val ctx = mockk<Context>(relaxed = true)
        val users = mockk<UserRepository>(relaxed = true)
        val resource = mockk<WritableResourceRepository>(relaxed = true)
        val apiHandler = ApiHandler(nemawashi, users, resource)
        every { ctx.pathParam("tasktype") } returns "sys@LATEST::echo"
        every { ctx.body<RemoteExecution>() } returns RemoteExecution(
                process = NemawashiProcess(),
                parameters = mapOf("value" to "hello world")
        )
        apiHandler.runTask(ctx)
    }

    @Test
    internal fun testGetTaskMetadata() {
        val ctx = mockk<Context>(relaxed = true)
        val users = mockk<UserRepository>(relaxed = true)
        val resource = mockk<WritableResourceRepository>(relaxed = true)
        val apiHandler = ApiHandler(nemawashi, users, resource)
        every { ctx.pathParam("tasktype") } returns "sys@LATEST::echo"
        apiHandler.getTaskMetadata(ctx)
    }


    @Test
    internal fun testAddUser() {
        val ctx = mockk<Context>(relaxed = true)
        val users = mockk<UserRepository>(relaxed = true)
        val resource = mockk<WritableResourceRepository>(relaxed = true)
        val apiHandler = ApiHandler(nemawashi, users, resource)
        every { ctx.body<User>() } returns User("test", "test", "Test user", emptyList())
        apiHandler.addUser(ctx)
    }


    @Test
    internal fun testGetUser() {
        val ctx = mockk<Context>(relaxed = true)
        val users = mockk<UserRepository>(relaxed = true)
        val resource = mockk<WritableResourceRepository>(relaxed = true)
        val apiHandler = ApiHandler(nemawashi, users, resource)
        apiHandler.getUser(ctx)
    }


    @Test
    internal fun testGetUsers() {
        val ctx = mockk<Context>(relaxed = true)
        val users = mockk<UserRepository>(relaxed = true)
        val resource = mockk<WritableResourceRepository>(relaxed = true)
        val apiHandler = ApiHandler(nemawashi, users, resource)
        apiHandler.getUsers(ctx)
    }

    @Test
    internal fun testGetResources() {
        val ctx = mockk<Context>(relaxed = true)
        val users = mockk<UserRepository>(relaxed = true)
        val resource = mockk<WritableResourceRepository>(relaxed = true)
        val apiHandler = ApiHandler(nemawashi, users, resource)
        every { ctx.pathParam("resourcetype") } returns "test::test"
        apiHandler.getResources(ctx)
    }

    @Test
    internal fun testGetResource() {
        val ctx = mockk<Context>(relaxed = true)
        val users = mockk<UserRepository>(relaxed = true)
        val resource = mockk<WritableResourceRepository>(relaxed = true)
        val apiHandler = ApiHandler(nemawashi, users, resource)
        every { ctx.pathParam("resourcetype") } returns "test::test"
        every { ctx.pathParam("id") } returns "id123"
        apiHandler.getResources(ctx)
    }

    @Test
    internal fun testSaveResource() {
        val ctx = mockk<Context>(relaxed = true)
        val users = mockk<UserRepository>(relaxed = true)
        val resource = mockk<WritableResourceRepository>(relaxed = true)
        val apiHandler = ApiHandler(nemawashi, users, resource)
        every { ctx.body<RemoteResource>() } returns RemoteResource("test::test", "test", emptyList())
        apiHandler.saveResource(ctx)
    }

    @Test
    internal fun testGetUserTasks() {
        val ctx = mockk<Context>(relaxed = true)
        val users = mockk<UserRepository>(relaxed = true)
        val resource = mockk<WritableResourceRepository>(relaxed = true)
        val apiHandler = ApiHandler(nemawashi, users, resource)
        every { ctx.user(users) } returns User("test", "test", "test", emptyList(), listOf("sys*::*", "http*::*"))
        apiHandler.getUserTasks(ctx)
    }

    @Test
    internal fun testGetUserApprovals() {
        nemawashi.approvalRequests.request(approvalRequest)
        val ctx = mockk<Context>(relaxed = true)
        val users = mockk<UserRepository>(relaxed = true)
        val resource = mockk<WritableResourceRepository>(relaxed = true)
        val apiHandler = ApiHandler(nemawashi, users, resource)
        every { ctx.user(users) } returns User("admin", "test", "test", emptyList(), listOf("sys*::*", "http*::*"))
        apiHandler.getUserApprovals(ctx)
    }

    @Test
    internal fun testGetUserApproval() {
        nemawashi.approvalRequests.request(approvalRequest)
        val ctx = mockk<Context>(relaxed = true)
        val users = mockk<UserRepository>(relaxed = true)
        val resource = mockk<WritableResourceRepository>(relaxed = true)
        val apiHandler = ApiHandler(nemawashi, users, resource)
        every { ctx.user(users) } returns User("admin", "test", "test", emptyList(), listOf("sys*::*", "http*::*"))
        every { ctx.pathParam("id") } returns approvalRequest.id
        apiHandler.getApproval(ctx)
    }


    @Test
    internal fun testRejectApproval() {
        nemawashi.approvalRequests.request(approvalRequest)
        val ctx = mockk<Context>(relaxed = true)
        val users = mockk<UserRepository>(relaxed = true)
        val resource = mockk<WritableResourceRepository>(relaxed = true)
        val apiHandler = ApiHandler(nemawashi, users, resource)
        every { ctx.user(users) } returns User("admin", "test", "test", emptyList(), listOf("sys*::*", "http*::*"))
        every { ctx.body<Rejection>() } returns Rejection(approvalRequest.id, "Rejected")
        apiHandler.rejectApproval(ctx)
    }


    @Test
    internal fun testApproveApproval() {
        nemawashi.approvalRequests.request(approvalRequest)
        val ctx = mockk<Context>(relaxed = true)
        val users = mockk<UserRepository>(relaxed = true)
        val resource = mockk<WritableResourceRepository>(relaxed = true)
        val apiHandler = ApiHandler(nemawashi, users, resource)
        every { ctx.user(users) } returns User("admin", "test", "test", emptyList(), listOf("sys*::*", "http*::*"))
        every { ctx.body<Approval>() } returns Approval(approvalRequest.id)
        apiHandler.approvalApproval(ctx)
    }

}