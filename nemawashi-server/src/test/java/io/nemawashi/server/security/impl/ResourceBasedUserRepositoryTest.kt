/*
 *
 *  *  Copyright 2018 Julian Exenberger
 *  *
 *  *  Licensed under the Apache License, Version 2.0 (the "License");
 *  *  you may not use this file except in compliance with the License.
 *  *  You may obtain a copy of the License at
 *  *    `*   `[`http://www.apache.org/licenses/LICENSE-2.0`](http://www.apache.org/licenses/LICENSE-2.0)
 *  *
 *  *  Unless required by applicable law or agreed to in writing, software
 *  *  distributed under the License is distributed on an "AS IS" BASIS,
 *  *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *  See the License for the specific language governing permissions and
 *  *  limitations under the License.
 *
 *
 */

package io.nemawashi.server.security.impl

import io.mockk.every
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.junit5.MockKExtension
import io.mockk.mockkClass
import io.mockk.verify
import io.nemawashi.api.resources.Resource
import io.nemawashi.api.resources.WritableResourceRepository
import io.nemawashi.server.security.Roles
import io.nemawashi.server.security.User
import io.nemawashi.util.crypto.Secret
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertNotNull
import kotlin.test.assertTrue

@ExtendWith(MockKExtension::class)
class ResourceBasedUserRepositoryTest {


    @RelaxedMockK
    lateinit var repo: WritableResourceRepository


    private val type = ResourceBasedUserRepository.CATEGORISED_TYPE

    private val roles = listOf(Roles.admin, Roles.taskExecutor)

    @BeforeEach
    internal fun setUp() {
        ResourceBasedUserRepository.clearCache()
    }

    @Test
    internal fun testAuthenticate() {
        val mockkClass = mockkClass(WritableResourceRepository::class)
        every { mockkClass.get(type, "test") } returns createUser()
        val resourceBasedUserRepository = ResourceBasedUserRepository(mockkClass)
        assertFalse { ResourceBasedUserRepository.isUserCached("test") }
        val roles = resourceBasedUserRepository.authenticate("test", "test")
        assertNotNull(roles)

        assertEquals(this.roles, roles.roles)
        assertTrue { ResourceBasedUserRepository.isUserCached("test") }
        ResourceBasedUserRepository.clearCache()
        assertFalse { ResourceBasedUserRepository.isUserCached("test") }
        assertNotNull(resourceBasedUserRepository.authenticate("test", "test"))
        assertTrue { ResourceBasedUserRepository.isUserCached("test") }
    }


    @Test
    internal fun testSave() {
        val mockkClass = mockkClass(WritableResourceRepository::class)
        every { mockkClass.save(eq(createUser())) } returns Unit
        val resourceBasedUserRepository = ResourceBasedUserRepository(mockkClass)
        resourceBasedUserRepository.save(User("test", "test", "Test user", roles))

        verify(atLeast = 1) { mockkClass.save(eq(createUser())) }
        assertFalse { ResourceBasedUserRepository.isUserCached("test") }
    }

    @Test
    internal fun testRemove() {
        val mockkClass = mockkClass(WritableResourceRepository::class)
        every { mockkClass.delete(type, "test") } returns true
        val resourceBasedUserRepository = ResourceBasedUserRepository(mockkClass)
        resourceBasedUserRepository.remove("test")

        verify(atLeast = 1) { mockkClass.delete(type, "test") }
        assertFalse { ResourceBasedUserRepository.isUserCached("test") }
    }


    @Test
    internal fun testList() {
        val mockkClass = mockkClass(WritableResourceRepository::class)
        every { mockkClass.find(type) } returns listOf(createUser(), createUser(), createUser())
        val resourceBasedUserRepository = ResourceBasedUserRepository(mockkClass)
        val result = resourceBasedUserRepository.list()
        assertEquals(3, result.size)

    }

    private fun createUser(): Resource {
        return Resource(
                type,
                "test",
                mapOf("userId" to "test", "password" to Secret("test"), "roles" to roles)
        )
    }
}